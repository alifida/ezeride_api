// ./express-server/controllers/todo.server.controller.js
import mongoose from 'mongoose';
//import models
import PayPackage from '../models/payPackage.model';
import DriverPackage from '../models/driverPackage.model';
import Driver from '../models/driver.model';
import DriverBank from '../models/driverBank.model';
import * as GFunctions from './functions';
import * as HelperFunc from './adminfunctions';

import { updateDriverCreditsInFB, debitDriverBankTransactions, updateSubcriptionEndDate } from './driver';
import * as driverBank from './driverBank';
import * as paymentCtrl from './paymentGateway/index';

var moment = require('moment');
const config = require('../config');
const _ = require('lodash');
const ObjectId = mongoose.Types.ObjectId;

export const addData = (req, res) => {
  // var newDoc = new PayPackage(req.body);
  if (req.body.type == 'subscription') {
    var newDoc = new PayPackage({
      name: req.body.name,
      type: req.body.type,
      amount: req.body.amount,
      PackageValidity: req.body.PackageValidity,
      scIds: req.body.scIds,
      vehicleType: [req.body.vehicleType],
      firstPurchaseOnly: req.body.firstPurchaseOnly
    })
  }
  else {
    var newDoc = new PayPackage({
      name: req.body.name,
      type: req.body.type,
      amount: req.body.amount,
      credit: req.body.credit,
      scIds: req.body.scIds,
      vehicleType: [req.body.vehicleType],
      firstPurchaseOnly: req.body.firstPurchaseOnly
    })
  }
  newDoc.save((err, datas) => {
    if (err) {
      return res.json({ 'success': false, 'message': err.message, 'err': err });
    }
    return res.status(200).json({ 'success': true, 'message': req.i18n.__("PACKAGE_CREATED_SUCCESSFULLY"), "datas": datas });
  })
};

export const SubscriptionValidity = async (req, res) => {
  DriverPackage.findById(req.body.id, function (err, doc) {
    if (err) {
      return res.json({ 'success': false, 'message': err.message, 'err': err });
    }
    // console.log(doc)
    if (doc.type === "subscription") {
      PayPackage.findOne({ name: doc.packageName }, { _id: 0, name: 1, PackageValidity: 1 }, function (err, docs) {
        if (err) {
          return res.json({ 'success': false, 'message': err.message, 'err': err });
        }
        var fromDate = moment(doc.startDate).format("YYYY-MM-DD");
        console.log(fromDate)

        var toDate = moment(fromDate).add(docs.PackageValidity, 'M').format('YYYY-MM-DD')
        console.log(toDate)

        var curDate = moment().format('YYYY-MM-DD');
        console.log(curDate)

        if (toDate == curDate) {
          return res.send("true");
        }
        else {
          return res.send("false");
        }
        //return res.status(200).json({'success':true, 'message':'Package created successfully', "datas" : docs});
      });
    }
  });
}

/**
 * Update PayPackage Details 
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const updateData = (req, res) => {
  /*   console.log(req.body)
      var updateDoc = 
      {
          name: req.body.name,
          amount: req.body.amount,
          credit: req.body.credit,
      } */

  PayPackage.findOneAndUpdate({ _id: req.body._id }, req.body/* updateDoc */, { new: true }, (err, todo) => {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
    return res.json({ 'success': true, 'message': req.i18n.__("DETAILS_UPDATED"), todo });
  });
};

/**
 * Delete PayPackage Details 
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const deleteData = (req, res) => {
  PayPackage.findByIdAndRemove(req.params.id, (err, docs) => {
    if (err) {
      return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") });
    }
    return res.json({ 'success': true, 'message': req.i18n.__("DETAILS_DELETED_SUCCESSFULLY"), docs });
  })
}

export const getData = (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var totalCount = 10;
  if (req.cityWise == 'exists') likeQuery['scIds.scId'] = { "$in": req.scId };
  PayPackage.find(likeQuery).count().exec((err, cnt) => {
    if (err) { }
    totalCount = cnt;
  });

  PayPackage.find(likeQuery, {}).skip(pageQuery.skip).limit(pageQuery.take).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.header('x-total-count', totalCount);
    res.send(docs);
  });

}

export const getinitalPackageDetails = (req, res) => {
  PayPackage.find({ name: 'Initial Credits' }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const addDriverPackFromApp = async (req, res) => {

  req.body.driverId = req.userId;
  req.body.startDate = GFunctions.getISOTodayDate();
  req.body.type = req.body.type ? req.body.type : "subscription";
  req.body.packageId = req.body.packageId;
  req.body.tranxId = req.body.tranxId;

  if (config.paymentGateway.paymentGatewayName == "paystack") {
    var PaymentRes = await paymentCtrl.checkPaymentTransaction(req.body.tranxId);
    // console.log(PaymentRes)
    if (PaymentRes) { // true if the payment completed with success
      addDriverPack(req, res);
    } else {
      return res.status(500).json({ 'success': false, 'message': req.i18n.__("Payment Error") });;
    }
    // addDriverPack(req, res);
  } else {
    addDriverPack(req, res);
  }
}

export const addDriverPack = (req, res) => {
  PayPackage.findOne({ _id: req.body.packageId }, {}, function (err, packageData) {
    if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
    if (!packageData) {
      return res.status(404).json({ 'success': false, 'message': req.i18n.__("NO_SUCH_PACKAGE_FOUND") });;
    } else {
      var packCredit = packageData.credit;
      Driver.findOne({ _id: req.body.driverId }, { wallet: 1, fname: 1, email: 1, actHolder: 1, actNo: 1, actBank: 1, actLoc: 1, actCode: 1, isSubcriptionActive: 1, subcriptionEndDate: 1 }, function (err, driverData) {
        if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
        if (!driverData) {
          return res.status(404).json({ 'success': false, 'message': req.i18n.__("NO_SUCH_DRIVER_FOUND") });;
        } else {

          var driverWallet = driverData.wallet;
          if ((driverData.isSubcriptionActive == false && driverData.subcriptionEndDate == null) || req.body.type != "subscription") {
            if (driverWallet != null && driverWallet != '') {
              var creditValue = Number(driverWallet) + Number(packCredit);
              driverData.wallet = creditValue;
            } else {
              var creditValue = packCredit;
              driverData.wallet = packCredit;
            }
            driverData.save(function (err, op) {
              if (err) { return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }); }

              // var driverPackageDetails = {
              //   driverId: req.body.driverId, 
              //   driverName: driverData.fname , 
              //   packageId: req.body.packageId, 
              //   packageName: packageData.name, 
              //   amount: packageData.amount, 
              //   credit: packageData.credit,
              // }

              // var newDoc = new DriverPackage(driverPackageDetails);

              req.body.type = req.body.type ? req.body.type : packageData.type;
              var paymentDateSort = GFunctions.getISODate();
              var paymentDate = GFunctions.getISODate("D-M-YYYY h:mm a");

              if (req.body.type == "subscription") {
                var uptoEndDate = GFunctions.getSubcriptionValidityDate(req.body.startDate, packageData.PackageValidity);
                var newDoc = new DriverPackage({
                  driverId: req.body.driverId,
                  driverName: driverData.fname,
                  // packageId: req.body.tranxId ? req.body.tranxId : req.body.packageId,
                  packageId: req.body.packageId,
                  packageName: packageData.name,
                  amount: packageData.amount,
                  type: req.body.type,
                  startDate: req.body.startDate,
                  endDate: uptoEndDate,
                  credit: packageData.credit ? packageData.credit : 0,
                })
              }
              else {
                var newDoc = new DriverPackage({
                  driverId: req.body.driverId,
                  driverName: driverData.fname,
                  packageId: req.body.packageId,
                  packageName: packageData.name,
                  amount: packageData.amount,
                  type: req.body.type,
                  purchaseDate: req.body.startDate,
                  credit: packageData.credit,
                })

                paymentDate = GFunctions.getDateTimeinThisFormat(req.body.startDate, 'YYYY-MM-DD', "D-M-YYYY h:mm a");
                paymentDateSort = GFunctions.getDateTimeinThisFormat(req.body.startDate, 'YYYY-MM-DD');
              }
              newDoc.save((err, datas) => {
                if (err) {
                  return res.json({ 'success': false, 'message': err.message, 'err': err });
                }

                var params = {
                  driverId: req.body.driverId,
                  driverName: driverData.fname,
                  trxId: req.body.packageId,
                  description: packageData.name,
                  amt: packageData.amount ? packageData.amount : 0,
                  credit: packageData.credit ? packageData.credit : 0,
                  type: "credit",
                  paymentDate: paymentDate,
                  paymentDateSort: paymentDateSort,
                }
                driverBank.driverBankTransactions(params, false)

                if (req.body.type == "subscription") {
                  // console.log(uptoEndDate);
                  updateDriverCreditsInFB(req.body.driverId, creditValue, moment(uptoEndDate).format('DD/MM/YYYY'));
                  updateSubcriptionEndDate(req.body.driverId, req.body.packageId, packageData.name, moment(uptoEndDate).format('YYYY-MM-DD'), datas._id);
                } else {
                  updateDriverCreditsInFB(req.body.driverId, creditValue);
                }
                // debitDriverBankTransactions(req.body.driverId, creditValue, req.body.packageId, creditValue, false, 'Credit');

                return res.status(200).json({ 'success': true, 'message': req.i18n.__("PACKAGE_ADDED_TO_DRIVER_SUCCESSFULLY"), "datas": datas });
              })
              //return res.json({'success':true,'message':'Details Updated successfully', 'drivertaxis' : docs.taxis }); 
            });
          } else {
            return res.status(409).json({ 'success': false, 'message': req.i18n.__('"' + driverData.fname + '" "DRIVER_ALREADY_SUBSCRIPTION_END_DATE" - ') + moment(driverData.subcriptionEndDate).format('YYYY-MM-DD') });
          }

        }

      });
    }

  });

}

//@TODO v2
export const calculateAndUpdateSubcriptionValidity = (driverId, packageId, startDate, PackageValidity) => {
  var uptoEndDate = GFunctions.getSubcriptionValidityDate(startDate, PackageValidity);
  console.log(uptoEndDate);
}

export const getDriverPack = (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var totalCount = 10;

  DriverPackage.find(likeQuery).count().exec((err, cnt) => {
    if (err) { }
    totalCount = cnt;
  });

  DriverPackage.find(likeQuery, {}).skip(pageQuery.skip).limit(pageQuery.take).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.header('x-total-count', totalCount);
    res.send(docs);
  });

}


export const driverPackageHistory = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var totalCount = 10;
  likeQuery['driverId'] = req.params.id;

  let TotCnt = DriverPackage.find(likeQuery).count();
  let Datas = DriverPackage.find(likeQuery, {}).skip(pageQuery.skip).limit(pageQuery.take); //have to catch Err
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }

}

export const getAllDrivers = (req, res) => {
  Driver.find({}, { fname: 1, lname: 1, code: 1 }).exec((err, docs) => {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    if (docs.length) {
      var resultarr = [];
      var result = docs.map(function (items) {
        var arrData = {
          id: items._id,
          name: items.fname + ' ' + items.lname,
          code: items.code
        };
        resultarr.push(arrData);
      });
      return res.send(resultarr);
      //return res.send(docs);
    } else {
      return res.send([]);
    }
  });
}

export const getCompyDrivers = (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  if (req.type == 'company') likeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  Driver.find(likeQuery, { fname: 1, lname: 1 }).exec((err, docs) => {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    if (docs.length) {
      var resultarr = [];
      var result = docs.map(function (items) {
        var arrData = {
          id: items._id,
          name: items.fname + ' ' + items.lname,
        };

        resultarr.push(arrData);
      });
      console.log(resultarr)
      return res.send(resultarr);
      //return res.send(docs);
    } else {
      return res.send([]);
    }
  });

}


export const getAllPackage = async (req, res) => {
  /*  PayPackage.aggregate([
     {
       $facet:
       {
         "subscriptionPackage": [
           { $match: { type: "subscription" } },
         ],
         "commissionPackage": [
           { $match: { type: "commission" } },
         ],
       }
     }
   ], function (err, result) {
     if (err) {
       return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
     }
     return res.send(result[0]);
   }); */

  PayPackage.find({}, { scIds: 0 }).exec((err, docs) => {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    if (docs.length) {
      return res.send(docs);
    } else {
      return res.send([]);
    }
  });

}

export const getSelectedPackage = async (req, res) => {
  PayPackage.findById({ _id: req.params.id }, { name: 1, type: 1 }).exec((err, docs) => {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    return res.send(docs);
  });
}

export const getSubReqPackage = async (req, res) => {
  PayPackage.find({ type: "subscription" }, { name: 1 }, function (err, docs) {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    if (docs.length) {
      return res.send(docs);
    } else {
      return res.send([]);
    }
  });
}

export const getComReqPackage = async (req, res) => {
  PayPackage.find({ $or: [{ type: "topup" }, { type: "commission" }] }, { name: 1 }, function (err, docs) {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
    }
    if (docs.length) {
      return res.send(docs);
    } else {
      return res.send([]);
    }
  });
}

export const deleteDriverPack = async (req, res) => {
  DriverPackage.findById(req.params.id, function (err, docs) {
    if (err) {
      return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'err': err });
    }
    // console.log(docs);
    if (docs) {
      var packCredit = docs.credit;


      Driver.findOne({ _id: docs.driverId }, { wallet: 1, fname: 1 }, function (err, driverData) {
        if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
        if (!driverData) {
          return res.status(404).json({ 'success': false, 'message': req.i18n.__("DRIVER_NOT_FOUND") });;
        } else {
          var driverWallet = driverData.wallet;

          if (driverWallet != null && driverWallet != '') {
            var creditValue = Number(driverWallet) - Number(packCredit);
            driverData.wallet = creditValue;
          } else {
            var creditValue = 0;
            driverData.wallet = 0;
          }
          driverData.save(function (err, op) {
            if (err) { return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }); }


            DriverPackage.findByIdAndRemove({ _id: req.params.id },
              function (err, datas) {
                if (err) {
                  return res.json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'err': err });
                }
                updateDriverCreditsInFB(docs.driverId, creditValue);
                return res.status(200).json({ 'success': true, 'message': "PACKAGE_DELETED_FROM_DRIVER_CREDITS_SUCCESSFULLY", "datas": datas });
              });


            //return res.json({'success':true,'message':'Details Updated successfully', 'drivertaxis' : docs.taxis }); 
          });

        }

      });


    } else {
      return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });
    }
  });
}


/**
 * Helper for maual
 * @param {*} req 
 * @param {*} res 
 */
export const modifyAllDriverPackage = (req, res) => {
  Driver.find({}).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    for (var i = 0; i < docs.length; i++) {
      var driverId = docs[i]._id;
      var fname = docs[i].fname;
      modifyAllDriverPackageHelp(driverId, fname);
    }
  });
}

export const modifyAllDriverPackageHelp = (driverId, fname, credit = '0', packageId = '', amount = 0) => {
  Driver.findOne({ _id: driverId }, { wallet: 1, fname: 1 }, function (err, driverData) {
    if (err) { console.log(err); }
    if (!driverData) {
      console.log('No Such Driver found.');
    } else {
      var driverWallet = driverData.wallet;
      if (driverWallet != null && driverWallet != '') {
        var creditValue = Number(credit);
        driverData.wallet = creditValue;
      } else {
        var creditValue = Number(credit);
        driverData.wallet = Number(credit);
      }
      driverData.save(function (err, op) {
        if (err) { return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }); }
        updateDriverCreditsInFB(driverId, creditValue);

        // var driverPackageDetails = {
        //   driverId: driverId,
        //   driverName: fname,
        //   packageId: packageId,
        //   packageName: packageData.name,
        //   amount: amount,
        //   credit: credit,
        // }
        // var newDoc = new DriverPackage(driverPackageDetails);
        // newDoc.save((err, datas) => {
        //   if (err) {
        //     console.log(err);
        //   }
        //   updateDriverCreditsInFB(driverId, creditValue);
        // })
      });
    }
  });
}


/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
export const getExpiryPackOfSubscription = async (req, res) => {
  //type="subcription",packageId
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  dateLikeQuery.type = "subscription";
  if (req.providerId) likeQuery['providerId'] = new mongoose.Types.ObjectId(req.providerId);

  if (_.isEmpty(dateLikeQuery)) {
    // var fromDate = new Date(GFunctions.getCommonMonthStartDate());
    var toDate = new Date(GFunctions.getISODate());
    dateLikeQuery =
      {
        endDate:
        {
          '$gte': toDate,
          // '$lte': toDate
        },
        type: "subscription",
      }
  };
  DriverPackage.aggregate([
    {
      "$match": dateLikeQuery
    },
    // {
    //   "$lookup": {
    //     "localField": "packageId",
    //     "from": "payPackages",
    //     "foreignField": "_id",
    //     "as": "packageinfo"
    //   }
    // },
    // { "$unwind": "$packageinfo" },
    // {
    //   "$match": {
    //     "packageinfo.type": "subscription"
    //   }
    // },
    {
      "$lookup": {
        "localField": "driverId",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    {
      "$unwind": "$userinfo"
    },
    {
      "$lookup": {
        "localField": "userinfo.providerId",
        "from": "admins",
        "foreignField": "_id",
        "as": "admininfo"
      }
    },
    {
      "$unwind": {
        path: "$admininfo",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      "$project": {
        "driverId": 1,
        "driverName": 1,
        "packageinfo": 1, //Full path 
        "packageName": 1,
        "amount": 1,
        "credit": 1,
        "type": 1,
        "startDate": 1,
        "purchaseDate": 1,
        "endDate": 1,
        // "providerName": { $cond: { if: { $eq: ['$admininfo', null ]}, then: "", else: '$admininfo.fname' } },
        "providerId": { $cond: [{ $ifNull: ['$userinfo.providerId', false] }, '$userinfo.providerId', ''] },
        "providerName": { $cond: [{ $ifNull: ['$admininfo', false] }, '$admininfo.fname', ''] }
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    { "$skip": pageQuery.skip },
    { "$limit": pageQuery.take },
  ], function (err, result) {
    if (err) {
      return res.status(409).json();
    }
    console.log(likeQuery)
    return res.send(result);
  });


}

export const reBackTopUp = async (req, res) => {
  DriverPackage.findOne({ '_id': req.body.packageId }, (err, data) => {
    if (err) { return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") }); }
    else if (!data) { return res.json({ 'success': false, 'message': req.i18n.__("DATA_NOT_FOUND") }); }
    else {
      if (typeof data.driverId === "undefined" || data.driverId === "") {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") })
      }
      DriverBank.findOne({ driverId: data.driverId }, function (err, doc) {
        if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
        else if (!doc) {
          var id1 = new ObjectId;

          req.body.paymentDate = moment().format('YYYY-MM-DD');
          var trxId = GFunctions.sendRandomizeCode('0a', 7);

          var newdoc = new DriverBank
            ({
              driverId: data.driverId,
              driverName: data.driverName,
              totalBal: 0,
              trx: {
                _id: id1,
                trxId: trxId,
                description: 'Rebacked TopUp Amount',
                amt: data.amount,
                type: 'debit',
                paymentDate: GFunctions.getDateTimeinThisFormat(req.body.paymentDate, 'YYYY-MM-DD', "D-M-YYYY h:mm a"),
                paymentDateSort: GFunctions.getDateTimeinThisFormat(req.body.paymentDate, 'YYYY-MM-DD'),
                bal: 0
              },
            });

          newdoc.save((err, docs) => {
            if (err) {
              return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
            }
            var tranx = newdoc.trx.id(id1);
            let Tranxamt = tranx.amt;
            let Tranxbal = tranx.bal;
            let types = tranx.type;
            var total;
            var total1;
            if (types == 'debit') {
              total = parseFloat(parseFloat(docs.totalBal) - parseFloat(Tranxamt)).toFixed(2);
            }
            if (types == 'credit') {
              total = parseFloat(parseFloat(docs.totalBal) + parseFloat(Tranxamt)).toFixed(2);
            }
            docs.totalBal = total;
            tranx.bal = total;

            docs.save(function (err, docs) {
              if (err) { }
              else {
                driverBank.updateDriverWallet(data.driverId, tranx)
                return res.json({ 'success': true, 'message': req.i18n.__("DRIVER_ACCOUNT_ADDED_SUCCESSFULLY"), 'docs': docs });
              }
            })
          })
        }
        else if (doc) {
          var id1 = new ObjectId;

          req.body.paymentDate = moment().format('YYYY-MM-DD');
          var trxId = GFunctions.sendRandomizeCode('0a', 7);

          var trxdetails =
          {
            _id: id1,
            trxId: trxId,
            description: 'Rebacked TopUp Amount',
            amt: data.amount,
            type: 'debit',
            paymentDate: GFunctions.getDateTimeinThisFormat(req.body.paymentDate, 'YYYY-MM-DD', "D-M-YYYY h:mm a"),
            paymentDateSort: GFunctions.getDateTimeinThisFormat(req.body.paymentDate, 'YYYY-MM-DD'),
            bal: 0
          }
          DriverBank.findOneAndUpdate({ driverId: data.driverId }, { $push: { trx: trxdetails } }, { 'new': true }, function (err, docs) {
            if (err) {
              return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR_UPDATING"), 'error': err });
            }
            var tranx = docs.trx.id(id1);
            let Tranxamt = tranx.amt;
            let Tranxbal = tranx.bal;
            let types = tranx.type;
            var total;
            var total1;
            if (types == 'debit') {
              total = parseFloat(parseFloat(docs.totalBal) - parseFloat(Tranxamt)).toFixed(2);
            }
            if (types == 'credit') {
              total = parseFloat(parseFloat(docs.totalBal) + parseFloat(Tranxamt)).toFixed(2);
            }
            docs.totalBal = total;
            tranx.bal = total;

            docs.save(function (err, docs) {
              if (err) { }
              else {
                driverBank.updateDriverWallet(data.driverId, tranx)
                return res.json({ 'success': true, 'message': req.i18n.__("DRIVER_ACCOUNT_UPDATED_SUCCESSFULLY"), 'docs': docs });
              }
            })
          })
        }
      });
    }
  })
}