// config.js 
const configObj = {
  'secret': 'abservetech',
  'appMode': 'pro', //dev, pro, test
  'port': 3002, //PORT
  'database': 'wheelocabs', //wheelocabs, rebustarv2server
  'appName': 'RebuStar',
  'resetPasswordTo': 'RebuStar',

  'utcOffset': '+05:30',
  'gmtZone': 'GMT+05:30',
  'phoneCode': '+91',

  'baseurl': 'http://10.1.1.21:3002/',
  'fileurl': 'http://10.1.1.21:3002/public/',
  'frontendurl': "https://abservetechdemo.com/projects/client/rebustar/#/auth/login",//'http://173.82.154.142:3001/webadmin/',
  'landingurl': "https://abservetechdemo.com/projects/client/rebustar/#/auth/login",//'http://173.82.154.142:3001/webadmin/',

  "project_id": "rebustar-d96c1", //Google Firebase Project Id
  'firebasekey' : {
    "appName": "Rebustar",
    "serviceAccount": "service-account.json",
    "authDomain": "rebustar-d96c1.firebaseapp.com",
    "databaseURL": "https://rebustar-d96c1.firebaseio.com/",
    "storageBucket": "rebustar-d96c1.appspot.com"
  },

  'googleApi': 'AIzaSyDNyKGO0gdsyZj-4a2m63VBZLB9OY1z2wg',
  'fcmServer': 'AIzaSyApabtgEYXz5lIrTU8GgthUBTPULXiOz0A',

  'requestRadius': 1.5, //Miles 1.5Miles => 2.41402KM
  'maxDistBtRiderAndDriver': 2500, //2500=>2.5KM
  'driversNeedToPickFromSurrounding': 5,
  'driversNeedToCallForATrip': 5,
  'requestTime': 15000, //30000 = 30 sec
  'userCancelTime': 90000, //120000 = 2 Minutes  //90000 = 1.5Min
  'requestType': 'onebyone', //onebyone or broadcast,
  'noOfDriverCancelAllowed': 2, //No Of Driver Canceled Allowed Per Day (No - 1)
  'noOfRiderCancelAllowed': 1, //No Of Driver Canceled Allowed Per Day(No - 1)
  'oldCancelBalanceRiderAllowed': 30, //Total Old Cancelled Bal. max limit allowed per Rider.
  'rideLaterRemainder': 4, //Remainder Notification before in Minutes
  'rideLaterStart': 2, //Ride Later Starts Before in Minutes
  'upcomingRideLaterTimeBuffer': 15, //Upcoming will show upto this time in App
  'makeDriverOfflineAfterInactive': 3, //makeDriverOfflineAfterInactive for this minutes

  //Email Config
  'emailGateway': 'gmail', //gmail, sendgrid, none
  'smtpConfig': {
    "host": "smtp.gmail.com",
    "port": 465,
    "secure": true,
    "auth": {
      "user": 'abservetech.smtp@gmail.com',
      "pass": 'smtp@345'
    },
    'sgAcessKey': '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',
    // 'sgAcessKey': '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',//BookNRide
    // 'sgAcessKey': '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', //WE Ride
  },   
  'mailFrom': '"Admin " <rebustar@abservetech.com>',
  'supportNo': '9876543210', 

  //SMS Config
  'smsGateway' : {
    'smsGatewayName': 'twilio', //twilio, localAPI, none
    'twilioaccountSid': 'AC1271c6fed2c6fe11abd2cb98cd768f84',
    'twilioauthToken': 'a7b2810a459d71590898cfbb34744cd1',
    'twilioNo': '+14352339850',
    'nexmoapiKey': 'Ae712dbe',
    'nexmoapisecret': 'WhjplopSLnue6Klt', 
    'nexmoapiNumber' :  '',
    'localAPIendpoint' : ''
  },

  'paymentGateway' : {
    'paymentGatewayName': 'braintree', // {stripe,paypal,braintree,paystack}, none
    'paymentGatewayCurrency': 'usd', 
    // 'stripeSk': 'sk_test_zqDfJeRJJK23IYjTipXyueBh', //rebustar Stripe Secret Key
    // 'stripeSk': 'sk_live_1LxfS7vA1knzM8K1f0nqF4lh', //Dash Stripe Secret Key
    'stripeSk': 'sk_test_9iGVVwQ07LNmGUGeIaTxconi', //Dash Stripe Secret Key
    'stripeConnectAccountType': 'express', //express

    'braintreeEnvironment': 'Sandbox', //braintree (Sandbox/)
    'braintreeMerchantId': 'c4qsnymzstrz64q7', //braintree
    'braintreePublicKey': 'tg4c8x2vqkkq8hxx', //braintree
    'braintreePrivateKey': '39ec8e5f4bddbe7e736a8176c048d803', //braintree
  }, 
   
  'currency': 'usd',
  'currencySymbol': '$',

  'distanceUnit': 'KM', //KM or Miles
  'distanceSymbol' : 'KM', //KM or Miles

  'referalAmt': 10, //Who Uses the Code
  'refererAmt': 10, //Who gave the Code

  'companyaddress': 'Company Address',
  'companymail' : 'abservetech@gmail.com', 
};

module.exports = configObj;