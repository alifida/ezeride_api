// ./express-server/controllers/todo.server.controller.js
import mongoose from 'mongoose';

//import models
import Vehicletype from '../models/vehicletype.model';
import * as HelperFunc from './adminfunctions';
import logger from '../helpers/logger';
import labels from '../helpers/labels.helper';
import * as GFunctions from './functions';
import { parse } from 'fast-csv';
const config = require('../config');
const featuresSettings = require('../featuresSettings');
const moment = require('moment');
const _ = require('lodash');

export const getCityBasedVehicleCharge = async (serviceTypeId, pickupCity, distanceInKM, timeInMinutes, reqtime, waitingTime = 0) => {
  try {
    var vehicleData = await Vehicletype.findById(serviceTypeId).exec(); // @v2TODO pass pickupCity as null

    var vehicleDetails = {
      "type": vehicleData.type,
      "seats": vehicleData.asppc,
      "image": vehicleData.file,
      "available": vehicleData.available,
      "description": vehicleData.description,
      "features": vehicleData.features,
      "serviceId": (vehicleData._id).toString(),
    };

    //fareDetails Default
    var fareDetails = {
      "perKMRate": vehicleData.bkm,
      "fareType": 'kmrate',
      "distance": distanceInKM, //Actual Distance
      "KMFare": parseFloat(parseFloat(distanceInKM) * parseFloat(vehicleData.bkm)).toFixed(2), //Fare for Traveled KM
      "BaseFare": vehicleData.baseFare ? vehicleData.baseFare : 0, //Base / Service fee
      "travelTime": timeInMinutes ? timeInMinutes : 0,
      "travelRate": vehicleData.timeFare ? vehicleData.timeFare : 0,
      "travelFare": 0,
      //Waiting Fare
      "timeRate": vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime ? vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime : 0, //Waiting Charge per min
      "waitingCharge": vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime ? vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime : 0, //Waiting Charge per min
      "waitingTime": waitingTime, //Fare for Waiting Time
      "waitingFare": 0, //Fare for Waiting Time
      //cancelation Fees
      "cancelationFeesRider": vehicleData.cancelationFeesRider,
      "cancelationFeesDriver": vehicleData.cancelationFeesDriver,
      //Pickup Charge
      "pickupCharge": 0, //Pickup Charge 
      //commision 
      "comison": vehicleData.comison ? vehicleData.comison : 0, //commision percentage       
      "comisonAmt": 0, //commision to admin 
      "isTax": vehicleData.isTax,
      "taxPercentage": vehicleData.taxPercentage ? vehicleData.taxPercentage : 0,
      "tax": 0, // tax amount 
      "minFare": vehicleData.mfare,//Minimum fare
      "flatFare": vehicleData.mfare,//flatFare   
      "oldCancellationAmt": 0,
      "fareAmtBeforeSurge": vehicleData.mfare,
      "totalFareWithOutOldBal": vehicleData.mfare,
      "totalFare": vehicleData.mfare,//Minimum fare is applied as Total  
      "BalanceFare": vehicleData.mfare,//Minimum fare is applied as Total  
      "DetuctedFare": 0,//Detucted via Promo/Digital,wallet etc 
      "paymentMode": 'Cash',
      "currency": config.currency,
    };

    /*var applyValues = {
      "applyCommission": featuresSettings.applyAdminCommission,
      "applyPeakCharge": featuresSettings.applyPeakCharge,
      "applyNightCharge": featuresSettings.applyNightCharge,
      "applyWaitingTime": featuresSettings.applyWaitingCharge,
      "applyTax": featuresSettings.applyTax,
      "applyPickupCharge": featuresSettings.applyPickupCharge,
    }; */

    var applyValues = featuresSettings.applyValues;

    var offers = {
      "offerPerUser": 0,
      "offerPerDay": 0,
      "discount": 0,
      "cmpyAllowance": false,
    };

    //Travel time fare //Final
    if (featuresSettings.applyTravelFare) fareDetails['travelFare'] = parseFloat(Number(timeInMinutes) * Number(vehicleData.timeFare)).toFixed(2)

    //waiting time fare //Final
    fareDetails['waitingFare'] = getWaitingFare(vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime, vehicleData.allowMinimumWaitingTimeInMinutes, vehicleData.isWaitingTimeExceddedChargesApplicable, waitingTime);

    //Night Fare Percentage = value if applied or 0
    fareDetails['nightObj'] = getFareIfTimeFallsIn(vehicleData.nightHours[0], reqtime, 'night');

    //Peak Fare Percentage
    fareDetails['peakObj'] = getFareIfTimeFallsIn(vehicleData.peakHours[0], reqtime, 'peak');
    if (!fareDetails['peakObj']) fareDetails['peakObj'] = getFareIfTimeFallsIn(vehicleData.peakHours[1], reqtime, 'peak');

    //Pickup Charge //Final
    fareDetails['pickupCharge'] = getPickupCharge(vehicleData.conveyancePerKm, vehicleData.conveyanceType, vehicleData.conveyanceAvailable);

    //Get Approx distance Obj
    fareDetails['distanceObj'] = getDistanceObj(vehicleData.distance, distanceInKM);

    //Distance wise modifications
    if (fareDetails['distanceObj'].length) {
      var distanceObj = fareDetails['distanceObj'][0];
      var distanceDetails = getPerKmRateForDist(fareDetails['distanceObj'][0]);
      fareDetails['fareType'] = distanceDetails.fareType;
      if (fareDetails['fareType'] == 'kmrate') {
        fareDetails['perKMRate'] = distanceDetails.fare;
      } else {
        fareDetails['flatFare'] = distanceDetails.fare;
        fareDetails['minFare'] = distanceDetails.fare;
      }

      //Modified Apply 
      applyValues['applyCommission'] = distanceObj.applyCommission;
      applyValues['applyPeakCharge'] = distanceObj.applyPeakCharge;
      applyValues['applyNightCharge'] = distanceObj.applyNightCharge;
      applyValues['applyWaitingTime'] = distanceObj.applyWaitingTime;
      applyValues['applyTax'] = distanceObj.applyTax;
      applyValues['applyPickupCharge'] = distanceObj.applyPickupCharge;
      //Modified Offers
      offers['cmpyAllowance'] = distanceObj.cmpyAllowance;
      offers['offerPerUser'] = distanceObj.offerPerUser;
      offers['offerPerDay'] = distanceObj.offerPerDay;
      offers['discount'] = distanceObj.discount;
    }

    //calculate Total Fare (Daily)= Base Fare +  ( KM * KM Rate ) + ( Travel Fare * Travel Time ) + ( Waiting Fare * Waiting Time ) + Pickup Charge + Tax - Discount
    //KM Rate 
    fareDetails['KMFare'] = parseFloat(parseFloat(distanceInKM) * parseFloat(fareDetails['perKMRate'])).toFixed(2); //Final
    fareDetails['totalFare'] = fareDetails['BalanceFare'] = Number(fareDetails['BaseFare']) + Number(fareDetails['KMFare']) + Number(fareDetails['travelFare']) + Number(fareDetails['waitingFare']) + Number(fareDetails['pickupCharge']);
    fareDetails['fareAmtBeforeSurge'] = Number(fareDetails['totalFare']);
    var totalFare = getTotalFare(fareDetails, applyValues, offers);
    fareDetails['fareAmt'] = totalFare.fareAmt;
    fareDetails['comisonAmt'] = totalFare.comisonAmt;
    fareDetails['tax'] = totalFare.tax;
    fareDetails['totalFareWithOutOldBal'] = Number(totalFare.totalFareWithOutOldBal);
    // fareDetails['fareAmtBeforeSurge'] = totalFare.fareAmtBeforeSurge;

    fareDetails['totalFare'] = fareDetails['BalanceFare'] = totalFare.totalFare;

    /* fareDetails = _.mapValues(fareDetails, function (v) { if(typeof v === 'number') {
      return parseFloat(v.toFixed(2));
    }else { return v; }  });//Round all to 2 Decimals */

    var resData = {
      'vehicleDetails': _.cloneDeep(vehicleDetails),
      'fareDetails': _.cloneDeep(fareDetails),
      'offers': _.cloneDeep(offers),
      'applyValues': _.cloneDeep(applyValues),
    }

    return resData;
  } catch (error) {
    logger.error(error);
    return error;
  }
};

//calculate Total Fare (Daily)= Base Fare +  ( KM * KM Rate ) + ( Travel Fare * Travel Time ) + ( Waiting Fare * Waiting Time ) + Pickup Charge + Tax - Discount
function getTotalFare(fareDetails, applyValues, offers) {
  var resObj = {
    'fareAmt': 0, 'fareAmtBeforeSurge': 0, 'comisonAmt': 0, 'tax': 0, 'flatFare': 0, 'oldCancellationAmt': 0, 'totalFareWithOutOldBal': 0, 'totalFare': 0
  };
  //Flat Rate
  if (fareDetails['fareType'] == 'flatrate') {
    resObj['fareAmt'] = fareDetails['flatFare'];
  } else { //KM Fare
    resObj['fareAmt'] = fareDetails['KMFare'];
  }

  //Add Waiting Time
  if (applyValues['applyWaitingTime']) {
    resObj['fareAmt'] = parseFloat(resObj['fareAmt']) + parseFloat(fareDetails['waitingFare']);
  }
  //Add Pickup Charge
  if (applyValues['applyPickupCharge']) {
    resObj['fareAmt'] = parseFloat(resObj['fareAmt']) + parseFloat(fareDetails['pickupCharge']);
  }

  resObj['fareAmt'] = Number(fareDetails['BaseFare']) + Number(fareDetails['fareAmt']) + Number(fareDetails['travelFare']);

  resObj['fareAmtBeforeSurge'] = resObj['fareAmt'];
  //Add Peak & Night Multipler
  if (applyValues['applyPeakCharge']) {
    if (fareDetails['peakObj'].isApply) resObj['fareAmt'] = parseFloat(resObj['fareAmt']) * parseFloat(fareDetails['peakObj'].percentageIncrease);
  }
  if (applyValues['applyNightCharge']) {
    if (fareDetails['nightObj'].isApply) resObj['fareAmt'] = parseFloat(resObj['fareAmt']) * parseFloat(fareDetails['nightObj'].percentageIncrease);
  }

  //Tax for (KM + Multipler + Waiting + Pickup )
  if (applyValues['applyTax']) {
    resObj['tax'] = parseFloat(fareDetails['taxPercentage']) * parseFloat(resObj['fareAmt']) / 100;
  }

  //Total Fare
  resObj['totalFare'] = parseFloat(resObj['fareAmt']) + parseFloat(resObj['tax']);
  //Check For Min Fare
  // if (fareDetails['fareType'] == 'kmrate'){
  if (parseFloat(resObj['totalFare']) < parseFloat(fareDetails['minFare'])) {
    resObj['totalFare'] = fareDetails['minFare'];
  }
  // }

  resObj['totalFareWithOutOldBal'] = resObj['totalFare'];
  //Calculate Commision
  resObj['comisonAmt'] = parseFloat(fareDetails['comison']) * parseFloat(resObj['totalFareWithOutOldBal']) / 100;

  resObj = _.mapValues(resObj, function (v) {
    if (typeof v === 'number') {
      return parseFloat(v.toFixed(2));
    } else { return v; }
  });//Round all to 2 Decimals 

  GFunctions.clearObj(resObj);
  return resObj;
}

/**
 * Get waiting fare if its ecxceeds minTime
 * @param {*} rate 
 * @param {*} minTime 
 * @param {*} isApplicable 
 * @param {*} waitingTime (Mins)
 */
function getWaitingFare(rate, minTime, isApplicable, waitingTime) {
  var waitingFare = 0;
  if (isApplicable) {
    if (Number(waitingTime) > Number(minTime)) {
      if (waitingTime <= 1) waitingTime = 0;
      waitingFare = Number(rate) * Number(waitingTime);
    }
  }
  return waitingFare;
}

/**
 * getFareIfTimeFallsIn = if HOurs within given time it gives percentage to increase
 * @param {*} hours
 * @param {*} now 
 *          "isApply": false,
            "percentageIncrease": 1.5,
            "alertLable": "Notes : Peak Fare x{PERCENTAGE} ({TIME})"
 */
function getFareIfTimeFallsIn(hours, now, forType = 'peak') {
  var resObj = { 'isApply': false, 'percentageIncrease': 0, 'alertLable': '' };
  var format = 'HH:mm:ss';
  if (!now || now == '') {
    now = GFunctions.sendTimeNow(format);
  } else {
    now = now;
  }

  var to = moment(hours.to, format),
    from = moment(hours.from, format),
    now = moment(now, format);

  if (from > to) { //22PM to 8AM 
    if (now.isBetween(to, from)) { resObj.isApply = false; } else { resObj.isApply = true; }
  } else {
    if (now.isBetween(from, to)) { resObj.isApply = true; } else { resObj.isApply = false; }
  }

  if (forType == 'peak') {
    resObj.percentageIncrease = Number(getPercentageMultipiler(hours.percentPeakFare));
    resObj.alertLable = GFunctions.convertLableDynamically(labels.isPeakExistAlertLabel, { 'PERCENTAGE': resObj.percentageIncrease, 'TIME': hours.from + ' - ' + hours.to });
  } else {
    resObj.percentageIncrease = Number(getPercentageMultipiler(hours.percentNightFare));
    resObj.alertLable = GFunctions.convertLableDynamically(labels.isNightExistAlertLabel, { 'PERCENTAGE': resObj.percentageIncrease, 'TIME': hours.from + ' - ' + hours.to });
  }

  return GFunctions.roundAllValuesToTwoDigits(resObj);
}

function getPercentageMultipiler(percentage) {
  return parseFloat(1 + (parseFloat(percentage) / 100)).toFixed(2);
}

function getPickupCharge(conveyancePerKm, conveyanceType, conveyanceAvailable) {
  if (conveyanceAvailable) {
    if (conveyanceType == 'flatrate') {
      return conveyancePerKm;
    }
  } else {
    return 0;
  }
}

function getDistanceObj(distanceArray, distanceInKM) {
  var filteredFareOffers;
  if (distanceArray) {
    var filteredFareOffers = _.filter(distanceArray, i => Number(i.distanceFrom) <= distanceInKM && Number(i.distanceTo) >= distanceInKM);
  }
  return filteredFareOffers;
}

function getPerKmRateForDist(distance) {
  var distObj = {};
  distObj.fareType = distance.distanceFareType;
  if (distance.distanceFareType == 'flatrate') {
    distObj.fare = distance.distanceFarePerFlatRate;
  } else {
    distObj.fare = distance.distanceFarePerKM;
  }
  return distObj;
}

