// ./express-server/app.js
import express from 'express';

var https = require('https');
const fs = require('fs');
const httpsoptions = {
 key: fs.readFileSync('/home/developers/ssl/private.key'),
  cert: fs.readFileSync('/home/developers/ssl/certificate.crt'),
  ca: fs.readFileSync('/home/developers/ssl/ca_bundle.crt')
};

import path from 'path';
import logger from 'morgan';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';

// import bb from 'express-busboy'; 
var bodyParser = require('body-parser');
var multer = require('multer');
var validator = require('express-validator');
const trimmer = require('express-trimmer');
const mustacheExpress = require('mustache-express');
var winston = require('winston'),
  expressWinston = require('express-winston');
  
var debugMode = false;

// import routes
import uberRoutes from './routes/uber.server.route';
import uberAppRoutes from './routes/uberapp.server.route';
import uberTestRoutes from './routes/ubertest.server.route';
import cron from './cron';

const config = require('./config');  
// define our app using express
const app = express();

app.engine('html', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');
// allow-cors
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  res.header('access-control-expose-headers', 'x-total-count');

  // allow preflight
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(trimmer);
app.use(validator());

if (debugMode) {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  // express-winston logger makes sense BEFORE the router
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: true,
        colorize: true
      })
    ]
  }));
}

// configure app
app.use(logger('dev'));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/webadmin', express.static(path.join(__dirname, '../webadmin')));
app.use('/landing', express.static(path.join(__dirname, '../landing')));

// set the port
const port = process.env.PORT || config.port;

// connect to database
mongoose.Promise = global.Promise;

app.use((req, res, next) => {
  var url = req.protocol + '://' + req.headers.host + req.originalUrl;
  connectWithRetry();
  next();
});


//Connect to Db 
const options = {
  useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: 30, // Retry up to 30 times
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0
};

const connectWithRetry = () => {
  // console.log('MongoDB connection with retry')
  mongoose.connect("mongodb://localhost/" + config.database, options).then(() => { 
  }).catch(err => {
    console.log('MongoDB connection unsuccessful, retry after 5 seconds.', err)
    setTimeout(connectWithRetry, 3000)
  })
};

connectWithRetry();

// add Source Map Support
SourceMapSupport.install();

app.post('/profile', (req, res, next) => {
  // console.dir(req.headers['content-type']);
  console.log(req.body);
  return res.end('Api working');
});

app.use('/adminapi', uberRoutes);
app.use('/api', uberAppRoutes);
app.use('/test', uberTestRoutes);


app.get('/', (req, res) => {
  return res.end('Api working');
})

// catch 404
app.use((req, res, next) => {
  res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});

// start the server
/*app.listen(port, () => {
  console.log(`App Server Listening at ${port}`);
});*/

// start the server
https.createServer(httpsoptions, app).listen(port,() => {
  console.log(`App Server Listening at ${port}`);
});