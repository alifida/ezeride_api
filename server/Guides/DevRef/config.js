// config.js  EDITABLE BY ADMIN
const configObj = {
  //PANEL General Settings
  'secret': 'abservetech', //NO NEED
  'appMode': 'pro',//NO NEED
  'port': 3002, //NO NEED
  'database': 'wheelocabs', //NO NEED
  'appName': 'RebuStar', //Editable = Text; Lable = App Name
  'resetPasswordTo': 'RebuStar', //Editable = Text; Lable = Default Password

  'utcOffset': '+05:30', // Editable = Text; Lable = Timezone offset
  'gmtZone': 'GMT+05:30',  //NO NEED
  'phoneCode': '+91', // Editable = Text; Lable = Default Phone Code

  'baseurl': 'http://10.1.1.21:3002/', //NO NEED
  'fileurl': 'http://10.1.1.21:3002/public/',//NO NEED 
  'frontendurl': "https://abservetechdemo.com/projects/client/rebustar/#/auth/login",//NO NEED
  'landingurl': "https://abservetechdemo.com/projects/client/rebustar/#/auth/login",//NO NEED
  'companyaddress': 'Company Address',  // Editable = Text; Lable = Company Address
  'companymail': 'abservetech@gmail.com',  // Editable = Text; Lable =  Company Mail
  //PANEL General Settings

  'requestRadius': 1.5,  // Editable = Text; Lable = Max Request Radius(In Miles)
  'maxDistBtRiderAndDriver': 2500, //2500=>2.5KM //NO NEED 
  'driversNeedToPickFromSurrounding': 5, //NO NEED   // Editable = Text;
  'driversNeedToCallForATrip': 5, //NO NEED   // Editable = Text;
  'requestTime': 15000,  //NO NEED   // Editable = Text;
  'userCancelTime': 90000,  //NO NEED   // Editable = Text;
  'requestType': 'onebyone',  //NO NEED 
  'noOfDriverCancelAllowed': 2, //Editable = Number; Lable = No Of Driver Canceled Allowed Per Day (No - 1)
  'noOfRiderCancelAllowed': 1, //Editable = Number; Lable = No Of Driver Canceled Allowed Per Day(No - 1)
  'oldCancelBalanceRiderAllowed': 30,  //NO NEED 
  'rideLaterRemainder': 4,  //NO NEED 
  'rideLaterStart': 2,  //NO NEED 
  'upcomingRideLaterTimeBuffer': 15, //NO NEED 
  'makeDriverOfflineAfterInactive': 3,  //NO NEED 
  'currency': 'usd',  // Editable = Text; Lable = Currency lable
  'currencySymbol': '$', // Editable = Text; Lable = Currency Symbol
  'distanceUnit': 'KM',  // Editable = Text; Lable = Distance Unit lable
  'distanceSymbol': 'KM',  // Editable = Text; Lable = Distance Unit Symbol
  'referalAmt': 10, //NO NEED 
  'refererAmt': 10, //NO NEED 
  //PANEL General Settings


  //PANEL FIREBASE
  "project_id": "rebustar-d96c1",  // Editable = Text; Lable = Project Id
  'firebasekey' : {
    "appName": "Rebustar", // Editable = Text; Lable = Default App Name
    "serviceAccount": "service-account.json", 
    "authDomain": "rebustar-d96c1.firebaseapp.com", // Editable = Text; Lable = Auth Domain
    "databaseURL": "https://rebustar-d96c1.firebaseio.com/", // Editable = Text; Lable =  Database URL
    "storageBucket": "rebustar-d96c1.appspot.com" // Editable = Text; Lable = Storage Bucket
  },
  'fcmServer': 'AIzaSyApabtgEYXz5lIrTU8GgthUBTPULXiOz0A',  // Editable = Text; Lable = FCM Server
  'googleApi': 'AIzaSyDNyKGO0gdsyZj-4a2m63VBZLB9OY1z2wg', // Editable = Text; Lable = Google API KEY
  //PANEL FIREBASE
 
  //PANEL Email Config
  'emailGateway': 'gmail', //Editable  = DropDown; gmail, sendgrid, none; Lable = Email Gateway
  'smtpConfig': {
    "host": "smtp.gmail.com", //NO NEED 
    "port": 465, //NO NEED 
    "secure": true,
    "auth": {
      "user": 'abservetech.smtp@gmail.com', //Editable  = text;  Lable = SMTP Email User Name //smtpConfig.auth.user = 'abservetech.smtp@gmail.com'
      "pass": 'smtp@345' //Editable  = text;  Lable = SMTP Email Password
    },
    'sgAcessKey': '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',  //Editable  = text;  Lable = SendGrid Key
  },   
  'mailFrom': '"Admin " <rebustar@abservetech.com>', //Editable  = text;  Lable = Mail From Address
  'supportNo': '9876543210',  //Editable  = text;  Lable = App Support Number
  //PANEL Email Config


  //PANEL SMS Config
  'smsGateway' : {
    'smsGatewayName': 'twilio', //Editable  = DropDown; twilio, nexmo, none; Lable = SMS Gateway  
    'twilioaccountSid': 'AC1271c6fed2c6fe11abd2cb98cd768f84',  //Editable  = text;  Lable = Twilio Account Sid
    'twilioauthToken': 'a7b2810a459d71590898cfbb34744cd1', //Editable  = text;  Lable = Twilio Auth Token
    'twilioNo': '+14352339850', //Editable  = text;  Lable = Twilio No.
    'nexmoapiKey': 'Ae712dbe', //Editable  = text;  Lable =  Nexmo API Key
    'nexmoapisecret': 'WhjplopSLnue6Klt',  //Editable  = text;  Lable =  Nexmo API Secret
    'nexmoapiNumber': '', //Editable  = text;  Lable =  Nexmo API Number
    'localAPIendpoint' : '' //NO NEED
  },
  //PANEL SMS Config

  //PANEL Payment Gateway Config
  'paymentGateway' : {
    'paymentGatewayName': 'braintree', //Editable  = DropDown;  stripe,paypal,braintree,none; Lable = Payment Gateway  
    'paymentGatewayCurrency': 'usd', //Editable  = text;  Lable =  Payment Gateway Currency
    'stripeSk': 'sk_test_9iGVVwQ07LNmGUGeIaTxconi', //Editable  = text;  Lable = Stripe Secret Key
    'stripeConnectAccountType': 'express', //NO NEED

    'braintreeEnvironment': 'Sandbox',  //Editable  = text;  Lable = Braintree Environment
    'braintreeMerchantId': 'c4qsnymzstrz64q7',//Editable  = text;  Lable = Braintree MerchantId
    'braintreePublicKey': 'tg4c8x2vqkkq8hxx', //Editable  = text;  Lable = Braintree PublicKey
    'braintreePrivateKey': '39ec8e5f4bddbe7e736a8176c048d803', //Editable  = text;  Lable = Braintree PrivateKey
  }, 
  //PANEL Payment Gateway Config

 
};
