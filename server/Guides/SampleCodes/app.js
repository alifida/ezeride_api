// ./express-server/app.js
import express from 'express';
import path from 'path';
import logger from 'morgan';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';
// import bb from 'express-busboy'; 
var bodyParser = require('body-parser');
var multer = require('multer'); 
const trimmer = require('express-trimmer');

var debugMode = true;

// import routes  
import AdminRoutes from './routes/admin.route'; 
import AppRoutes from './routes/app.route'; 
import SettingRoutes from './routes/setting.route'; 
import TestRoutes from './routes/test.route'; 

var winston = require('winston'),
    expressWinston = require('express-winston');

// define our app using express
const app = express(); 
// allow-cors
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With'); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
  res.header('access-control-expose-headers', 'x-total-count'); 

    // allow preflight
    if (req.method === 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
  }); 

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
  
if(debugMode){  
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');  
  // express-winston logger makes sense BEFORE the router
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
            json: true,
            colorize: true
          })
    ] 
  }));
}
 
// configure app
app.use(logger('dev'));
app.use('/public', express.static(path.join(__dirname, 'public'))); 
app.use(trimmer)
// app.use('/terms', express.static(path.join(__dirname, '/controllers/html/tnc.html'))); 
// app.use('/privacy', express.static(path.join(__dirname, '/controllers/html/privacypolicy.html'))); 

// set the port
const port = process.env.PORT || 3005;

// connect to database
mongoose.Promise = global.Promise; 

app.use((req, res, next) => {
  var url =  req.protocol + '://' + req.headers.host + req.originalUrl; 
  connectWithRetry();  
  next();
});


//Connect to Db 
const options = {
    useMongoClient: true,
    autoIndex: false, // Don't build indexes
    reconnectTries: 30, // Retry up to 30 times
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
};

const connectWithRetry = () => {
  // console.log('MongoDB connection with retry')
  //mongoose.connect("mongodb://localhost/qride", options).then(()=>{
  mongoose.connect("mongodb://localhost/qride_local", options).then(()=>{
    // console.log('MongoDB is connected')// 
  }).catch(err=>{
    console.log('MongoDB connection unsuccessful, retry after 5 seconds.')
    setTimeout(connectWithRetry, 3000)
  })
};

connectWithRetry();   

// add Source Map Support
SourceMapSupport.install();
  
app.use('/api', AppRoutes); 
app.use('/adminapi', AdminRoutes);  
app.use('/test', TestRoutes); 
app.use('/setting',SettingRoutes);

if(debugMode){  
  app.use(expressWinston.errorLogger({
    transports: [
      new winston.transports.Console({
            json: true,
            colorize: true
          })
    ] 
  }));
}

app.get('/', (req,res) => {
  return res.end('Api working');
})
 
// catch 404
app.use((req, res, next) => {
  res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});
 
// start the server
app.listen(port,() => {
  console.log(`App Server Listening at ${port}`);
});
