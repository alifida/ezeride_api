// ./express-server/routes/uber.server.route.js
import express from 'express';

const trimRequest = require('trim-request');
const verifyToken = require('./VerifyToken');

// File uploading
import path from 'path';
var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/')
  },
  filename: (req, file, cb) => {
    var filetypes = /jpeg|jpg|png/;
    var mimetype = filetypes.test(file.mimetype);
    var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    if (mimetype && extname) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  }
});
var upload = multer({ storage: storage });
var cpUpload = upload.single('file');
// File uploading 

//Driver Vehicle Image file
var VIFstorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/driverVehicle/')
  },
  filename: (req, file, cb) => {
    var filetypes = /jpeg|jpg|png/;
    var mimetype = filetypes.test(file.mimetype);
    var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    if (mimetype && extname) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  }
});
var VIFupload = multer({ storage: VIFstorage });
var mwVIFupload = VIFupload.single('file');
//Driver Vehicle Image file

//Driver Bank Transation Image file
var VBTstorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/driverBankTransation/')
  },
  filename: (req, file, cb) => {
    var filetypes = /jpeg|jpg|png/;
    var mimetype = filetypes.test(file.mimetype);
    var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    if (mimetype && extname) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  }
});
var VBTFupload = multer({ storage: VBTstorage });
var mwVBTupload = VBTFupload.single('file');
//Driver Bank Transation Image file

//import controller file
// import * as todoController from '../controllers/uber.server.controller';
import * as companyCtrl from '../controllers/company.controller';
import * as adminCtrl from '../controllers/admin';
import * as driverCtrl from '../controllers/driver';
import * as drivertaxiCtrl from '../controllers/drivertaxi';
import * as vehicletypeCtrl from '../controllers/vehicletype';
import * as ridersCtrl from '../controllers/rider';
import * as commonCtrl from '../controllers/common';
import * as tripCtrl from '../controllers/trips';
import * as appCtrl from '../controllers/app';
import * as appOBOReqFlowCtrl from '../controllers/appOBOReqFlow';
import * as stripeCtrl from '../controllers/stripe';
import * as paymentCtrl from '../controllers/paymentGateway/index';
import * as offersCtrl from '../controllers/offers';
import * as getServiceCityController from '../controllers/servicecity.controller';
import * as driverPayPackageCtrl from '../controllers/driverPayPackage';
import * as cmsCtrl from '../controllers/cmsapis';

import * as appValidate from '../validations/appValidate';
import * as packageCtrl from '../controllers/package.controller';
import * as driverBankCtrl from '../controllers/driverBank';

// get an instance of express router
const router = express.Router();

//Fire Base
// router.route('/getFb').get(driverCtrl.getFb);
// router.route('/setFb').get(appCtrl.setFb);
router.route('/fcmp').post(appCtrl.fcmp);


// router.route('/seti').get(appCtrl.seti);
// router.route('/stop').get(appCtrl.stop); 

router.route('/filetest/:id?').post(cpUpload, commonCtrl.filetest).put(cpUpload, commonCtrl.filetest);

router.route('/admin').post(adminCtrl.addData).get(adminCtrl.getData);

router.route('/company/delete/:id').delete(companyCtrl.deleteCompany);

router.route('/company/update/:id').put(companyCtrl.updateCompany);

router.route('/company').get(companyCtrl.getCompany).post(companyCtrl.addCompany);

router.route('/vehicletypelists').get(vehicletypeCtrl.vehicletypelists);
//Driver
router.route('/verifyNumberDriver').post(appValidate.verifyNumberDriver, driverCtrl.verifyNumber);
router.route('/driver').post(appValidate.driverAddData, driverCtrl.addData).patch(cpUpload, appValidate.driverAddData, driverCtrl.addData).get(verifyToken, driverCtrl.getAppData).put(verifyToken, cpUpload, driverCtrl.updateAppData);
router.route('/driverProfileUpdate').post(verifyToken, cpUpload, driverCtrl.updateAppData);
router.route('/driverDocs').post(verifyToken, cpUpload, driverCtrl.uploadDriverDocs);
router.route('/driverlogin').post(appValidate.driverLogin, driverCtrl.login);
router.route('/drivertaxi/:id').get(driverCtrl.getAppDrivertaxis);
router.route('/drivertaxi').post(driverCtrl.addDrivertaxisData).delete(driverCtrl.deleteAppDrivertaxis).put(driverCtrl.updateAppDrivertaxis);
router.route('/drivertaxiImage').put(mwVIFupload, driverCtrl.updateAppDrivertaxiImage);
router.route('/driverTaxiDocs').post(cpUpload, driverCtrl.uploadDriverTaxiDocs);
router.route('/driverpwd').put(verifyToken, driverCtrl.updatePassword);
router.route('/driverBankDetails').get(verifyToken, driverCtrl.driverBankDetails);
router.route('/driverBankTransaction').post(verifyToken, driverCtrl.driverBankTransaction);
router.route('/driverForgotPassword').post(driverCtrl.driverForgotPassword).patch(driverCtrl.changePasswordByApp);
router.route('/driverChangePassword/:code/:id').get(driverCtrl.changePasswordTemplate).post(driverCtrl.changePassword);
router.route('/driverResetPassword').post(driverCtrl.driverResetPasswordWithOTP);
router.route('/driverActiveTripType').patch(verifyToken, driverCtrl.driverActiveTripType);
router.route('/getVehicleServiceAvailablity/:type').get(verifyToken, driverCtrl.getVehicleServiceAvailablity);
router.route('/driverWalletReport').get(verifyToken, driverBankCtrl.driverWalletReportApp);
router.route('/driverBankReport').get(verifyToken, driverBankCtrl.driverBankReportApp);

//Wallet : Driver Payout
router.route('/listBanks').get(verifyToken, paymentCtrl.getListOfBanks);
router.route('/addDriverBank').put(verifyToken, paymentCtrl.addDriverBank);
router.route('/driverRequestPayoutTransferAmount').put(verifyToken, paymentCtrl.driverRequestPayoutTransferAmount);
//Earnings
router.route('/driverEarningsBtDate').post(verifyToken, driverCtrl.driverEarningsBtDate);
router.route('/driverEarningsForMonth').post(verifyToken, driverCtrl.driverEarningsForMonth);
router.route('/adminup').post(cpUpload, adminCtrl.uploadas);

//RIDER
router.route('/verifyNumber').post(appValidate.verifyNumberRider, ridersCtrl.verifyNumber);
router.route('/riders').post(appValidate.riderAddData, ridersCtrl.addData);
router.route('/riderslogin').post(appValidate.riderLogin, ridersCtrl.login);
router.route('/rider').get(verifyToken, ridersCtrl.getAppData).put(verifyToken, cpUpload, ridersCtrl.updateAppData);
router.route('/riderImage').put(verifyToken, cpUpload, ridersCtrl.riderImage);
router.route('/riderUpdateVerifiedData').put(verifyToken, ridersCtrl.riderUpdateVerifiedData);
router.route('/rider').post(verifyToken, cpUpload, ridersCtrl.updateAppData);
router.route('/riderpwd').put(verifyToken, ridersCtrl.updatePassword);
router.route('/rideremgcontact').post(verifyToken, ridersCtrl.addEmgContact).get(verifyToken, ridersCtrl.getEmgContact).delete(verifyToken, ridersCtrl.delEmgContact).put(verifyToken, ridersCtrl.putEmgContact);
router.route('/ridersAddress/:addressId?').post(verifyToken, ridersCtrl.addRidersAddress).delete(verifyToken, ridersCtrl.deleteRiderAddress).get(verifyToken, ridersCtrl.getRidersAddress);
//router.route('/riderForgotPassword').post(ridersCtrl.riderForgotPassword).patch(ridersCtrl.riderResetPassword); 
router.route('/riderForgotPassword').post(ridersCtrl.riderForgotPassword).patch(ridersCtrl.changePasswordByApp);
router.route('/riderChangePassword/:code/:id').get(ridersCtrl.changePasswordTemplate).post(ridersCtrl.changePassword);
router.route('/riderResetPassword').post(ridersCtrl.riderResetPasswordWithOTP);
//router.route('/driverForgotPassword').post(driverCtrl.driverForgotPassword).patch(driverCtrl.driverResetPassword); 
router.route('/emergencyMsg').post(verifyToken, ridersCtrl.emergencyMsg)

// router.route('/drivertaxi').get(drivertaxiCtrl.getDrivertaxis).post(drivertaxiCtrl.addData);

//offers
router.route('/offersList').get(offersCtrl.showOfferList).post(offersCtrl.showOfferList);

//requestTaxi
router.route('/serviceBasicFare').post(appCtrl.getServiceBasicfare);
router.route('/estimationFare').post(appValidate.estimationFare, appCtrl.getestimationFare, appCtrl.getestimationFareMultiDrop);
router.route('/estimationFareMultiDrop').post(appValidate.estimationFare, appCtrl.getestimationFareMultiDrop);
router.route('/estimationFareForHailTaxi').post(verifyToken, appValidate.estimationFareForHailTaxi, appCtrl.getestimationFare);
router.route('/requestTaxi').post(verifyToken, appValidate.requestTaxi, appCtrl.requestTaxi, appCtrl.requestTaxiMulti);
//
// router.route('/requestTaxiMulti').post(verifyToken, appCtrl.requestTaxiMulti);
//
router.route('/requestHailTaxi').post(verifyToken, appValidate.requestHailTaxi, appCtrl.requestHailTaxi);
router.route('/requestTaxiRetry').put(verifyToken, appCtrl.requestTaxiRetry);
router.route('/cancelTaxi').put(verifyToken, appCtrl.cancelTaxi);
router.route('/setCurrentTaxi').put(verifyToken, appCtrl.setCurrentTaxi);
router.route('/setOnlineStatus').put(verifyToken, appCtrl.setOnlineStatus);
router.route('/DriverLocation').put(verifyToken, appCtrl.DriverLocation);
router.route('/declineRequest').put(verifyToken, appCtrl.declineRequest);
router.route('/acceptRequest').put(verifyToken, appCtrl.acceptRequest);
router.route('/cancelTrip').put(verifyToken, appCtrl.cancelTrip);
router.route('/cancelCurrentTrip').put(verifyToken, appCtrl.cancelCurrentTrip);
router.route('/tripCurrentStatus').put(appValidate.tripCurrentStatus, appCtrl.tripCurrentStatus);
//verifyToken
router.route('/tripDriverDetails').put(verifyToken, appCtrl.tripDriverDetails);
router.route('/riderFeedback').put(verifyToken, appCtrl.riderFeedback);
router.route('/driverFeedback').put(verifyToken, appCtrl.driverFeedback);
router.route('/clearStatus').put(verifyToken, appCtrl.clearStatus);
router.route('/riderTripHistory').post(verifyToken, appCtrl.riderTripHistory);
router.route('/driverTripHistory').post(verifyToken, appCtrl.driverTripHistory);
router.route('/myRatings').get(verifyToken, appCtrl.myRatings);
router.route('/feedbackLists').get(verifyToken, appCtrl.feedbackLists);
router.route('/myEarnings').post(verifyToken, appCtrl.myEarnings);
router.route('/pastTripDetail').put(verifyToken, appCtrl.pastTripDetail);
router.route('/pastTripDetailRider').put(verifyToken, appCtrl.pastTripDetailRider);
router.route('/validatePromo').put(verifyToken, appValidate.validatePromo, appCtrl.validatePromo);
router.route('/retryNoResponseRequestApp').put(verifyToken, appCtrl.retryNoResponseRequestApp);
router.route('/updateDropLocation').put(verifyToken, appCtrl.updateDropLocation);
router.route('/tripPaymentStatus').put(verifyToken, appCtrl.tripPaymentStatus);
// router.route('/upo').post(appCtrl.upo);

// Rentals
router.route('/requestRentalTaxi').post(verifyToken, appValidate.requestTaxi, appCtrl.requestRentalTaxi);

// router.route('/temp').post(appCtrl.temp);
// router.route('/jointest').post(appCtrl.jointest);
// router.route('/testWallet').post(appCtrl.testWallet);

//requestTaxi

//requestScheduleTaxi
router.route('/requestScheduleTaxi').post(verifyToken, appCtrl.requestScheduleTaxi);
// router.route('/tess').post(appCtrl.tess);
router.route('/riderUpcomingScheduleTaxi').get(verifyToken, appCtrl.riderUpcomingScheduleTaxi);
router.route('/userCancelScheduleTaxi').put(verifyToken, appCtrl.userCancelScheduleTaxi);
router.route('/driverUpcomingScheduleTaxi').get(verifyToken, appCtrl.driverUpcomingScheduleTaxi);
router.route('/acceptScheduleRequest').put(verifyToken, appCtrl.acceptScheduleRequest);
router.route('/driverCancelScheduleTaxi').put(verifyToken, appCtrl.driverCancelScheduleTaxi);

router.route('/cronas').get(appCtrl.cronas);
router.route('/getISO').post(appCtrl.getISO);

//requestScheduleTaxi

router.route('/driverBankTransactionsLedgerApp').get(verifyToken, driverCtrl.driverBankTransactionsLedgerApp);

//SafeRide
router.route('/checkSafeRideEligible').post(verifyToken, appCtrl.checkSafeRideEligible);
router.route('/requestSafeTaxi').post(verifyToken, appCtrl.requestSafeTaxi);
router.route('/safePayment').post(verifyToken, cpUpload, appCtrl.safePayment);

//SafeRide


//Stripe
router.route('/myBalance').get(stripeCtrl.myBalance);
router.route('/createCustomer').post(stripeCtrl.createCustomer);
router.route('/listCustomers').get(stripeCtrl.listCustomers);
router.route('/addAmount').post(stripeCtrl.addAmount);
router.route('/stripeRedirectLink').get(paymentCtrl.stripeRedirectLink); //https://stripe.com/connect/default/oauth/test?code={AUTHORIZATION_CODE}
// http://localhost:3002/api/stripeRedirectLink?code=AUTHORIZATION_CODE&state=userId

//BrainTree
router.route('/bt_client_token').get(paymentCtrl.get_bt_client_token);

//Paytm
router.route('/generate_paytm_checksum').post(paymentCtrl.generate_paytm_checksum);

//frimi
// router.route('/generateToken').post(paymentCtrl.generateAccessToken);
// router.route('/registerDirectDebit').post(paymentCtrl.registerDirectDebit);
// router.route('/directDebitPayment').post(paymentCtrl.directDebitPayment);
// router.route('/deRegisterDirectDebit').post(paymentCtrl.deRegisterDirectDebit);

//Wallet : Payment Rider
router.route('/addCard').put(verifyToken, paymentCtrl.addCard);
router.route('/chargeCard').put(stripeCtrl.chargeCard);
router.route('/addToMyWallet').put(verifyToken, appCtrl.addToMyWallet);
router.route('/myWalletHistory').get(verifyToken, appCtrl.myWalletHistory);
router.route('/myWalletCreditHistory').get(verifyToken, appCtrl.myWalletCreditHistory);
router.route('/myWalletDebitHistory').get(verifyToken, appCtrl.myWalletDebitHistory);
router.route('/myWallet').get(verifyToken, appCtrl.myWallet);

// router.route('/addDriverBank').put(verifyToken,stripeCtrl.myWallet); 

//Stripe

//Request 
router.route('/vehicletype').get(vehicletypeCtrl.getvehicleTypes).post(vehicletypeCtrl.addData);
router.route('/getServiceCityDetails').get(getServiceCityController.getServiceCity);

//Common
router.route('/countries').get(commonCtrl.getCountriesForApp);
router.route('/state/:id').get(commonCtrl.getStateForApp);
router.route('/city/:id').get(commonCtrl.getCityForApp);
router.route('/companies').get(commonCtrl.getCompanies);
router.route('/languages').get(commonCtrl.getLanguages);
router.route('/currency').get(commonCtrl.getCurrency);
router.route('/curnlang').get(commonCtrl.getCurnlang);
router.route('/commondata').get(commonCtrl.getCurnlang);
router.route('/carmake').get(commonCtrl.getCarMake);
router.route('/years').get(commonCtrl.getYears);
router.route('/carmakeandyear').get(commonCtrl.getCarMakeAndYear);
router.route('/companyDrivers/:id').get(driverCtrl.getCmpyDrivers);
router.route('/trips/').get(tripCtrl.getTripDetails);
router.route('/tripDetails/:id').get(tripCtrl.getATripDetails);
router.route('/aboutus').get(commonCtrl.sendAboutus);
router.route('/privacypolicy').get(commonCtrl.sendPrivacypolicy);
router.route('/tnc').get(commonCtrl.sendTnc);
router.route('/drivertnc').get(commonCtrl.sendDriverTnc);
router.route('/contactUs').post(verifyToken, commonCtrl.contactUs).put(verifyToken, commonCtrl.replyContactUs).get(verifyToken, commonCtrl.viewContactUsHistory);
router.route('/faqTemplate/:ifaqcategorytitle').get(commonCtrl.faqTemplate);

router.route('/logout').get(verifyToken, commonCtrl.logout);

router.route('/db').get(commonCtrl.getDbStatus);
router.route('/rental').post(appCtrl.getRentalPackage);

//Pay Package
router.route('/getpayPackage').get(verifyToken, driverPayPackageCtrl.getAllPackage);
router.route('/newDriverBankTransactions').post(verifyToken, mwVBTupload, driverCtrl.newDriverBankTransactions);
// router.route('/newDriverBankTransactions').post(verifyToken, driverCtrl.newDriverBankTransactions);

//Discounts
router.route('/getAvailableDiscounts').get(commonCtrl.getAvailableDiscounts);

//Frontend
router.route('/riderPushToken').put(verifyToken, ridersCtrl.riderPushToken);

//web view
router.route('/webView/helpCategory').get(cmsCtrl.listHelpCategory);
router.route('/webView/helpPage/:id').get(cmsCtrl.listHelpPage);

router.route('/temp').get(driverCtrl.temp);

router.route('/').get(companyCtrl.getCompany);
router.route('/listActiveCompany').get(companyCtrl.activeCompany);

export default router;
