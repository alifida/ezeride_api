import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const autoIncrement = require('mongoose-ai');
autoIncrement.initialize(mongoose);
const featuresSettings = require('../featuresSettings');   

var TripsSchema = new Schema({
	createdAt:{ type: Date, default: Date.now },
	tripno : { type: String }, 
	//tripno :String,
	requestFrom: { type: String, enum: ["app", "admin", "hotel"], default: "app" },
	requestId: { type: String, default: "" },
	triptype: { type: String, default: "daily" },//{ daily/rental/outstation }
	bookingType: { type: String, default: "rideNow" }, //{ rideNow/rideLater(schedule)/safeRide/hailRide }
	bookingFor: { type: String, default: "self" }, //{ self/others }
	notes: { type: String, default: "" },
	other :{
		ph: { type: String },
		phCode: { type: String },
		name: { type: String },
	},
	date: String,  //Formated Trip Start date time = D-M-YYYY h:mm a , 19-05-2018 08:00 AM;
	hotelid: { type: ObjectId, ref: 'hotels', default : null },
	cpy: String,
	cpyid: { type: ObjectId, ref: 'companydetails', default: null },
	dvr: String,
	dvrid: { type: ObjectId, ref: 'drivers', default: null },
	rid: String,
	ridid: { type: ObjectId, ref: 'riders', default: null },
	fare: { type: String , default: 0  },//total fare 
	vehicle: String,//Taxi name
	service: String,//service Id
	paymentSts: { type: String, default: "pending" },//Paid,pending
	paymentMode: { type: String }, //Cash,Digital  cash, card
	noofseats: { type: Number, default: 1 },
	packageId: { type: ObjectId, ref: 'rentalPackages', default: null },
	csp: { //Cost split up 
		base: { type: Number, default: 0   }, //Base Fare
		dist: { type: String },
		distfare: { type: Number },
		time: { type: String },
		timefare: { type: Number, default: 0  },
		comison: { type: Number, default: 0  },
		hotelcommision: { type: Number, default: 0  }, //commision amount
		promo: { type: String }, //If valid Code else 0
		promoamt: { type: Number, default: 0  },
		cost: { type: Number, default: 0  },
		conveyance: { type: Number, default: 0  },
		tax: { type: Number, default: 0  },
		companyAllowance: { type: Number, default: 0  },
		via: { type: String }, //Cash,Digital  cash, card
		taxPercentage: { type: Number, default: 0  },
		driverCancelFee: { type: Number, default: 0  },
		riderCancelFee: { type: Number, default: 0  },
		nightChargeApplied: { type: Boolean },
		isNight: { type: Boolean, default : false },
		isPeak: { type: Boolean, default: false },
		nightPer: { type: Number, default: 0 },
		peakPer: { type: Number, default: 0  },
	},

	dsp: { //details split up from cityLimitCalculation
		/*distanceKM: String,
		start: String, // pickup address	
		startcoords: { type: [Number] },
		
	    endStop:{type:Array},
	    endcoords:{type:Array},
		pickupCity: { type: String },*/
		 distanceKM: String,
        start: String, // pickup address
        end: String, // drop address
        startcoords: { type: [Number] },
        endcoords: { type: [Number] },
        pickupCity: { type: String },
	},

	acsp: { //Actual Cost split up
		base: { type: Number }, //Base Fare
		minFare: { type: Number }, //Base Fare
		dist: String,
		distfare: { type: Number },
		time: String, //Total trip time
		timefare: { type: Number  }, //timeFare if exists
		waitingCharge: { type: Number  }, //waitingCharge
		waitingTime: { type: String }, //waitingCharge
		comison: { type: Number  }, //Admin Commision
		hotelcommision: { type: Number, default: 4  },		
		promoamt: { type: Number  },
		bal: { type: Number  }, //bal to pay for this Trip
		detect: { type: Number}, //Promoamt Amount / Any From Admin to Rider as Offers = from Admin //this amount need to pay to driver
		actualcost: { type: Number  }, //total amount for this Ride = settle to Driver after Comision
		costBeforeDiscount: { type: Number }, 
		cost: { type: Number  }, //Paid By Rider = Combination of Stipe + Wallet + Cash - Promo
		via: String, //Cash,Digital  cash, card
		conveyanceKM: { type: Number  }, //In KM = KM traveled upto Pickup location
		conveyance: { type: Number   }, //In conveyance Amount (pickup amt)
		chId: { type: String }, //Charge Id for Digital
		safe: { type: Number  },
		oldBalance: { type: Number  }, //Old Bal if exists
		tax: { type: Number  },//Tax Amount
		taxPercentage: { type: Number  },  //tax Percentage
		totalFareWithOutOldBal: { type: Number  },
		fareAmtBeforeSurge: { type: Number  },
		fareType: { type: String }, //flatrate or percentage
		isNight: { type: Boolean, default: false },
		isPeak: { type: Boolean, default: false },
		nightPer: { type: Number },
		peakPer: { type: Number }, 
		walletdebt: { type: Number },
		carddebt: { type: Number },//All Digital trans
		outstanding: { type: Number },
		discountPercentage: { type: Number, default: 0 },
		discountName: { type: String, default : ""},
	},

	additionalFee: Array,

	adsp: { //Actual details split up
		start: { type: String },//Start Time
		end: { type: String },//End Time
		from: { type: String },
		to: { type: String },
		pLat: { type: Number },
		pLng: { type: Number },
		dLat: { type: Number },
		dLng: { type: Number },
		map: { type: String }
	},

	applyValues: {
		applyNightCharge: { type: Boolean, default: featuresSettings.applyValues.applyNightCharge },
		applyPeakCharge: { type: Boolean, default: featuresSettings.applyValues.applyPeakCharge },
		applyWaitingTime: { type: Boolean, default: featuresSettings.applyValues.applyWaitingTime },
		applyTax: { type: Boolean, default: featuresSettings.applyValues.applyTax },
		applyCommission: { type: Boolean, default: featuresSettings.applyValues.applyCommission },
		applyPickupCharge: { type: Boolean, default: featuresSettings.applyValues.applyPickupCharge },
	}, 

	estTime: String,

	status: { type: String, default: "open" },// noresponse,Cancelled,Finished,processing,accepted,
	tripOTP: [String],//[Start,End]

	review: { type: String, default: "" },
	// reqDvr: [String], //For BroadCast
	reqDvr: Array, //For One By One
	curReq: [Number],//For One By One

	needClear: { type: String, default: "yes" },
	riderfb: {  //Feadback from Rider to Driver
		rating: { type: String },
		cmts: { type: String }
	},
	driverfb: { //Feadback from Driver to Rider
		rating: { type: String },
		cmts: { type: String }
	},

	tripDT: { type: String, default: "" },//19-05-2018 08:00 AM
	utc: { type: String, default: "" },//  GMT+05:30
	tripFDT: { type: Date, default: Date.now },  //Trip Start date time =   2018-05-17T20:00:00+05:30 
	gmtTime: { type: String, default: "" }, //Sat, 19 May 2018 02:30:00 GMT
	scId: { type: ObjectId, ref: 'serviceavailablecities', default: null },
	scity: { type: String, default: "" },
	isMultiLocation : { type:Boolean, default:false},
	multiLocation : { type:Array, default:[]},

});

//Example if GMT-04:00
/* createdAt: "2019-05-21T07:42:11.150Z" = without - 4, actl time at 0.
date: "21-05-2019 03:42 am" = with -4 = Formated
gmtTime: "Tue, 21 May 2019 05:34:00 GMT" = with -4 = Formated to GMT
tripDT: "21-05-2019 03:42 am" = with 4 = Formated(same as date)
tripFDT: "2019-05-21T05:34:17.860Z" =  with 4 = in ISO Format
utc: "GMT-04:00" = -4 */

TripsSchema.plugin(autoIncrement.plugin, {
	model: 'Trips',
	field: 'tripno',
	startAt: 1000,
	incrementBy: 1
});

var Trips = mongoose.model('trips', TripsSchema);
module.exports = Trips;  

 