db.commondatas.insert(
    {
    "_id": "1",
    "datas": [
        {
            "id": "USD",
            "name": "USD"
        },
        {
            "id": "EUR",
            "name": "EUR"
        },
        {
            "id": "AUD",
            "name": "AUD"
        },
        {
            "id": "INR",
            "name": "INR"
        }
    ]
    }
    )

db.commondatas.insert(
    {
    "_id": "2",
    "datas": [
        {
            "id": "EN",
            "name": "English"
        },
        {
            "id": "FN",
            "name": "French"
        },
        {
            "id": "AR",
            "name": "Arabic"
        }
    ]
    } )

db.commondatas.insert(
    {
    "_id": "3",
    "datas": [
        {
            "id": "2018",
            "name": "2018"
        },
        {
            "id": "2017",
            "name": "2017"
        },
        {
            "id": "2016",
            "name": "2016"
        },
        {
            "id": "2015",
            "name": "2015"
        },
        {
            "id": "2014",
            "name": "2014"
        },
        {
            "id": "2013",
            "name": "2013"
        },
        {
            "id": "2012",
            "name": "2012"
        },
        {
            "id": "2011",
            "name": "2011"
        },
        {
            "id": "2010",
            "name": "2010"
        },
        {
            "id": "2009",
            "name": "2009"
        },
        {
            "id": "2008",
            "name": "2008"
        },
        {
            "id": "2007",
            "name": "2007"
        },
        {
            "id": "2006",
            "name": "2006"
        },
        {
            "id": "2005",
            "name": "2005"
        },
        {
            "id": "2004",
            "name": "2004"
        },
        {
            "id": "2003",
            "name": "2003"
        },
        {
            "id": "2002",
            "name": "2002"
        },
        {
            "id": "2001",
            "name": "2001"
        },
        {
            "id": "2000",
            "name": "2000"
        }
    ]
    } )
    
   db.commondatas.insert( {
    "_id": "4",
    "datas": [
        {
            "id": "1",
            "name": "Red",
            "code": "ff0000"
        },
        {
            "id": "2",
            "name": "Black",
            "code": "0"
        },
        {
            "id": "3",
            "name": "Cream/Butter",
            "code": "ffffcc"
        },
        {
            "id": "4",
            "name": "Green",
            "code": "00ff00"
        },
        {
            "id": "5",
            "name": "Blue",
            "code": "0000ff"
        },
        {
            "id": "6",
            "name": "Yellow",
            "code": "ffff00"
        },
        {
            "id": "7",
            "name": "Orange",
            "code": "ff3300"
        },
        {
            "id": "8",
            "name": "Pink",
            "code": "ffcccc"
        },
        {
            "id": "9",
            "name": "White",
            "code": "ffffff"
        },
        {
            "id": "10",
            "name": "Light Blue",
            "code": "00cccc"
        },
        {
            "id": "11",
            "name": "Light Green",
            "code": "99cc00"
        },
        {
            "id": "12",
            "name": "Ash",
            "code": "999999"
        },
        {
            "id": "13",
            "name": "Gold"
        },
        {
            "id": "14",
            "name": "Silver"
        },
        {
            "id": "15",
            "name": "Purple"
        },
        {
            "id": "16",
            "name": "Brown"
        },
        {
            "id": "17",
            "name": "Growen"
        },
        {
            "id": "18",
            "name": "GRAY"
        },
        {
            "id": "19",
            "name": "MAROON"
        }
    ]
    }
)

db.companydetails.insert(
{
    "email": "default@gmail.com",
    "pwd": "123456",
    "name": "Default",
    "phone": "1234567890",
    "cnty": "",
    "city": "",
    "state": "",
    "adln1": "Default",
    "adln2": "Default",
    "vat": "Default",
    "createdAt": "2019-01-03T13:40:28.119Z",
    "__v": 0
}
)

db.admins.insert({ 
  "hash": "dd1af803dc45f909be235ddc1b08050d5bdd85335d778f1a3a74e0cfc31c522ac50c2d7896501153e345ed685dd397d680985aa5580eea54f6559a9286de5bf2",
  "salt": "5732910bc6523360b7ae96d929c08f41",
  "fname": "Admin",
  "lname": "Taxi",
  "email": "abservetech.com@gmail.com",
  "phone": "00447535654319",
  "group": "superadmin",
  "createdAt": "2018-07-19T06:05:08.780Z",
  "__v": 0
})