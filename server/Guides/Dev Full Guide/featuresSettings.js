const featuresSettings = {
  "isCityWise": false,
  "isServiceAvailable": false,
  "isMultipleCompaniesAvailable": false,
  "isMultipleCompaniesDriversAvailable": false,
  "isCommisionToAdmin": false,
  "isDoubleChargeNeeded": false,
  "fareCalculationType": "normal",
  "gst": {
    "gstFareBreakPercentage": 85,
    "gstOnFareBreakPercentage1": 5,
    "gstOnFareBreakPercentage2": 18
  },
  "applyTravelFare": true,
  "applyValues": {
    "applyNightCharge": true,
    "applyPeakCharge": true,
    "applyWaitingTime": true,
    "applyTax": true,
    "applyCommission": true,
    "applyPickupCharge": true
  },
  "isUpdateDriverPerDayEarnings": false,
  "isETANeeded": false,
  "isRiderCancellationAmtApplicable": true,
  "isDriverCancellationAmtApplicable": false,
  "calculateDriverEarningAtEveryDay": false,
  "applyBlockOldCancellationAmt": true,
  "isDriverCreditModuleEnabled": true,
  "isDriverSubscriptionWorkWithTripConcept": true,
  "addBookingFeeToCommision": false,
  "driverPayouts": {
    "adminCommision": "driverWallet",
    "payoutType": "driverPrepaidWallet",
    "deductAmountFromDriverWallet": "commision",
    "driverCreditAmountAlertLimit": 100,
    "driverCreditAmountOfflineLimit": 0,
    "driverPayoutAmountLimitMax": 100,
    "driverStripeConnect": true,
    "driverStripeConnectCountry": "US",
    "driverStripeSplitPayout": true,
    "driverStripeSplitPayoutDirectlyAtEveryTripEnd": true,
    "driverDirectPayout": true
  },
  "riderWallet": true,
  "liveTaxiMeter": false,
  "applyAdditionalKMFareModel": false,
  "riderRechargeWalletInClientSide": true,
  "riderCard": true,
  "riderTripPaidInClientSide": false,
  "defaultPhoneCode": "+91",
  "defaultlang": "EN",
  "defaultcur": "USD",
  "defaultCountryId": "101",
  "defaultStateId": "35",
  "primarycur": "USD",
  "secondarycur": true,
  "secondarycurName": "INR",
  "secondarycurSymbol": "$",
  "conversionRate": 3,
  "roundOff": 2,
  "referalSettings": {
    "isRiderReferalCodeAvailable": "true",
    "riderReferalAmount": "100",
    "riderRefererAmount": 90,
    "isDriverReferalCodeAvailable": "true",
    "driverRefererAmount": 90,
    "driverReferalAmount": "100",
    "DriverTakenTripCount": 0,
  },
  "isCompanyPriorityDriverRequest": false,
  "isPromoCodeAvailable": true,
  "isOffersForRideAvailable": false,
  "passwordVerificationMethod": "sms",
  "passwordVerificationMethodForUser": "sms",
  "registerOTPVerificationMethod": "sms",
  "tripsAvailable": [
    "Daily",
    "Package",
    "Outstation"
  ],
  "hailTaxi": false,
  "shareTaxi": false,
  "socialLogin": false,
  "callMasking": false,
  "addFareWithServices": false,
  "payPackageTypes": [
    "commision",
    "subscription"
  ],
  "expirationNotificationBefore": 3,
  "addAdditionalFaresInTrip": false,
  "defaultPaymentMethod": "cash",
  "riderSignupBonus": false,
  "riderSignupBonusAmount": 0,
  "applyMandatoryDiscount": false,
  "discountsAvailable": [
    {
      "name": "Signup",
      "percentage": 5
    }
  ],
  "updateDriverPerDayOnlineTime": false,
  "getVehicleListAlongWithFeatures": false,
  "convertAllFareToNearbyFive": false,
  "convertAllFareToGivenMultipler": false,
  "multipler": 1,
  "dobMandatory": false,
  "redTaxiModel": false,
  "manualPickupChargeFromMTD": false,
  "addETAtoServicevehicles": false,
  "latestYearList": false,
  "latestYearCount": 5,
  "requestDriverWithInStateAllowed": false,
  "multipleZones": false,
  "driverDocumentExpiryReasons": [
    {
      key: "insuranceexp",
      value: "Insurance",
    }, {
      key: "licenceexp",
      value: "Licence",
    },
    {
      key: "passingexp",
      value: "Passing",
    }
  ],
  "taxiDocumentExpiryReasons": [
    {
      key: "permitDate",
      value: "Permit",
    }, {
      key: "insuranceDate",
      value: "Insurance",
    },
    {
      key: "registrationDate",
      value: "FitnessCertificate ",
    }
  ],
  "checkDropPoint": true,
  "languages": ['en', 'es'],
  "apiOptimisation": {
    "distanceMatrix": false
  },
  "driverCanAddMultipleVehicleCategory": false,
  "checkInactiveCon": true,
  "updateTripPaths": true,
  "locationUpdateAfter": {
    "daily": 1,
    "rental": 10,
    "outstation": 20,
  }
}; module.exports = featuresSettings
