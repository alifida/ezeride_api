
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var Riderid
  var triptype

    window.onload = function () {
        LoadMap();
    };
 
    var map;
    var marker;

    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(19.42847, -99.12766),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            SetMarker(19.42847, -99.12766);
    };

    function SetMarker(lat,lng) {
        //Remove previous Marker.
        if (marker != null) {
            marker.setMap(null);
        }
 
        //Set Marker on Map.
        var myLatlng = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon:"ic_standard.png"
            
        });
            var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
 //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();
        infoWindow.setContent("<div style = 'width:45px;min-height:10px'>" + "located" + "</div>");
        infoWindow.open(map, marker);
    };
    function openNav() {
    document.getElementById("mySidenav").style.width = "400px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

var Address = new function(){
  var path =  window.location.href;
//   var path=config.host
 // console.log(path)
  var id=path.split("#")
 Riderid=id[1]

  apiToCall(Riderid)

  
}

function apiToCall(id){
    var xhttp=new XMLHttpRequest();
    xhttp.onreadystatechange= function(){
      if(xhttp.readyState == 4 && xhttp.status== 200){
        let obj=xhttp.responseText
       // console.log(obj)
        let data=JSON.parse(obj)
        accessData(data)
       
      }
    }
    xhttp.open("get",config.EndApi+'test/shareMyLocation/'+id, true)
    xhttp.send()
}

function displayResultContent(result){
   // console.log(result)
    var DriverData=result.driver
    var RiderData=result.rider
    var TripDetails=result.tripDetails
  // console.log(RiderData)
    document.getElementById("driverName").innerHTML =DriverData.name
    document.getElementById("driveremail").innerHTML =DriverData.email
    document.getElementById("driverphone").innerHTML =DriverData.phone
    document.getElementById("drivercurService").innerHTML =DriverData.curService
    document.getElementById("Driverimage_inner_container").innerHTML ='<img src='+ DriverData.profile+' alt="" class="img-rounded img-responsive" />'

    document.getElementById("riderName").innerHTML =RiderData.name
    document.getElementById("rideremail").innerHTML =RiderData.email
    document.getElementById("riderphone").innerHTML =RiderData.phone
    document.getElementById("riderimage_inner_container").innerHTML ='<img src='+ RiderData.profile+' alt="" class="img-rounded img-responsive" />'
  
    document.getElementById("TripPickup").innerHTML ='<cite title="San Francisco, USA">'+TripDetails.pickupAt+'<i class="glyphicon glyphicon-map-marker"></i></cite>'
    document.getElementById("TripDropAt").innerHTML ='<cite title="San Francisco, USA">'+TripDetails.dropAt+'<i class="glyphicon glyphicon-map-marker"></i></cite>'
    document.getElementById("TripStartAt").innerHTML =TripDetails.startAt
    document.getElementById("TripEndAt").innerHTML =TripDetails.endAt
    document.getElementById("TripStatus").innerHTML =TripDetails.status

 
    document.getElementById("SupportEmail").innerHTML =result.supportEmail
    document.getElementById("Supportnumber").innerHTML =result.supportNo
}

function accessData(data){
    if(data.success == true){
       var triptype=data.result.tripid
       var RiderId=data.result.driver._id
        trackLocationFromFirebase(triptype,RiderId)
      displayResultContent(data.result)
    }
    else{
     
      document.getElementById("statusFalse").innerHTML ="False"
    }
  
  }

function trackLocationFromFirebase(Riderid,triptype){
    firebase.initializeApp(config.firebasekey);   
    var database = firebase.database()
    var baseNode = 'drivers_location/' + triptype + '/' + Riderid
    var locationRef = database.ref(baseNode); 
    locationRef.on('value', function(snapshot) {
        if (snapshot.exists()) {
      var SelectedDocs=snapshot.val()
      var lati=SelectedDocs.l[0]
      var long=SelectedDocs.l[1]
      SetMarker(lati.toString(), long.toString())
        }
        else{
           console.log("false")
           document.getElementById("statusFalse").innerHTML ="False"
        }
  })

}
  
