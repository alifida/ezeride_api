import mongoose from 'mongoose';
const _ = require('lodash');

//import models
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';
import * as GFunctions from './functions';
import { noDriverFoundSMS } from './smsGateway';
import { findAndSendFCMToDriver } from './app';

var firebase = require('firebase');
var config = require('../config');

const onebyoneConfig = config.onebyoneConfig;

//ONEBYONE Flow
var distance = require('google-distance-matrix');
const featuresSettings = require('../featuresSettings');

//Find Drivers
export const findNearbyDriversAndSendRequest = async (tripdata, userreq, userid, pickupLng, pickupLat, serviceType) => {
  var requestRadius = config.requestRadius;
  if (tripdata.triptype == 'rental') requestRadius = config.rentalRequestRadius ? config.rentalRequestRadius : config.requestRadius;
  if (tripdata.triptype == 'outstation') requestRadius = config.outstationRequestRadius ? config.outstationRequestRadius : config.requestRadius;
  var neededService = serviceType;
  userreq.pickupLng = pickupLng;
  userreq.pickupLat = pickupLat;

  // var driverFind = {
  //   coords: {
  //     $geoWithin: {
  //       $centerSphere: [[parseFloat(pickupLng), parseFloat(pickupLat)],
  //       requestRadius / 3963.2]
  //     },
  //   }, online: 1, 'curStatus': { $in: ['free'] }, curService: neededService //curStatus : 'free' or 'requested'
  // };

  var maxDistanceInMeter = Number(requestRadius) * 1000;
  var driverFind = {
    'online': true,
    'curStatus': { $in: ['free'] },
    'curService': neededService
  };

  if (featuresSettings.driverCanAddMultipleVehicleCategory) {
    driverFind['curService'] = { $in: [neededService] };
  }

  if (featuresSettings.getVehicleListAlongWithFeatures) {
    if (userreq.features) {
      driverFind["taxis.vehicletype"] = neededService;
      driverFind["taxis.feature"] = { "$all": userreq.features };
    }
  }

  if (featuresSettings.requestDriverWithInStateAllowed) {
    if (tripdata.isDriverAllowedOtherStates == true || tripdata.isDriverAllowedOtherStates == 'true') {
      driverFind['isDriverAllowedOtherStates'] = tripdata.isDriverAllowedOtherStates ? tripdata.isDriverAllowedOtherStates : false;
    }
  }

  if (featuresSettings.redTaxiModel) {
    driverFind["taxis.vehicletype"] = neededService;
    if (tripdata.triptype == 'rental') {
      driverFind["taxis.isRental"] = true;
    } else if (tripdata.triptype == 'outstation') {
      driverFind["taxis.isOutstation"] = true;
    } else if (tripdata.triptype == 'daily') {
      driverFind["taxis.isDaily"] = true;
    }
  }

  if (userreq.driverAssignmentType == 'manual-assign') {
    driverFind = {
      _id: mongoose.Types.ObjectId(userreq.driverId)
    }
  }

  var driversNeedToPickFromSurrounding = config.driversNeedToPickFromSurrounding;
  if (onebyoneConfig.enable) {
    //Increment count, skip not in previous called Drivers onebyoneConfig TODO
    driversNeedToPickFromSurrounding = onebyoneConfig.incrementSteps;
  }


  // Driver.find(
  //   driverFind 
  // ).limit(config.driversNeedToPickFromSurrounding).exec((err, driverdata) => {
  //   // ).exec((err, driverdata) => { //50 Enough
  Driver.aggregate([
    {
      "$geoNear": {
        "near": {
          "type": "Point",
          "coordinates": [parseFloat(pickupLng), parseFloat(pickupLat)]
        },
        "maxDistance": maxDistanceInMeter,
        "spherical": true,
        "distanceField": "distance",
        // "distanceMultiplier": 0.000621371
      }
    },
    {
      '$match': driverFind
    }
  ]).limit(config.driversNeedToPickFromSurrounding).exec((err, driverdata) => {
    if (err) {
      GFunctions.notifyRider(userid, "No Driver Found", tripdata._id); //Error on Server
    } else {
      if (!driverdata.length) {
        noDriverFoundSMS(tripdata.requestFrom, tripdata.ridid, tripdata.requestId);
        GFunctions.notifyRider(userid, "No Driver Found", tripdata._id);
        // console.log('NDF')
      } else {
        filterNSendOBORequestToDrivers(tripdata, userreq, driverdata, userid, pickupLng, pickupLat);
        clearTheUserRequestAfterSomeTime(tripdata._id, userid, tripdata.requestId);
      }
    }
  });
}

function clearTheUserRequestAfterSomeTime(tripId, userid, adminId = '') {
  setTimeout(function () {
    needToCancelRequest(tripId, userid, adminId);
  }
    , config.userCancelTime); //30000 = 30 sec
}

//Need To Cancel Request
function needToCancelRequest(tripId, userid, adminId) {
  Trips.findOne({ _id: tripId, needClear: 'yes' }, { _id: 1, tripno: 1, ridid: 1 }, function (err, docs) {
    if (err) {
    } else if (!docs) {
      // console.log(1) 
    } else {
      clearTheTripOBOFlow(tripId, adminId);
      GFunctions.updateRiderFbStatus(userid, "canceled", 0);
    }
  })
}

function filterNSendOBORequestToDrivers(tripdata, userreq, driverdata, userid, pickupLng, pickupLat) {
  if (featuresSettings.apiOptimisation && featuresSettings.apiOptimisation.distanceMatrix) {
    var sortedDistanceArray = addDocIdAndGetOnlyDriversArray(driverdata); //Merging In Driver and Geo
    updateTaxiWithFoundDrivers(sortedDistanceArray, tripdata._id, userid);
  } else {
    var convertedLatLon = convertCordsToGDMFormat(driverdata);
    var originsPoints = parseFloat(pickupLat) + "," + parseFloat(pickupLng);
    originsPoints = originsPoints.toString();
    var origins = [originsPoints];
    var destinations = convertedLatLon;
    distance.key(config.googleApi);
    distance.units('metric');
    distance.mode('driving');
    // console.log(driverdata);
    distance.matrix(origins, destinations, function (err, distances) {
      if (err) { console.log('GGMErr NDF', err) }
      else if (distances.status == 'OK') {
        var resOutput = distances.rows[0].elements;
        var distanceArray = addDocIdAndGetOnlyDistanceArry(driverdata, resOutput); //Merging In Driver and Geo
        if (featuresSettings.isCompanyPriorityDriverRequest == true) {
          var sortedDistanceArray = sortFunc(distanceArray); //In Meters 
        } else {
          var sortedDistanceArray = distanceArray.sort(dynamicSort("distVal")); //In Meters
        }

        updateTaxiWithFoundDrivers(sortedDistanceArray, tripdata._id, userid);
      }
      else {
        console.log('GGMErr NDF in Land')
      }
    })
  }
}

/**
 * Log Found Drivers and No of Drivers in Trip Doc
 * @param {*} requestedDrivers 
 * @param {*} requestId 
 * @param {*} userid 
 */
function updateTaxiWithFoundDrivers(requestedDrivers, requestId, userid, reqCounter = 1) {
  var curReqAry = [requestedDrivers.length, 0];
  var update = {
    reqDvr: requestedDrivers,
    curReq: curReqAry,
    reqCounter: reqCounter
  };
  Trips.findOneAndUpdate({ _id: requestId }, update, { new: true }, (err, doc) => {
    if (err) {
      console.log("updateTaxiStatus", err);
    }
    else {
      // console.log('updateTaxiWithFoundDrivers',' ok')
      callTheOBOLoop(requestId);//Let start OBO loop for this Trip
    }
  })
}

/**
 * callTheOBOLoop : Will process OBO Req if needed
 * @param {*} trip_id 
 */
export const callTheOBOLoop = (trip_id) => {
  // console.log('In callTheOBOLoop');
  //Loop untill needClear : 'no' 
  Trips.findOne({ _id: trip_id, needClear: 'yes' }, function (err, docs) {
    if (err) { console.log("NTF") }
    if (docs) {

      //Find Current Driver 
      var curReqDriverIndex = docs.curReq[1];
      var maxIndex = docs.curReq[0];

      var allDriversAvail = docs.reqDvr;

      if (onebyoneConfig.enable) {
        if (Number(docs.reqCounter - 1) >= onebyoneConfig.incrementTimes) {
          var isNotCalledDriversExists = allDriversAvail.filter(driver => driver.called == 0);
          if (!isNotCalledDriversExists.length) {
            clearTheTripOBOFlow(trip_id);
            GFunctions.notifyRider(docs.ridid, "No Driver Found", trip_id); // Run out off Driver
          }
          return false;
        } //We itrated required times
        var notCalledDrivers = allDriversAvail.filter(driver => driver.called == 0);
        var curDrivers = [];
        var perRoundDrivers = onebyoneConfig.incrementSteps;
        for (var i = 0; i < perRoundDrivers; i++) {
          var cur = notCalledDrivers[i];
          if (cur && cur.called == 0) curDrivers.push(cur);
          if (curDrivers.length == perRoundDrivers) break;
        }
        if (curDrivers.length) { //Free drivers found
          curDrivers.forEach(element => {
            sendRequestToDriversIfFree(docs, element.drvId);
          });
        } else {
          //Is next iteration needed ? //onebyoneConfig TODO call findNearbyDriversAndSendRequest
          clearTheTripOBOFlow(trip_id);
          GFunctions.notifyRider(docs.ridid, "No Driver Found", trip_id); // Run out off Driver
        }
      }
      //onebyoneConfig just call one by one
      else {
        var obj = allDriversAvail.find(function (obj) { return obj.called === 0; });
        if (obj) {
          sendRequestToDriversIfFree(docs, obj.drvId);
          // console.log("INOBO", obj.drvId);
        } else {
          clearTheTripOBOFlow(trip_id);
          GFunctions.notifyRider(docs.ridid, "No Driver Found", trip_id); // Run out off Driver
        }
      }

    }
  });
}

function updateCurIndexToTrip(trip_id, curReqDriverIndex, maxIndex) {
  var curReqAry = [maxIndex, curReqDriverIndex];
  Trips.findByIdAndUpdate(trip_id, {
    'curReq': curReqAry
  }, { 'new': true },
    function (err, doc) {
      if (err) { console.log('updateCurIndexToTrip', err); }
      // console.log('updateCurIndexToTrip', ' OK');
    }
  );
}

function sendRequestToDriversIfFree(docs, curReqDriverId) {
  Driver.findById(curReqDriverId,
    function (err, doc) {
      if (err) { updateAsThisDriveriSCalled(docs, curReqDriverId, 1); }
      var curStatus = doc.curStatus;
      if (curStatus == "free") {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 1);
      } else if (curStatus == "requested") {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 0);
      } else {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 1);
      }
    }
  );

}

function updateAsThisDriveriSCalled(tripDoc, driverid, callStatus = 1) {
  Trips.update(
    { _id: tripDoc._id, 'reqDvr.drvId': mongoose.Types.ObjectId(driverid) },
    {
      $set: {
        'reqDvr.$.called': callStatus
      }
    },
    { new: true },
    function (err, doc) {
      if (err) { console.log('err', err) }
      if (callStatus) {
        sendRequestToDrivers(tripDoc, driverid);
      } else {
        callTheOBOLoop(tripDoc._id);
      }
    }
  )

}


function updateAsThisDriverIsErrAndCallOBO(tripDoc, driverid) {
  Trips.update(
    { _id: tripDoc._id, 'reqDvr.drvId': mongoose.Types.ObjectId(driverid) },
    {
      $set: {
        'reqDvr.$.called': 1
      }
    },
    { new: true },
    function (err, doc) {
      if (err) { console.log('err', err) }
      console.log('doc', doc)
    }
  )
}

//Update fb tripDoc hsa limited values
function sendRequestToDrivers(tripDoc, curReqDriverId) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");
  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    request: {
      totalKM: tripDoc.dsp.distanceKM ? tripDoc.dsp.distanceKM : 'NA',
      totalFare: tripDoc.fare ? tripDoc.fare : 'NA',
      drop_address: tripDoc.dsp.end ? tripDoc.dsp.end : 'NA',
      etd: tripDoc.estTime ? tripDoc.estTime : 'NA',
      picku_address: tripDoc.dsp.start ? tripDoc.dsp.start : 'NA',
      request_id: tripDoc._id,
      status: "1",
      datetime: tripDoc.tripDT ? tripDoc.tripDT : '0',
      request_type: tripDoc.bookingType ? tripDoc.bookingType : 'rideNow',
      triptype: tripDoc.triptype ? tripDoc.triptype : 'daily',
      review: "Taxi Request",
      request_no: "0"
    }
  };

  var child = (curReqDriverId).toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData, function (error) {
    if (error) { } else {
      // Send FCM
      // findAndSendFCMToDriver(child, "New Request");
      updateDriverReqStatusINMongo(curReqDriverId, tripDoc._id);
      // findAndSendFCMToDriver(curReqDriverId,'Received New Trip Request.');
    }
  });

}

function updateDriverReqStatusINMongo(driverid, tripId) {
  Driver.findByIdAndUpdate(driverid, {
    'curStatus': 'requested',
    // 'curTrip': tripId,
  }, { 'new': true },
    function (err, doc) {
      if (err) { }
      // findAndSendFCMToDriver(child, "New Request"); //Only
      clearDriverTaxiRequest(driverid, tripId);
    }
  );
}

function clearDriverTaxiRequest(driverid, tripId) {
  setTimeout(function () {
    needToResetDriver(driverid, tripId);
  }
    , config.requestTime); //30000 = 30 sec
}

function clearMyTripStatusFB(driverid, tripId) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");

  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    request: {
      totalKM: "0",
      totalFare: "0",
      drop_address: "0",
      etd: "0",
      picku_address: "0",
      request_id: "0",
      status: "0",
      datetime: "0",
      request_type: "0",
      review: "Time Out",
      request_no: "0"
    }
  };

  var child = (driverid).toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData, function (error) {
    if (error) { } else { }
  });
}

export const needToResetDriver = (driverid, tripId) => {
  Driver.findOne({ _id: driverid, 'curStatus': 'requested' }, {},
    // Driver.findOne({ _id: driverid, 'curStatus': 'requested', 'curTrip': tripId }, {}, //Use this if Timeout error happens for Previous Rides
    function (err, doc) {
      if (err) { }
      if (!doc) { }
      if (doc) {
        clearMyTripStatusFB(driverid, tripId);
        // updateAsThisDriveriSDeclined(tripId, driverid);
        changeMyTripStatusMongo(driverid, 'free');
        callTheOBOLoop(tripId);

      }
    }
  );
}

export const needToResetDeclinedDriver = (driverid, tripId) => {
  Driver.findOne({ _id: driverid, 'curStatus': 'requested' }, {},
    function (err, doc) {
      if (err) { }
      if (!doc) { }
      if (doc) {
        updateAsThisDriveriSDeclined(tripId, driverid);
        changeMyTripStatusMongo(driverid, 'free');
        callTheOBOLoop(tripId);
      }
    }
  );
}

function updateAsThisDriveriSDeclined(tripId, driverid) {
  Trips.update(
    { _id: tripId, 'reqDvr.drvId': mongoose.Types.ObjectId(driverid) },
    {
      $set: {
        'reqDvr.$.called': 2
      }
    },
    { new: true },
    function (err, doc) {
      if (err) { console.log('err', err) }
      console.log('doc', doc)
    }
  )
}

function changeMyTripStatusMongo(driverid, msg = "free") {
  Driver.findByIdAndUpdate(driverid, {
    'curStatus': msg
  }, { 'new': true },
    function (err, doc) {
      if (err) { }
    }
  );
}


//Helper OBOR
/**
 * convertCordsToGDMFormat
 * @param {*} docs 
 */
function convertCordsToGDMFormat(docs) {
  var resultDoc = docs.map(function (items) {
    var destinations = "";
    var tmpDoc = items.coords;
    destinations = tmpDoc[1] + "," + tmpDoc[0];
    return destinations;
  });
  return resultDoc;
}

/**
 * addDocIdAndGetOnlyDistanceArry
 * @param {*} docs 
 * @param {*} GDMop 
 */
function addDocIdAndGetOnlyDistanceArry(docs, GDMop) {
  var totalArray = docs.length;
  var GMDistAry = [];
  for (let i = 0; i < totalArray; i++) {
    let isDistOk = GDMop[i].status;
    if (isDistOk == 'OK') {
      let tempObj = {};
      let distBtPickAndDrop = GDMop[i].distance.value;
      if (Number(distBtPickAndDrop) <= Number(config.maxDistBtRiderAndDriver)) { //Only if Distance is lesser than max
        tempObj['drvId'] = docs[i]._id;
        tempObj['called'] = 0;
        //Also Limit only 10 Drivers @TODO
        tempObj['distVal'] = GDMop[i].distance.value;
        if (featuresSettings.isCompanyPriorityDriverRequest == true) {
          if (docs.isIndividual == false) {
            tempObj['company'] = 1;
          } else {
            tempObj['company'] = 0;
          }
        }
        GMDistAry.push(tempObj);
      }
    }
  }
  return GMDistAry;
}

/**
 * addDocIdAndGetOnlyDriversArray
 * @param {*} docs 
 * @param {*} GDMop 
 */
function addDocIdAndGetOnlyDriversArray(docs, GDMop) {
  var totalArray = docs.length;
  var GMDistAry = [];
  for (let i = 0; i < totalArray; i++) {
    let tempObj = {};
    tempObj['drvId'] = docs[i]._id;
    tempObj['called'] = 0;
    // tempObj['distVal'] = 0;
    tempObj['distVal'] = Number((docs[i].distance).toFixed(2));
    console.log(tempObj);
    GMDistAry.push(tempObj);
  }
  return GMDistAry;
}

/**
 * dynamicSort
 * @param {*} property 
 */
function dynamicSort(property) {
  var sortOrder = 1;
  if (property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    return result * sortOrder;
  }
}

/**
 * sortFunc
 * @param {*} property 
 */
function sortFunc(driverArr) {
  var distanceBased = driverArr.sort(function (a, b) {
    if (a.distVal > b.distVal) {
      return -1;
    } else {
      return 1
    }
  });

  var companyBased = distanceBased.sort(function (a, b) {
    if (a.company > b.company) {
      return -1;
    } else {
      return 1
    }
  });
  return companyBased;
}

//ONEBYONE TEST END


//Convert String To ID
export const convertSTO = (req, res) => {
  Trips.find({}).skip(0).exec((err, tripvalue) => {
    if (err) { }
    //var m=0;
    for (var i = 0; i < tripvalue.length; i++) {
      var driverId = tripvalue[i].dvrid;
      var tripID = tripvalue[i]._id;
      removeFromDoc(tripID, driverId);
    }
  })
}


function removeFromDoc(tripID, driverId) {
  Trips.update({ _id: tripID }, { $unset: { dvrid: 1 } }).exec((err, data) => {
    if (err) { }
    addToFromDoc(tripID, driverId);
  });
}

function addToFromDoc(tripID, driverId) {
  Trips.update({ _id: tripID }, { $set: { "dvrid": driverId } }).exec((err, data) => {
    if (err) { }
    console.log("Success 1");
  })
}

//Cancel from Rider side
export const cancelOBOTaxiRequest = async (driverdata, requestId, msg = "User Cancelled") => {
  changeMyTripStatusMongo(driverId, 'free');//clearing it first to avoid set timeout calling

  clearTheTripOBOFlow(requestId);

  if (onebyoneConfig.enable) {
    //TODO ? why clear all ? if already request stoped to him in 1st run
    driverdata.forEach(element => {
      changeMyTripStatusMongo(element.drvId, 'free');//clearing it first to avoid set timeout calling
      clearTheTripOBOFlowForDriver(element.drvId);
    })
  } else {
    //Find Current Req Driver
    var driverId = getLastCalledDriver(driverdata);
    if (driverId) {
      changeMyTripStatusMongo(driverId, 'free');//clearing it first to avoid set timeout calling
      clearTheTripOBOFlowForDriver(driverId);
    }
  }

}

export const clearTheTripOBOFlow = async (requestId, adminId) => {
  var update = {
    needClear: "no",
    status: "noresponse",
  }
  Trips.findOneAndUpdate({ _id: requestId }, update, { new: true }, (err, doc) => {
    if (err) { } else {
      if (!doc.dvrid) {
        // console.log('cancelOBOTaxiRequestmk3', doc.dvrid);
        noDriverFoundSMS(doc.requestFrom, doc.ridid, adminId);
      }
    }
  })
}

function getLastCalledDriver(driverdata) {
  let list = _.filter(driverdata, item => item.called === 1);
  var last = list.slice(-1).pop();
  if (last) {
    return last.drvId;
  } else {
    return false;
  }
}

/**
 * Clearing Driver in Fb and Mongo to free him
 * @param {*} req 
 * @param {*} res 
 */
export const clearTheTripOBOFlowForDriver = (driverId, msg = "User Cancelled") => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");
  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    request: {
      totalKM: "0",
      totalFare: "0",
      drop_address: "0",
      etd: "0",
      picku_address: "0",
      request_no: "0",
      request_id: "0",
      status: "0",
      datetime: "0",
      request_type: "0",
      review: msg
    }
  };
  var child = driverId.toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData, function (error) {
    if (error) { } else { }
  });
}

