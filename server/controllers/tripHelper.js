import mongoose from 'mongoose';

// RFCNG

const invNum = require('invoice-number');

//import models
import Vehicle from '../models/vehicletype.model';
import Rider from '../models/rider.model';
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';
import Promo from '../models/promo.model';
import DriverPayment from '../models/driverpayment.model';
import Wallet from '../models/wallet.model';
import Schedule from '../models/schedules.model';
import ShareRides from '../models/shares.model';
import * as GFunctions from './functions';
import { sendEmail } from './mailGateway';
const config = require('../config');

/**
 * [validatePromoAndReturnAmt Only Validate and return amount no Db logging]
 * @param  {[type]} promoCode [description]
 * @return {[type]}           [Amount float or 0]
 */
export const validatePromoAndReturnAmt = (promoCode) => {
  return new Promise(function (resolve, reject) {
    if (promoCode) {
      resolve(1);
    } else {
      resolve(0);
    }
  })
}

/**
 * getCommisionAmt 
 * @param  {Number} comison       [percentage]
 * @param  {[type]} totalAmtToPay [total amt]
 * @return {[float]}              [Commision amt]
 */
export const getCommisionAmt = (comison = 0, totalAmtToPay) => {
  var comisonamt = parseFloat((parseFloat(totalAmtToPay) * parseFloat(comison)) / 100).toFixed(2);
  if (comisonamt) {
    return comisonamt;
  } else {
    return 0;
  }
}


export const sendTripReceipt = (tripId = '', mailTo = '') => {
  Trips.findById(tripId, function (err, doc) {
    if (err) { console.log(err) }
    if (!doc) { }
    else {
      /* var data = {
        amountpaid: doc.fare, datepaid: doc.date, paymentmethod: doc.csp.via,
        chargedescription: 'Total Fare for Trip ' + doc.tripno, tripno: doc.tripno,
        triptime: doc.date, ridername: doc.rid,
        from: doc.adsp.from ? doc.adsp.from : doc.dsp.start,
        tripto: doc.adsp.to ? doc.adsp.to : doc.dsp.end,
        accessfee: doc.acsp.tax ? doc.acsp.tax : 0,
        distanceKM: doc.acsp.dist + config.distanceSymbol,
        minfare: doc.acsp.distfare,
        distancefare: doc.cost,
        starttime: doc.adsp.start ? doc.adsp.start : '',
        endtime: doc.adsp.end ? doc.adsp.end : '',
        tripdistance: doc.estTime,
        tripvehicle: doc.vehicle,
        discountName: doc.acsp.discountName ? doc.acsp.discountName : '',
        detect: doc.adsp.detect ? doc.adsp.detect : 0,
      }; */

      var data = {
        amountpaid: doc.fare, datepaid: doc.date, paymentmethod: doc.csp.via,
        chargedescription: 'Total Fare for Trip ' + doc.tripno, tripno: doc.tripno,
        triptime: doc.date, ridername: doc.rid,
        from: doc.adsp.from ? doc.adsp.from : doc.dsp.start,
        tripto: doc.adsp.to ? doc.adsp.to : doc.dsp.end,
        accessfee: doc.acsp.tax ? doc.acsp.tax : 0,
        distanceKM: doc.acsp.dist + config.distanceSymbol,
        minfare: doc.acsp.distfare,
        distancefare: doc.cost,
        starttime: doc.adsp.start ? doc.adsp.start : '',
        endtime: doc.adsp.end ? doc.adsp.end : '',
        tripdistance: doc.estTime,
        tripvehicle: doc.vehicle,
        discountName: doc.acsp.discountName ? doc.acsp.discountName : '',
        detect: doc.adsp.detect ? doc.adsp.detect : 0,
        currency: config.currencySymbol,
        appname: config.appname,
        time: doc.csp.time,
        detect: doc.acsp.detect,
        dist: doc.csp.dist,
        Percentage: doc.acsp.discountPercentage,
        waiting: doc.acsp.waitingTime,
        Charge: doc.acsp.waitingCharge,
        minFare: doc.acsp.minFare,
        link: config.frontendurl,
        fare: doc.csp.cost,
      };

      sendEmail(mailTo, data, 'TripInvoice');
    }
  })
}

export const sendTripGSTReceipt = (tripId = '', mailTo = '') => {
  Trips.findById(tripId, function (err, doc) {
    if (err) { console.log(err) }
    if (!doc) { }
    else {
      var data = {
        finalamt: doc.acsp.cost,
        datepaid: doc.date,
        paymentmethod: doc.csp.via,
        chargedescription: 'Total Fare for Trip ' + doc.tripno,
        tripno: doc.tripno,
        triptime: doc.date,
        ridername: doc.rid,
        from: doc.adsp.from,
        tripto: doc.adsp.to,
        accessfee: doc.acsp.tax ? doc.acsp.tax : 0,
        distanceKM: doc.acsp.dist,
        distancefare: doc.acsp.distfare,

        /* //Indian GST
        fee1: (doc.acsp.fare1).toFixed(2),
        cgstperentage1: parseFloat(doc.acsp.taxper1 / 2).toFixed(2),
        cgst1: parseFloat(doc.acsp.tax1 / 2).toFixed(2),
        sgstperentage1: parseFloat( doc.acsp.taxper1 / 2 ).toFixed(2),
        sgst1: parseFloat(doc.acsp.tax1 / 2).toFixed(2),
        subtotal1: (Number(doc.acsp.fare1) + Number(doc.acsp.tax1)).toFixed(2) , 
        fee2: (doc.acsp.fare2).toFixed(2),
        cgstperentage2: parseFloat(doc.acsp.taxper2 / 2).toFixed(2),
        cgst2: parseFloat(doc.acsp.tax2 / 2).toFixed(2),
        sgstperentage2: parseFloat(doc.acsp.taxper2 / 2).toFixed(2),
        sgst2: parseFloat(doc.acsp.tax2 / 2).toFixed(2),
        convtotal:  (Number(doc.acsp.fare2) + Number(doc.acsp.tax2)).toFixed(2) , 
        finalamt: (doc.acsp.cost).toFixed(2),  */

        //V2
        fee1: doc.acsp.fee1,
        cgstperentage1: doc.acsp.cgstperentage1,
        cgst1: doc.acsp.cgst1,
        sgstperentage1: doc.acsp.sgstperentage1,
        sgst1: doc.acsp.sgst1,
        subtotal1: doc.acsp.subtotal1,
        fee2: doc.acsp.fee2,
        cgstperentage2: doc.acsp.cgstperentage2,
        cgst2: doc.acsp.cgst2,
        sgstperentage2: doc.acsp.sgstperentage2,
        sgst2: doc.acsp.sgst2,
        convtotal: doc.acsp.convtotal,
        finalamt: doc.acsp.finalamt,

      };

      sendEmail(mailTo, data, 'TripGSTTaxInvoice');
    }
  })
}

function getindianGSTSplits(params) {

}

export const getTripNo = async () => {
  let tripno = 10001; //default first trip number

  /*  let tripDoc = await Trips.findOne({},{'tripno':1},{ "sort": { "createdAt": -1 }});
   if(tripDoc){
     tripno = invNum.next(tripDoc.tripno)
   } 
   return tripno.toString(); */

  let tripDoc = await Trips.count();
  if (tripDoc) tripno = tripDoc;
  tripno = tripno + 1;
  var tripnoprefix = GFunctions.getISOTodayDateForTripPrefix();

  return tripnoprefix + tripno;
}