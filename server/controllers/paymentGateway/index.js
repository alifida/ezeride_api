const config = require('../../config');
const featuresSettings = require('../../featuresSettings');
import logger from '../../helpers/logger';

import DriverBank from '../../models/driverBank.model';
import Driver from '../../models/driver.model';
import Rider from '../../models/rider.model';
import Wallet from '../../models/wallet.model';
import mongoose from 'mongoose';
import * as GFunctions from '../functions';
import { updateDriverWallet } from '../driverBank';
import DriverWallet from '../../models/driverWallet.model';


switch (config.paymentGateway.paymentGatewayName) {
  case 'stripe':
    var paymentGatewayCtrlName = './stripe';
    break;
  case 'braintree':
    var paymentGatewayCtrlName = './braintree';
    break;
  case 'paytm':
    var paymentGatewayCtrlName = './paytm';
    break;
  case 'paystack':
    var paymentGatewayCtrlName = './paystack';
    break;
  default:
    var paymentGatewayCtrlName = './stripe';
    break;
}

const paymentGatewayFunctions = require(paymentGatewayCtrlName);

export const getPUsers = (req, res) => {
  paymentGatewayFunctions.listCustomers(req, res);
}

export const addCard = async (req, res) => {
  if (featuresSettings.riderCard && featuresSettings.riderRechargeWalletInClientSide == false) {
    try {
      var desc = "Rider - " + req.userId;
      var resObj = await paymentGatewayFunctions.findNAddCardTOUser(req.body.cardToken, desc);
      if (resObj.success) {

        if (config.paymentGateway.paymentGatewayName == "stripe") {
          var last4 = '';
          if (resObj.data.sources.data.length > 0) {
            last4 = resObj.data.sources.data[0].last4;
          }
          var cardDetails = {
            id: resObj.data.id,
            currency: resObj.data.currency ? resObj.data.currency : featuresSettings.defaultcur,
            last4: last4
          };
        }

        else if (config.paymentGateway.paymentGatewayName == "paystack") {
          var cardDetails = {
            id: resObj.data.authCode,
            amount: resObj.data.amount,
            currency: featuresSettings.defaultcur,
            last4: resObj.data.last4
          };
        }

        addCardToMongo(req.userId, cardDetails, res, req);
      }
    } catch (error) {
      logger.error(error);
      return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_CARD") + ' :' + error.message });
    }
  } else {
    return res.json({ 'success': true, 'message': req.i18n.__("THIS_FEATURE_NOT_AVAILABLE") });
  }
}

export const transferAmountNRecharge = async (res, cardId, desc, cur = "usd", amt = 0, userId, walletId, userData, req) => {
  try {
    var resObj = await paymentGatewayFunctions.transferAmountNRechargeUser(cardId, desc, cur, amt, userData.email);
    if (resObj.success) {
      var chargeDetails = {
        id: resObj.data.id,
        amount: resObj.data.amount, // (*100 in stripe)
      };
      RechargeMyWallet(res, chargeDetails, userId, walletId, req);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_AMOUNT") });
  }
}

export const transferAmountToWallet = async (req, res) => {
  var cardDetails = {
    id: '',
    last4: '',
    currency: ''
  };
  try {
    if (config.paymentGateway.paymentGatewayName == 'paypal') {
      req.body.amount = parseFloat(req.body.amount * 100).toFixed(2);
      cardDetails.id = 'paypal';
      cardDetails.currency = req.body.currency;
    }

    if (config.paymentGateway.paymentGatewayName == 'paytm') {
      req.body.amount = parseFloat(req.body.amount * 100).toFixed(2);
      cardDetails.id = 'paytm';
      cardDetails.currency = req.body.currency;
    }

    var chargeDetails = {
      id: req.body.id,
      amount: req.body.amount, // (*100 in stripe)
      currency: req.body.currency
    };
    console.log(chargeDetails,"chargeDetails")
    createNTransferAmountToWallet(res, chargeDetails, cardDetails, req.userId);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_AMOUNT") });
  }
}


export const chargeExistingUserCard = async (cardId, desc, cur = featuresSettings.defaultcur, amt = 0, email = "") => {
  try {
    var resObj = false;
    if (config.paymentGateway.paymentGatewayName == 'stripe') {
	console.log('inside gateway ****** stripe');
      resObj = await paymentGatewayFunctions.chargeExistingUserCard(cardId, desc, cur, amt);
	console.log('inside gateway resObj  ****** ',resObj);
    }

    if (config.paymentGateway.paymentGatewayName == 'paystack') {
      resObj = await paymentGatewayFunctions.chargeExistingUserCard(cardId, desc, cur, amt, email);
    }

    var chargeDetails = {
      status: false,
      id: '',
      amount: 0,
    };

    if (resObj.success) {
      chargeDetails.status = true;
      chargeDetails.id = resObj.charge.id;
      chargeDetails.amount = resObj.charge.amount;// (*100 in stripe)
    }else{
	console.log('inside gateway resObj failed ****** ');

	}

    return chargeDetails;

  } catch (error) {
    logger.error(error);
    return chargeDetails;
  }
}

//Helpers
function addCardToMongo(userId, cardDetails, res, req) {
  Rider.findById(userId, function (err, docs) {
    if (err) { } else if (!docs) { }
    else {
      docs.card.id = cardDetails.id;
      docs.card.currency = cardDetails.currency;
      docs.card.last4 = cardDetails.last4;
      docs.save(function (err, op) {
        if (err) { }
        else {
          addCardDetailsToWallet(userId, cardDetails);
          return res.status(200).json({ 'success': true, 'message': req.i18n.__("CARD_DETAILS_ADDED_SUCCESSFULLY") });
        }
      })

    }
  });
}

function addCardDetailsToWallet(userId, cardDetails) {
  // 1.chk Wallet
  Wallet.findOne({ ridid: userId }, function (err, doc) {
    if (err) { }
    else if (!doc) {
      //Add New Wallet Details
      var newDoc = new Wallet(
        {
          ridid: userId,
          bal: 0,
          card: {
            id: cardDetails.id,
            currency: cardDetails.currency,
            last4: addCardDetailsToWallet.last4
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          console.log("addCardDetailsToWallet2", userId);
        } else {
          console.log("addCardDetailsToWallet3", userId);
        }
      })
      //Add New Wallet Details
    } else if (doc) {
      doc.card.id = cardDetails.id;
      doc.card.currency = cardDetails.currency;
      doc.card.last4 = cardDetails.last4;
      doc.save(function (err, op) {
        if (err) {
          console.log("addCardDetailsToWallet4", userId);
        }
        else {
          console.log("addCardDetailsToWallet5", userId);
        }
      })
    }
  })
  // 1.chk Wallet
}

function RechargeMyWallet(res, charge, userId, walletId, req) {
  var id = mongoose.Types.ObjectId();
  var tranxData = {
    _id: id,
    trxid: charge.id,
    amt: (parseFloat(charge.amount) / 100),
    date: GFunctions.sendTimeNow(),
    type: 'Credit'
  }

  Wallet.findByIdAndUpdate(walletId, {
    $push: { trx: tranxData }
  }, { 'new': true },
    function (err, doc) {
      if (err) {
        console.log("RechargeMyWallet3", err);
        return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
      }
    }
  );

  Wallet.findById(walletId, function (err, docs) {
    if (err) { } else if (!docs) { }
    else {
      let oldbal = docs.bal;
      let newbal = (parseFloat(oldbal) + (parseFloat(charge.amount)) / 100);
      docs.bal = newbal;
      docs.save(function (err, op) {
        if (err) {
          console.log("RechargeMyWallet2", err);
          console.log(err);
          return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
        }
        else {
          console.log("RechargeMyWallet1", err);

          return res.status(200).json({ 'success': true, 'message': req.i18n.__("AMOUNT_ADDED_TO_WALLET"), 'balance': newbal });
          // console.log("RechargeMyWallet 2", charge.amount );
        }

      })
    }
  });

}

function createNTransferAmountToWallet(res, chargeDetails, cardDetails, userId) {
  // 1.chk Wallet
  Wallet.findOne({ ridid: userId }, function (err, doc) {
    if (err) { }
    else if (!doc) {
      //Add New Wallet Details
      var newDoc = new Wallet(
        {
          ridid: userId,
          bal: 0,
          card: {
            id: cardDetails.id,
            currency: cardDetails.currency,
            last4: cardDetails.last4,
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          console.log("addCardDetailsToWallet2", userId);
        } else {
          RechargeMyWallet(res, chargeDetails, userId, docs._id)
          console.log("addCardDetailsToWallet3", userId);
        }
      })
      //Add New Wallet Details
    } else if (doc) {
      RechargeMyWallet(res, chargeDetails, userId, doc._id);

    }
  })
  // 1.chk Wallet
}

/**
 * Retrive the list of bank name and slug and code for add bank details on driver side
 * Used only on paystack payment gateway
 * @param {*} req
 * @param {*} res
 */
export const getListOfBanks = async (req, res) => {
  try {
    var bankList = await paymentGatewayFunctions.getBankDetail();
    return res.status(200).json({ "success": true, "message": req.i18n.__("DETAILS_FETCH"), bankList: bankList })
  } catch (error) {
    logger.error(error);
    return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_FETCHING_BANK") + error.message });
  }
}

//
export const addDriverBank = async (req, res) => {
  try {
    var driverDetails = await Driver.findOne({ _id: mongoose.Types.ObjectId(req.userId) });
    req.body.fname = driverDetails.fname;
    req.body.lname = driverDetails.lname;
    req.body.phone = driverDetails.phone;

    var bankDetails = {
      email: req.body.email,
      holdername: req.body.holdername,
      acctNo: req.body.acctNo,
      banklocation: req.body.banklocation,
      bankname: req.body.bankname,
      swiftCode: req.body.swiftCode,
      currency: featuresSettings.defaultcur,
      chid: '',
    };

    if (config.paymentGateway.paymentGatewayName == 'stripe') {
      var desc = "Driver - " + req.userId;
      var resObj = false;
      if (featuresSettings.driverPayouts.driverStripeConnect) { //If payout from stripe available
        //create a connect account for Direct tranx
        resObj = await paymentGatewayFunctions.createConnectFromBankForDriver(req.body, featuresSettings.driverPayouts.driverStripeConnectCountry);
        if (resObj.success) {
          updateConnectIsCreated(req.userId, bankDetails);
          var stripeAcctId = resObj.charge.id;
          bankDetails.chid = stripeAcctId;
          addDriverBankDetailsToWallet(bankDetails, req, res);
        } else {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        }
      } else {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("THIS_FEATURE_NOT_AVAILABLE") });
      }
    }

    else if (config.paymentGateway.paymentGatewayName == 'paystack') {

      if (req.body.metaData == undefined) {
        var metaValue = {};
      } else {
        var metaValue = req.body.metaData;
      }
      var originalData = {
        recipientName: req.body.holdername,
        recipientDesc: req.body.banklocation,
        recipientAccountNo: req.body.acctNo,
        recipientBankCode: req.body.swiftCode,
        metaData: metaValue,
      };

      var bankDetailsRef = {
        email: req.body.email,
        holdername: req.body.holdername,
        acctNo: req.body.acctNo,
        banklocation: req.body.banklocation,
        bankname: req.body.bankname,
        swiftCode: req.body.swiftCode,
        currency: featuresSettings.defaultcur,
        chid: '',
      };


      resObj = await paymentGatewayFunctions.createTransferRecipientFromBankForDriver(originalData);
      // console.log(resObj)
      if (resObj.success) {
        updateConnectIsCreated(req.userId, bankDetailsRef);
        var stripeAcctId = resObj.data.recipient_code;
        bankDetails.chid = stripeAcctId;
        addDriverBankDetailsToWallet(bankDetails, req, res);
      } else {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
      }
    }

    else {
      //Just add bank details
      addDriverBankDetailsToWallet(bankDetails, req, res);
    }

  } catch (error) {
    logger.error(error);
    return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") + error.message });
  }
}

function updateConnectIsCreated(dvrid, bankDetails) {

  var updateDetails = {
    isConnected: true,
    actMail: bankDetails.holdername,
    actHolder: bankDetails.holdername,
    actNo: bankDetails.acctNo,
    actBank: bankDetails.bankname,
    actCode: bankDetails.swiftCode,
    actLoc: bankDetails.banklocation,
  };

  Driver.findOneAndUpdate({ _id: dvrid }, updateDetails, { new: true }, (err, doc) => {
    if (err) console.log(err);
    console.log('updateConnectIsCreated true');
  });
}

/**
 * Add the Driver card details to Mongo
 * @input
 * @param
 * @return
 * @response
 */
function addDriverBankDetailsToWallet(bankDetails, req, res) {
  // 1.chk Wallet
  DriverBank.findOne({ driverId: req.userId }, function (err, doc) {
    if (err) {
      return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
    }
    else if (!doc) {
      //Add New Wallet Details
      var newDoc = new DriverBank(
        {
          driverId: req.userId,
          totalBal: 0,
          bank: {
            email: bankDetails.email,
            holdername: bankDetails.holdername,
            acctNo: bankDetails.acctNo,
            banklocation: bankDetails.banklocation,
            bankname: bankDetails.bankname,
            swiftCode: bankDetails.swiftCode,
            currency: bankDetails.currency,
            chid: bankDetails.chid,
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        } else {
          return res.json({ 'success': true, 'message': req.i18n.__("BANK_DETAILS_ADDED_SUCCESSFULLY") });
        }
      })
      //Add New Wallet Details

    } else if (doc) {

      doc.bank.email = bankDetails.email;
      doc.bank.holdername = bankDetails.holdername;
      doc.bank.acctNo = bankDetails.acctNo;
      doc.bank.banklocation = bankDetails.banklocation;
      doc.bank.bankname = bankDetails.bankname;
      doc.bank.swiftCode = bankDetails.swiftCode;
      doc.bank.currency = bankDetails.currency;
      doc.bank.chid = bankDetails.chid;

      doc.save(function (err, op) {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        }
        else {
          return res.json({ 'success': true, 'message': req.i18n.__("BANK_DETAILS_ADDED_SUCCESSFULLY") });
        }
      })

    }
  })
  // 1.chk Wallet
}


export const driverRequestPayoutTransferAmount = async (req, res) => {
  try {
    if (featuresSettings.driverPayouts.driverDirectPayout) {
      if (Number(req.body.amount) < Number(featuresSettings.driverPayouts.driverPayoutAmountLimitMax)) {
        return res.status(409).json({ "status": false, "message": req.i18n.__("YOU_REQUEST_AMOUNT_MORE_THAN_ONE") + featuresSettings.driverPayouts.driverPayoutAmountLimitMax + featuresSettings.defaultcur })
      }

      var driverWalletData = await DriverWallet.findOne({ driverId: req.userId }).exec();
      var driverBankData = await DriverBank.findOne({ driverId: req.userId }).exec();

      if (driverWalletData) {
        let totalBal = driverWalletData.totalBal;
        req.body.stripeAcctId = driverBankData.bank.chid;
        if (!req.body.stripeAcctId) {
          return res.status(409).json({ "status": false, "message": req.i18n.__("YOU_DONT_HAVE_ACCOUNT_CREATED_TRANSFER_CREDITS") })
        }
        if (Number(totalBal) < Number(req.body.amount)) {
          return res.status(409).json({ "status": false, "message": req.i18n.__("YOU_DONT_HAVE_SUFFICENT_CREDITS_YOUR_WALLET") })
        }
        transfersAmountToDriverConnect(req, res, totalBal);
      } else {
        return res.status(409).json({ "status": false, "message": req.i18n.__("NO_DRIVER_CREDIT_WALLET") })
      }

    }
    else {
      return res.json({ 'success': true, 'message': req.i18n.__("THIS_FEATURE_NOT_AVAILABLE") });
    }

  } catch (error) {
    return res.json({ 'success': true, 'message': req.i18n.__("REQUEST_PAYOUT_ERROR") });
  }
}


async function transfersAmountToDriverConnect(req, res, totalAmt) {
  try {
    var Striperes = await paymentGatewayFunctions.transfersConnectAmtDriver(req.body.amount, req.body.stripeAcctId, featuresSettings.defaultcur);
    // console.log(Striperes);
    var transactionID;

    if (config.paymentGateway.paymentGatewayName == 'stripe') {
      transactionID = Striperes.charge.id;
    } else if (config.paymentGateway.paymentGatewayName == 'paystack') {
      transactionID = Striperes.charge.transfer_code;
    }
    if (Striperes) {

      var tripParams = {
        driverId: req.userId,
        trxId: transactionID,
        description: "Payout - " + req.body.amount,
        amt: req.body.amount,
        paymentDate: GFunctions.getISODate("D-M-YYYY h:mm a"),
        paymentDateSort: GFunctions.getISODate(),
        type: 'debit'
      }
      //Update Driver Wallet.
      updateDriverWallet(tripParams.driverId, tripParams, true);

      var balanceInWallet = Number(totalAmt) - Number(req.body.amount);

      return res.json({ 'success': true, 'message': req.i18n.__("AMOUNT_HAS_TRANSFER_TO_ACCOUNT"), 'balance': balanceInWallet });

    }
  } catch (err) {
    return res.status(500).json({ 'success': false, 'message': err.message });
  }
}



export const stripeRedirectLink = async (req, res) => {
  try {
    // https://gogetdash.com:3001/public/stripe/stripe-express-fail.html
    var stripeSuccess = config.fileurl + "stripe/stripe-express-success.html";
    var stripeFail = config.fileurl + "stripe/stripe-express-fail.html";

    var bankDetails = {
      email: '',
      holdername: '',
      acctNo: '',
      banklocation: '',
      bankname: '',
      swiftCode: '',
      currency: '',
      chid: '',
    };


    if (config.paymentGateway.stripeConnectAccountType == 'express') {
      var data = req.query;
      if (data.code) {
        var Striperes = await paymentGatewayFunctions.createExpressConnectForDriver(data.code);
        if (Striperes.success) {
          updateConnectIsCreated(data.state, bankDetails);
          var stripeObj = JSON.parse(Striperes.data);
          var stripeAcctId = stripeObj.stripe_user_id;
          bankDetails.chid = stripeAcctId;
          addDriverCHIDDetailsToWallet(bankDetails, data.state, res, stripeFail, stripeSuccess);

        } else {
          console.log('stripeFail1')

          res.redirect(stripeFail);
        }
      } else {
        console.log('stripeFail11')
        res.redirect(stripeFail);
      }
    }
  } catch (err) {
    console.log('stripeFail2')
    console.log(err);
    res.redirect(stripeFail);
  }
}

function addDriverCHIDDetailsToWallet(bankDetails, userId, res, stripeFail, stripeSuccess) {
  // 1.chk Wallet
  DriverBank.findOne({ driverId: userId }, function (err, doc) {
    if (err) {
      res.redirect(stripeFail);
    }
    else if (!doc) {
      //Add New Wallet Details
      var newDoc = new DriverBank(
        {
          driverId: userId,
          totalBal: 0,
          bank: {
            email: bankDetails.email,
            holdername: bankDetails.holdername,
            acctNo: bankDetails.acctNo,
            banklocation: bankDetails.banklocation,
            bankname: bankDetails.bankname,
            swiftCode: bankDetails.swiftCode,
            currency: bankDetails.currency,
            chid: bankDetails.chid,
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          res.redirect(stripeFail);
        } else {
          res.redirect(stripeSuccess);
        }
      })
      //Add New Wallet Details

    } else if (doc) {
      console.log('bankDetails', bankDetails)
      doc.bank.email = bankDetails.email;
      doc.bank.holdername = bankDetails.holdername;
      doc.bank.acctNo = bankDetails.acctNo;
      doc.bank.banklocation = bankDetails.banklocation;
      doc.bank.swiftCode = bankDetails.swiftCode;
      doc.bank.currency = bankDetails.currency;
      doc.bank.chid = bankDetails.chid;

      doc.save(function (err, op) {
        if (err) {
          res.redirect(stripeFail);
        }
        else {
          res.redirect(stripeSuccess);
        }
      })

    }
  })
  // 1.chk Wallet
}


export const get_bt_client_token = async (req, res) => {
  try {
    var token = await paymentGatewayFunctions.get_client_token();
    return res.status(200).json({ 'success': true, 'token': token });
  } catch (err) {
    return res.status(500).json({ 'success': false, 'message': err.message });
  }
}

export const transferAmountUsingNonceNRecharge = async (req, res, userId, walletId, msg, data) => {
  try {
    var nonceFromTheClient = req.body.payment_method_nonce;
    var rechargeAmount = req.body.rechargeAmount;
    var resObj = await paymentGatewayFunctions.transferAmountUsingNonceNRecharge(nonceFromTheClient, rechargeAmount, msg, data);
    if (resObj.success) {
      var chargeDetails = {
        id: resObj.id,
        amount: Number(resObj.amount) * 100, // (*100 for Braintree)
      };
      RechargeMyWallet(res, chargeDetails, userId, walletId);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_AMOUNT") });
  }
}

export const transferTripAmountUsingNonce = async (nonceFromTheClient, tripAmount, msg, data) => {
  try {
    var resObj = await paymentGatewayFunctions.transferAmountUsingNonceNRecharge(nonceFromTheClient, tripAmount, msg, data);
    if (resObj.success) {
      var chargeDetails = {
        id: resObj.id,
        amount: Number(resObj.amount) * 100, // (*100 for Braintree)
      };
      return chargeDetails;
    }
    return false;
  } catch (error) {
    return false;
  }
}

export const chargeExistingUserCardWithInstantSplitToDriver = async (custid, stripeDesc, currency, totalamount, commisionamount, driverConnectAcctId) => {
  try {
    var data = {
      commisionamount: commisionamount,
      totalamount: totalamount,
      custid: custid,
      driverConnectAcctId: driverConnectAcctId,
      stripeDesc: stripeDesc,
      currency: currency
    };

    var chargeDetails = {
      status: false,
      id: '',
      amount: 0,
    };

    var Striperes = await paymentGatewayFunctions.makeStripeSplitPayment(data);

    if (Striperes.success) {
      chargeDetails.status = true;
      chargeDetails.id = Striperes.chargeId;
      chargeDetails.amount = Striperes.chargeAmount; // (*100 in stripe)
    }

    return chargeDetails;

  } catch (err) {
    console.log(err)
    return chargeDetails;
  }
}

export const makeInstantPayouts = async (stripeDesc, currency, totalamount, commisionamount, driverConnectAcctId) => {
  try {
    var data = {
      commisionamount: commisionamount,
      totalamount: totalamount,
      driverConnectAcctId: driverConnectAcctId,
      stripeDesc: stripeDesc,
      currency: currency
    };

    var chargeDetails = {
      status: false,
      id: '',
      amount: 0,
    };

    var Striperes = await paymentGatewayFunctions.makeInstantPayouts(data);

    if (Striperes.success) {
      chargeDetails.status = true;
      chargeDetails.id = Striperes.chargeId;
      chargeDetails.amount = Striperes.chargeAmount; // (*100 in stripe)
    }

    return chargeDetails;

  } catch (err) {
    console.log(err)
    return chargeDetails;
  }
}

//Paytm
export const generate_paytm_checksum = async (req, res) => {
  try {
    var token = await paymentGatewayFunctions.generate_paytm_checksum(req);
    return res.status(200).json({ 'success': true, 'checksum': token });
  } catch (err) {
    return res.status(500).json({ 'success': false, 'message': err.message });
  }
}

/**
 * url  : /adminapi/addDriverBankDetail
 * body : _id, email, holdername, acctNo, banklocation, bankname, swiftCode,
*/
export const addDriverBankDetailForAdmin = async (req, res) => {
  try {
    var driverDetails = await Driver.findOne({ _id: mongoose.Types.ObjectId(req.body._id) });
    req.body.fname = driverDetails.fname;
    req.body.lname = driverDetails.lname;
    req.body.phone = driverDetails.phone;

    var bankDetails = {
      email: req.body.email,
      holdername: req.body.holdername,
      acctNo: req.body.acctNo,
      banklocation: req.body.banklocation,
      bankname: req.body.bankname,
      swiftCode: req.body.swiftCode,
      currency: featuresSettings.defaultcur,
      chid: '',
    };

    if (config.paymentGateway.paymentGatewayName == 'stripe') {
      var desc = "Driver - " + req.userId;
      var resObj = false;
      if (featuresSettings.driverPayouts.driverStripeConnect) { //If payout from stripe available
        //create a connect account for Direct tranx
        resObj = await paymentGatewayFunctions.createConnectFromBankForDriver(req.body, featuresSettings.driverPayouts.driverStripeConnectCountry);
        if (resObj.success) {
          updateConnectIsCreated(req.userId, bankDetails);
          var stripeAcctId = resObj.charge.id;
          bankDetails.chid = stripeAcctId;
          addDriverBankDetailForAdminToWallet(bankDetails, req, res);
        } else {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        }
      } else {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("THIS_FEATURE_NOT_AVAILABLE") });
      }
    }

    else if (config.paymentGateway.paymentGatewayName == 'paystack') {

      if (req.body.metaData == undefined) {
        var metaValue = {};
      } else {
        var metaValue = req.body.metaData;
      }
      var originalData = {
        recipientName: req.body.holdername,
        recipientDesc: req.body.banklocation,
        recipientAccountNo: req.body.acctNo,
        recipientBankCode: req.body.swiftCode,
        metaData: metaValue,
      };

      var bankDetailsRef = {
        email: req.body.email,
        holdername: req.body.holdername,
        acctNo: req.body.acctNo,
        banklocation: req.body.banklocation,
        bankname: req.body.bankname,
        swiftCode: req.body.swiftCode,
        currency: featuresSettings.defaultcur,
        chid: '',
      };


      resObj = await paymentGatewayFunctions.createTransferRecipientFromBankForDriver(originalData);
      // console.log(resObj)
      if (resObj.success) {
        updateConnectIsCreated(req.userId, bankDetailsRef);
        var stripeAcctId = resObj.data.recipient_code;
        bankDetails.chid = stripeAcctId;
        addDriverBankDetailForAdminToWallet(bankDetails, req, res);
      } else {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
      }
    }

    else {
      //Just add bank details
      addDriverBankDetailForAdminToWallet(bankDetails, req, res);
    }

  } catch (error) {
    logger.error(error);
    return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") + error.message });
  }
}

/**
 *
*/
export const viewDriverBankDetailForAdmin = (req, res) => {
  DriverBank.find({ driverId: req.params.id }, { trx: 0 }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    if (docs.length) {
      return res.json({ "bankDetails": docs[0].bank });
    }
    else {
      return res.json({ "bankDetails": { "email": "", "holdername": "", "acctNo": "", "banklocation": "", "bankname": "", "swiftCode": "" } });
    }
  })
}

function addDriverBankDetailForAdminToWallet(bankDetails, req, res) {
  // 1.chk Wallet
  DriverBank.findOne({ driverId: req.body._id }, function (err, doc) {
    if (err) {
      return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
    }
    else if (!doc) {
      //Add New Wallet Details
      var newDoc = new DriverBank(
        {
          driverId: req.body._id,
          totalBal: 0,
          bank: {
            email: bankDetails.email,
            holdername: bankDetails.holdername,
            acctNo: bankDetails.acctNo,
            banklocation: bankDetails.banklocation,
            bankname: bankDetails.bankname,
            swiftCode: bankDetails.swiftCode,
            currency: bankDetails.currency,
            chid: bankDetails.chid,
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        } else {
          return res.json({ 'success': true, 'message': req.i18n.__("BANK_DETAILS_ADDED_SUCCESSFULLY") });
        }
      })
      //Add New Wallet Details

    } else if (doc) {

      doc.bank.email = bankDetails.email;
      doc.bank.holdername = bankDetails.holdername;
      doc.bank.acctNo = bankDetails.acctNo;
      doc.bank.banklocation = bankDetails.banklocation;
      doc.bank.bankname = bankDetails.bankname;
      doc.bank.swiftCode = bankDetails.swiftCode;
      doc.bank.currency = bankDetails.currency;
      doc.bank.chid = bankDetails.chid;

      doc.save(function (err, op) {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_ADDING_BANK") });
        }
        else {
          return res.json({ 'success': true, 'message': req.i18n.__("BANK_DETAILS_ADDED_SUCCESSFULLY") });
        }
      })

    }
  })
  // 1.chk Wallet
}

export const checkPaymentTransaction = async (referenceId) => {
  try {
    var resObj;
    if (config.paymentGateway.paymentGatewayName == 'paystack') {
      resObj = await paymentGatewayFunctions.checkPaymentTransaction(referenceId);
    }

    if (resObj) {
      return true;
    } else {
      return false;
    }


  } catch (error) {
    logger.error(error);
    return false;
  }
};
