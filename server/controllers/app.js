import mongoose, { Query } from 'mongoose';

//import models
import Vehicle from '../models/vehicletype.model';
import Rider from '../models/rider.model';
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';
import Promo from '../models/promo.model';
import DriverPayment from '../models/driverpayment.model';
import HotelPayment from '../models/hotelpayment.model';
import Hotel from './../models/hotel.model';
import ServiceAvailableCities from '../models/serviceAvailableCities.model';
import TripLocation from '../models/tripLocations.model';

import Wallet from '../models/wallet.model';
import Schedule from '../models/schedules.model';
import ShareRides from '../models/shares.model';
import SafeRide from '../models/saferide.model';
import EstimationModel from '../models/estimation.model';
import { chargeExistingUserCard, test, transferAmountNRecharge, holdChargeCard } from './stripe';
import { updatePromoCodeUsedLogctrl } from './promocode';
import * as GFunctions from './functions';
import { myEarningsDriver, debitDriverBankTransactions, updateDriverWalletCredits, updateDriverEarningAtEveryDay, myEarningsDriverBasicSplits, updateDriverPerDayEarnings, updateDriverPerDayOnlineTime, verifyCurTripStatus } from './driver';
import * as TripHelpers from './tripHelper';
import * as PackageHelpers from './package.controller';
import * as CityLimitCalculationHelper from "../helpers/cityLimitCalculation.helper";
import logger from '../helpers/logger';
import { getCityBasedVehicleCharge, getVehicleChargeApprox } from './fareCalculation';
import { findNearbyDriversAndSendRequest, cancelOBOTaxiRequest, callTheOBOLoop, clearTheTripOBOFlow, needToResetDriver, needToResetDeclinedDriver } from './onebyonerequest';
import { addCancelationAmtToRider, updateRiderOldBalanceDetails, updateRiderWalletCreditsInRiderFirebase } from './rider';
import { requestNearbyDriversETA } from './driver';
import * as paymentCtrl from './paymentGateway/index';
import labels from '../helpers/labels.helper';
import { updateDriverWallet } from './driverBank';
import RentalPackage from '../models/rentalPackage.model';
import { getRentalEstimationFare } from '../modules/rental/rental.controller';
import { getHotelCommison } from './reqFrmHote.controller';
import * as smsGateway from './smsGateway';
import Admin from '../models/admin.model';
import DriverBank from '../models/driverBank.model';
import * as HelperFunc from './adminfunctions';
import { getVehicleDataForLiveMeter } from './vehicletype';
import { insidePolygon } from 'geolocation-utils';
import DriverPackage from '../models/driverPackage.model';
import { updateSubscriptionEndDateMongoAndFb } from './driver';
import { updateHotelWallet } from './reqFrmHote.controller';

const _ = require('lodash');
const moment = require("moment");
const geolib = require("geolib");
const distance = require('google-distance-matrix');
const firebase = require('firebase');
const config = require('../config');
const featuresSettings = require('../featuresSettings');
const crypto = require('crypto');
const async = require('async');
const cron = require('node-cron');
const turf = require('@turf/turf');
const constantsValues = require('../constants');

const noDriverFound = 'Our Drivers Are Busy Now Please Try Again'; //Our Drivers Are Busy Now Please Try Again  OR  No Driver Found
const requestTypeMethod = config.requestType;

const onebyoneConfig = config.onebyoneConfig;

//cancelation module
import { addCancelationStepsToDriver, addCancelationStepsToRider } from '../modules/cancelation/cancelation.controller';

/**
 * Send Only Available service
 * @input
 * @param
 * @return
 * @response
 */
export const getUserServiceBasicfare = async (req, res) => {
	// var userCity = await  GFunctions.getCityFromLatLon();
	Vehicle.find({
		$and: [
			{ $or: [{ loc: "All" }] }
			// { $or: [{loc:  "All" }, { loc : userCity }] }
		]
	}, {}).exec((err, docs) => {
		if (err) {
			return res.status(409).json([]);
		}
		return res.json(docs);
	});
}

/**
 * Services to show in Rider App
 */
export const getServiceBasicfare = async (req, res) => {
	const body = req.body || {};
	var scID, vehicleData = null,
		// where = { softDel: false },
		where = {},
		pickupCity = '';
	var arr = [];

	if (typeof body.tripType === "undefined") {
		body.tripType = 'daily';
	}

	try {
		if (featuresSettings.isCityWise) {
			//1. First Find which city pickup address is
			//2. If City is in SC lists (in SC list check for nearby Cities too)
			// return cityData;
			// scId =
			// where.scID = scId
			//3. get vehicles for that City or Get Default Vehicle with (Service Not Avalable Alert)

			// var cityData = await CityLimitCalculationHelper.findCityAndAddress(body.pickupLat, body.pickupLng);

			//1. First Find which city pickup address is
			//2. If City is in SC lists (in SC list check for nearby Cities too)
			// return cityData;
			// scId =
			// where.scID = scId
			//3. get vehicles for that City or Get Default Vehicle with (Service Not Avalable Alert)
			// var cityData = await CityLimitCalculationHelper.findCityAndAddress(body.pickupLat, body.pickupLng);
			// var serviceAvailableCities = await ServiceAvailableCities.findOne({
			// 	'softDelete' : false,
			// 	"$or": [
			// 		{"city"        : cityData.city},
			// 		{"nearby.city" : cityData.city}
			// 	]
			// })

			let availableService = await ServiceAvailableCities.find({ "softDelete": false, "city": { "$ne": "Default" } }, { "cityBoundaryPolygon": 1 }).lean()//.distinct('cityBoundaryPolygon')
			//let point = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], cityBoundaryPolygon)
			let pickPoint = false;
			let availableServiceLength = availableService.length
			for (let i = 0; i < availableServiceLength; i++) {
				if (availableService[i].cityBoundaryPolygon.length != 0) {
					//console.log("---------",availableService[i])
					pickPoint = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], availableService[i].cityBoundaryPolygon)
					//console.log(point)
					if (pickPoint) {
						arr.push(availableService[i]._id)
						break;
					}
				}
			}

			if (!pickPoint) {
				return res.status(409).json({ 'success': false, 'message': req.i18n.__("SERVICE_NOT_AVAILABEL_IN_THIS_LOCATION") })
			}

			if (featuresSettings.checkDropPoint) {
				if (pickPoint && typeof body.dropLat != "undefined" && body.dropLat != '' && typeof body.dropLng != "undefined" && body.dropLng != '') {
					let dropPoint = false
					for (let i = 0; i < availableServiceLength; i++) {
						if (availableService[i].cityBoundaryPolygon.length != 0) {
							//console.log(availableService[i])
							dropPoint = insidePolygon([parseFloat(body.dropLng), parseFloat(body.dropLat)], availableService[i].cityBoundaryPolygon)
							//console.log(point)
							if (dropPoint) {
								break
							}
						}
					}

					if (!dropPoint) {
						return res.status(409).json({ 'success': false, 'message': req.i18n.__("DROP_LOCATION_OUTSIDE_BOUNDARY") })
					}
				}
			}
			where.scIds = { $elemMatch: { scId: { $in: arr } } };
		}

		where.tripTypeCode = body.tripType;

		if (featuresSettings.addFareWithServices) {
			vehicleData = await Vehicle.find(where, {
				_id: 1, type: 1, tripTypeCode: 1, bkm: 1, file: 1, available: 1, isRideLater: 1, asppc: 1
				, mfare: 1, timeFare: 1, baseFare: 1, taxPercentage: 1, conveyancePerKm: 1, features: 1, isShareAvailable: 1
			}).sort({ displayorder: 1 }).exec(); // @v2TODO pass loc as null

			const from = body.pickupLat + ',' + body.pickupLng;
			const to = body.dropLat + ',' + body.dropLng;
			var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);
			if (config.distanceUnit == 'Miles') {
				var distanceInUnit = parseFloat(gdmResult.distanceValue * 0.000621371).toFixed(2);
			} else {
				var distanceInUnit = parseFloat(gdmResult.distanceValue / 1000).toFixed(2);
			}
			var timeInMinutes = parseFloat(gdmResult.timeValue / 60).toFixed(2);

			var newResArray = [];
			vehicleData.forEach(function (u) {
				var vehicleDataParams = {
					perKMRate: u.bkm,
					timeInMinutes: u.timeFare,
					BaseFare: u.baseFare,
					tax: u.taxPercentage,
					minFare: u.mfare,
				};
				var getVehicleChargeApproxio = getVehicleChargeApprox(distanceInUnit, timeInMinutes, vehicleDataParams);
				newResArray.push({
					_id: u._id,
					type: u.type,
					tripTypeCode: u.tripTypeCode,
					bkm: u.bkm,
					file: u.file,
					available: u.available,
					isRideLater: u.isRideLater,
					asppc: u.asppc,
					features: u.features,
					totalFare: getVehicleChargeApproxio,
				});
			});

			vehicleData = newResArray;
			// vehicleData.conveyancePerKm = getVehicleChargeApproxio;
		} else if (featuresSettings.addETAtoServicevehicles) {
			vehicleData = await Vehicle.find(where, {
				_id: 1, type: 1, tripTypeCode: 1, bkm: 1, file: 1, available: 1, isRideLater: 1, asppc: 1
				, mfare: 1, timeFare: 1, baseFare: 1, taxPercentage: 1, conveyancePerKm: 1, features: 1, isShareAvailable: 1
			}).sort({ displayorder: 1 }).exec(); // @v2TODO pass loc as null

			var nearbydriverEta = await requestNearbyDriversETA(body.pickupLat, body.pickupLng);
			var newResArray = [];
			vehicleData.forEach(function (u) {
				newResArray.push({
					_id: u._id,
					type: u.type,
					tripTypeCode: u.tripTypeCode,
					bkm: u.bkm,
					file: u.file,
					available: u.available,
					isRideLater: u.isRideLater,
					asppc: u.asppc,
					features: u.features,
					isShareAvailable: u.isShareAvailable,
					eta: getETAForTheService(nearbydriverEta, u.type),
				});
			});

			if (featuresSettings.tripsAvailable.indexOf("Package") > -1) {
				newResArray.push({
					_id: 1,
					type: "Rental",
					tripTypeCode: "Rental",
					bkm: 0,
					file: "public/vehicle/file-rental.png",
					available: true,
					isRideLater: true,
					asppc: 5,
					features: "",
					isShareAvailable: false,
					eta: "15 Min",
				});
			}

			if (featuresSettings.tripsAvailable.indexOf("Outstation") > -1) {
				newResArray.push({
					_id: 2,
					type: "Outstation",
					tripTypeCode: "Outstation",
					bkm: 0,
					file: "public/vehicle/file-outstation.png",
					available: true,
					isRideLater: true,
					asppc: 5,
					features: "",
					isShareAvailable: false,
					eta: "30 Min",
				});
			}

			vehicleData = newResArray;
		} else {
			vehicleData = await Vehicle.find(where, { _id: 1, type: 1, tripTypeCode: 1, bkm: 1, file: 1, available: 1, isRideLater: 1, asppc: 1, features: 1, isShareAvailable: 1 }).sort({ displayorder: 1 }).exec(); // @v2TODO pass loc as null
		}

		if (vehicleData.length) {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), 'vehicleCategories': vehicleData, 'pickupCity': pickupCity });
		}
		else {
			let availableService = await ServiceAvailableCities.find({ "softDelete": false, "city": "Default" }, {});
			if (availableService.length) {
				vehicleData = await Vehicle.find({ "tripTypeCode": body.tripType, 'scIds.scId': availableService[0]._id }, { _id: 1, type: 1, tripTypeCode: 1, bkm: 1, file: 1, available: 1, isRideLater: 1, asppc: 1, features: 1, isShareAvailable: 1 }).sort({ displayorder: 1 }).exec(); // @v2TODO pass loc as null
				return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), 'vehicleCategories': vehicleData, 'pickupCity': pickupCity });
			}
		}
		//4. Filter Vehicle For Distinct
		// vehicleData = GFunctions.getDistinctInArray(vehicleData, 'type'); //Distinct Vehicle Type
		GFunctions.clearObj(body, 0); GFunctions.clearObj(vehicleData);//Clear
	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SERVICE_NOT_FOUND"), 'error': error });
	}
}

function getETAForTheService (array, service) {
	var duration = 'NA';
	if (array == 'NA') return duration;
	array.forEach(function (obj) {
		if (obj.curService == service) {
			duration = obj.duration;
		}
	})
	return duration;
}

export const getestimationFare1 = (req, res) => {
	Vehicle.findById(req.body.serviceTypeId, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (!docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("SERVICE_NOT_FOUND"), 'error': err });
		//make it as CB
		const from = req.body.pickupLat + ',' + req.body.pickupLng;
		const to = req.body.dropLat + ',' + req.body.dropLng;

		var origins = [from];
		var destinations = [to];

		distance.key(config.googleApi);
		distance.units('metric');
		distance.mode('driving');

		distance.matrix(origins, destinations, function (err, distances) {
			if (err) {
				// return console.log(err);
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}
			if (!distances) {
				// return console.log('no distances');
				return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_DISTANCES"), 'distance': 0 });
			}
			if (distances.status == 'OK') {
				for (var i = 0; i < origins.length; i++) {
					for (var j = 0; j < destinations.length; j++) {
						var origin = distances.origin_addresses[i];
						var destination = distances.destination_addresses[j];
						if (distances.rows[0].elements[j].status == 'OK') {

							var basekm = docs.bkm;
							var IniDistance = docs.bkm; //C
							var CostForInitialperkm = docs.bfare; //D
							var InitialWaiting = docs.iniwait;
							var CostForInitialpermin = docs.inipermin;
							var Afterinitialdiatanceperkm = docs.ppm; //G
							var Afterinitialwaitingpermin = docs.ppmin;
							var Minfare = docs.mfare;//J

							var distancecal = distances.rows[i].elements[j].distance.text;
							var distanceVal = distances.rows[i].elements[j].distance.value; //meters
							var duration = distances.rows[i].elements[j].duration.text;
							var durationVal = distances.rows[i].elements[j].duration.value;

							//247 calculation
							var distanceInKM = parseFloat(distanceVal / 1000); //L
							var CostForKM = calCostForKM(distanceInKM, IniDistance, CostForInitialperkm, Afterinitialdiatanceperkm);//L,C,D,G = M
							var CostForWaiting = calCostForWaiting(0, InitialWaiting, Afterinitialwaitingpermin, CostForInitialpermin);//N,E,H,F = O
							var TotalCost = calTotalCost(CostForKM, CostForWaiting, Minfare);//M,O,J

							if (parseFloat(distanceVal / 1000) > basekm) {
								distanceVal = parseFloat(distanceVal) - parseFloat(basekm * 1000);
							} else { distanceVal = 0; }

							var distanceFare = parseFloat((parseFloat(docs.ppm) * distanceVal) / 1000).toFixed(2);

							var time = duration;
							var timeFare = parseFloat((parseFloat(docs.ppmin) * (durationVal / 60))).toFixed(2);

							var currency = docs.currency;
							var basefare = docs.bfare;
							var discountAmt = req.body.promoAmt;//Amt after checked
							// var subtotal = parseFloat(  parseFloat(basefare) + parseFloat(distanceFare) + parseFloat(timeFare)  + parseFloat(discountAmt) ).toFixed(2);

							return res.json({
								'success': true, 'message': 'Distance from ' + origin + ' to ' + destination + ' is ' + distancecal,
								'basefare': Minfare, 'distance': distancecal, 'distanceFare': CostForKM, 'time': time, 'timeFare': 0,
								'discountAmt': discountAmt, 'subtotal': TotalCost, 'currency': currency
							});
						} else {
							return res.status(409).json({ 'success': false, 'message': destination + ' is not reachable by land from ' + origin, 'distance': 0 });
						}
					}
				}
			}
		});
		//make it as CB
	});
}

/**
 * Estimation for Pickup and Drop location
 * @param {*} req
 * @param {*} res
 */
export const getestimationFare = async (req, res) => {
	try {
		const body = req.body || {};
		// var vehicleData = await Vehicle.findById(body.serviceTypeId).exec(); // @v2TODO pass loc as null
		const from = body.pickupLat + ',' + body.pickupLng;
		const to = body.dropLat + ',' + body.dropLng;
		var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);

		if (config.distanceUnit == 'Miles') {
			var distanceInUnit = parseFloat(gdmResult.distanceValue * 0.000621371).toFixed(2);
		} else {
			var distanceInUnit = parseFloat(gdmResult.distanceValue / 1000).toFixed(2);
		}

		var timeInMinutes = parseFloat(gdmResult.timeValue / 60).toFixed(2);

		var tripTime = body.time ? body.time : body.tripTime;
		let vehicleCharge = await getCityBasedVehicleCharge(body.serviceTypeId, body.pickupCity, distanceInUnit, timeInMinutes, tripTime);
		var distanceUnit = config.distanceSymbol ? config.distanceSymbol : ' KM';
		gdmResult.distanceLable = vehicleCharge.fareDetails.distance + distanceUnit;
		gdmResult.startCords = [body.pickupLng, body.pickupLat];
		gdmResult.endcoords = [body.dropLng, body.dropLat];
		gdmResult.from = body.from ? body.from : gdmResult.from;
		gdmResult.to = body.to ? body.to : gdmResult.to;
		vehicleCharge.fareDetails.distanceObj = null;

		var promoAmt = 0;
		console.log('req.body.promocode*************: ',req.body);
		if (req.body.promoCode) {
			var promoCodeUpper= req.body.promoCode.toUpperCase();
			 console.log('promoCode*********: ',promoCodeUpper);
			var promoAmtData = await validatePromoForEstimation(req.body.promoCode, req.body.riderId);
			 console.log('promoAmtData *********: ',promoAmtData);
			if (promoAmtData.success) {
				promoAmt = promoAmtData.discountAmt;
				console.log('promoCode Value *********: ',promoAmt);
			/*	
				if (Number(promoAmt) >= Number(vehicleCharge.fareDetails.totalFare)) {
					promoAmt = vehicleCharge.fareDetails.totalFare;
				}
				*/
			}

			var divideAmount = ((Number(vehicleCharge.fareDetails.totalFare).toFixed(2)) / 100).toFixed(2);
			
			var deductedFare = Number(divideAmount  * promoAmt).toFixed(2);
			 
			vehicleCharge.fareDetails.totalFare = getPromoCodeByPercentage(vehicleCharge.fareDetails.totalFare,promoAmt);
			// vehicleCharge.fareDetails.totalFare = (Number(vehicleCharge.fareDetails.totalFare) - Number(promoAmt)).toFixed(2); //reduce Signup Discount amt
			vehicleCharge.fareDetails.BalanceFare = vehicleCharge.fareDetails.totalFare;
			//vehicleCharge.fareDetails.DetuctedFare =  Number(promoAmt);
			vehicleCharge.fareDetails.DetuctedFare = deductedFare;		
			
		   }
		// GFunctions.clearObj(body, 0), GFunctions.clearObj(vehicleCharge, 0);

		if (featuresSettings.riderSignupBonus) {
			var riderWalletdata = await Wallet.findOne({ ridid: req.body.riderId }, { ridid: 1, bal: 1 }).exec();
			if (riderWalletdata) {
				var signupDiscountAmt = (Number(vehicleCharge.fareDetails.totalFare * (Number(featuresSettings.discountsAvailable[0].percentage) / 100))).toFixed(2);
				if (Number(riderWalletdata.bal) >= Number(signupDiscountAmt)) {
					vehicleCharge.fareDetails.detuctedPercentage = Number(featuresSettings.discountsAvailable[0].percentage);
					vehicleCharge.fareDetails.totalFare = (Number(vehicleCharge.fareDetails.totalFare) - Number(signupDiscountAmt)).toFixed(2); //reduce Signup Discount amt
					vehicleCharge.fareDetails.BalanceFare = vehicleCharge.fareDetails.totalFare;
					vehicleCharge.fareDetails.DetuctedFare = Number(signupDiscountAmt);
					// 15 % Discount on each hire up to Rs. 5000 /=
					vehicleCharge.fareDetails.remarks = featuresSettings.discountsAvailable[0].percentage + "% Discount on each hire up to " + config.currencySymbol + featuresSettings.riderSignupBonusAmount;
				}
			}
		}
		var newDoc = new EstimationModel({
			distanceDetails: gdmResult,
			vehicleDetailsAndFare: vehicleCharge,
		});

		var savedDoc = await newDoc.save();
		var estimationId = '';

		if (savedDoc) {
			estimationId = savedDoc._id;
		}

		var vfareDetails = convertAllNumbersToString(vehicleCharge.fareDetails);
		vehicleCharge.fareDetails = vfareDetails;

		return res.status(200).json({
			'success': true, 'message': req.i18n.__("ESTIMATION_FARE_DETAILS"),
			'distanceDetails': gdmResult, 'vehicleDetailsAndFare': vehicleCharge, 'pickupCity': body.pickupCity,
			'estimationId': estimationId
		});

	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_GETTING_ESTIMATION_FARE"), 'error': error });
	}
}

/**
Calculating PromoCode with percentage of actual value
*/

function getPromoCodeByPercentage(totalFare,discountAmount){
	
	var divideAmount = (Number(totalFare).toFixed(2) / 100).toFixed(2);
	console.log('divide by 100: ',divideAmount);
	var percentageAmount = Number(divideAmount * discountAmount).toFixed(2);
	console.log('totalFare : ',totalFare);
	console.log('discountAmount: ',discountAmount);
	console.log('percentAmount: ',percentageAmount);
	var finalDeductionAmount = (Number(totalFare).toFixed(2) - Number(percentageAmount)).toFixed(2);
	console.log('finalDeductionAmount: ',finalDeductionAmount);
	return finalDeductionAmount;
}

function getDiscountAmountByPromoCode(totalFare,discountAmount){
	var percentageAmount = Number((totalFare / 100) * discountAmount).toFixed(2);
	console.log('percentAmount: ',percentageAmount); 
	return percentageAmount;
}



/**
 * Convert numbers in given obj to String
 * @param {*} fareDetails
 */
function convertAllNumbersToString (fareDetails) {
	fareDetails = _.mapValues(fareDetails, function (v) {
		if (typeof v === 'number') {
			return v.toFixed(2);
		} else { return v; }
	});//Round all to 2 Decimals
	return fareDetails;
}

/**
 *
 * CostForKM
 * //L,C,D,G
 * @param {*} distanceInKM
 * @param {*} IniDistance
 * @param {*} CostForInitialperkm
 * @param {*} Afterinitialdiatanceperkm
 * @returns =IF(L4>C4,(L4-C4)*G4+D4,D4)
 */
function calCostForKM (distanceInKM, IniDistance, CostForInitialperkm, Afterinitialdiatanceperkm) {
	distanceInKM = parseFloat(distanceInKM);
	IniDistance = parseFloat(IniDistance);
	CostForInitialperkm = parseFloat(CostForInitialperkm);
	Afterinitialdiatanceperkm = parseFloat(Afterinitialdiatanceperkm);
	if (distanceInKM > IniDistance) {
		var costForKM = ((distanceInKM - IniDistance) * Afterinitialdiatanceperkm) + CostForInitialperkm;
		return parseFloat(costForKM).toFixed(2);
	} else {
		return parseFloat(CostForInitialperkm).toFixed(2);
	}
}

/**calCostForWaiting
 * //N,E,H,F
 * @param {*} waitingMin
 * @param {*} InitialWaiting
 * @param {*} Afterinitialwaitingpermin
 * @param {*} CostForInitialpermin
 * =IF(N4>E4,(N4-E4)*H4+F4,IF(N4>0,F4,0))
 */
function calCostForWaiting (waitingMin, InitialWaiting, Afterinitialwaitingpermin, CostForInitialpermin) {
	waitingMin = parseFloat(waitingMin);
	waitingMin = (waitingMin / 60);
	waitingMin = waitingMin.toFixed(2);
	waitingMin = parseFloat(waitingMin);
	InitialWaiting = parseFloat(InitialWaiting);
	Afterinitialwaitingpermin = parseFloat(Afterinitialwaitingpermin);
	CostForInitialpermin = parseFloat(CostForInitialpermin);
	if (waitingMin > InitialWaiting) {
		var waitingCost = (waitingMin - InitialWaiting) * (Afterinitialwaitingpermin + CostForInitialpermin);
		return parseFloat(waitingCost).toFixed(2);
	} else {
		if (waitingMin > 0) {
			return CostForInitialpermin;
		} else {
			return 0;
		}
	}
}

/**calTotalCost
 * //M,O,J
 * @param {*} CostForKM
 * @param {*} CostForWaiting
 * @param {*} Minfare
 * =IF((M4+O4)>J4,M4+O4,J4)
 */
function calTotalCost (CostForKM, CostForWaiting, Minfare) {
	CostForKM = parseFloat(CostForKM);
	CostForWaiting = parseFloat(CostForWaiting);
	Minfare = parseFloat(Minfare);
	if ((CostForKM + CostForWaiting) > Minfare) {
		return CostForKM + CostForWaiting;
	} else {
		return Minfare;
	}
}


/**
 * Set Current Taxi
 * @input
 * @param
 * @return
 * @response
 */
export const setCurrentTaxi = (req, res) => {
	freeTheDriver(req.userId);
	Driver.findById(req.userId, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (!docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("DRIVER_NOT_FOUND") });

		var taxi = docs.taxis.id(req.body.makeid);

		var obj = "";
		docs.currentTaxi = req.body.makeid;
		docs.curService = taxi.vehicletype;
		// docs.curService = taxi.vehicletype;
		docs.serviceStatus = taxi.taxistatus;
		docs.share = taxi.share;
		docs.noofshare = taxi.noofshare;
		docs.curVehicleNo = taxi.registrationnumber;
		docs.others1 = taxi.others1;

		docs.save(function (err, op) {
			if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
			return res.json({ 'success': true, 'message': req.i18n.__("TAXI_CHANGED_SUCCESSFULLY") });
		});
	});
}

/**
 * Set Driver online or offline
 * @param {*} req
 * @param {*} res
 */
export const setOnlineStatus = (req, res) => {
	var status = req.body.status;
	var lastUpdate = null;
	console.log("********* Online Status ***********: ",status);
	if (status == "1" || status == 1) {
		lastUpdate = GFunctions.getRespCountryDateTime();
	}
	var update = {
		online: req.body.status,
		lastUpdate: lastUpdate
	}
	
	if (req.body.status) {
		console.log("******** inside status *************");
		Driver.findOne({ _id: req.userId, $or: [{ 'status.docs': "pending" }, { 'softdel': "inactive" }] }, { 'status': 1, softdel: 1 }, (err, doc) => {
			if (err) {
				
				return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
			}
			else if (doc) {	
				console.log("******** inside docs *************");
			 	 res.status(409);  		 	 
				return res.json({ 'success': false, 'message': req.i18n.__("YOUR_ACCOUNT_WAS_NOT_ACCECPTED_ACTIVATED"), 'error': err });
			}
		});
	}
	Driver.findOneAndUpdate({ _id: req.userId }, update, { new: false }, (err, doc) => {
		if (err) {
			console.log("******** some error *************");

			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (featuresSettings.updateDriverPerDayOnlineTime) {
			console.log("*******updateDriverPerDayOnlineTime******", featuresSettings.updateDriverPerDayOnlineTime);
			console.log("*******status******", status);

			updateDriverPerDayOnlineTime(req.userId, status);
		}
		if (req.body.status == 0) {
			console.log("******** inside Status 0 , 409 *************");
		/*	res.status(409); */
			return res.json({'success': true, 'message': 'Offline' });
		}
		console.log("*******userId******", req.userId);
		GFunctions.getDriverFBStatusAndUpdate(req.userId);
		if (doc && (doc.curTrip && doc.curStatus != "free")) {
			verifyCurTripStatus(doc);
		}
		return res.json({ 'success': true, 'message': req.i18n.__("ONLINE") });
	})
}

/**
 * Set Driver Current location
 * @param {*} req
 * @param {*} res
 */
export const DriverLocation = (req, res) => {
	var driverLat = parseFloat(req.body.lat);
	var driverLng = parseFloat(req.body.lon);
	// console.log(req.body)
	if (driverLat && driverLng) {
		var update = {
			online: req.body.status,
			coords: [driverLng, driverLat],
			lastUpdate: GFunctions.getRespCountryDateTime(),
			"driverLocation.coordinates": [driverLng, driverLat]
		}
		Driver.findOneAndUpdate({
			_id: req.userId
		}, update, {
				new: false
			}, (err, doc) => {
				if (err) {
					return res.status(500).json({
						'success': false,
						'message': req.i18n.__("SOME_ERROR"),
						'error': err
					});
				}
				if (featuresSettings.updateTripPaths) {
					if (doc.curTrip!=null) {
						console.log(doc.curTrip)
						findAndUpdateTripLocation(doc.curTrip, driverLat, driverLng)
					}
				}
				return res.json({
					'success': true,
					'message': req.i18n.__("LOCATION_CHANGED_SUCCESSFULLY")
				});
			})
	} else {
		return res.status(409).json({
			'success': false,
			'message': req.i18n.__("LOCATION_NOT_VALID")
		});
	}

}

/**
 * [Taxi Request from User] = Checked
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const requestTaxi = async (req, res) => {
	try {
		const body = req.body || {};

		if (!body.promo == "") {
			console.log('body.promo*************',body.promo);
			var promoAmtData = await Promo.findOne({ code: body.promo.toUpperCase() }, { amount: 1, code: 1 }).exec();
			if (promoAmtData) body.promoAmt = promoAmtData.amount;
		}

		var riderDoc = null;
		if (body.bookingType == "rideLater") {
			riderDoc = await Rider.findById(req.userId).exec();
		}

		if (body.paymentMode == "") body.paymentMode = 'cash';
		var newDoc = new Trips(
			{
				// tripno: await TripHelpers.getTripNo(),
				requestFrom: body.requestFrom,
				requestId: body.adminId ? body.adminId : '',
				triptype: body.tripType,
				bookingType: body.bookingType,
				bookingFor: body.bookingFor,
				notes: body.notesToDriver ? body.notesToDriver : '',
				other: {
					ph: body.otherPh ? body.otherPh : '',
					phCode: body.otherPhCode ? body.otherPhCode : '',
					name: body.otherName ? body.otherName : '',
				},
				date: req.body.tripShownDate,
				cpy: null,
				cpyid: null,
				dvr: null,
				dvrid: null,
				rid: req.name,
				ridid: req.userId,
				hotelid: req.body.hotelId ? req.body.hotelId : null,
				fare: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
				vehicle: body.vehicleDetailsAndFare['vehicleDetails']['type'],
				service: body.vehicleDetailsAndFare['vehicleDetails']['serviceId'],
				paymentMode: body.paymentMode,
				csp: { //Cost split up RFCNG
					base: body.vehicleDetailsAndFare['fareDetails']['BaseFare'] ? body.vehicleDetailsAndFare['fareDetails']['BaseFare'] : 0,
					dist: body.distanceDetails['distanceValue'],
					distfare: body.vehicleDetailsAndFare['fareDetails']['KMFare'],
					time: body.distanceDetails['timeValue'],
					timefare: body.vehicleDetailsAndFare['fareDetails']['waitingFare'],
					comison: body.vehicleDetailsAndFare['fareDetails']['comisonAmt'],
					promoamt: body.promoAmt,
					promo: body.promo.toUpperCase(),
					cost: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
					conveyance: body.manualPickupCharge ? body.manualPickupCharge : body.vehicleDetailsAndFare['fareDetails']['pickupCharge'],
					tax: body.vehicleDetailsAndFare['fareDetails']['tax'],
					taxPercentage: body.vehicleDetailsAndFare['fareDetails']['taxPercentage'],
					via: body.paymentMode,
					driverCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesDriver'],
					riderCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesRider'],
					isNight: body.vehicleDetailsAndFare['fareDetails']['nightObj']['isApply'],
					isPeak: body.vehicleDetailsAndFare['fareDetails']['peakObj']['isApply'],
					nightPer: body.vehicleDetailsAndFare['fareDetails']['nightObj']['percentageIncrease'],
					peakPer: body.vehicleDetailsAndFare['fareDetails']['peakObj']['percentageIncrease'],
					currency: body.vehicleDetailsAndFare['fareDetails']['currency'] ? body.vehicleDetailsAndFare['fareDetails']['currency'] : config.currency,
					hotelcommision: body.vehicleDetailsAndFare['fareDetails']['hotelcommisionAmt'] ? body.vehicleDetailsAndFare['fareDetails']['hotelcommisionAmt'] : 0,
				},
				dsp: {
					distanceKM: body.vehicleDetailsAndFare['fareDetails']['distance'] ? body.vehicleDetailsAndFare['fareDetails']['distance'] : 'NA',
					start: body.distanceDetails['from'],
					end: body.distanceDetails['to'],
					startcoords: body.distanceDetails['startCords'],
					endcoords: body.distanceDetails['endcoords'],
				},
				isDriverAllowedOtherStates: req.body.isDownTown ? req.body.isDownTown : false,
				estTime: body.distanceDetails['timeLable'],
				status: "processing",
				tripOTP: [GFunctions.sendRandomizeCode('0', 4), GFunctions.sendRandomizeCode('0', 4)],
				scId: null,
				tripDT: body.tripDT,
				utc: body.utc,
				tripFDT: body.tripFDT,
				gmtTime: body.gmtTime,
				noofseats: body.noofseats,
				isWithoutEsti: body.isWithoutEstimation ? body.isWithoutEstimation : true,
			}
		);

		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}

			var msg = "TAXI_REQUEST_SENDED";
			if (body.bookingType != "rideLater") {
				updateRiderFbStatus(req.userId, "Processing", tripdata._id, body.tripType); //processing = Req intermediate state

				if (requestTypeMethod == 'onebyone') {
					findNearbyDriversAndSendRequest(tripdata, body, req.userId, body.distanceDetails['startCords'][0], body.distanceDetails['startCords'][1], body.serviceType);//For One By One
				} else {
					requestNearbyDrivers(tripdata, body, req.userId);
				}

			} else if (body.bookingType == "rideLater") {
				var msg = "DAILT_TAXI_REQUEST_SCH";
				// setInCRON(tripdata._id, tripdata);
				if (body.bookingType == "rideLater" && riderDoc != null) {
					smsGateway.sendSmsMsg(riderDoc.phone, '', riderDoc.phcode, 'rideLaterReceived', { "TRIPNO": tripdata.tripno });
				}
				if (body.processNow) {
					// updateRiderFbStatus(req.userId, "Processing", tripdata._id, body.tripType); //processing = Req intermediate state
					findNearbyDriversAndSendRequest(tripdata, body, req.userId, body.distanceDetails['startCords'][0], body.distanceDetails['startCords'][1], body.serviceType);//For One By One
				}
			}
			return res.status(200).json({ 'success': true, 'message': req.i18n.__(msg), "requestDetails": tripdata._id, "tripId": tripdata.tripno });
		})
	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': error.toString() });
	}
};

/**
 * Request Hail Taxi from Driver App
 * @param {*} req
 * @param {*} res
 */
export const requestHailTaxi = async (req, res) => {
	try {
		const body = req.body || {};
		let driverDoc = await Driver.findById(req.userId).exec();

		var newDoc = new Trips(
			{
				requestFrom: body.requestFrom,
				triptype: body.tripType,
				bookingType: body.bookingType,
				date: req.body.tripShownDate,
				cpy: null,
				cpyid: null,
				dvr: driverDoc.fname,
				dvrid: req.userId,
				rid: body.riderName ? body.riderName : null,
				ridid: body.riderId ? body.riderId : null,
				fare: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
				vehicle: body.vehicleDetailsAndFare['vehicleDetails']['type'],
				service: body.vehicleDetailsAndFare['vehicleDetails']['serviceId'],
				paymentMode: body.paymentMode,
				csp: { //Cost split up RFCNG
					base: body.vehicleDetailsAndFare['fareDetails']['BaseFare'] ? body.vehicleDetailsAndFare['fareDetails']['BaseFare'] : 0,
					dist: body.distanceDetails['distanceValue'],
					distfare: body.vehicleDetailsAndFare['fareDetails']['KMFare'],
					time: body.distanceDetails['timeValue'],
					timefare: body.vehicleDetailsAndFare['fareDetails']['waitingFare'],
					comison: body.vehicleDetailsAndFare['fareDetails']['comisonAmt'],
					promoamt: body.promoAmt,
					promo: body.promo.toUpperCase(),
					cost: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
					conveyance: body.vehicleDetailsAndFare['fareDetails']['pickupCharge'],
					tax: body.vehicleDetailsAndFare['fareDetails']['tax'],
					taxPercentage: body.vehicleDetailsAndFare['fareDetails']['taxPercentage'],
					via: body.paymentMode,
					driverCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesDriver'],
					riderCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesRider'],
					isNight: body.vehicleDetailsAndFare['fareDetails']['nightObj']['isApply'],
					isPeak: body.vehicleDetailsAndFare['fareDetails']['peakObj']['isApply'],
					nightPer: body.vehicleDetailsAndFare['fareDetails']['nightObj']['percentageIncrease'],
					peakPer: body.vehicleDetailsAndFare['fareDetails']['peakObj']['percentageIncrease'],
					currency: body.vehicleDetailsAndFare['fareDetails']['currency'] ? body.vehicleDetailsAndFare['fareDetails']['currency'] : config.currency,
				},
				dsp: {
					distanceKM: body.vehicleDetailsAndFare['fareDetails']['distance'] ? body.vehicleDetailsAndFare['fareDetails']['distance'] : 'NA',
					start: body.distanceDetails['from'],
					end: body.distanceDetails['to'],
					startcoords: body.distanceDetails['startCords'],
					endcoords: body.distanceDetails['endcoords'],
				},
				estTime: body.distanceDetails['timeLable'],
				status: "accepted",
				review: "driver accepted",
				tripOTP: [GFunctions.sendRandomizeCode('0', 4), GFunctions.sendRandomizeCode('0', 4)],
				scId: driverDoc.scId,
				scity: driverDoc.scity,
				tripDT: body.tripDT,
				utc: body.utc,
				tripFDT: body.tripFDT,
				needClear: "no",
				gmtTime: body.gmtTime,
				reqDvr: [
					{
						distVal: 0,
						called: 1,
						drvId: req.userId
					}
				]
			}
		);

		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}

			changeMyTripStatus(req.userId, tripdata.tripno);
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id, "tripno": tripdata.tripno });
		})
	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': error.toString() });
	}
};

/**
 * Used to update Drop location of trip after Trip Started
 */
export const updateDropLocation = async (req, res) => {
	try {
		var tripData = await Trips.findOne({ tripno: req.body.trip_id });

		const from = tripData.dsp.startcoords[1] + ',' + tripData.dsp.startcoords[0];
		const to = req.body.dropLat + ',' + req.body.dropLng;
		var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);

		var distanceInKM = parseFloat(gdmResult.distanceValue / 1000).toFixed(2);
		var timeInMinutes = parseFloat(gdmResult.timeValue / 60).toFixed(2);
		let vehicleCharge = await getCityBasedVehicleCharge(tripData.service, req.body.pickupCity, distanceInKM, timeInMinutes, req.body.time);

		var update = {
			'fare': vehicleCharge.fareDetails.totalFare,
			'estTime': gdmResult.timeLable,

			'csp.cost': vehicleCharge.fareDetails.totalFare,
			'csp.dist': gdmResult.distanceValue,
			'csp.distfare': vehicleCharge.fareDetails.KMFare,
			'csp.time': gdmResult.timeValue,
			'csp.timefare': vehicleCharge.fareDetails.waitingFare,
			'csp.comison': vehicleCharge.fareDetails.comisonAmt,
			'csp.tax': vehicleCharge.fareDetails.tax,

			'dsp.endcoords': [req.body.dropLng, req.body.dropLat],
			'dsp.end': gdmResult.to,
			'dsp.distanceKM': vehicleCharge.fareDetails.distance,
		}

		Trips.findOneAndUpdate({ tripno: req.body.trip_id }, update, { new: true }, (err, doc) => {
			if (err) {
				return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_UPDATING_DROP_LOCATION"), 'error': err });
			}
			else {
				findAndSendFCMToDriver(doc.dvrid, 'Current Trip Drop Location Changed.', 'updateDropLocation');
				return res.status(200).json({ 'success': true, 'message': req.i18n.__("DROP_LOCATION_UPDATED") });
			}
		})
	} catch (error) {
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("ERROR_UPDATING_DROP_LOCATION"), 'error': error });
	}
};

//Find Drivers
async function requestNearbyDrivers (tripdata, userreq, userid) {
	var requestRadius = config.requestRadius;
	var neededService = userreq.serviceName;
	Driver.find({
		coords: {
			$geoWithin: {
				$centerSphere: [[parseFloat(userreq.pickupLng), parseFloat(userreq.pickupLat)],
				requestRadius / 3963.2]
			},
		}, online: "1", curStatus: "free", curService: neededService
	}).exec((err, driverdata) => {
		if (err) {
			notifyRider(userid, noDriverFound, tripdata._id);
		}
		if (driverdata.length <= 0) {
			notifyRider(userid, noDriverFound, tripdata._id);
		} else {
			sendRequestToDrivers(tripdata, userreq, driverdata, userid);
		}
	});
}

//Update fb
function sendRequestToDrivers (tripdata, userreq, driverdata, userid) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}

	userreq.dropAddress = userreq.dropAddress ? userreq.dropAddress : ""
	userreq.time = userreq.time ? userreq.time : ""
	userreq.pickupAddress = userreq.pickupAddress ? userreq.pickupAddress : ""


	var db = firebase.database();
	var ref = db.ref("drivers_data");
	var requestData = {
		accept: {
			others: "0",
			trip_id: "0"
		},
		request: {
			drop_address: userreq.dropAddress,
			etd: userreq.time,
			picku_address: userreq.pickupAddress,
			request_id: tripdata._id,
			status: "1",
			datetime: "0",
			request_type: "Normal",
			review: "Taxi Request",
			request_no: "0"
		}
	};

	var requestedDrivers = [];
	var arrayLength = driverdata.length; //2

	//Mth 1 To all no check
	for (var i = 0; i < arrayLength; i++) {
		// console.log(driverdata[i]._id); // the _id
		var child = (driverdata[i]._id).toString();

		var usersRef = ref.child(child);
		requestedDrivers.push(child);
		console.log('requestData', requestData)
		usersRef.update(requestData, function (error) {
			if (error) { } else {
				findAndSendFCMToDriver(child, "New Request", 'newquest');
			} // Send FCM
		});
		if (i == (arrayLength - 1)) updateTaxiStatus(requestedDrivers, tripdata._id, userid);
	}
}


function findAndSendFCMToDriver (userId, msg, content) {
	Driver.findById(userId, function (err, docs) {
		if (err) { }
		else {
			if (docs) {
				if (docs.fcmId) {
					GFunctions.sendFCMMsg(docs.fcmId, msg, content);
				}
			}
		}
	});
}


function updateTaxiStatus (requestedDrivers, requestId, userid) {
	var update = {
		reqDvr: requestedDrivers
	}
	Trips.findOneAndUpdate({ _id: requestId }, update, { new: true }, (err, doc) => {
		if (err) {
			console.log("updateTaxiStatus", err);
		}
		else {
			rebackTaxiRequest(requestedDrivers, requestId, userid);
		}
	})
}

function rebackTaxiRequest (driverdata, requestId, userid) {
	setTimeout(function () {
		Trips.findOne({ _id: requestId, needClear: "yes" }, function (err, docs) {
			if (docs) { clearDriverRequest(driverdata, requestId, userid); }
		})
	}
		, 60000); //30000 = 30 sec
} //change trip status too,  ask to resend

function clearDriverRequest (driverdata, requestId, userid) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");

	var requestData = {
		accept: {
			others: "0",
			trip_id: "0"
		},
		request: {
			drop_address: "0",
			etd: "0",
			picku_address: "0",
			request_id: "0",
			status: "0",
			datetime: "0",
			request_type: "0",
			review: "Time Out",
			request_no: "0"
		}
	};

	var arrayLength = driverdata.length;
	for (var i = 0; i < arrayLength; i++) {
		var child = (driverdata[i]).toString();
		var usersRef = ref.child(child);
		usersRef.update(requestData, function (error) {
			if (error) { } else { }
		});
		if (i == (arrayLength - 1)) notifyRider(userid, noDriverFound, requestId);
	}
}

function notifyRider (userid, msg, requestId, tripstatus = "noresponse") {
	var update = {
		status: tripstatus,
		review: msg
	};

	Trips.findOneAndUpdate({ _id: requestId }, update, { new: false }, (err, doc) => {
		if (err) { } else { updateRiderFbStatus(userid, msg, requestId); }
	});
}

/**
 * updateRiderFbStatus =
 * @param {*} userid
 * @param {*} msg
 * @param {*} requestId
 */
export function updateRiderFbStatus (userid, msg, requestId = '0', triptype = 'daily') {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("riders_data");
	var requestData = {
		tripstatus: msg,
		requestId: requestId,
		triptype: triptype,
	};
	var child = userid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
}

export function clearRiderFbStatusAfterTripEnd (userid, msg, requestId = '0') {
	if (userid) {
		if (!firebase.apps.length) {
			firebase.initializeApp(config.firebasekey);
		}
		var db = firebase.database();
		var ref = db.ref("riders_data");
		var requestData = {
			tripstatus: msg,
			requestId: requestId,
			current_tripid: "",
		};
		var child = userid.toString();
		var usersRef = ref.child(child);
		usersRef.update(requestData);
	}
}

//Taxi Request from User Ends

//Taxi Cancel Before Driver Accepted it
export const cancelTaxi = (req, res) => {
	var update = {
		status: "Cancelled",
		review: constantsValues.cancelReqByUser
	}

	Trips.findOneAndUpdate({ _id: req.body.requestId, status: 'processing' }, update, { new: false }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (doc) {
			if (requestTypeMethod == 'onebyone') {
				cancelOBOTaxiRequest(doc.reqDvr, req.body.requestId); //For OBO
			} else {
				cancelTaxiRequest(doc.reqDvr, req.body.requestId); //For BroadCast
			}
			updateRiderFbStatus(req.userId, "canceled", req.body.requestId);
			return res.json({ 'success': true, 'message': req.i18n.__("TAXI_CANCELLED_SUCCESSFULLY") });  //Have to send cancelation fee
		} else {
			return res.status(400).json({ 'success': false, 'message': req.i18n.__("TRIP_ALREADY_CANCELLED") });
		}
	})
}

//what if it Cancelled another User concurrent req. ?
function cancelTaxiRequest (driverdata, requestId, msg = "User Cancelled") {
	setTimeout(function () {
		//fb
		if (!firebase.apps.length) {
			firebase.initializeApp(config.firebasekey);
		}
		var db = firebase.database();
		var ref = db.ref("drivers_data");

		var requestData = {
			accept: {
				others: "0",
				trip_id: "0"
			},
			request: {
				drop_address: "0",
				etd: "0",
				picku_address: "0",
				request_id: "0",
				datetime: "0",
				request_type: "0",
				status: "0",
				review: msg,
				request_no: "0"
			}
		};

		var arrayLength = driverdata.length; //2
		for (var i = 0; i < arrayLength; i++) {
			var child = (driverdata[i]).toString();
			var usersRef = ref.child(child);
			usersRef.update(requestData, function (error) {
				if (error) { } else { }
			});
		}
		//fb
	}
		, 0);

	var update = {
		needClear: "no"
	}

	Trips.findOneAndUpdate({ _id: requestId }, update, { new: true }, (err, doc) => {
		if (err) { } else { }
	})

}
//Taxi Cancel End

export const declineRequest = (req, res) => {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");
	var requestData = {
		accept: {
			others: "0",
			trip_id: "0"
		},
		request: {
			drop_address: "0",
			etd: "0",
			picku_address: "0",
			request_no: "0",
			request_id: "0",
			status: "0",
			datetime: "0",
			request_type: "0",
			review: "Declined"
		}
	};
	// console.log(req.userId);
	var child = req.userId.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData, function (error) {
		if (error) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }); }
		return res.json({ 'success': true, 'message': req.i18n.__("REQUEST_DECLINED_SUCCESSFULLY") });
	});

	if (requestTypeMethod == 'onebyone') {
		needToResetDeclinedDriver(req.userId, req.body.requestId); //clear trip mongo id For One By One OBO
	}
}

/**
 * Driver Accepted the Request
 * @input
 * @param
 * @return
 * @response
 */
export const acceptRequest = async (req, res) => {
	let driverDoc = await Driver.findById(req.userId).exec();
	var currentTaxi = driverDoc.taxis.id(driverDoc.currentTaxi);
	var vehiclename = currentTaxi.color + "+" + currentTaxi.model;

	var update = {
		status: "accepted",
		review: "driver accepted",
		dvr: req.name,
		dvrid: req.userId,
		scId: driverDoc.scId,
		scity: driverDoc.scity,
		needClear: "no",
	}

	if (featuresSettings.isMultipleCompaniesDriversAvailable) {
		if (!driverDoc.isIndividual) {
			update.cpyid = driverDoc.cmpy;
		}
	}

	Trips.findOneAndUpdate({ _id: req.body.requestId, status: "processing" }, update, { new: true }, async (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}


		//ETA
		var timeInMinutes = 0;
		try {
			if (featuresSettings.isETANeeded) {
				const from = driverDoc.coords[1] + ',' + driverDoc.coords[0];
				const to = doc.dsp.startcoords[1] + ',' + doc.dsp.startcoords[0];
				var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);
				timeInMinutes = parseFloat(gdmResult.timeValue / 60).toFixed(2);
			}
		} catch (error) {
			console.log(error)
		}
		//ETA

		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_PROCESSED") });
		res.json({
			'success': true, 'message': req.i18n.__("REQUEST_ACCEPTED_SUCCESSFULLY"), "requestId": req.body.requestId, "tripId": doc.tripno,
			eta: timeInMinutes
		});

		if (doc.requestFrom === "admin" || doc.bookingFor == 'others' || doc.requestFrom === "web" || doc.bookingType === "rideLater") {
			let riderDoc = await Rider.findById(doc.ridid).exec();
			if (riderDoc !== null) {
				var toPhone = riderDoc.phone;
				var toPhoneCode = riderDoc.phcode;
				if (doc.bookingFor == 'others') {
					toPhone = doc.other.ph;
					toPhoneCode = doc.other.phCode;
				}
				if (doc.requestFrom == "admin" || doc.requestFrom == "web") {
					smsGateway.sendSmsMsg(toPhone, '', toPhoneCode, 'sendTripAcceptedSMSToRider', { 'TRIPNO': doc.tripno, 'VEHICLENNAME': vehiclename, 'VEHICLENO': driverDoc.curVehicleNo, 'DRIVERNAME': driverDoc.fname, 'DRIVERNO': driverDoc.phone, 'OTP': doc.tripOTP[0] });
				} else {
					// console.log('acceptRequestsendTripAcceptedSMSToRiderForApp', toPhone)
					smsGateway.sendSmsMsg(toPhone, '', toPhoneCode, 'sendTripAcceptedSMSToRiderForApp', { 'TRIPNO': doc.tripno, 'VEHICLENNAME': vehiclename, 'VEHICLENO': driverDoc.curVehicleNo, 'DRIVERNAME': driverDoc.fname, 'DRIVERNO': driverDoc.phone, 'OTP': doc.tripOTP[0] });
				}
			}
			if (doc.requestId) findAndSendFCMToAdmin(doc.requestId, "Driver Has Accepted The Trip Request - " + doc.tripno);
		}


		if (requestTypeMethod == 'onebyone') {
			if (onebyoneConfig.enable) {
				var index = doc.reqDvr.indexOf(req.userId);
				if (index !== -1) doc.reqDvr.splice(index, 1);
				cancelOBOTaxiRequest(doc.reqDvr, req.body.requestId);
			}
		} else {
			var index = doc.reqDvr.indexOf(req.userId); //For Broadcast
			if (index !== -1) doc.reqDvr.splice(index, 1);  //For Broadcast
			cancelOtherTaxiRequest(doc.reqDvr, req.body.requestId, "");  //Only needed for Broadcast method
		}

		// if (doc.bookingType == 'rideLater') {
		// 	setInCRON(req.body.requestId, doc);
		// } else {
		updateRiderFbAcceptStatus(doc.ridid, "Accepted", doc.tripno, req.userId);
		changeMyTripStatus(req.userId, doc.tripno, doc.bookingType, doc);
		// }

		findAndSendFCMToRider(doc.ridid, "Driver Has Accepted Your Trip Request", 'acceptRequest');

	})
}

//req to trip id
function updateRiderFbAcceptStatus (userid, msg, tripno = '', tripdriver) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("riders_data");
	var requestData = {
		current_tripid: tripno,
		tripstatus: msg,
		tripdriver: tripdriver
	};
	var child = userid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
}

function cancelOtherTaxiRequest (driverdata, requestId, msg = "") {
	setTimeout(function () {
		//fb
		if (!firebase.apps.length) {
			firebase.initializeApp(config.firebasekey);
		}
		var db = firebase.database();
		var ref = db.ref("drivers_data");

		var requestData = {
			accept: {
				others: "0",
				trip_id: "0"
			},
			request: {
				drop_address: "0",
				etd: "0",
				picku_address: "0",
				request_id: "0",
				status: "0",
				datetime: "0",
				request_type: "0",
				review: msg,
				request_no: "0"
			}
		};

		var arrayLength = driverdata.length;
		for (var i = 0; i < arrayLength; i++) {
			var child = (driverdata[i]).toString();
			var usersRef = ref.child(child);
			usersRef.update(requestData, function (error) {
				if (error) { } else { }
			});
		}
	}
		, 0);
}  //trip db, inform rider fb, clear for others

function changeMyTripStatus (driverid, tripno, bookingType, tripData) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");

	var requestData = {
		accept: {
			others: "0",
			trip_id: tripno
		}
	};
	if (bookingType == "rideLater") {
		requestData = {
			accept: {
				others: "0",
				trip_id: tripno
			},
			request: {
				drop_address: "0",
				etd: "0",
				picku_address: "0",
				request_id: tripData._id,
				status: "2", // 2
				datetime: "0",
				request_type: "0", //0
				review: "0" //0
			}
		};
	}
	var child = driverid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
	if (bookingType == "rideLater") {
		addTripDatatoFb(driverid, tripno, bookingType, tripData);
	}
	//also change to busy in mongo = so that not get another
	changeMyTripStatusMongo(driverid, tripno, "onPickup");
}

/**
 * Trip Current Status Update from Driver
 * @input
 * @param
 * @return
 * @response
 */
function changeMyTripStatusMongo (driverid, tripno, msg = "free") {
	Driver.findById(driverid, function (err, docs) {
		if (err) {
			logger.error("changeMyTripStatusMongo", err);
		} else if (!docs) {
			logger.info("changeMyTripStatusMongo No Doc");
		}
		else {
			if (msg == 'Progress') {
				let oldnos = docs.rating.tottrip;
				oldnos++;
				docs.rating.tottrip = oldnos;
			}

			if (msg == 'free') {
				docs.curTrip = "";
			} else if (msg == 'Accept' || msg == 'Arrived' || msg == 'Progress' || msg == 'onPickup') {
				docs.curTrip = tripno;
			}

			docs.curStatus = msg;
			docs.save(function (err, op) {
				if (err) { logger.error("changeMyTripStatusMongo", err); }
				else { logger.info("changeMyTripStatusMongo"); }
			})
		}
	});
}

//Request Taxi Retry
export const requestTaxiRetry = async (req, res) => {
	try {
		const body = req.body || {};

		if (!body.promo == "") {
			var promoAmtData = await Promo.findOne({ code: body.promo.toUpperCase() }, { amount: 1, code: 1 }).exec();
			if (promoAmtData) body.promoAmt = promoAmtData.amount;
		}

		var newDoc = {
			status: "processing",
			needClear: "yes",
			//Need to add New Driver ID @TODO
		};

		var dataChanged = false;
		if (req.body.estimationId) dataChanged = true;

		if (dataChanged) {
			newDoc = {
				// tripno: await TripHelpers.getTripNo(),
				requestFrom: body.requestFrom,
				requestId: body.adminId ? body.adminId : '',
				triptype: body.tripType,
				bookingType: body.bookingType,
				bookingFor: body.bookingFor ? body.bookingFor : 'self',
				notes: body.notesToDriver ? body.notesToDriver : '',
				other: {
					ph: body.otherPh ? body.otherPh : '',
					phCode: body.otherPhCode ? body.otherPhCode : '',
					name: body.otherName ? body.otherName : '',
				},
				date: req.body.tripShownDate,
				cpy: null,
				cpyid: null,
				dvr: null,
				dvrid: null,
				hotelid: req.body.hotelId ? req.body.hotelId : null,
				fare: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
				vehicle: body.vehicleDetailsAndFare['vehicleDetails']['type'],
				service: body.vehicleDetailsAndFare['vehicleDetails']['serviceId'],
				paymentMode: body.paymentMode,
				csp: { //Cost split up RFCNG
					base: body.vehicleDetailsAndFare['fareDetails']['BaseFare'] ? body.vehicleDetailsAndFare['fareDetails']['BaseFare'] : 0,
					dist: body.distanceDetails['distanceValue'],
					distfare: body.vehicleDetailsAndFare['fareDetails']['KMFare'],
					time: body.distanceDetails['timeValue'],
					timefare: body.vehicleDetailsAndFare['fareDetails']['waitingFare'],
					comison: body.vehicleDetailsAndFare['fareDetails']['comisonAmt'],
					promoamt: body.promoAmt,
					promo: body.promo.toUpperCase(),
					cost: body.vehicleDetailsAndFare['fareDetails']['totalFare'],
					conveyance: body.vehicleDetailsAndFare['fareDetails']['pickupCharge'],
					tax: body.vehicleDetailsAndFare['fareDetails']['tax'],
					taxPercentage: body.vehicleDetailsAndFare['fareDetails']['taxPercentage'],
					via: body.paymentMode,
					driverCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesDriver'],
					riderCancelFee: body.vehicleDetailsAndFare['fareDetails']['cancelationFeesRider'],
					isNight: body.vehicleDetailsAndFare['fareDetails']['nightObj']['isApply'],
					isPeak: body.vehicleDetailsAndFare['fareDetails']['peakObj']['isApply'],
					nightPer: body.vehicleDetailsAndFare['fareDetails']['nightObj']['percentageIncrease'],
					peakPer: body.vehicleDetailsAndFare['fareDetails']['peakObj']['percentageIncrease'],
					currency: body.vehicleDetailsAndFare['fareDetails']['currency'] ? body.vehicleDetailsAndFare['fareDetails']['currency'] : config.currency,
					hotelcommision: body.vehicleDetailsAndFare['fareDetails']['hotelcommisionAmt'] ? body.vehicleDetailsAndFare['fareDetails']['hotelcommisionAmt'] : 0,
				},
				dsp: {
					distanceKM: body.vehicleDetailsAndFare['fareDetails']['distance'] ? body.vehicleDetailsAndFare['fareDetails']['distance'] : 'NA',
					start: body.distanceDetails['from'],
					end: body.distanceDetails['to'],
					startcoords: body.distanceDetails['startCords'],
					endcoords: body.distanceDetails['endcoords'],
				},
				estTime: body.distanceDetails['timeLable'],
				status: "processing",
				tripOTP: [GFunctions.sendRandomizeCode('0', 4), GFunctions.sendRandomizeCode('0', 4)],
				scId: null,
				tripDT: body.tripDT,
				utc: body.utc,
				tripFDT: body.tripFDT,
				gmtTime: body.gmtTime,
				noofseats: body.noofseats,
				needClear: "yes",
			}
		}

		var tripData = await Trips.findOneAndUpdate({ _id: body.requestId, status: { $in: ['noresponse', 'processing', 'Cancelled'] } }, newDoc, { new: true });

		body.pickupLng = tripData.dsp['startcoords'][0];
		body.pickupLat = tripData.dsp['startcoords'][1];

		if (tripData.bookingType == "rideNow") {
			var riderId = tripData.ridid;
			var tripId = tripData._id;
			updateRiderFbStatus(riderId, "Processing", tripId); //processing = Req intermediate state
		}
		if (requestTypeMethod == 'onebyone') {
			findNearbyDriversAndSendRequest(tripData, body, tripData.ridid, tripData.dsp['startcoords'][0], tripData.dsp['startcoords'][1], tripData.vehicle);//For One By One
		} else {
			// requestNearbyDrivers(tripdata, body, req.userId);
		}

		return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripData._id });

	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': error.toString() });
	}
};

//Cancel Trip by Driver / After accepting trip
export const cancelTrip = (req, res) => {
	var update = {
		status: "Cancelled",
		review: constantsValues.cancelTaxiByDriver,
		dvrid: req.userId,
		needClear: "no"
	}

	Trips.findOneAndUpdate({ tripno: req.body.tripId }, update, { new: false }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		// console.log(doc);
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_PROCESSED") });
		notifyRider(doc.ridid, constantsValues.cancelTaxiByDriver, req.body.requestId, "Cancelled");
		updateStatusInFirebase(req.body.tripId, "5");
		addCancelationStepsToDriver(doc.dvrid, req.body.tripId);
		changeMyTripStatusMongo(req.userId, doc.tripno, "free");
		changeRiderTripStatusMongo(req.userId, doc.tripno, "free");
		findAndSendFCMToRider(doc.ridid, "Trip Cancelled By Driver", 'requestTaxiRetry');
		return res.json({ 'success': true, 'message': req.i18n.__("TAXI_CANCELLED_SUCCESSFULLY"), "requestId": req.body.requestId, "tripId": req.body.tripno });
	})
}


/**
 * Trip Current Status Update from Driver
 * @input
 * @param
 * @return
 * @response
 */
export const tripCurrentStatus = (req, res) => {
	var fare = {
		"perKMRate": 0, 
		"fareType": "kmrate",
		"distance": "0",
		"KMFare": "0.00",
		"timeRate": 0,
		"waitingCharge": 0,
		"waitingTime": 0,
		"waitingFare": 0,
		"cancelationFeesRider": 0,
		"cancelationFeesDriver": 0,
		"pickupCharge": 0,
		"comison": 0,
		"comisonAmt": 0,
		"isTax": true,
		"taxPercentage": 0,
		"tax": 0,
		"minFare": 0,
		"flatFare": 0,
		"oldCancellationAmt": 0,
		"fareAmtBeforeSurge": 0,
		"totalFareWithOutOldBal": 0,
		"totalFare": 0,
		"nightObj": {
			"isApply": false,
			"percentageIncrease": 0,
			"alertLable": "Notes : Peak Fare x{PERCENTAGE} ({TIME})"
		},
		"peakObj": {
			"isApply": false,
			"percentageIncrease": 0,
			"alertLable": "Notes : Night Fare x{PERCENTAGE} ({TIME})"
		},
		"distanceObj": null,
		"fareAmt": 0,
		"duration": "0",
		"discountAmt": 0,
		"BalanceFare": 0,
		"DetuctedFare": 0,
		"paymentMode": 'cash',
		"applyValues": featuresSettings.applyValues
	};

	//Also check is paid
	Trips.findOne({ tripno: req.body.tripId }, async function (err, doc) {
		if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND"), 'error': err });

		var getVehicleDataForLiveMetersData = {};
		if (featuresSettings.liveTaxiMeter) {
			getVehicleDataForLiveMetersData = await getVehicleDataForLiveMeter(doc.vehicle);
		}
		//If Trip Exists Only
		//Rider Profile
		Rider.find({ _id: doc.ridid }, { hash: 0, salt: 0, EmgContact: 0 }).exec((err2, riderdoc) => {
			if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err2 }); }
			if (riderdoc.length) {
				var othersPhone = '';
				var otherName = '';
				if (doc.bookingFor == 'others') {
					othersPhone = doc.other.phCode + doc.other.ph;
					otherName = doc.other.name;
				}
				var riderdoc = {
					"id": riderdoc[0]._id,
					"cur": riderdoc[0].cur,
					"lang": riderdoc[0].lang,
					"cntyname": riderdoc[0].cntyname,
					"phone": riderdoc[0].phone,
					"phcode": riderdoc[0].phcode,
					"email": riderdoc[0].email,
					"lname": riderdoc[0].lname,
					"fname": riderdoc[0].fname,
					"profileurl": config.baseurl + riderdoc[0].profile,
					"points": riderdoc[0].rating.rating,
					"fcmId": riderdoc[0].fcmId,
					"balance": riderdoc[0].balance,
					"othersPhone": othersPhone,
					"otherName": otherName,
					"notes": doc.notes ? doc.notes : '',
				};
			} else {
				var riderdoc = {};
			}
			//Rider Profile End

			//If Driver Accepted
			if (req.body.status == 1) {
				changeMyTripStatusMongo(doc.dvrid, req.body.tripId, "Accept");
				changeRiderTripStatusMongo(riderdoc.id, req.body.tripId, "Accept");
				return res.json({ 'success': true, 'message': req.i18n.__("ACCEPT"), "rider": riderdoc, "pickupdetails": doc.dsp, "status": "Accept", "fare": fare, "taxitype": doc.vehicle, "startOTP": doc.tripOTP[0], "endOTP": doc.tripOTP[1], vehicleDataForLiveMeter: getVehicleDataForLiveMetersData });
			}
			//If Driver Arrived
			else if (req.body.status == 2) {
				changeMyTripStatusMongo(doc.dvrid, req.body.tripId, "Arrived");
				changeRiderTripStatusMongo(riderdoc.id, req.body.tripId, "Arrived");
				GFunctions.sendFCMMsg(riderdoc.fcmId, "Your Driver Has Arived", 'driver');
				return res.json({ 'success': true, 'message': req.i18n.__("ARRIVE"), "rider": riderdoc, "pickupdetails": doc.dsp, "status": "Arrive Now", "fare": fare, "taxitype": doc.vehicle, "startOTP": doc.tripOTP[0], "endOTP": doc.tripOTP[1], vehicleDataForLiveMeter: getVehicleDataForLiveMetersData });
			}
			//If Driver Trip Started : Progress
			else if (req.body.status == 3) {
				if (!req.body.startTime) req.body.startTime = GFunctions.sendTimeNow();
				if (!req.body.fromAddress) req.body.fromAddress = doc.dsp.start;
				if (!req.body.pickupLat) req.body.pickupLat = doc.dsp.startcoords[1];
				if (!req.body.pickupLng) req.body.pickupLng = doc.dsp.startcoords[0];
				updateActualPickupFare(req.body.tripId, fare, req.body); // Only PICKup
				changeMyTripStatusMongo(doc.dvrid, req.body.tripId, "Progress");
				changeRiderTripStatusMongo(riderdoc.id, req.body.tripId, "Progress");
				GFunctions.sendFCMMsg(riderdoc.fcmId, "Trip Started", 'tripstart');
				return res.json({
					'success': true, 'message': req.i18n.__("TRIP_IN_PROGRESS"), "rider": riderdoc, "pickupdetails": doc.dsp,
					"status": "Start Trip", "fare": fare, "taxitype": doc.vehicle, "startOTP": doc.tripOTP[0], "endOTP": doc.tripOTP[1]
					, vehicleDataForLiveMeter: getVehicleDataForLiveMetersData
				});
			}
			//If Driver Trip Ended : Complete
			else if (req.body.status == 4) {
				if (featuresSettings.isDriverSubscriptionWorkWithTripConcept) {
					checkSubscriptionConcept(doc.dvrid, doc.tripFDT, 'fromTrip');
				}
				freeTheDriver(doc.dvrid);
				checkReferal(doc.dvrid);
				changeMyTripStatusMongo(doc.dvrid, req.body.tripId, "free");
				changeRiderTripStatusMongo(riderdoc.id, req.body.tripId, "free");
				Trips.findOne({ tripno: req.body.tripId, status: "Finished" }, function (err, tripEndedExits) {
					if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
					if (tripEndedExits) return res.status(409).json({ 'success': false, 'message': req.i18n.__("TRIP_ALREADY_ENDED"), 'error': err });
					calculateFinalAmount(req, res, doc, fare, riderdoc);
				})

			}//If Driver Trip Ended : Complete Else End

		}) //Rider Profile End

	}); //Trip Details End
}


async function calculateFinalAmount (req, res, tripData, fare, riderdoc) {
	try {
		if (!req.body.distance || (Number(req.body.distance) <= 0)) req.body.distance = tripData.dsp.distanceKM;
		if (config.distanceUnit == 'Miles') {
			var distanceInUnit = parseFloat(req.body.distance * 0.621371).toFixed(2);
		} else {
			var distanceInUnit = req.body.distance;
		}

		var timeInMinutes = req.body.duration;
		if (timeInMinutes < 0 || timeInMinutes == null) {
			var tripStartTime = tripData.acsp.startTime;
			var tripEndTime = GFunctions.getISODate();
			if (req.body.endFromAdmin) {
				tripStartTime = req.body.startTime ? req.body.startTime : tripData.acsp.startTime;
				tripEndTime = req.body.endTime ? GFunctions.getDateTimeinThisFormat(reqdata.body.endTime, "HH:mm:ss", "YYYY-MM-DDTHH:mm:ss.SSS[Z]") : GFunctions.getISODate();
			}
			timeInMinutes = GFunctions.getMinsBtDateTime(tripStartTime, tripEndTime);
		}

		var waitingTime = req.body.waitingSecond ? req.body.waitingSecond : 0;//Sec
		var conveyanceKM = tripData.acsp.conveyanceKM;//Sec
		waitingTime = parseFloat(waitingTime) / 60;
		waitingTime = Math.ceil(waitingTime);

		if (!req.body.endAddress) {
			const from = tripData.dsp.startcoords[1] + ',' + tripData.dsp.startcoords[0];
			const to = req.body.dropLat + ',' + req.body.dropLng;
			var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);
			req.body.endAddress = gdmResult.to ? gdmResult.to : tripData.dsp.end;
		}

		if (!req.body.dropLat) req.body.dropLat = tripData.dsp.endcoords[1];
		if (!req.body.dropLng) req.body.dropLng = tripData.dsp.endcoords[0];

		// console.log(featuresSettings.addAdditionalFaresInTrip)
		var additionalFee = [];
		if (featuresSettings.addAdditionalFaresInTrip) {
			additionalFee = req.body.additionalFee ? JSON.parse(req.body.additionalFee) : null;
		}

		req.body.tripTime = req.body.tripTime ? req.body.tripTime : tripData.adsp.start;

		var discountPercentage = req.body.discountPercentage ? req.body.discountPercentage : 0;
		var discountName = req.body.discountName ? req.body.discountName : "";
		let vehicleCharge = await getCityBasedVehicleCharge(tripData.service, tripData.dsp.pickupCity, distanceInUnit, timeInMinutes,
			req.body.tripTime, waitingTime, additionalFee, discountPercentage, tripData._id);
		var fareDetails = vehicleCharge.fareDetails;
		fareDetails['applyValues'] = vehicleCharge.applyValues;
		fareDetails.distanceObj = null;
		fareDetails.distance = distanceInUnit;
		fareDetails.duration = timeInMinutes;

		if (riderdoc.balance < 0) { //For old cancelation fee
			fareDetails.oldBalance = riderdoc.balance;
			fareDetails.totalFare = Number(fareDetails.totalFare) + Number(Math.abs(riderdoc.balance));
		}
		fareDetails.discountAmt = tripData.csp.promoamt;

		//Check if Promocode amount is greater than Total Fare
		 
	 /*
		if (Number(fareDetails.discountAmt) >= Number(fareDetails.totalFare)) {
			console.log('*****finnal inside promocode');
			fareDetails.discountAmt = fareDetails.totalFare;//all total fare has been discounted
		}
	 */	 

		 

		if (featuresSettings.riderSignupBonus && tripData.csp.via.toLowerCase() == 'cash') {
			var riderWalletdata = await Wallet.findOne({ ridid: riderdoc.id }, { ridid: 1, bal: 1 }).exec();
			if (riderWalletdata) {
				var signupDiscountAmt = (Number(fareDetails.totalFare * (Number(featuresSettings.discountsAvailable[0].percentage) / 100))).toFixed(2);
				if (Number(riderWalletdata.bal) >= Number(signupDiscountAmt)) {
					if (discountPercentage <= 0) {
						fareDetails.detuctedPercentage = Number(featuresSettings.discountsAvailable[0].percentage);
						discountPercentage = featuresSettings.discountsAvailable[0].percentage;
						discountName = 'Signup Bonus';
						fareDetails.totalFare = (Number(fareDetails.totalFare) - Number(signupDiscountAmt)).toFixed(2); //reduce Signup Discount amt
						fareDetails.BalanceFare = fareDetails.totalFare;
						fareDetails.DetuctedFare = Number(signupDiscountAmt);
						vehicleCharge.fareDetails.remarks = featuresSettings.discountsAvailable[0].percentage + "% Discount on each hire up to " + config.currencySymbol + featuresSettings.riderSignupBonusAmount;
						findNChargeExistingUserWallet(riderdoc.id, tripData._id, Number(signupDiscountAmt));
					}
				}
			}
		}
		
		var dividedAmount =  Number(fareDetails.totalFare / 100).toFixed(2);
		var fareDicountedAmount = Number(dividedAmount * fareDetails.discountAmt).toFixed(2);
		fareDetails.discountAmt = fareDicountedAmount;			


		fareDetails.BalanceFare = Number(Number(fareDetails.totalFare) - Number(fareDicountedAmount )).toFixed(2); //@v2TODO
		fareDetails.totalFare = Number(Number(fareDetails.totalFare) - Number(fareDicountedAmount)).toFixed(2); //Promo reduced from Total fare
		fareDetails.paymentMode = tripData.csp.via.toLowerCase();
		
		 

		//Discount
		fareDetails['discountPercentage'] = discountPercentage;
		fareDetails['discountName'] = discountName;

		var driverWalletDetuctionType = 'debit';
		var driverWalletDetuctionAmt = 0;

		fareDetails.hotelcommisionAmt = 0;
		if (tripData.hotelid) {
			var hotelcommisionObj = await getHotelCommison(fareDetails.totalFare, tripData.hotelid);
			if (hotelcommisionObj) {
				fareDetails.BalanceFare = Number(fareDetails.BaseFare) + Number(hotelcommisionObj.hotelcommisionAmt);
				fareDetails.totalFare = Number(fareDetails.totalFare) + Number(hotelcommisionObj.hotelcommisionAmt);
				fareDetails.hotelcommisionAmt = hotelcommisionObj.hotelcommisionAmt;
				fareDetails.hotelcommision = hotelcommisionObj.hotelcommision;
				var hotelParams = {
					hotelid: tripData.hotelid,
					hotelname: hotelcommisionObj.hotelName,
					trxId: tripData.tripno,
					description: "trips - credit",
					amt: fareDetails.hotelcommisionAmt,
					paymentDate: tripData.date,
					paymentDateSort: GFunctions.getISODate(),
					type: "credit"
				}
				updateHotelWallet(hotelParams.hotelid, hotelParams);
			}
		}

		fareDetails.companyCommisionAmt = 0;
		if (tripData.cpyid) {
			var companyCommisionObj = await getCompanyCommison(fareDetails.totalFare, tripData.cpyid);
			fareDetails.companycommisionAmt = Number(companyCommisionObj);
		}

		var addToDriverWallet = true;

		//@TODOv2 For Chareging Credit / Stripe / Wallet == Modified fare detail needed
		var paymentRes = false;
		var carddebt = 0;
		var cashpaid = fareDetails.BalanceFare;
		if (fareDetails.paymentMode == 'card' && featuresSettings.riderCard && featuresSettings.riderTripPaidInClientSide == false) {
			paymentRes = await findNChargeExistingUserCard(tripData.ridid, tripData._id, fareDetails.BalanceFare, fareDetails.comisonAmt, tripData.dvrid, tripData.tripno);
			console.log('***** Payment Response *****',paymentRes);
		}
		//Updating Digital Payment if exists
		if (paymentRes) {
			if (paymentRes.success) {
				console.log('***** Payment Response Success*****');

				carddebt = paymentRes.detectedAmt;
				cashpaid = paymentRes.balancetopay;
				fareDetails.BalanceFare = paymentRes.balancetopay;
				fareDetails.DetuctedFare = paymentRes.detectedAmt;
				fareDetails.tranxid = paymentRes.tranxid;
				driverWalletDetuctionType = 'credit';
				driverWalletDetuctionAmt = Number(carddebt) - Number(fareDetails.comisonAmt); //Only Trip amount except commision
				addToDriverWallet = paymentRes.addToWallet
			}else{	
				console.log('********* failed tripData *********: ',tripData);
				findAndSendFCMToDriver(tripData.dvrid, "Payment Failed! Collect Trip Amount manually.");
				findAndSendFCMToRider(tripData.ridid, "Your Payment is decliend, Kindly pay the amount manually.");
			}
		}

		if (fareDetails.paymentMode == 'card' && featuresSettings.riderTripPaidInClientSide) {
			driverWalletDetuctionType = 'credit';
			driverWalletDetuctionAmt = fareDetails.BalanceFare; //check
		}

		if (fareDetails.paymentMode == 'card' && config.paymentGateway.paymentGatewayName == 'braintree') {
			driverWalletDetuctionType = 'credit';
			driverWalletDetuctionAmt = fareDetails.BalanceFare;
		}

		if (fareDetails.paymentMode == 'card' && featuresSettings.riderTripPaidInClientSide && config.paymentGateway.paymentGatewayName == 'paytm') {
			driverWalletDetuctionType = 'credit';
			driverWalletDetuctionAmt = fareDetails.BalanceFare;
		}

		//For Chareging  Wallet == Modified fare detail needed
		var walletRes = false;
		var walletdebt = 0;
		var cashpaid = fareDetails.BalanceFare;
		if (fareDetails.paymentMode == 'wallet' && featuresSettings.riderWallet) {
			walletRes = await findNChargeExistingUserWallet(tripData.ridid, tripData._id, fareDetails.BalanceFare);
		}
		//Updating Digital Payment if exists
		if (walletRes) {
			if (walletRes.success) {
				walletdebt = walletRes.detectedAmt;
				cashpaid = walletRes.balancetopay;
				fareDetails.BalanceFare = walletRes.balancetopay;
				fareDetails.totalFare = walletRes.balancetopay;
				fareDetails.DetuctedFare = walletRes.detectedAmt;
				fareDetails.tranxid = walletRes.tranxid;
				driverWalletDetuctionType = 'credit';
				// driverWalletDetuctionAmt = walletdebt;
				driverWalletDetuctionAmt = Number(walletdebt) - Number(fareDetails.comisonAmt);
			}
		}


		if (Number(fareDetails.oldCancellationAmt) > 0 && featuresSettings.applyBlockOldCancellationAmt) {
			updateRiderOldBalanceDetails(tripData.ridid, fareDetails.oldCancellationAmt);
		}

		var DriverPaymentDetailsSplits = {};
		var digital = parseFloat(carddebt) + parseFloat(fareDetails.discountAmt) + Number(walletdebt); //
		var outstanding = parseFloat(fareDetails.totalFare) - parseFloat(digital); //
		var inhand = parseFloat(cashpaid) + parseFloat(outstanding); //
		DriverPaymentDetailsSplits.amttopay = fareDetails.totalFare;
		DriverPaymentDetailsSplits.cashpaid = cashpaid;
		DriverPaymentDetailsSplits.commision = fareDetails.comisonAmt;
		DriverPaymentDetailsSplits.promoamt = fareDetails.discountAmt;
		DriverPaymentDetailsSplits.walletdebt = walletdebt;
		DriverPaymentDetailsSplits.carddebt = carddebt;
		DriverPaymentDetailsSplits.digital = digital;
		DriverPaymentDetailsSplits.outstanding = outstanding;
		DriverPaymentDetailsSplits.inhand = inhand;
		DriverPaymentDetailsSplits.amttodriver = parseFloat(fareDetails.totalFare) - parseFloat(fareDetails.comisonAmt);
		DriverPaymentDetailsSplits.toSettle = parseFloat(fareDetails.totalFare) - parseFloat(fareDetails.comisonAmt) - parseFloat(inhand);

		var updateFareDetails = await updateFareDetailsInTrip(fareDetails, req, tripData, req.body.endAddress, DriverPaymentDetailsSplits);
		if (!updateFareDetails) return res.status(409).json({ 'success': false, 'message': req.i18n.__("Trip Details Not Updated...") });
		//Deduct Driver Commsion from Driver Wallet
		if (featuresSettings.driverPayouts.adminCommision == 'driverWallet' && addToDriverWallet) {
			if (fareDetails.paymentMode == 'cash') {
				if (featuresSettings.driverPayouts.deductAmountFromDriverWallet == 'totalFare') {
					driverWalletDetuctionAmt = fareDetails.totalFare;
				} else if (featuresSettings.driverPayouts.deductAmountFromDriverWallet == 'commision') {
					driverWalletDetuctionAmt = fareDetails.comisonAmt;
				}
			}
			if (driverWalletDetuctionType == 'debit') {
				driverWalletDetuctionAmt = Number(driverWalletDetuctionAmt) - Number(fareDetails.discountAmt);
			} else {
				driverWalletDetuctionAmt = Number(driverWalletDetuctionAmt) + Number(fareDetails.discountAmt);
			}
			var tripParams = {
				driverId: tripData.dvrid,
				trxId: tripData.tripno,
				description: "trips - " + driverWalletDetuctionType,
				amt: driverWalletDetuctionAmt,
				paymentDate: tripData.date,
				paymentDateSort: GFunctions.getISODate(),
				type: driverWalletDetuctionType
			}

			//Update Driver Wallet.
			if (featuresSettings.payPackageTypes && featuresSettings.payPackageTypes.includes("subscription")) {
				let tripDriverData = await DriverBank.findOne({ driverId: tripData.dvrid }, { isSubcriptionActive: 1, subcriptionEndDate: 1, code: 1 });
				if (tripDriverData.isSubcriptionActive && driverWalletDetuctionType == 'debit') {
					//no need to Debit the wallet
				} else {
					updateDriverWallet(tripParams.driverId, tripParams)
				}
			} else {
				updateDriverWallet(tripParams.driverId, tripParams)
			}
		}

		GFunctions.sendFCMMsg(riderdoc.fcmId, "Trip Ended", 'tripEnd');

		if (featuresSettings.isPromoCodeAvailable) {
			if (typeof tripData.csp.promo != "undefined" || tripData.csp.promo != "") {
				updatePromoCodeUsedLogctrl(tripData.csp.promo.toUpperCase(), tripData.tripId, tripData.ridid); // chnage this Promo code is used by this User for this Trip
			}
		}

		if (tripData.requestFrom === "admin" || tripData.bookingFor == 'others') {
			let riderDoc = await Rider.findById(tripData.ridid).exec();
			if (riderDoc !== null) {
				var toPhone = riderDoc.phone;
				var toPhoneCode = riderDoc.phcode;
				if (tripData.bookingFor == 'others') {
					toPhone = tripData.other.ph;
					toPhoneCode = tripData.other.phCode;
				}
				smsGateway.sendSmsMsg(toPhone, '', toPhoneCode, 'tripEndPaymentToRider', { 'FEE': fareDetails.totalFare });
			}
		}

		if (featuresSettings.fareCalculationType == 'indiaGst') {
			TripHelpers.sendTripGSTReceipt(tripData._id, riderdoc.email); // send Trip Receipt
		} else {
			if ((tripData.bookingType == "hailRide")) {
				var riderMailId = riderdoc.email ? riderdoc.email : req.body.cusemailid;
				if (riderMailId) {
					TripHelpers.sendTripReceipt(tripData._id, riderMailId); // send Trip Receipt
				}
			} else {
				var data = Driver.findOne({ "_id": tripParams.driverId });
				TripHelpers.sendTripReceipt(tripData._id, riderdoc.email); // send Trip Receipt	to rider
				TripHelpers.sendTripReceipt(tripData._id, data.email); // send Trip Receipt	to driver
			}
		}

		tripData.adsp.dLat = req.body.dropLat;
		tripData.adsp.dLng = req.body.dropLng;
		// GFunctions.saveStaticMapForTrip(tripData); // Save Gmap
		if (featuresSettings.updateTripPaths) {
			var tripPath = await TripLocation.findOne({ 'tripId': tripData._id })
			if (tripPath) GFunctions.saveStaticMapForTrip(tripData, tripPath.loc); // Save Gmap
			else GFunctions.saveStaticMapForTrip(tripData, []); // Save Gmap
		}
		else {
			var tripPath = []
			GFunctions.saveStaticMapForTrip(tripData, tripPath); // Save Gmap
		}

		if (featuresSettings.isUpdateDriverPerDayEarnings) {
			updateDriverPerDayEarnings(tripData.dvrid, 1, fareDetails.totalFare, fareDetails.comisonAmt);
		}

		clearRiderFbStatusAfterTripEnd(tripData.ridid, "", 0); // as finished

		if (tripData.bookingType == 'rideLater') {
			removeFromSchedule(tripData._id);
		}

		return res.json({
			'success': true, 'message': req.i18n.__("TRIP_ENDED"), "rider": riderdoc, "pickupdetails": tripData.dsp,
			"status": "Trip Ended", "fare": fareDetails, "taxitype": tripData.vehicle,
			"startOTP": tripData.tripOTP[0], "endOTP": tripData.tripOTP[1],
		});

	} catch (error) {
		logger.error(error);
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': error.toString() });
	}
}

function freeTheDriver (driverid) {
	Driver.findByIdAndUpdate(driverid, {
		'curStatus': 'free'
	}, { 'new': true },
		function (err, doc) {
			if (err) { }
		}
	);
}

function checkReferal (driverId) {
	Driver.findById(driverId, { referrealRecharge: 1, referredCode: 1 }, (err, doc) => {
		if (doc.referrealRecharge == false) { // Not yet get the subscription amount
			if (doc.tripCount == 0) { // Tripcount was zero need to apply referal
				processReferalCode(doc.referredCode, driverId);
			} else { // Update driver document with tripCount alone
				updateReferalDetail(driverId, referredCode, doc.tripCount - 1, false);
			}
		}
	})
}

//function to check subscription concept and end-date changes
export const checkSubscriptionConcept = async (driverId, tripDate, requestFrom) => {

	var tripDetails;

	//check subscription was activated
	var subscriptionDetail = await Driver.findById(driverId, { isSubcriptionActive: 1, subscriptionPackId: 1, subscriptionPackPurchaseId: 1 }).exec();

	var requestData = { isSubcriptionActive: false, subcriptionEndDate: 'NA' }
	var puchaseID = subscriptionDetail.subscriptionPackPurchaseId;

	if (subscriptionDetail.isSubcriptionActive) { // Driver have subscription
		// get the pack purchase-date and end-date with the use of packPurchaseId
		var packageDetails = await DriverPackage.findById(puchaseID, { startDate: 1, endDate: 1 }).exec();
		//check the driver(driverId) had any trip (tripFDT) after purchase the package start-date(startDate)
		if (requestFrom == "fromTrip") { // need to check this is a first trip in the current package time
			tripDetails = await Trips.find({
				$and: [
					{ dvrid: driverId },
					{
						tripFDT: {
							$gte: packageDetails.startDate,
							$lt: tripDate,
						}
					}
				]
			}, {}).exec(); //tripFDT
		} else if (requestFrom == "fromCron") { // check had any trip between the current package time
			tripDetails = await Trips.find({
				$and: [
					{ dvrid: driverId },
					{
						tripFDT: {
							$gte: packageDetails.startDate,
							$lte: packageDetails.endDate
						}
					}
				]
			}, {}).exec(); //tripFDT
		}

		//check tripDetails array not empty
		//if empty no trip yet neet to change endDate
		//else had some trip no changes
		if (tripDetails.length != 0) {
			if (requestFrom == 'fromCron') {
				Driver.update({ _id: driverId }, { isSubcriptionActive: false, subcriptionEndDate: null }, { multi: true }).exec();

				if (!firebase.apps.length) {
					firebase.initializeApp(config.firebasekey);
				}
				var db = firebase.database();
				var ref = db.ref("drivers_data");
				driverIds.map(function (items) {
					var child = items.toString();
					var usersRef = ref.child(child);
					usersRef.update(requestData);

					usersRef.update(requestData, function (error) {
						if (error) { console.log(error); } else {
							console.log("updateDriverCreditsInFB");
						}
					});
				})
			} else {
				console.log("Driver had some trip, no need to change anything");
			}

		} else { // update  end date on mongo (driverpackage,driver) and fb
			updateSubscriptionEndDateMongoAndFb(driverId, tripDate, puchaseID);
		}
	} else {
		console.log("Driver don't have subscription concept no need to change anything");
	}
}


async function updateFareDetailsInTrip (fare, reqdata, tripDetails, dropAddress, DriverPaymentDetailsSplits) {
	var paymentStatus = 'Paid'; var balancetopay = 0;
	if (fare.paymentMode == 'card' && featuresSettings.riderTripPaidInClientSide) {
		paymentStatus = 'pending';
		balancetopay = Number(fare.totalFare);
	}

	var totalDeductedFare = Number(fare.DetuctedFare) ? Number(fare.DetuctedFare) : 0;
	if (Number(fare.mandatorydiscountAmt) > 0) {
		totalDeductedFare = totalDeductedFare + Number(fare.mandatorydiscountAmt) ? Number(fare.mandatorydiscountAmt) : 0;
	}
	if (Number(fare.discountAmt) > 0) {
		totalDeductedFare = totalDeductedFare + Number(fare.discountAmt);
	}

	var updateCashData = {
		status: "Finished",
		"acsp.base": fare.BaseFare ? fare.BaseFare : 0,
		"acsp.minFare": fare.minFare ? fare.minFare : 0,
		"acsp.dist": fare.distance,
		"acsp.distfare": Number(fare.KMFare),
		"acsp.time": fare.travelTime,
		"acsp.timefare": Number(fare.travelFare) ? Number(fare.travelFare) : 0,
		"acsp.conveyance": Number(fare.pickupCharge) ? Number(fare.pickupCharge) : 0,
		"acsp.waitingCharge": Number(fare.waitingFare) ? Number(fare.waitingFare) : 0,
		"acsp.waitingTime": Number(fare.waitingTime) ? Number(fare.waitingTime) : 0,
		"acsp.totalFareWithOutOldBal": Number(fare.totalFareWithOutOldBal),
		"acsp.fareAmtBeforeSurge": Number(fare.fareAmtBeforeSurge),
		"acsp.oldBalance": Number(fare.oldCancellationAmt),
		"acsp.cashpaid": Number(fare.totalFare), //As cash have to pay fare.balance all by amt
		"acsp.comison": Number(fare.comisonAmt),
		"acsp.promoamt": Number(fare.discountAmt) ? Number(fare.discountAmt) : 0,
		"acsp.walletdebt": Number(fare.DetuctedFare) ? Number(fare.DetuctedFare) : 0,
		"acsp.carddebt": Number(fare.DetuctedFare) ? Number(fare.DetuctedFare) : 0,
		"acsp.hotelcommision": Number(fare.hotelcommisionAmt) ? Number(fare.hotelcommisionAmt) : 0,
		"acsp.outstanding": 0,
		"acsp.bal": balancetopay, //0 Consider Paid Full (Even for Digital)
		"acsp.detect": totalDeductedFare,
		"acsp.costBeforeDiscount": Number(fare.totalFare) + Number(fare.discountAmt),
		"acsp.actualcost": Number(fare.totalFare),
		"acsp.fareType": fare.fareType,
		"fare": Number(fare.totalFare),
		"acsp.cost": Number(fare.totalFare),
		"acsp.tax": Number(fare.tax) ? Number(fare.tax) : 0,
		"acsp.taxPercentage": Number(fare.taxPercentage),
		"acsp.via": fare.paymentMode ? fare.paymentMode : 'cash',
		"acsp.chId": Number(fare.tranxid) ? Number(fare.tranxid) : "",
		"acsp.isNight": fare.nightObj.isApply,
		"acsp.isPeak": fare.peakObj.isApply,
		"acsp.nightPer": fare.nightObj.percentageIncrease ? fare.nightObj.percentageIncrease : 0,
		"acsp.peakPer": fare.peakObj.percentageIncrease ? fare.peakObj.percentageIncrease : 0,
		"adsp.end": reqdata.body.endTime,
		"adsp.to": reqdata.body.endAddress ? reqdata.body.endAddress : dropAddress,
		"adsp.dLat": Number(reqdata.body.dropLat) ? Number(reqdata.body.dropLat) : tripData.dsp.endcoords[1],
		"adsp.dLng": Number(reqdata.body.dropLng) ? Number(reqdata.body.dropLng) : tripData.dsp.endcoords[0],
		"applyValues": fare.applyValues,
		"acsp.endTime": GFunctions.getISODate(),
		"paymentSts": paymentStatus,
		"acsp.discountName": fare.discountName ? fare.discountName : "",
		"acsp.discountPercentage": fare.discountPercentage ? fare.discountPercentage : 0,
		"additionalFee": fare.additionalFee ? fare.additionalFee : null,
		//India GST
		/* "acsp.fare1": fare.indiaGSTAmounts.totalfare1,
		"acsp.fare2": fare.indiaGSTAmounts.totalfare2,
		"acsp.tax1": fare.indiaGSTAmounts.gst1On1,
		"acsp.tax2": fare.indiaGSTAmounts.gst2On2,
		"acsp.taxper1": fare.indiaGSTAmounts.gstPerOn1,
		"acsp.taxper2": fare.indiaGSTAmounts.gstPerOn2, */
		//India GST

	}

	if (reqdata.body.endFromAdmin != undefined || typeof reqdata.body.endFromAdmin != "undefined") {
		updateCashData['acsp.startTime'] = reqdata.body.startTime ? reqdata.body.startTime : tripDetails.acsp.startTime;
		updateCashData['acsp.endTime'] = reqdata.body.endTime ? reqdata.body.endTime : GFunctions.getISODate();
		updateCashData["adsp.pLat"] = Number(reqdata.body.pickupLat) ? Number(reqdata.body.pickupLat) : tripDetails.adsp.pLat;
		updateCashData["adsp.pLng"] = Number(reqdata.body.pickupLng) ? Number(reqdata.body.pickupLng) : tripDetails.adsp.pLng;
		updateCashData["adsp.from"] = reqdata.body.fromAddress ? reqdata.body.fromAddress : tripDetails.adsp.from;
	}

	var ispaid = "no";
	var todvr = "no";
	if (featuresSettings.driverPayouts.adminCommision == 'driverWallet' && featuresSettings.driverPayouts.payoutType == 'driverPrepaidWallet') {
		if (fare.paymentMode == "cash") {
			ispaid = "yes";
			todvr = "yes";
		}
		if (fare.paymentMode == "card" && Number(fare.DetuctedFare) == 0) {
			ispaid = "yes";
			todvr = "yes";
		}
		if (fare.paymentMode == "Wallet" && Number(fare.DetuctedFare) == 0) {
			ispaid = "yes";
			todvr = "yes";
		}
	} else {
		var ispaid = "no";
		var todvr = "no";
	}

	var DriverPaymentDetails = {
		tripno: tripDetails.tripno,
		driver: tripDetails.dvrid,
		dvrfname: tripDetails.dvr,
		amttopay: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.amttopay), //should be real total
		cashpaid: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.cashpaid), //should be cash
		commision: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.commision),
		promoamt: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.promoamt),
		amttodriver: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.amttodriver),
		walletdebt: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.walletdebt),
		carddebt: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.carddebt),
		digital: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.digital),
		outstanding: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.outstanding),
		inhand: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.inhand),
		toSettle: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.toSettle),
		mtd: fare.paymentMode,
		ispaid: ispaid,
		todvr: todvr,
		chId: ""
	}
	// console.log(updateCashData)
	// Trips.findOneAndUpdate({ _id: tripDetails._id, status: { $ne: "Finished" } }, updateCashData, { 'new': true },
	try {
		var doc = await Trips.findOneAndUpdate({ _id: tripDetails._id }, updateCashData, { 'new': true });
		// function (err, doc) {
		if (!doc) {
			return false;
		} else {
			// if (doc) {
			addDriverPayment(DriverPaymentDetails); //Called after Successful Transaction
			// debitDriverBankTransactions(DriverPaymentDetails.driver, DriverPaymentDetails.toSettle, DriverPaymentDetails.tripno, DriverPaymentDetails.commision);
			if (tripDetails.hotelid != null) {
				Hotel.findById({ _id: tripDetails.hotelid }, {}, (err, data) => {
					data.amount += parseFloat(fare.hotelcommisionAmt);
					data.save((err, data) => {
						var hotelPaymentDetails = {
							tripno: tripDetails.tripno,
							hotel: data._id,
							hotelname: data.fname,
							amttopay: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.amttopay), //should be real total
							commision: GFunctions.sendFormatedNumber(DriverPaymentDetailsSplits.commision),
							amttohotel: fare.hotelcommisionAmt,
							mtd: fare.paymentMode,
							toHotel: ispaid,
							chId: ""
						}
						addHotelPayment(hotelPaymentDetails)
					});

				})
			}
			if (featuresSettings.calculateDriverEarningAtEveryDay) {
				updateDriverEarningAtEveryDay(DriverPaymentDetails.driver, DriverPaymentDetails.amttopay);
			}
			updateTripFinalDataInFirebase(doc, fare);
			logger.info('updateFareDetailsInTrip', 1);
			return true
			// }
		}
		// }
		// );
	} catch (err) {
		console.log(err)
		return false
	}
}

function updateTripFinalDataInFirebase (tripDoc, fare) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("trips_data"); //Todo

	var isNight = "0";
	var isPickup = "0";
	var isPeak = "0";
	var isWaiting = tripDoc.applyValues.applyWaitingTime;
	var isTax = tripDoc.applyValues.applyTax;
	if (tripDoc.applyValues.applyNightCharge && tripDoc.csp.isNight) {
		isNight = fare.nightObj.alertLable;
	}
	if (tripDoc.applyValues.applyPeakCharge && tripDoc.csp.isPeak) {
		isPeak = fare.peakObj.alertLable;
	}

	if (Number(tripDoc.acsp.conveyance) > 0) {
		isPickup = '1';
	}

	var distance_fare = tripDoc.acsp.distfare;
	if (distance_fare == null) {
		distance_fare = tripDoc.csp.distfare;
	}

	/* var time = tripDoc.acsp.time;
	if (time == null) {
		time = tripDoc.csp.time;
	} */

	// var time = fare.waitingTime;
	var time = tripDoc.acsp.time ? tripDoc.acsp.time : tripDoc.estTime;
	var duration = tripDoc.acsp.time ? tripDoc.acsp.time : tripDoc.estTime;
	if (time == null) {
		// time = tripDoc.estTime;
		// tripDoc.acsp.time ? tripDoc.acsp.time : tripDoc.csp.estTime,
		time = 0;
	}

	var requestData = {
		basefare: fare.BaseFare,
		Drop_address: tripDoc.adsp.to ? tripDoc.adsp.to : tripDoc.dsp.end,
		cancel_fare: "0", //Todo
		cancelby: "0", //Todo
		convance_fare: tripDoc.csp.conveyance ? tripDoc.csp.conveyance : 0,
		datetime: "0", //Todo
		discount: tripDoc.acsp.detect ? tripDoc.acsp.detect : 0,
		distance: tripDoc.acsp.dist ? tripDoc.acsp.dist : tripDoc.dsp.distanceKM,
		distance_fare: distance_fare,
		driver_alavance_dis: "0", //Todo
		driver_rating: "0", //Todo
		duration: duration,
		isNight: isNight,
		isPickup: isPickup,
		isWaiting: isWaiting ? '1' : '0',
		isTax: isTax ? '1' : '0',
		ispay: fare.BalanceFare,
		isPeak: isPeak,
		pay_type: tripDoc.acsp.via ? tripDoc.acsp.via : tripDoc.asp.via,
		pickup_address: tripDoc.adsp.from ? tripDoc.adsp.from : tripDoc.dsp.start,
		rider_rating: 0,//Todo
		status: "4",
		tax: tripDoc.acsp.tax ? tripDoc.acsp.tax : tripDoc.csp.tax,
		time: time,
		time_fare: tripDoc.acsp.timefare ? tripDoc.acsp.timefare : tripDoc.csp.timefare,
		waiting_fare: tripDoc.acsp.waitingCharge ? tripDoc.acsp.waitingCharge : 0,
		total_fare: tripDoc.fare,
		trip_type: tripDoc.acsp.fareType ? tripDoc.acsp.fareType : 'kmrate',
		waitingTime: tripDoc.acsp.waitingTime ? tripDoc.acsp.waitingTime : 0,
		inSecondaryCur: fare.inSecondaryCur ? fare.inSecondaryCur : 0,
		// additionalFee:  fare.additionalFee ? JSON.stringify(fare.additionalFee) : '',
		discountName: fare.discountName ? fare.discountName : "",
		discountPercentage: fare.discountPercentage ? fare.discountPercentage : 0,
	};

	if (featuresSettings.addAdditionalFaresInTrip) {
		var additionalFee = addAdditionalFaresInTrip(fare.additionalFee);
		requestData = Object.assign(requestData, additionalFee);
	}

	requestData = convertAllNumbersToString(requestData);

	var child = (tripDoc.tripno).toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData, function (error) {
		if (error) { } else { }
	});
}

function addAdditionalFaresInTrip (arrayValue) {
	var newAdditionalObj = {};
	if (arrayValue) {
		//Is parse needed ?
		arrayValue.forEach((element, index, array) => {
			newAdditionalObj[element.name] = element.amount;
		});
	}
	return newAdditionalObj;
}

export const tripPaymentStatus = async (req, res) => {
	try {
		var cashpaid = 0;
		if (config.paymentGateway.paymentGatewayName == 'braintree') {
			let riderDocs = await Rider.findOne({ _id: req.userId });
			var nonceFromTheClient = req.body.payment_method_nonce;
			var tripAmount = req.body.tripAmount;
			var msg = 'Trip Payment - ' + req.body.trip_id;
			var data = {
				firstName: riderDocs.fname,
				lastName: riderDocs.lname,
				phone: riderDocs.phone,
				email: riderDocs.email,
			};
			var resObj = await paymentCtrl.transferTripAmountUsingNonce(nonceFromTheClient, tripAmount, msg, data);
			if (resObj) {
				req.body.id = resObj.id;
				req.body.amount = resObj.amount;
			} else {
				req.body.id = '';
				req.body.amount = 0;
				cashpaid = req.body.tripAmount;
			}
		}

		if (config.paymentGateway.paymentGatewayName == 'paytm') {
			let riderDocs = await Rider.findOne({ _id: req.userId });
			var tripAmount = req.body.tripAmount;
			var msg = 'Trip Payment - ' + req.body.trip_id;
			var data = {
				firstName: riderDocs.fname,
				lastName: riderDocs.lname,
				phone: riderDocs.phone,
				email: riderDocs.email,
			};
			req.body.id = '';
			req.body.amount = req.body.tripAmount;
			cashpaid = req.body.tripAmount;

			updateTripFinalStatusForPaytmDataInFirebase(req.body.trip_id);
		}

		var updateData = {
			'paymentSts': 'Paid',
			'acsp.chId': req.body.id,
			"acsp.carddebt": Number(req.body.amount) ? Number(req.body.amount) : 0,
			"acsp.cashpaid": cashpaid,
			"acsp.bal": 0,
			"acsp.detect": Number(req.body.amount) ? Number(req.body.amount) : 0,
		}
		let docs = await Trips.findOneAndUpdate({ tripno: req.body.trip_id }, updateData, { new: true });
		if (!docs) {
			return res.status(409).json({
				'success': false, 'message': req.i18n.__("TRIP_DETAILS_NOT_FOUND")
			});
		} else {
			return res.json({
				'success': true, 'message': req.i18n.__("TRIP_PAYMENT_UPDATED")
			});
		}
	} catch (err) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("FAILED_UPDATING_PAYMENT_STATUS"), 'error': err });
	}
}

function updateTripFinalStatusForPaytmDataInFirebase (tripno) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("trips_data"); //Todo

	var requestData = {
		status: "6",
	};

	var child = (tripno).toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData, function (error) {
		if (error) { } else { }
	});
}

/**
 * Detect Payment From Wallet or Stripe and Update bal and return remaining balance
 * @param  {[type]} tripDetails [description]
 * @param  {[type]} fare        [description]
 * @param  {[type]} reqdata     [description]
 * @param  {[type]} riderdoc    [description]
 * @param  {[type]} res         [description]
 * @return {[type]}             balancetopay
 */
async function updateActualFare (tripDetails, fare, reqdata, riderdoc, res) {
	var promoamt = fare.promoamt;
	const newfare = GFunctions.sendFormatedNumber(fare.balance);

	//Driver Payment Initial
	var dpcashpaid = newfare;
	var dpwalletdebt = 0;
	var dpstripedebt = 0;
	var dpdigital = parseFloat(fare.promoamt);
	var dpoutstanding = 0;
	var dpinhand = 0;
	var dpamttodriver = 0;
	var dptoSettle = 0;

	//Normal Cash
	var updateCashData = {
		status: "Finished",
		"acsp.base": fare.basefare,
		"acsp.dist": fare.distance,
		"acsp.distfare": fare.distanceFare,
		"acsp.time": fare.time,
		"acsp.timefare": fare.timeFare,
		"acsp.amttopay": fare.subtotal,
		"acsp.cashpaid": fare.balance, //As cash have to pay fare.balance all by amt
		"acsp.commision": fare.commision,
		"acsp.comison": fare.comison,
		"acsp.promoamt": fare.promoamt,
		"acsp.walletdebt": 0,
		"acsp.stripedebt": 0,
		"acsp.outstanding": 0,
		"acsp.bal": 0, //0 Consider Paid Full
		"acsp.detect": fare.promoamt,
		"acsp.actualcost": fare.subtotal,
		"acsp.cost": GFunctions.sendFormatedNumber(fare.balance),
		"acsp.via": fare.paymentMode,
		"acsp.chId": "",

		"adsp.end": reqdata.endTime,
		"adsp.to": reqdata.endAddress,
		"adsp.dLat": reqdata.dropLat,
		"adsp.dLng": reqdata.dropLng,
		"paymentSts": "Paid"
	}
	var updateData = updateCashData;

	//Wallet Mode
	if (fare.paymentMode == "wallet") {
		var walletRes = await findNChargeExistingUserWallet(tripDetails.ridid, tripDetails.id, newfare);

		if (walletRes) { //chk is res is success also
			var balancetopay = walletRes.balancetopay;
			fare.balance = balancetopay;
			fare.detectedAmt = walletRes.detectedAmt;
			var updateWalletData = {
				status: "Finished",
				"acsp.base": fare.basefare,
				"acsp.dist": fare.distance,
				"acsp.distfare": fare.distanceFare,
				"acsp.time": fare.time,
				"acsp.timefare": fare.timeFare,
				"acsp.amttopay": fare.subtotal,
				"acsp.cashpaid": 0, //As no cash has paid
				"acsp.commision": fare.commision,
				"acsp.comison": fare.comison,
				"acsp.promoamt": fare.promoamt,
				"acsp.walletdebt": walletRes.detectedAmt,
				"acsp.stripedebt": 0,
				"acsp.outstanding": balancetopay,
				"acsp.bal": balancetopay, //balancetopay : Amount to pay
				"acsp.detect": parseFloat(walletRes.detectedAmt) + parseFloat(fare.promoamt),
				"acsp.actualcost": fare.subtotal,
				"acsp.cost": newfare,
				"acsp.via": fare.paymentMode,
				"adsp.end": reqdata.endTime,
				"adsp.to": reqdata.endAddress,
				"adsp.dLat": reqdata.dropLat,
				"adsp.dLng": reqdata.dropLng,
				"paymentSts": "Paid",
				"acsp.chId": walletRes.tranxid
			}
			dpwalletdebt = walletRes.detectedAmt;
		} else {
			var updateWalletData = {
				status: "Finished",
				"acsp.base": fare.basefare,
				"acsp.dist": fare.distance,
				"acsp.distfare": fare.distanceFare,
				"acsp.time": fare.time,
				"acsp.timefare": fare.timeFare,
				"acsp.amttopay": fare.subtotal,
				"acsp.cashpaid": 0, //As no cash has paid
				"acsp.commision": fare.commision,
				"acsp.comison": fare.comison,
				"acsp.promoamt": fare.promoamt,
				"acsp.walletdebt": 0,
				"acsp.stripedebt": 0,
				"acsp.outstanding": newfare,
				"acsp.bal": newfare, //balancetopay : Amount to pay
				"acsp.detect": parseFloat(fare.promoamt),
				"acsp.actualcost": fare.subtotal,
				"acsp.cost": newfare,
				"acsp.via": fare.paymentMode,
				"adsp.end": reqdata.endTime,
				"adsp.to": reqdata.endAddress,
				"adsp.dLat": reqdata.dropLat,
				"adsp.dLng": reqdata.dropLng,
				"paymentSts": "Paid",
				"acsp.chId": walletRes.tranxid
			}
		}
		var updateData = updateWalletData;

		dpdigital = dpdigital + parseFloat(dpwalletdebt);
		dpoutstanding = parseFloat(fare.subtotal) - parseFloat(dpdigital);
		dpcashpaid = 0;
	} //Wallet Mode

	//Stripe Mode
	if (fare.paymentMode == "card") {
		var stripeRes = await findNChargeExistingUserCard(tripDetails.ridid, tripDetails.id, newfare);

		if (stripeRes) { //chk is res is success also
			var balancetopay = stripeRes.balancetopay;
			fare.balance = balancetopay;
			fare.detectedAmt = stripeRes.detectedAmt;
			var updateStripeData = {
				status: "Finished",
				"acsp.base": fare.basefare,
				"acsp.dist": fare.distance,
				"acsp.distfare": fare.distanceFare,
				"acsp.time": fare.time,
				"acsp.timefare": fare.timeFare,
				"acsp.amttopay": fare.subtotal,
				"acsp.cashpaid": 0, //As no cash has paid
				"acsp.commision": fare.commision,
				"acsp.comison": fare.comison,
				"acsp.promoamt": fare.promoamt,
				"acsp.walletdebt": 0,
				"acsp.stripedebt": stripeRes.detectedAmt,
				"acsp.outstanding": balancetopay,
				"acsp.bal": balancetopay, //balancetopay : Amount to pay
				"acsp.detect": parseFloat(stripeRes.detectedAmt) + parseFloat(fare.promoamt),
				"acsp.actualcost": fare.subtotal,
				"acsp.cost": newfare,
				"acsp.via": fare.paymentMode,
				"adsp.end": reqdata.endTime,
				"adsp.to": reqdata.endAddress,
				"adsp.dLat": reqdata.dropLat,
				"adsp.dLng": reqdata.dropLng,
				"paymentSts": "Paid",
				"acsp.chId": stripeRes.tranxid
			}
			dpstripedebt = stripeRes.detectedAmt;
		} else {
			var updateStripeData = {
				status: "Finished",
				"acsp.base": fare.basefare,
				"acsp.dist": fare.distance,
				"acsp.distfare": fare.distanceFare,
				"acsp.time": fare.time,
				"acsp.timefare": fare.timeFare,
				"acsp.amttopay": fare.subtotal,
				"acsp.cashpaid": 0, //As no cash has paid
				"acsp.commision": fare.commision,
				"acsp.comison": fare.comison,
				"acsp.promoamt": fare.promoamt,
				"acsp.walletdebt": 0,
				"acsp.stripedebt": 0,
				"acsp.outstanding": newfare,
				"acsp.bal": newfare, //balancetopay : Amount to pay
				"acsp.detect": parseFloat(fare.promoamt),
				"acsp.actualcost": fare.subtotal,
				"acsp.cost": newfare,
				"acsp.via": fare.paymentMode,
				"adsp.end": reqdata.endTime,
				"adsp.to": reqdata.endAddress,
				"adsp.dLat": reqdata.dropLat,
				"adsp.dLng": reqdata.dropLng,
				"paymentSts": "Paid",
				"acsp.chId": walletRes.tranxid
			}
		}
		var updateData = updateStripeData;

		dpcashpaid = 0;
		dpdigital = dpdigital + parseFloat(dpstripedebt);
		dpoutstanding = parseFloat(fare.subtotal) - parseFloat(dpdigital);
	} //Stripe Mode

	dpinhand = parseFloat(dpcashpaid) + parseFloat(dpoutstanding);
	dptoSettle = parseFloat(fare.subtotal) - parseFloat(fare.commision) - parseFloat(dpinhand);
	dpamttodriver = parseFloat(fare.subtotal) - parseFloat(fare.commision);

	var DriverPaymentDetails = {
		tripno: tripDetails.tripno,
		driver: tripDetails.dvrid,
		dvrfname: tripDetails.dvr,
		amttopay: GFunctions.sendFormatedNumber(fare.subtotal), //should be real total
		cashpaid: GFunctions.sendFormatedNumber(dpcashpaid), //should be cash
		commision: fare.commision,
		promoamt: GFunctions.sendFormatedNumber(fare.promoamt),
		walletdebt: GFunctions.sendFormatedNumber(dpwalletdebt),
		stripedebt: GFunctions.sendFormatedNumber(dpstripedebt),
		digital: GFunctions.sendFormatedNumber(dpdigital),
		outstanding: GFunctions.sendFormatedNumber(dpoutstanding),
		inhand: GFunctions.sendFormatedNumber(dpinhand),
		amttodriver: GFunctions.sendFormatedNumber(dpamttodriver),
		toSettle: GFunctions.sendFormatedNumber(dptoSettle),
		mtd: fare.paymentMode,
		ispaid: "yes",
		todvr: "yes",
		chId: ""
	}

	debitDriverBankTransactions(DriverPaymentDetails.driver, DriverPaymentDetails.toSettle, DriverPaymentDetails.tripno, DriverPaymentDetails.commision);
	try {
		let docs = await Trips.findOneAndUpdate({ _id: tripDetails.id }, updateData, { new: true });
		if (!docs) {
			return res.status(409).json({
				'success': true, 'message': req.i18n.__("TRIP_ENDED"), "rider": riderdoc,
				"pickupdetails": tripDetails.dsp[0], "status": "Trip End", "fare": fare, "taxitype": tripDetails.taxi
			});
		} else {
			//add new to fares
			addDriverPayment(DriverPaymentDetails); //Called after Successful Transaction
			return res.json({
				'success': true, 'message': req.i18n.__("TRIP_ENDED"), "rider": riderdoc, "pickupdetails": tripDetails.dsp[0],
				"status": "Trip End", "fare": fare, "taxitype": tripDetails.taxi
			});
		}
	} catch (err) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
	}

}

/**
 * Find Stripe = If exists charge and return balance to pay with other details
 * @input
 * @param
 * @return retunObj
 * @response
 */
async function findNChargeExistingUserCard (userId, tripid, amt, commisionamount, driverId, tripno) {
	var retunObj = {
		success: false,
		balancetopay: amt,
		tranxid: '',
		detectedAmt: 0,
		addToWallet: true,
	}

	try {
		let docs = await Rider.findById(userId);
		if (!docs) {
			return 0;
		} else {
			var desc = "Trip -" + tripno;
			var res = false;

			if (featuresSettings.riderCard && config.paymentGateway.paymentGatewayName == 'stripe') {
				console.log('******* Inside    Stripe ******');
				if (featuresSettings.driverPayouts.driverStripeSplitPayoutDirectlyAtEveryTripEnd) {
					console.log('****** insidee stripe another if ***********');
					//Check he has active connect account if so split now
					// let docs = await Driver.findById(driverId);
					let driverBankDocs = await DriverBank.findOne({ driverId: driverId });
					if (driverBankDocs) {
						console.log('****** driver bank docs exists ***********');
						var driverConnectAcctId = driverBankDocs.bank.chid;
					}

					if (driverConnectAcctId) {
						//spliting Directly to admin and driver
						res = await paymentCtrl.chargeExistingUserCardWithInstantSplitToDriver(docs.card.id, desc, featuresSettings.defaultcur, amt, commisionamount, driverConnectAcctId);
						console.log('******** driverConnectAcctId ********',driverConnectAcctId);
						if (res.status) {
							console.log('******** res.status********',res.status);
							retunObj.addToWallet = false; // as we already transfered
						} else {
							res = await paymentCtrl.chargeExistingUserCard(docs.card.id, desc, featuresSettings.defaultcur, amt);
						}

						//instead of spliting Directly 1.Charge customer 2.Payto Driver instantly
						// res = await paymentCtrl.chargeExistingUserCard(docs.card.id, desc, featuresSettings.defaultcur, amt);
						// if (res.status){
						// 	var payoutRes = await paymentCtrl.makeInstantPayouts(desc, featuresSettings.defaultcur, amt, commisionamount, driverConnectAcctId );
						// 	if (payoutRes.status){
						// 		retunObj.addToWallet = false;
						// 	}
						// }
					}
					//else if no connect account add to his Wallet.
					else {
						res = await paymentCtrl.chargeExistingUserCard(docs.card.id, desc, featuresSettings.defaultcur, amt);
						console.log('*********no account addedd ********',res );
					}

				} else {
					//Charging from Card Id Supported By = Stripe
					res = await paymentCtrl.chargeExistingUserCard(docs.card.id, desc, featuresSettings.defaultcur, amt);
				}
			}

			if (featuresSettings.riderCard && config.paymentGateway.paymentGatewayName == 'paystack') {
				//Charging from Card Id Supported By = Paystack
				res = await paymentCtrl.chargeExistingUserCard(docs.card.id, desc, featuresSettings.defaultcur, amt, docs.email);
			}

			if (res.status) {
				var detectedAmt = (parseFloat(res.amount) / 100);
				retunObj.balancetopay = parseFloat(amt) - parseFloat(detectedAmt);  //Have to check with Live act if amt > detectedAmt
				retunObj.tranxid = res.id;
				retunObj.detectedAmt = detectedAmt;
				retunObj.success = true;
				if (retunObj.addToWallet == false) {
					findAndSendFCMToDriver(driverId, "Payment Success! Trip Amount has been transfered to your bank.");
				} else {
					findAndSendFCMToDriver(driverId, "Payment Success! Trip Amount " + detectedAmt + " has been transfered to your wallet.");
				}
			} else {
				console.log('******* Failed Scenario ***********');
				findAndSendFCMToDriver(driverId, "Payment Failed! Collect Trip Amount manually.");
			}
			return retunObj;
		}
	} catch (err) {
		return retunObj;
	}
}


/**
 * Find Wallet = If exists charge and return balance to pay with other details
 * @param  {[type]} userId [description]
 * @param  {[type]} tripid [description]
 * @param  {[type]} amt    [description]
 * @return {[obj]/0}        [description]
 */
async function findNChargeExistingUserWallet (userId, tripid, amt) {
	var retunObj = {
		success: false,
		balancetopay: amt,
		tranxid: '',
		detectedAmt: 0
	}

	try {
		let docs = await Wallet.findOne({ ridid: userId });
		if (!docs) {
			return retunObj;
		} else {
			//Wallet Available
			var walletbal = docs.bal;
			var balancetopay = amt;
			var oldbal = docs.bal;
			var charged = 0;
			if (oldbal > amt) { //If wallet has sufficient bal
				walletbal = (parseFloat(oldbal) - parseFloat(amt));
				balancetopay = 0;
				charged = amt;
			} else {
				walletbal = 0;
				balancetopay = (parseFloat(amt) - parseFloat(oldbal));
				charged = (parseFloat(amt) - parseFloat(balancetopay));
			}
			//Wallet Available
			//have to combine this func and save here itself : 1 find only
			var IsBalUpdated = await chargeWalletAndReturnBal(tripid, docs.id, walletbal, charged);
			if (IsBalUpdated) {
				retunObj.success = true;
				retunObj.balancetopay = balancetopay;
				retunObj.detectedAmt = charged;
				return retunObj;
			} else {
				retunObj.success = true;
				retunObj.balancetopay = amt;
				retunObj.detectedAmt = 0;
				return retunObj;
			}
		}
	} catch (err) {
		return retunObj;
	}
}

/**
 * Add Detail Payment Details to Driver Account
 * @param {[type]} doc     [description]
 * @param {[type]} fare    [description]
 * @param {[type]} reqBody [description]
 */
function addDriverPayment (details) {
	const newDoc = new DriverPayment(details);
	newDoc.save((err, docs) => {
		if (err) {
			logger.error(`addDriverPayment ${err}`);
		} else {
			// console.log(`addDriverPayment ${details}`);
		}
	})
}

/**
 * [chargeWalletAndReturnBal description]
 * @param  {[type]} tripid    [description]
 * @param  {[type]} walletId  [description]
 * @param  {[type]} walletbal [description]
 * @param  {[type]} charged   [description]
 * @return {[type]}           [description]
 */
async function chargeWalletAndReturnBal (tripid, walletId, walletbal, charged) {
	var updateData = {
		bal: walletbal
	}
	try {
		let docs = await Wallet.findOneAndUpdate({ _id: walletId }, updateData, { new: true });
		if (!docs) {
			return 0;
		} else {
			updateRiderWalletCreditsInRiderFirebase(docs.ridid, walletbal);
			logTranxHistory(tripid, charged, walletId);
			return 1;
		}
	} catch (err) {
		return 0;
	}
}

/**
 * Log Wallet Debit Details
 * @input
 * @param
 * @return
 * @response
 */
function logTranxHistory (tripid, amt, walletId) {
	var id = mongoose.Types.ObjectId();
	var tranxData = {
		_id: id,
		trxid: tripid,
		amt: amt,
		date: GFunctions.sendTimeNow(),
		type: 'Debit'
	}

	Wallet.findByIdAndUpdate(walletId, {
		$push: { trx: tranxData }
	}, { 'new': true },
		function (err, doc) {
			if (err) {
				return '';
			}
			return id;
		}
	);
}

//Update actual Pickup Location
function updateActualPickupFare (tripId, fare, reqdata) {
	var startTime = GFunctions.getISODate();
	var startTimeToHM = reqdata.startTime;
	if (reqdata.endFromAdmin == "admin") {
		startTime = reqdata.startTime
		startTimeToHM = moment(reqdata.startTime).format("HH:mm");
	}
	var update = {
		acsp: {
			base: 0,
			dist: 0,
			distfare: 0,
			time: 0,
			timefare: 0,
			comison: 0,
			cost: 0,
			startTime: startTime,
			via: "Cash"
		},
		adsp: { //Actual details split up
			start: startTimeToHM,
			end: "",
			from: reqdata.fromAddress, //From google
			to: "",
			pLat: reqdata.pickupLat,
			pLng: reqdata.pickupLng,
			dLat: 0,
			dLng: 0,
		},
		status: "Progress"
	}
	Trips.findOneAndUpdate({ tripno: tripId, status: { "$ne": "Progress" } }, update, { new: true }, (err, doc) => {
		if (err) { }
		logger.info('updateActualPickupFare');
	})
}

export const testWallet = (req, res) => {
	findNChargeExistingUserWallet(req.body.userId, req.body.tripid, req.body.amt);
}

/**
 * Send Accepted Trip Driver details to Rider
 * @input tripId
 * @param
 * @return
 * @response tripDriverDetails
 */
export const tripDriverDetailsOld = (req, res) => {

	Trips.findOne({ tripno: req.body.tripId }, function (err, doc) {
		if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND"), 'error': err });

		//Driver Profile 2
		var DriverDetailsAry = [];
		Driver.find({ _id: doc.dvrid }, { hash: 0, salt: 0 }).exec((err2, driverdocs) => {
			if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
			if (!driverdocs.length) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("DRIVER_NOT_AVAILABLE") }); }
			var DriverObj = {
				"fname": driverdocs[0].fname,
				"lname": driverdocs[0].lname,
				"email": driverdocs[0].email,
				"phone": driverdocs[0].phone,
				"phcode": driverdocs[0].phcode,
				"gender": driverdocs[0].gender,
				"cntyname": driverdocs[0].cntyname,
				"statename": driverdocs[0].statename,
				"cityname": driverdocs[0].cityname,
				"lang": driverdocs[0].lang,
				"cur": driverdocs[0].cur,
				"profileurl": config.baseurl + driverdocs[0].profile,
				"rating": driverdocs[0].rating.rating,
				"taxitype": doc.taxi,
			};

			DriverDetailsAry.push({ profile: DriverObj });
			// DriverDetailsAry.push({  rating : driverdocs[0].rating   });
			var taxi = driverdocs[0].taxis.id(driverdocs[0].currentTaxi);
			DriverDetailsAry.push({ currentActiveTaxi: taxi });
			// DriverDetailsAry.push({  profileurl: config.baseurl+driverdocs[0].profile  });
			return res.json({
				'success': true, 'message': req.i18n.__("TRIP_STATUS"), "driver": DriverDetailsAry, "pickupdetails": doc.dsp,
				"serviceType": doc.vehicle, "others1": driverdocs[0].others1, "startOTP": doc.tripOTP[0], "endOTP": doc.tripOTP[1]
			});
		})
	});
}

export const tripDriverDetails = (req, res) => {

	Trips.findOne({ tripno: req.body.tripId }, function (err, doc) {
		if (err) { return res.status(500).json({ 'success': false, 'message': err.message, 'err': err }); }
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND"), 'error': err });

		//Driver Profile 2
		var DriverDetailsAry = [];
		Driver.findOne({ _id: doc.dvrid }, { hash: 0, salt: 0 }).exec((err2, driverdocs) => {
			if (err2) { return res.status(500).json({ 'success': false, 'message': err2.message, 'err': err2 }); }
			if (!driverdocs) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("DRIVER_NOT_AVAILABLE") }); }
			var DriverObj = {
				"fname": driverdocs.fname,
				"lname": driverdocs.lname,
				"email": driverdocs.email,
				"phone": driverdocs.phone,
				"phcode": driverdocs.phcode,
				"gender": driverdocs.gender,
				"cntyname": driverdocs.cntyname,
				"statename": driverdocs.statename,
				"cityname": driverdocs.cityname,
				"lang": driverdocs.lang,
				"cur": driverdocs.cur,
				"profileurl": config.baseurl + driverdocs.profile,
				"rating": driverdocs.rating.rating,
				"taxitype": doc.vehicle,
			};

			DriverDetailsAry.push({ profile: DriverObj });
			// DriverDetailsAry.push({  rating : driverdocs[0].rating   });
			// var taxi = driverdocs[0].taxis.id(driverdocs[0].currentTaxi);

			driverdocs = driverdocs.toObject();
			var taxi = driverdocs.taxis.filter(function (el) {
				return el._id == driverdocs.currentTaxi;
			});
			delete taxi[0]['vehicletype'];
			taxi[0].vehicletype = doc.vehicle;

			DriverDetailsAry.push({ currentActiveTaxi: taxi[0] });
			// DriverDetailsAry.push({  profileurl: config.baseurl+driverdocs[0].profile  });
			return res.json({
				'success': true, 'message': req.i18n.__("TRIP_STATUS"), "driver": DriverDetailsAry,
				"pickupdetails": doc.dsp,
				"serviceType": doc.vehicle, "others1": driverdocs.others1,
				"startOTP": doc.tripOTP[0], "endOTP": doc.tripOTP[1]
			});
		})
	});
}

/**
 * Driver Feedback for Trip
 */
export const driverFeedback = (req, res) => {
	var update = {
		driverfb: {
			rating: req.body.rating,
			cmts: req.body.comments
		}
	}
	Trips.findOneAndUpdate({ tripno: req.body.tripId }, update, { new: false }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (doc) {
			freeTheDriver(doc.dvrid);
			UpdateRiderRating(req.body.rating, doc.ridid);
		}
		return res.json({ 'success': true, 'message': req.i18n.__("FEEDBACK_ADDED_SUCCESSFULLY") });
	})
}

/**
 * Update Rider overall ratings
 * @param {*} rating
 * @param {*} ridid
 */
function UpdateRiderRating (rating = 0, ridid) {
	Rider.findById(ridid, function (err, docs) {
		if (err) { } else if (!docs) { }
		else {
			let oldrating = parseFloat(docs.rating.rating);
			let oldnos = docs.rating.nos;
			let rate = oldrating * oldnos;
			oldnos++;
			if (parseFloat(rating) > 0) docs.rating.rating = ((rate + parseFloat(rating)) / oldnos).toFixed(2);
			docs.rating.nos = oldnos;
			docs.save(function (err, op) {
				if (err) { }
				else { console.log("UpdateRiderRating", rating); }
			})
		}
	});
}

/**
 * Rider Feedback for Trip
 * @param {*} req
 * @param {*} res
 */
export const riderFeedback = (req, res) => {
	var update = {
		riderfb: {
			rating: req.body.rating,
			cmts: req.body.comments
		}
	}
	Trips.findOneAndUpdate({ tripno: req.body.tripId }, update, { new: false }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if(doc != null && doc.dvrid != null){ 
		UpdateDriverRating(req.body.rating, doc.dvrid);
		}
		return res.json({ 'success': true, 'message': req.i18n.__("FEEDBACK_ADDED_SUCCESSFULLY") });
	})
}

/**
 * Update Driver Overall rating
 * @param {*} rating
 * @param {*} dvrid
 */
function UpdateDriverRating (rating = 0, dvrid) {
	Driver.findById(dvrid, function (err, docs) {
		if (err) { } else if (!docs) { }
		else {
			let oldnos = docs.rating.nos;
			let rate = parseFloat(docs.rating.rating) * oldnos;
			oldnos++;
			if (parseFloat(rating) > 0) docs.rating.rating = ((rate + parseFloat(rating)) / oldnos).toFixed(2);
			docs.rating.nos = oldnos;
			if (rating == 5 || rating == "5") { let starnos = docs.rating.star; starnos++; docs.rating.star = starnos; }
			docs.save(function (err, op) {
				if (err) { }
				else { console.log("UpdateRiderRating", rating); }
			})
		}
	});
}

export const clearStatus = (req, res) => {
	var update = {
		curStatus: "free"
	}
	Driver.findOneAndUpdate({ _id: req.body.driverid }, update, { new: true }, (err, doc) => {
		if (err) { }
		return res.json({ 'success': true, 'message': req.i18n.__("STATUS_SET_TO_FREE") });
	})
}

function findAndSendFCMToRider (userId, msg, content) {
	Rider.findById(userId, function (err, docs) {
		if (err) { }
		else {
			if (docs.fcmId) GFunctions.sendFCMMsg(docs.fcmId, msg, content);
		}
	});
}

export const findAndSendFCMToAdmin = (userId, msg) => {
	userId = mongoose.Types.ObjectId(userId);
	Admin.findById(userId, function (err, docs) {
		if (err) { console.log(err) }
		else {
			if (docs) {
				if (docs.fcmId) GFunctions.sendAdminPushMsg(docs.fcmId, msg);
			}
		}
	});
}

export const fcmp = (req, res) => {
	GFunctions.sendFCMMsg(req.body.fcmId, "RebuStar app", "test hi");
	console.log(fcmp);
}

/**
 * Rider Finished trip History
 */
export const riderTripHistory = (req, res) => {
	var pageQuery = HelperFunc.paginationBuilder(req.query);
	var skip = pageQuery.skip,
		limit = pageQuery.take;

	Trips.find({
		ridid: req.userId,
		/* $and: [
			// { $or: [{status:  "Finished" }, { status : "Cancelled" }] }
			{ $or: [{ status: "Finished" }] }
		] */
		status: { $in: ['processing', 'Finished', 'accepted', 'Progress'] },
	}, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0 },
		{ sort: { createdAt: -1 } }
	)
		.limit(limit)
		.skip(skip)
		.exec((err, docs) => {
			if (err) {
				return res.status(409).json([]);
			}
			return res.json(docs);
		});
}

/* export const riderTripHistory = (req, res) => {
	Trips.find({
		ridid: req.userId, $and: [
			// { $or: [{status:  "Finished" }, { status : "Cancelled" }] }
			{ $or: [{ status: "Finished" }] }
		]
	}, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0, acsp: 0 },
		{ sort: { createdAt: -1 } }
	).exec((err, docs) => {
		if (err) {
			return res.status(409).json([]);
		}
		return res.json(docs);
	});
} */

/**
 * Driver Finished History
 * @param {*} req
 * @param {*} res
 */
export const driverTripHistory = (req, res) => {
	var pageQuery = HelperFunc.paginationBuilder(req.query);
	var skip = pageQuery.skip,
		limit = pageQuery.take;

	var whereQuery = {
		dvrid: req.userId, $and: [
			// { $or: [{status:  "Finished" }, { status : "Cancelled" }] }
			{ $or: [{ status: "Finished" }] }
		]
	};
	if (req.body.date) {
		var baseData = 'createdAt'; //  tripFDT/createdAt

		// var fromDate = moment(req.body.date, 'MMMM D, YYYY' ).format('YYYY-MM-DD');
		// var toDate = moment(req.body.date, 'MMMM D, YYYY').add(1, 'days').format('YYYY-MM-DD');

		var fromDate = moment(req.body.date, ['MMMM D, YYYY', 'D, MMMM YYYY']).format('YYYY-MM-DD');
		var toDate = moment(req.body.date, ['MMMM D, YYYY', 'D, MMMM YYYY']).add(1, 'days').format('YYYY-MM-DD');

		whereQuery[baseData] = { $gte: new Date(fromDate), $lte: new Date(toDate) };
	}


	Trips.find(whereQuery, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0 },
		{
			// skip:0, // Starting Row
			// limit:10, // Ending Row
			sort: {
				createdAt: -1 //Sort by Id Added DESC
			}
		}
	)
		.limit(limit)
		.skip(skip)
		.exec((err, docs) => {
			if (err) {
				return res.status(409).json([]);
			}
			return res.json(docs);
		});
}

/* export const driverTripHistory = (req, res) => {
	Trips.find({
		dvrid: req.userId, $and: [
			// { $or: [{status:  "Finished" }, { status : "Cancelled" }] }
			{ $or: [{ status: "Finished" }] }
		]
	}, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0, acsp: 0 },
		{
			// skip:0, // Starting Row
			// limit:10, // Ending Row
			sort: {
				createdAt: -1 //Sort by Id Added DESC
			}
		}
	).exec((err, docs) => {
		if (err) {
			return res.status(409).json([]);
		}
		return res.json(docs);
	});
} */

/**
 * Driver Current Ratings
 * @input
 * @param
 * @return
 * @response
 */
export const myRatings = (req, res) => {
	var obj = [];
	Driver.find({ _id: req.userId }, {}).exec((err, docs) => {
		if (err) {
			return res.status(409).json([]);
		}
		if (docs.length) {
			obj.push(docs[0].rating);
			return res.json(obj);
		}
	});
}

/**
 * Driver : Feedbacks to Driver //Sort By DESC
 * @input
 * @param
 * @return
 * @response
 */
export const feedbackLists = (req, res) => {
	Trips.aggregate([
		// { "$match": { "dvrid": req.userId  } },
		{ "$match": { "dvrid": new mongoose.Types.ObjectId(req.userId), "riderfb.cmts": { $exists: true, $ne: "" } } },
		{ "$sort": { _id: -1 } },
		{
			"$lookup": {
				"localField": "ridid",
				"from": "riders",
				"foreignField": "_id",
				"as": "userinfo"
			}
		},
		{ "$unwind": "$userinfo" },
		{
			"$project": {
				"tripno": 1,
				"riderfb": 1,
				"userinfo.fname": 1,
				"userinfo.profile": 1 //Full path
			}
		}
	], function (err, result) {
		if (err) {
			return res.status(409).json();
		}
		return res.json({ "feedbacks": result, "profileurl": config.baseurl });
	});
}


/**
 * Rider Cancelled the Ongoing Trip //Need to add cancelation fee
 * @input
 * @param
 * @return
 * @response
 */
export const cancelCurrentTrip = (req, res) => {
	var update = {
		status: "Cancelled",
		review: constantsValues.cancelTaxiByUser,
		needClear: "no"
	}

	Trips.findOneAndUpdate({ tripno: req.body.tripId }, update, { new: true }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		// console.log(doc);
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_ACTIVE_TRIP_FOUND") });
		notifyRider(doc.ridid, constantsValues.cancelTaxiByUser, doc.id, "Cancelled");
		updateStatusInFirebase(req.body.tripId, "5");
		addCancelationStepsToRider(doc.ridid, req.body.tripId);
		changeMyTripStatusMongo(doc.dvrid, doc.tripno, "free");
		changeRiderTripStatusMongo(doc.ridid, doc.tripno, "free");
		findAndSendFCMToDriver(doc.dvrid, "Trip Cancelled", 'cancelCurrentTrip');
		return res.json({ 'success': true, 'message': req.i18n.__("TAXI_CANCELLED_SUCCESSFULLY"), "tripId": req.body.tripId });
	})
}

export const cancelCurrentTripFromAdmin = (req, res) => {
	var update = {
		status: "Cancelled",
		review: "admin Cancelled",
		needClear: "no"
	}

	Trips.findOneAndUpdate({ tripno: req.body.tripId }, update, { new: true }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		// console.log(doc);
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_ACTIVE_TRIP_FOUND") });
		notifyRider(doc.ridid, "admin Cancelled", doc.id, "Cancelled");
		updateStatusInFirebase(req.body.tripId, "5");
		// addCancelationStepsToRider(doc.ridid, req.body.tripId);
		changeMyTripStatusMongo(doc.dvrid, doc.tripno, "free");
		changeRiderTripStatusMongo(doc.ridid, doc.tripno, "free");
		findAndSendFCMToDriver(doc.dvrid, "Trip Cancelled", 'cancelCurrentTrip');
		return res.json({ 'success': true, 'message': labels.cancelTripByRider, "tripId": req.body.tripId });
	})
}

export function updateStatusInFirebase (tripid, status) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("trips_data");
	var requestData = {
		status: status,
	};
	var child = tripid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
}


export const changeRiderTripStatusMongo = async (riderId, tripno, msg = "free") => {
	Rider.findById(riderId, function (err, docs) {
		if (err) {
			logger.error("changeRiderTripStatusMongo", err);
		} else if (!docs) {
			logger.info("changeRiderTripStatusMongo No Doc");
		}
		else {
			if (msg == 'free') {
				docs.curTripno = null;
			} else if (msg == 'Accept' || msg == 'Arrived' || msg == 'Progress') {
				docs.curTripno = tripno;
			}
			docs.curStatus = msg;
			docs.save(function (err, op) {
				if (err) { logger.error("changeRiderTripStatusMongo", err); }
				else { logger.info("changeRiderTripStatusMongo"); }
			})
		}
	});
}

/**
 * Driver Earnings = total,daily,weekly,monthly,yearly
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const myEarnings = async (req, res) => {
	var obj = {
		total: 0,
		daily: 0,
		weekly: 0,
		monthly: 0,
		yearly: 0
	};
	var driverId = new mongoose.Types.ObjectId(req.userId);
	var nowdate = new Date();
	var groupbyval = { _id: '$driver', totalAmount: { $sum: "$amttodriver" }, nos: { $sum: 1 } };
	var totalmatch = { "$match": { "driver": driverId } };
	var dailymatch = { "$match": { "driver": driverId, "createdAt": { "$gte": GFunctions.pastDay(), "$lt": nowdate } } };
	var weeklymatch = { "$match": { "driver": driverId, "createdAt": { "$gte": GFunctions.sendPast7Day(), "$lt": nowdate } } };
	var monthlymatch = { "$match": { "driver": driverId, "createdAt": { "$gte": GFunctions.sendPast31Day(), "$lt": nowdate } } };
	var yearmatch = { "$match": { "driver": driverId, "createdAt": { "$gte": GFunctions.sendPast31Day(), "$lt": nowdate } } };

	var promisesToMake = [myEarningsDriverBasicSplits(totalmatch, groupbyval), myEarningsDriverBasicSplits(dailymatch, groupbyval), myEarningsDriverBasicSplits(weeklymatch, groupbyval), myEarningsDriverBasicSplits(monthlymatch, groupbyval), myEarningsDriverBasicSplits(totalmatch, groupbyval)];
	var promises = Promise.all(promisesToMake);
	promises.then(function (results) {
		var resstr = JSON.stringify(results);
		resstr = resstr.substr(1).slice(0, -1);
		var resarray = resstr.split(",");
		obj.total = resarray[0] ? resarray[0] : 0;
		obj.daily = resarray[1] ? resarray[1] : 0;
		obj.weekly = resarray[2] ? resarray[2] : 0;
		obj.monthly = resarray[3] ? resarray[3] : 0;
		obj.yearly = resarray[4] ? resarray[4] : 0;
		return res.json(obj);
	}).catch(function (error) {
		return res.status(500).json(obj);
	});
}

/**
 * Past 1 Trip Details with Map  //pending check we aleady done it
 * @input
 * @param
 * @return
 * @response
 */
export const pastTripDetail = async (req, res) => {
	try {
		var tripData = await Trips.findOne({ _id: req.body.tripId }, {}).lean();
		var riderData = await Rider.findOne({ _id: tripData.ridid }, { fname: 1, profile: 1, phone: 1 });
		if (riderData) {
			riderData.profile = config.baseurl + riderData.profile;
		}

		tripData.csp = GFunctions.convertAllNumbersToString(tripData.csp);
		tripData.acsp = GFunctions.convertAllNumbersToString(tripData.acsp);

		return res.status(200).json({ 'success': true, 'TripDetail': tripData, 'ProfileDetail': riderData, 'Mapurl': tripData.adsp.map });
	} catch (error) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND"), 'error': error });
	}
}


/**
 * Past 1 Trip Details with Map  //pending check we aleady done it
 * @input
 * @param
 * @return
 * @response
 */
export const pastTripDetailRider = async (req, res) => {
	try {
		var tripData = await Trips.findOne({ _id: req.body.tripId }, {}).lean();
		var driverData = await Driver.findOne({ _id: tripData.dvrid }, { fname: 1, profile: 1, phone: 1 });
		driverData.profile = config.baseurl + driverData.profile;
		tripData.acsp = convertAllNumbersToString(tripData.acsp);
		tripData.csp = convertAllNumbersToString(tripData.csp);
		return res.status(200).json({ 'success': true, 'TripDetail': tripData, 'ProfileDetail': driverData, 'Mapurl': tripData.adsp.map });
	} catch (error) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND"), 'error': error });
	}
}

/**
 * Add Amount To Given User Wallet // Set is amount Exists in Stripe First 1.chk User, Card added, amt available, then add to admin, update balnce and trans details
 * @input
 * @param
 * @return
 * @response
 */
export const addToMyWallet = async (req, res) => {
	if (featuresSettings.riderWallet) {
		// 1.chk User
		Rider.findOne({ _id: req.userId }, function (err, rideDoc) {
			if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
			if (!rideDoc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_RIDER_FOUND"), 'error': err });

			if (featuresSettings.riderRechargeWalletInClientSide == false && config.paymentGateway.paymentGatewayName == 'stripe') { //Rider he added card already now only recharging
				//1. Check Wallet Exists
				Wallet.findOne({ ridid: req.userId }, async function (err, doc) {
					if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
					if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_WALLET_FOUND_PLEASE_ADD_CARD_FIRST"), 'error': err });
					var msg = 'Wallet Recharge - ' + doc.id;
					paymentCtrl.transferAmountNRecharge(res, doc.card.id, msg, doc.card.currency, req.body.rechargeAmount, req.userId, doc.id, rideDoc, req);
				})
				//1. Check Wallet Exists
			}

			else if (featuresSettings.riderRechargeWalletInClientSide == false && config.paymentGateway.paymentGatewayName == 'braintree') { //Rider he added card already now only recharging
				var data = {
					firstName: rideDoc.fname,
					lastName: rideDoc.lname,
					phone: rideDoc.phone,
					email: rideDoc.email,
				};
				Wallet.findOne({ ridid: req.userId }, async function (err, doc) {
					if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
					if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_WALLET_FOUND_PLEASE_ADD_CARD_FIRST"), 'error': err });
					var msg = 'Wallet Recharge - ' + doc.id;
					paymentCtrl.transferAmountUsingNonceNRecharge(req, res, req.userId, doc.id, msg, data);
				})

			}

			else if (config.paymentGateway.paymentGatewayName == 'paystack') { //Rider he added card already now only recharging
				var data = {
					firstName: rideDoc.fname,
					lastName: rideDoc.lname,
					phone: rideDoc.phone,
					email: rideDoc.email,
				};
				Wallet.findOne({ ridid: req.userId }, async function (err, doc) {
					if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
					if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_WALLET_FOUND_PLEASE_ADD_CARD_FIRST"), 'error': err });
					var msg = 'Wallet Recharge - ' + doc.id;
					paymentCtrl.transferAmountNRecharge(res, doc.card.id, msg, doc.card.currency, req.body.rechargeAmount, req.userId, doc._id, data, req);
				})
			}

			else if (featuresSettings.riderRechargeWalletInClientSide == true) {
				// console.log(req.body)
				// req.body.amount = req.body.rechargeAmount;
				paymentCtrl.transferAmountToWallet(req, res);
			}
		})
		// 1.chk User
	} else {
		return res.status(401).json({ 'success': true, 'message': req.i18n.__("THIS_FEATURE_NOT_AVAILABLE") });
	}
}


/**
 * Amounts In My Wallet Transaction
 * @input
 * @param
 * @return
 * @response
 */
export const myWalletHistory = (req, res) => {
	Wallet.aggregate([
		{ "$match": { "ridid": new mongoose.Types.ObjectId(req.userId) } },

		{ "$unwind": "$trx" },
		// { "$match": { "trx.type": "Credit" } },

		{ "$sort": { "trx._id": -1 } }, //working

		{
			"$group": {
				"_id": "$_id",
				"transaction": {
					"$push": {
						"_id": "$trx._id",
						"trxid": "$trx.trxid",
						"amt": "$trx.amt",
						"date": "$trx.date",
						"type": "$trx.type"
					}
				}
			}
		}
	], function (err, docs) {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (docs.length) {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("WALLET_DETAILS"), "transaction": docs[0].transaction });
		}
		else {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("NO_WALLET_DETAILS_FOUND") });
		}
	});
}

/**
* Amounts In My Wallet Transaction / Credits
* @input
* @param
* @return
* @response
*/
export const myWalletCreditHistory = (req, res) => {
	Wallet.aggregate([
		{ "$match": { "ridid": new mongoose.Types.ObjectId(req.userId) } },

		{ "$unwind": "$trx" },
		{ "$match": { "trx.type": "Credit" } },

		{ "$sort": { "trx._id": -1 } }, //working

		{
			"$group": {
				"_id": "$_id",
				"transaction": {
					"$push": {
						"trxid": "$trx.trxid",
						"amt": "$trx.amt",
						"date": "$trx.date",
						"type": "$trx.type"
					}
				}
			}
		}
	], function (err, docs) {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (docs.length) {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("WALLET_DETAILS"), "transaction": docs[0].transaction });
		}
		else {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("NO_WALLET_DETAILS_FOUND") });
		}
	});
}


/**
* Amounts In My Wallet Transaction / Debit
* @input
* @param
* @return
* @response
*/
export const myWalletDebitHistory = (req, res) => {
	Wallet.aggregate([
		{ "$match": { "ridid": new mongoose.Types.ObjectId(req.userId) } },

		{ "$unwind": "$trx" },
		{ "$match": { "trx.type": "Debit" } },

		{ "$sort": { "trx._id": -1 } }, //working

		{
			"$group": {
				"_id": "$_id",
				"transaction": {
					"$push": {
						"trxid": "$trx.trxid",
						"amt": "$trx.amt",
						"date": "$trx.date",
						"type": "$trx.type"
					}
				}
			}
		}
	], function (err, docs) {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (docs.length) {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("WALLET_DETAILS"), "transaction": docs[0].transaction });
		}
		else {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("NO_WALLET_DETAILS_FOUND") });
		}

	});
}

/**
 * Amounts In My Wallet
 * @input
 * @param
 * @return
 * @response
 */
export const myWallet = (req, res) => {
	Wallet.find({ ridid: req.userId }).exec((err, docs) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (docs.length) {
			// console.log(docs);
			var bal = docs[0].bal;
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("WALLET_DETAILS"), "balance": bal.toString() });
		}
		else {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("NO_WALLET_DETAILS_FOUND"), "balance": "0" });
		}
	})
}

/**
 * Validate Promo
 * @input
 * @param
 * @return
 * @response
 */
export const validatePromo = async (req, res) => {
	var promocodeUpper = req.body.promoCode.toUpperCase();
	Promo.find({ code: promocodeUpper, status: true }).exec((err, docs) => {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (!docs.length) return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE"), "discountAmt": 0 }); //No Code Found

		var isValidCode = true;

		//Check trip type
		if (req.body.tripType) {
			var tripType = docs[0].tripType;
			if (tripType && tripType.length) {
				var isTripTypeInclude = _.includes(tripType, req.body.tripType)
				if (!isTripTypeInclude) return res.status(409).json({ 'success': false, 'message': req.i18n.__("CODE_NOT_VALID_FOR_THIS_TRIP"), "discountAmt": 0 }); //Code Not available for requested city
			}
		}

		//Check Used
		var noofuse = docs[0].noofuse;
		if (noofuse) {
			if (docs[0].used >= noofuse) return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_USAGE"), "discountAmt": 0 });  //Code Usage Exceeds
		}

		//Is Per User Usage Limit Exceeded
		if (req.body.userId && req.body.userId != undefined || typeof req.body.userId != 'undefined') req.userId = req.body.userId
		if (docs[0].users.includes(req.userId)) {
			var perUsageLimit = docs[0].perUserUsage; //noOfIndividualUsage
			var userIncluded = _.filter(docs[0].users.map(s => mongoose.Types.ObjectId(s)), mongoose.Types.ObjectId(req.userId));
			if (userIncluded.length >= perUsageLimit) return res.status(409).json({ 'success': false, 'message': req.i18n.__("USAGE_LIMIT_EXCEEDED"), "discountAmt": 0 });
		}

		// //Is User Already Used
		// if (docs[0].users.includes(req.userId)) {
		// 	return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_USED"), "discountAmt": 0 });
		// }

		//Check Date
		var todayDate = new Date();
		var todayDateZoneWise = moment(todayDate).utcOffset(config.utcOffset);
		var start = docs[0].start;
		var end = docs[0].end;
		end = moment(end).utcOffset(config.utcOffset).hours(23).minutes(59).seconds(59);
		// var endDateZoneWise = moment(end).utcOffset(config.utcOffset).hours(23).minutes(59).seconds(59);
		if (start) {
			var isafter = moment(todayDateZoneWise._d).isSameOrAfter(start);
			if (!isafter) return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_ACTIVE"), "discountAmt": 0 });//Code Not Yet Started
		}
		if (end) {
			end = moment(end).add(1, 'days');
			var isBefore = moment(todayDateZoneWise._d).isSameOrBefore(end);
			if (!isBefore) return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_EXPIRED"), "discountAmt": 0 });//Expired
		}

		//Check Time
		var startTime = docs[0].startTime;
		var endTime = docs[0].endTime;
		// Temorary Commented By Mudassar Nazir because causing issues of start and end time
		/*
		if (startTime != "" && endTime != "") {
			if (startTime != "0" && endTime != "0") {
				var timearry = moment(todayDate).utcOffset(config.utcOffset).format("HH:mm:ss");
				timearry = GFunctions.getFormatTime(timearry);
				if (startTime > endTime) { // 9PM to 5 AM
					if ((timearry > endTime) && (timearry < startTime)) { // if like 7AM
						return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_N/A"), "discountAmt": 0 });
					}
				} else { //6PM to  9PM
					if ((timearry > startTime) && (timearry < endTime)) { // if like 7PM
					} else {
						return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_N/A"), "discountAmt": 0 });
					}
				}
			}
		}
		*/

		//Check Day
		var day = docs[0].days;
		if (day != "") {
			var curDay = moment(todayDate).format("ddd");
			if (day.indexOf(curDay) > -1) { }
			else {
				return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_DAY"), "discountAmt": 0 });
			}
		}

		//For First Ride
		var forFirst = docs[0].forFirst;
		if (forFirst) {
			Trips.findOne({ ridid: req.userId, status: "Finished" }).exec((err, tripsdocs) => {
				if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
				if (tripsdocs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_CODE_FIRST_TIME"), "discountAmt": 0 });
				return res.status(200).json({ 'success': true, 'message': req.i18n.__("VALID_CODE"), "discountAmt": docs[0].amount });
			})
		} //For First Ride
		else {
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("VALID_CODE"), "discountAmt": docs[0].amount });
		}
	})
}


export const validatePromoForEstimation = async (promoCode, userId) => {
	try {
		var docs = await Promo.find({ code: promoCode.toUpperCase(), status: true }).exec();
		if (docs) {
			var isValidCode = true;

			//Check Used
			var noofuse = docs[0].noofuse;
			if (noofuse) {
				if (docs[0].used >= noofuse) return { 'success': false, "discountAmt": 0 };
			}

			//Is User Already Used
			if (docs[0].users.includes(userId)) {
				return { 'success': false, "discountAmt": 0 };
			}

			//Check Date
			var todayDate = new Date();
			var todayDateZoneWise = moment(todayDate).utcOffset(config.utcOffset);
			var start = docs[0].start;
			var end = docs[0].end;
			if (start) {
				var isafter = moment(todayDateZoneWise._d).isSameOrAfter(start);
				 console.log('isafter************: ',isafter); 
				if (!isafter) return { 'success': false, "discountAmt": 0 };
			}
			if (end) {
				end = moment(end).add(1, 'days');
				var isBefore = moment(todayDateZoneWise._d).isSameOrBefore(end);
				 console.log('isBefore ************: ',isBefore);
				if (!isBefore) return { 'success': false, "discountAmt": 0 };
			}

			//Check Time
			var startTime = docs[0].startTime;
			var endTime = docs[0].endTime;
			//comment by Mudassar Nazir because start and end time was not working properly
		/*	
			if (startTime != "" && endTime != "") {
				if (startTime != "0" && endTime != "0") {
					var timearry = moment(todayDate).format("HH:mm:ss");
					timearry = GFunctions.getFormatTime(timearry);
					if (startTime > endTime) { // 9PM to 5 AM
						if ((timearry > endTime) && (timearry < startTime)) { // if like 7AM
							return { 'success': false, "discountAmt": 0 };
						}
					} else { //6PM to  9PM
						if ((timearry > startTime) && (timearry < endTime)) { // if like 7PM
						} else {
							return { 'success': false, "discountAmt": 0 };
						}
					}
				}
			}

			*/

			//Check Day
			var day = docs[0].days;
			if (day != "") {
				var curDay = moment(todayDate).format("ddd");
				if (day.indexOf(curDay) > -1) { }
				else {
					return { 'success': false, "discountAmt": 0 };
				}
			}

			//For First Ride
			var forFirst = docs[0].forFirst;
			if (forFirst) {
				var tripData = await Trips.findOne({ ridid: userId, status: "Finished" }).exec();
				if (tripData) {
					return { 'success': false, "discountAmt": 0 }
				} else {
					return { 'success': true, "discountAmt": docs[0].amount }
				}
			} //For First Ride
			else {
				return { 'success': true, "discountAmt": docs[0].amount };
			}

		} else {
			return { 'success': false, "discountAmt": 0 }
		}
	} catch (error) {
		return { 'success': false, "discountAmt": 0 }
	}
}

//ScheduleTaxi


/** Taxi Request Schedule from User / SEND if only Not Already Booked
 * Taxi Request Schedule from User
 * @input
 * @param
 * @return
 * @response
 */
export const requestScheduleTaxi = (req, res) => {
	var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
	var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
	var reqtripFDT = GFunctions.getISODate(newDateFormat);
	var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
	var gmtFTime = new Date(newDateFormat + " " + "GMT+05:30").toGMTString();

	if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
		req.body.promoAmt = 0;
	}
	var promoAmt = req.body.promoAmt;
	var promoCode = req.body.promo.toUpperCase();

	var newDoc = new Trips(
		{
			triptype: "Schedule", //Ride,Schedule,Rental,Hail
			// tripno: crypto.randomBytes(5).toString('hex') , //change this logic or crypt Datetime
			date: req.body.tripShownDate,
			cpy: null,
			cpyid: null,
			dvr: null,
			dvrid: null,
			rid: req.name,
			ridid: req.userId,
			fare: req.body.totalfare,
			taxi: req.body.serviceName,
			service: req.body.serviceid,
			csp: [{ //Cost split up
				base: req.body.basefare,
				dist: req.body.distance,
				distfare: req.body.distanceFare,
				time: req.body.time,
				timefare: req.body.timeFare,
				comison: "",

				promoamt: promoAmt, //AMount //if needed if valid chk and set amt
				promo: promoCode.toUpperCase(), //Validate if needed , add only valid code

				cost: req.body.totalfare,
				via: req.body.paymentMode,
			}],
			dsp: [{ //details split up
				start: "",
				end: "",
				from: req.body.pickupAddress,
				to: req.body.dropAddress,
				pLat: req.body.pickupLat,
				pLng: req.body.pickupLng,
				dLat: req.body.dropLat,
				dLng: req.body.dropLng
			}],
			estTime: req.body.time,
			status: "processing",
			tripDT: reqtripDT,
			utc: req.body.utc,
			tripFDT: reqtripFDT,
			gmtTime: gmtFTime
		}
	);
	Trips.findOne({ ridid: req.userId, status: "processing", tripFDT: reqtripFDT }, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_ALREADY"), 'error': err });
		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}
			// updateRiderFbStatus(req.userId,"Processing",tripdata._id);
			requestNearbyDriversSch(tripdata, req.body, req.userId, reqtripDT);
			//Or one Driver
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id });
		})
	});
};

/**
 * Set Taxi Request Schedule from User
 * @input
 * @param
 * @return
 * @response
 */
function requestNearbyDriversSch (tripdata, userreq, userid, reqtripDT) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var neededService = userreq.serviceName;
	Driver.find({
		coords: {
			$geoWithin: {
				$centerSphere: [[parseFloat(userreq.pickupLng), parseFloat(userreq.pickupLat)],
				30 / 3963.2]
			},
		}, online: "1", curStatus: "free", curService: neededService
	}).exec((err, driverdata) => {
		if (err) { notifyRider(userid, noDriverFound, tripdata._id); } //Make User Know it  = update fb, change status to "waiting to accept",
		if (driverdata.length <= 0) {
			notifyRider(userid, noDriverFound, tripdata._id);
		} else {
			sendRequestToDriversSch(tripdata, userreq, driverdata, userid, reqtripDT);
		}
	});
}

/**
 * Set Taxi Request Schedule from User in Firebase
 * @input
 * @param
 * @return
 * @response
 */
function sendRequestToDriversSch (tripdata, userreq, driverdata, userid, reqtripDT) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");

	var requestData = {
		accept: {
			others: "0",
			trip_id: "0"
		},
		request: {
			drop_address: userreq.dropAddress,
			etd: userreq.time,
			picku_address: userreq.pickupAddress,
			request_id: tripdata._id,
			status: "1",
			datetime: reqtripDT,
			request_no: 0,
			request_type: "Schedule",
			review: "Taxi Request"
		}
	};

	var requestedDrivers = [];
	var arrayLength = driverdata.length; //2
	for (var i = 0; i < arrayLength; i++) {
		// console.log(driverdata[i]._id); // the _id
		var child = (driverdata[i]._id).toString();
		var usersRef = ref.child(child);
		requestedDrivers.push(child);
		usersRef.update(requestData, function (error) {
			if (error) { } else {
			} // Send FCM
		});
		if (i == (arrayLength - 1)) updateTaxiStatus(requestedDrivers, tripdata._id, userid);
	}
}



/**
 * Driver Accepted the Schedule Request
 * @input
 * @param
 * @return
 * @response
 */
export const acceptScheduleRequest = async (req, res) => {
	var update = {
		status: "accepted",
		review: "driver accepted",
		dvrid: req.userId,
		dvr: req.name,
		needClear: "no"
	}

	try {
		let docs = await Trips.findOne({ dvrid: req.userId, status: "Accepted", tripDT: req.body.reqtripDT }); // find DT
		if (!docs) {
			//Trip Get Accepted Only IF driver has no Other Trips
			Trips.findOneAndUpdate({ _id: req.body.requestId, status: { $not: /Accepted/ } }, update, { new: true }, (err, doc) => {
				if (err) {
					return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
				}
				if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_PROCESSED") });
				var index = doc.reqDvr.indexOf(req.userId);
				if (index !== -1) doc.reqDvr.splice(index, 1);
				findAndSendFCMToRider(doc.ridid, "Driver Has Accepted Your Schedule Trip Request", 'acceptScheduleRequest');
				cancelOtherTaxiRequest(doc.reqDvr, req.body.requestId, "");
				setInCRON(req.body.requestId, doc);  //make
				return res.json({ 'success': true, 'message': req.i18n.__("REQUEST_ACCEPTED_SUCCESSFULLY"), "requestId": req.body.requestId, "tripId": doc.tripno });
			})
			//Trip Get Accepted Only IF driver has no Other Trips

		} else {
			return res.status(409).json({ 'success': false, 'message': req.i18n.__("ANOTHER_TRIP_FOR_THIS_TIME_ALREADY_EXISTS") });
		}
	} catch (err) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
	}


}

/**
 * Set CRON
 * @input
 * @param
 * @return
 * @response
 */
function setInCRON (requestId, tripDate) {
	var newDateFormat = GFunctions.sendFormatedDTime(tripDate.tripDT);
	var gmtFTime = new Date(newDateFormat + " " + tripDate.utc).toGMTString();
	var newDoc = new Schedule(
		{
			gmtTime: gmtFTime,
			tripid: requestId,
			schTime: tripDate.tripFDT,
			status: "open"
		}
	);
	newDoc.save((err, datas) => {
		if (err) {
			logger.error(err);
		} else {
			logger.info('Sch Added')
		}
	})
}

/**
 * Schedule all Taxi History / Only
 * @input
 * @param
 * @return
 * @response
 */
export const riderUpcomingScheduleTaxi = (req, res) => {
	var nowdate = GFunctions.getUpcomingSchListMinusBuffer(config.upcomingRideLaterTimeBuffer);
	var where = {
		ridid: req.userId, bookingType: "rideLater",
		status: { $in: ['accepted', 'processing', 'noresponse'] },
		/* tripFDT: {
			"$gte": new Date().toISOString()
		}   */
		tripFDT: {
			"$gte": nowdate
		}
		// gmtTime: { $gte: timeGMT5MinutesBefore }
	};
	Trips.find(
		where
		, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0, acsp: 0, applyValues: 0 }).exec((err, docs) => {
			if (err) {
				return res.status(409).json([]);
			}
			return res.json(docs);
		});
};


/**
 * Schedule all Taxi History / Only
 * @input
 * @param
 * @return
 * @response
 */
export const driverUpcomingScheduleTaxi = (req, res) => {
	var nowdate = GFunctions.getUpcomingSchListBuffer(config.upcomingRideLaterTimeBuffer);
	Trips.find({
		dvrid: req.userId, bookingType: "rideLater", status: "accepted", tripFDT: {
			"$gte": new Date().toISOString()
		}
	}
		, { cpy: 0, cpyid: 0, dvr: 0, dvrid: 0, csp: 0, rid: 0, ridid: 0, service: 0, needClear: 0, createdAt: 0, driverfb: 0, riderfb: 0, reqDvr: 0, acsp: 0, applyValues: 0 }).exec((err, docs) => {
			if (err) {
				return res.status(409).json([]);
			}
			return res.json(docs);
		});
};


/**
 * Schedule Taxi Cancel Before Driver Accepted it, After Accepted
 * @input
 * @param
 * @return
 * @response
 */
export const userCancelScheduleTaxi = (req, res) => {
	var update = {
		status: "Cancelled",
		review: "user Cancelled"
	}

	Trips.findOneAndUpdate({ _id: req.body.requestId }, update, { new: true }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (!doc) {
			return res.json({ 'success': true, 'message': req.i18n.__("NO_TAXI_SCHEDULS_FOUND") });
		}
		if (doc.dvrid) {
			changeMyTripStatusMongo(doc.dvrid, 'tripno', "free");
			findAndSendFCMToDriver(doc.dvrid, "Schedule Trip Cancelled By Rider", 'userCancelScheduleTaxi');
		}
		return res.json({ 'success': true, 'message': req.i18n.__("TRIP_CANCELLED_SUCCESSFULLY") });  //Have to send cancelation fee
	})
}


/**
 * Schedule Taxi Cancel by Driver
 * @input
 * @param
 * @return
 * @response
 */
export const driverCancelScheduleTaxi = (req, res) => {
	var update = {
		status: "processing",
		review: "Driver Cancelled"
	}

	Trips.findOneAndUpdate({ _id: req.body.requestId }, update, { new: true }, (err, doc) => {
		if (err) {
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (!doc) {
			return res.json({ 'success': true, 'message': req.i18n.__("NO_TAXI_SCHEDULS_FOUND") });
		}
		if (doc.ridid) {
			changeMyTripStatusMongo(doc.dvrid, doc.tripno, "free");
			findAndSendFCMToRider(doc.ridid, "Schedule Trip Cancelled By Driver", 'driverCancelScheduleTaxi');
		}
		return res.json({ 'success': true, 'message': req.i18n.__("TAXI_CANCELLED_SUCCESSFULLY") });  //Have to send cancelation fee
	})
}

/**
 * Schedule Taxi Cancel by Driver
 * @input
 * @param
 * @return
 * @response
 */
export const cronas = (req, res) => {
	cron.schedule('*/1 * * * *', function () {
		console.log('running every  1 minute');
		sendSchAndTaxiReqStatus();
	});
	return res.json({ 'success': true, 'message': req.i18n.__("CRON_SETED") });
}


export const getISO = (req, res) => {
	var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
	var utc = 'GMT+05:30';
	var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
	var timeISONow = GFunctions.getSCHNotificationGMTDT();
	var myDate = new Date(newDateFormat);
	var myDate = new Date("05/21/2018 12:08 PM GMT+0530").toGMTString();
	return res.json({ 'timeISONow': timeISONow, 'myDate': myDate });
};


/**
 * Send SCH Notification and Sch Taxi Request Status
 * @input
 * @param
 * @return
 * @response
 */
export const sendSchAndTaxiReqStatus = () => {
	var timeGMT1 = GFunctions.getScheduleTaxiRequestTime(config.rideLaterStart); //ON time changes (like 5min)
	var timeGMT2 = GFunctions.getScheduleTaxiRequestTime(config.rideLaterRemainder); //Before few Mins (like 15min), send push notification remainder.
	Schedule.find({
		gmtTime: { $in: [timeGMT1, timeGMT2] }
	}, function (err, docs) {
		if (err) { logger.error(err); }
		sendSCHStartNotificationToDriver(docs, timeGMT1);
	});
}

/**
 * remove From Sch After trip end
 * @input
 * @param
 * @return
 * @response
 */
export const removeFromSchedule = (tripid) => {
	Schedule.findOneAndRemove({
		tripid: tripid
	}, function (err, data) {
		if (err) { logger.error(err); }
		console.log("removeFromSchedule", data);
	});
}

/**
 * Send SCH Start Notification to Driver, Rider and Trip Data
 Once Job completed, set status to open => requested,
 After Trip ended make to Empty = Del Sub Doc
 * @input
 * @param
 * @return
 * @response
 */
function sendSCHStartNotificationToDriver (allSchTrips, timeGMT1) {
	for (var i = 0; i < allSchTrips.length; i++) {
		if (timeGMT1 == allSchTrips[i].gmtTime) {
			console.log('sendSCHStartNotificationToDriver1')
			FindAndSendRequestToDrivers(allSchTrips[i].tripid); //ON time changes
		} else {
			console.log('sendSCHStartNotificationToDriver2')
			//Before few Mins (like 15min), send push notification remainder.
			FindAndSendNotificationToAll(allSchTrips[i].tripid);
		}
	}
}

/**
 * Get Driver and trip details and send notification
 * @input
 * @param
 * @return
 * @response
 */
function FindAndSendRequestToDrivers (tripid) {
	var updateData = {
		status: "started"
	}

	Trips.findByIdAndUpdate(tripid, updateData, { 'new': true },
		function (err, doc) {
			if (err) {
				logger.error(err);
			} else {
				addTripDatatoFb(doc);
			}
		}
	);
};


/**
* Add Trip Data To Firebase
* @input
* @param
* @return
* @response
*/
function addTripDatatoFb (driverid, tripno, bookingType, tripdata) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("trips_data");
	var requestData = {
		Drop_address: "0",
		base_fare: "0",
		cancelby: "0",
		datetime: "0",
		distance: "0",
		distance_fare: "0",
		driver_rating: "0",
		duration: "0",
		pickup_address: "0",
		rider_rating: "0",
		time_fare: "0",
		total_fare: "0",
		status: "1",
		discount: "0",
		ispay: "0",
		pay_type: "0",
		detected: "0",
		Toll_Amount: "0",
		Toll_request: "0",
		Toll_confirm: "0",
	};
	var id = tripdata.tripno;
	id = id.toString();
	var usersRef = ref.child(id);
	usersRef.update(requestData, function (snapshot) {
		// sendSCHStartRequestToDrivers(tripdata);
		// sendSCHStartAlertToRider(tripdata);
	});
}


/**
 * Set SCH Start Notification to Driver in Firebase
 * @input
 * @param
 * @return
 * @response
 */
function sendSCHStartRequestToDrivers (tripdata) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");

	var requestData = {
		accept: {
			others: "0",
			trip_id: tripdata.tripno  //tripId
		},
		request: {
			drop_address: "0",
			etd: "0",
			picku_address: "0",
			request_id: tripdata._id,
			status: "2", // 2
			datetime: "0",
			request_type: "0", //0
			review: "0" //0
		}
	};

	var userid = tripdata.dvrid;
	var child = userid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
	findAndSendFCMToDriver(tripdata.dvrid, "You has One Scheduled Trip On this Time", 'sendSCHStartRequestToDrivers');
}


function FindAndSendNotificationToAll (tripid) {
	console.log('FindAndSendNotificationToAll');
	Trips.findOne({ _id: tripid }, { dvrid: 1, ridid: 1, }, function (err, doc) {
		if (err) {
			logger.error(err);
		};
		if (doc.dvrid) {
			console.log(doc.tripDT);
			console.log(tripDT);
			findAndSendFCMToDriver(doc.dvrid, "You has One Upcoming Scheduled Trip On " + doc.tripDT + " Time", 'sendnotification');
		}
		if (doc.ridid) {
			findAndSendFCMToDriver(doc.ridid, "You has One Upcoming Scheduled Trip On " + doc.tripDT + " Time", 'sendnotification');
		}
	})
};

/**
 * Set SCH Start Notification to Rider in Firebase
 * @input
 * @param
 * @return
 * @response
 */
function sendSCHStartAlertToRider (tripdata) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("riders_data");

	var requestData = {
		tripdriver: tripdata.dvrid,
		tripstatus: 'Accepted',
		current_tripid: tripdata.tripno
	};

	var userid = tripdata.ridid;
	var child = userid.toString();
	var usersRef = ref.child(child);
	usersRef.update(requestData);
	findAndSendFCMToRider(tripdata.ridid, "You has One Scheduled Trip On this Time", 'sendSCHStartAlertToRider');
}


//ScheduleTaxi


//SafeTaxi


/**
 * Check Safe Ride Eligible
 * @input
 * @param
 * @return
 * @response
 */
export const checkSafeRideEligible = async (req, res) => {
	// var subtotal = req.body.subtotal;
	var subtotal = 0.5;
	var via = req.body.via;
	if (via == 'card') {
		//Just holding to check the Card eligible
		var balanceAvail = await findNHoldExistingUserCard(req.userId, subtotal);
		if (balanceAvail.holdAmt) {
			return res.json({ 'success': true, 'message': req.i18n.__("SAFE_RIDE_ELIGIBLE"), 'bal': balanceAvail.holdAmt });
		} else {
			return res.json({ 'success': false, 'message': req.i18n.__("SAFE_RIDE_ELIGIBLE"), 'bal': 0 });
		}
	}
	else if (via == 'wallet') {
		var balanceAvail = await findAndGetBalanceInWallet(req.userId, subtotal);
		if (balanceAvail) {
			return res.json({ 'success': true, 'message': req.i18n.__("SAFE_RIDE_ELIGIBLE"), 'bal': balanceAvail });
		} else {
			return res.json({ 'success': false, 'message': req.i18n.__("SAFE_RIDE_ELIGIBLE"), 'bal': 0 });
		}
	}
}

/**
 * Find Stripe = If exists hold charge and return captured amt
 * @input
 * @param
 * @return retunObj
 * @response
 */
async function findNHoldExistingUserCard (userId, amt) {
	var returnObj = {
		holdAmt: 0,
		tranxid: ''
	}
	try {
		let docs = await Rider.findById(userId);
		if (!docs) {
			return 0;
		} else {
			// var desc = "Trip -" + tripid;
			var desc = "Trip - Safe Hold";
			var res = await holdChargeCard(docs.stripe.id, desc, 'USD', amt);
			console.log('findNHoldExistingUserCard', res.charge.amount, res.charge.id);
			if (res.success) {
				returnObj.tranxid = res.charge.id;
				returnObj.holdAmt = (parseFloat(res.charge.amount) / 100);
				return returnObj;
			} else {
				return returnObj;
			}
		}
	} catch (err) {
		return 0;
	}

}

/**
 * findAndGetBalanceInWallet
 * @input
 * @param
 * @return balance in wallet
 * @response
 */
async function findAndGetBalanceInWallet (userId, amt) {
	try {
		let docs = await Wallet.findOne({ ridid: userId });
		if (!docs) {
			return 0;
		} else {
			//Wallet Available
			var walletbal = docs.bal;
			return walletbal;
		}
	} catch (err) {
		return 0;
	}
}

/**
 * Request safe taxi
 * @input
 * @param
 * @return balance in wallet
 * @response
 */
export const requestSafeTaxi = (req, res) => {
	// console.log('requestTaxi',req.body.promo);

	var promoAmt = req.body.promoAmt;
	var promoCode = req.body.promo.toUpperCase();

	var newDoc = new Trips(
		{
			triptype: "SafeRide",
			tripno: crypto.randomBytes(5).toString('hex'), //change this logic or crypt Datatime
			date: GFunctions.sendTimeNow(),
			cpy: "",
			cpyid: "",
			dvr: "",
			dvrid: "",
			rid: req.name,
			ridid: req.userId,
			fare: req.body.totalfare,
			taxi: req.body.serviceName,
			service: req.body.serviceid,
			csp: [{ //Cost split up
				base: req.body.basefare,
				dist: req.body.distance,
				distfare: req.body.distanceFare,
				time: req.body.time,
				timefare: req.body.timeFare,
				comison: "",

				promoamt: promoAmt, //AMount //if needed if valid chk and set amt
				promo: promoCode.toUpperCase(), //Validate if needed , add only valid code

				cost: req.body.totalfare,
				via: req.body.paymentMode,
			}],
			dsp: [{ //details split up
				start: "",
				end: "",
				from: req.body.pickupAddress,
				to: req.body.dropAddress,
				pLat: req.body.pickupLat,
				pLng: req.body.pickupLng,
				dLat: req.body.dropLat,
				dLng: req.body.dropLng,
			}],
			estTime: req.body.time,
			status: "processing"
		}
	);
	Trips.findOne({ ridid: req.userId, status: "processing" }, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_ALREADY"), 'error': err });
		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}
			updateRiderFbStatus(req.userId, "Processing", tripdata._id);
			requestNearbySafeDrivers(tripdata, req.body, req.userId);
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id });
		})
	});
};

/**
 * Set safe Taxi Request  from User
 * @input
 * @param
 * @return
 * @response
 */
function requestNearbySafeDrivers (tripdata, userreq, userid) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var neededService = userreq.serviceName;
	Driver.find({
		coords: {
			$geoWithin: {
				$centerSphere: [[parseFloat(userreq.pickupLng), parseFloat(userreq.pickupLat)],
				30 / 3963.2]
			},
		}, online: "1", curStatus: "free"
	}).exec((err, driverdata) => {
		if (err) { notifyRider(userid, noDriverFound, tripdata._id); } //Make User Know it  = update fb, change status to "waiting to accept",
		if (driverdata.length <= 0) {
			notifyRider(userid, noDriverFound, tripdata._id);
		} else {
			sendRequestToSafeDrivers(tripdata, userreq, driverdata, userid);
		}
	});
}

/**
 * Set safe Taxi Request  from User
 * @input
 * @param
 * @return
 * @response
 */
function sendRequestToSafeDrivers (tripdata, userreq, driverdata, userid) {
	if (!firebase.apps.length) {
		firebase.initializeApp(config.firebasekey);
	}
	var db = firebase.database();
	var ref = db.ref("drivers_data");

	var requestData = {
		accept: {
			others: "0",
			trip_id: "0"
		},
		request: {
			drop_address: userreq.dropAddress,
			etd: userreq.time,
			picku_address: userreq.pickupAddress,
			request_id: tripdata._id,
			status: "1",
			datetime: "Pickup Drunken Rider",
			request_type: "Safe",
			review: "Taxi Request"
		}
	};

	var requestedDrivers = [];
	var arrayLength = driverdata.length; //2
	for (var i = 0; i < arrayLength; i++) {
		// console.log(driverdata[i]._id); // the _id
		var child = (driverdata[i]._id).toString();
		var usersRef = ref.child(child);
		requestedDrivers.push(child);
		usersRef.update(requestData, function (error) {
			if (error) { } else {
				findAndSendFCMToDriver(child, "Safe Taxi Request", 'sendrequest');
			} // Send FCM
			// if (error) {} else { requestedDrivers.push(child); } //fb is slow
		});
		if (i == (arrayLength - 1)) updateTaxiStatus(requestedDrivers, tripdata._id, userid);
	}
}

/**
 * Add Safe Payment //upload file, log file , add amount ,
 * @input
 * @param
 * @return
 * @response
 */
export const safePayment = (req, res) => {
	console.log(req.body.tripId);
	Trips.findOne({ _id: req.body.tripId }, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		if (!docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND") });
		docs.acsp.safe = req.body.amount;
		addDriverSafePayment(docs, req.body.amount, req);
		docs.save(function (err, op) {
			if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
			return res.json({ 'success': true, 'message': req.i18n.__("SAFE_AMOUNT_ADDED_SUCCESSFULLY") });
		});
	});
}


/** addDriverSafePayment : addDriverSafePayment = Logic Needed To change
 * Add Detail Payment Details to Safe Driver Account
 * @input
 * @param
 * @return
 * @response tripDriverDetails
 */
function addDriverSafePayment (doc, amt, req) {
	var filepath = "";
	if (req['file'] != null) {
		filepath = req['file'].path;
	} else { }

	var amttopay = parseFloat(doc.acsp.cost) + parseFloat(amt);
	var details = {
		tripno: doc.tripno,
		driver: doc.dvrid,
		amttopay: doc.dvrid,
		amtpaid: amttopay,
		bal: amt,
		file: filepath
	}

	const newDoc = new SafeRide(details);
	newDoc.save((err, docs) => {
		if (err) {
			console.log(`addDriverSafePayment ${err}`);
		} else {
			console.log(`addDriverSafePayment ${details}`);
		}
	})
}


//SafeTaxi



//UberPool
export const getPool = (req, res) => {
	var array = [
		{ latitude: 9.936873, longitude: 78.098899 }, //big
		{ latitude: 9.924068, longitude: 78.122432 }, //vetri
	];
	getRectangleBorders();
	return res.json();
}


function getDistanceBttwoCords (slat, slon, elat, elon) {
	var distance = geolib.getDistance(
		{ latitude: slat, longitude: slon }, //start
		{ latitude: elat, longitude: elon }, //end
	);
	return distance;
}

function getRectangleBorders (slat = 0, slon = 0, elat = 0, elon = 0, dist = 1000) {
	var shareTrip = {};
	var headingTwds = geolib.getCompassDirection(
		{ latitude: slat, longitude: slon }, //R. Luther King, 2399 - Jd Clodoaldo, Cacoal - RO, 78975-000, Brazil
		{ latitude: elat, longitude: elon },
	);
	headingTwds = headingTwds.exact;
	if (headingTwds.length > 2) { headingTwds = headingTwds.substring(1); }
	shareTrip.direction = headingTwds;

	var distance = geolib.getDistance(
		{ latitude: slat, longitude: slon }, //start
		{ latitude: elat, longitude: elon }, //end
	);
	shareTrip.distance = distance;

	var initialPoint = { lat: slat, lon: slon }
	var dist = parseFloat(distance) / 2; //it will incresae Rectangle width
	var bearing = getBearing1(headingTwds);
	console.log(bearing);
	var oneCorner = geolib.computeDestinationPoint(initialPoint, dist, bearing);
	shareTrip.oneCorner = oneCorner;

	var bearing = getBearing2(headingTwds);
	console.log(bearing);
	var twoCorner = geolib.computeDestinationPoint(initialPoint, dist, bearing);
	shareTrip.twoCorner = twoCorner;

	console.log(shareTrip);

	return shareTrip;
}

function getBearing1 (head) {
	switch (head) {
		case 'N':
			return 45;
		case 'NW':
			return 0;
			break;
		case 'W':
			return 225;
			break;
		case 'SW':
			return 180;
			break;
		case 'S':
			return 135;
			break;
		case 'SE':
			return 90;
			break;
		case 'E':
			return 45;
			break;
		case 'NE':
			return 0;
			break;
		default:
			return 0;
	}
}


function getBearing2 (head) {
	switch (head) {
		case 'N':
			return 315;
		case 'NW':
			return 270;
			break;
		case 'W':
			return 315;
			break;
		case 'SW':
			return 270;
			break;
		case 'S':
			return 225;
			break;
		case 'SE':
			return 180;
			break;
		case 'E':
			return 135;
			break;
		case 'NE':
			return 90;
			break;
		default:
			return 0;
	}
}

export const setPool = (req, res) => {
	getRectangleBorders(9.924068, 78.122432, 9.919876, 78.101676);
	return res.json();
}


async function addToShareTripLists (requestId, availablestatus = "yes", driverid, dsp) {

	try {
		let Driverdocs = await ShareRides.findOne({ _id: driverid });
		if (!Driverdocs) {
			var corners = getRectangleBorders(dsp[0].pLat, dsp[0].pLng, dsp[0].dLat, dsp[0].dLng);
			var bordersAry = [];
			bordersAry.push(corners.oneCorner);
			bordersAry.push(corners.twoCorner);
			var newDoc = new ShareRides(
				{
					dvrid: driverid,
					available: availablestatus,
					direction: corners.direction,
					distance: corners.distance,
					tripId: requestId,

					start:
					{
						latitude: dsp[0].pLat,
						longitude: dsp[0].pLng
					},
					end: {
						latitude: dsp[0].dLat,
						longitude: dsp[0].dLng
					},
					left: corners.oneCorner,
					right: corners.twoCorner,

				}
			);
			newDoc.save((err, datas) => {
				if (err) {
				}
			})
		} else {
		}
	} catch (err) {
	}

}

/**
 * [removeShareTripLists description] If trip completed
 * @param  {[type]} requestId [description]
 * @return {[type]}           [description]
 */
function removeShareTripLists (requestId) {
	ShareRides.remove({ tripId: requestId }, (err, docs) => {
		if (err) {
			console.log(err);
		}
	})
}


export const shareAvail = (req, res) => {
	findShareTaxiFirst(9.92365986513009, 78.12716322615196, 9.902627, 78.144025);
	return res.json();
}

/**
 * [findShareTaxiFirst description]
 * @param  {Number} slat [description]
 * @param  {Number} slon [description]
 * @param  {Number} elat [description]
 * @param  {Number} elon [description]
 * @param  {Number} dist [description]
 * @return {[type]}      [description]
 */
async function findShareTaxiFirst (slat = 0, slon = 0, elat = 0, elon = 0, dist = 1000) {
	var shareTrip = {};
	var tripID = false;
	var headingTwds = geolib.getCompassDirection(
		{ latitude: slat, longitude: slon },
		{ latitude: elat, longitude: elon },
	);
	headingTwds = headingTwds.exact;
	if (headingTwds.length > 2) { headingTwds = headingTwds.substring(1); }
	shareTrip.direction = headingTwds;

	shareTrip.tripID = await findShareAvailableAndSendTripIds(slat, slon, elat, elon, 1000, headingTwds);
	console.log(shareTrip.tripID);
	console.log(shareTrip);
	return (shareTrip);
}

/**
 * [findShareAvailableAndSendTripIds description]
 * @param  {Number} slat    [description]
 * @param  {Number} slon    [description]
 * @param  {Number} elat    [description]
 * @param  {Number} elon    [description]
 * @param  {Number} dist    [description]
 * @param  {String} heading [description]
 * @return {[type]}         [description] will return Trip Id if available
 */
function findShareAvailableAndSendTripIds (slat = 0, slon = 0, elat = 0, elon = 0, dist = 1000, heading = 'S') {
	return new Promise(function (resolve, reject) {
		var isExists = false;
		ShareRides.find({}, function (err, docs) {
			if (err) console.log(err);
			if (docs) {

				docs.forEach(function (doc) {
					isExists = isPointExistsInPoly(slat, slon, doc);
					if (isExists == true) {
						var oldShare = {};
						oldShare.tripId = doc.tripId;
						oldShare.dvrid = doc.dvrid;
						console.log(oldShare);
						resolve(oldShare);
					}
				}
				);

				if (isExists == false) {
					resolve(isExists);
				}
			}
		});

	});

}

/**
 * [isPointExistsInPoly description]
 * @param  {Number}  slat [description]
 * @param  {Number}  slon [description]
 * @param  {[type]}  doc  [description]
 * @return {Boolean}      [description]
 */
function isPointExistsInPoly (slat = 0, slon = 0, doc) {
	var isExists = false;
	var toSearch = [];
	toSearch.push(doc.start);
	toSearch.push(doc.left);
	toSearch.push(doc.right);
	toSearch.push(doc.end);

	isExists = geolib.isPointInside(
		{ latitude: slat, longitude: slon },
		toSearch
	);
	return isExists;

}

//Admin Request Taxi in MTD
export const requestTaxiFromMTD3 = async (req, res) => {
	console.log(req.body);
}

/**
 * [Taxi Request from User] = Checked
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const requestTaxiFromMTD = async (req, res) => {
	console.log(req.body)
	if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
		req.body.promoAmt = 0;
	}
	var promoAmt = req.body.promoAmt;
	var promoCode = req.body.promo.toUpperCase();

	if (req.body.typeR == 'pack') {
		var reqTripType = 'Package';
		if (req.body.newPackage == 1) {
			PackageHelpers.addPackage(req, res);
		}

		req.body.basefare = req.body.amt;
		req.body.totalfare = req.body.amt;

	} else {
		var reqTripType = 'Ride';
	}
	var newDoc = new Trips(
		{
			triptype: reqTripType,
			date: GFunctions.sendTimeNow(),
			cpy: "",
			cpyid: "",
			dvr: null,
			dvrid: null,
			rid: req.body.userName,
			ridid: req.body.userId,
			fare: req.body.totalfare,
			taxi: req.body.serviceName,
			service: req.body.serviceid,
			csp: [{ //Cost split up RFCNG
				base: req.body.basefare,
				dist: req.body.distance,
				distfare: req.body.distanceFare,
				time: req.body.time,
				timefare: req.body.timeFare,
				comison: "",
				promoamt: promoAmt,
				promo: promoCode.toUpperCase(),
				cost: req.body.totalfare,
				via: req.body.paymentMode,
			}],
			dsp: [{ //details split up
				start: "",
				end: "",
				from: req.body.pickupAddress,
				to: req.body.dropAddress,
				pLat: req.body.pickupLat,
				pLng: req.body.pickupLng,
				dLat: req.body.dropLat,
				dLng: req.body.dropLng,
			}],

			package: {
				pkname: req.body.pkname,
				amt: req.body.amt,
				minkm: req.body.minkm,
				minhr: req.body.minhr,
				pkm: req.body.pkm,
				phr: req.body.phr,
			},

			estTime: req.body.time,
			status: "processing"
		}
	);
	//checking is this user has processing trips of type RIDE
	Trips.findOne({ ridid: req.body.userId }, function (err, docs) {
		if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		// if (docs) return res.status(409).json({ 'success': false, 'message': 'Request Already In Process.', 'error': err });
		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}
			console.log("test");
			updateRiderFbStatus(req.body.userId, "Processing", tripdata._id); //processing = Req intermediate state
			if (req.body.autoAssign == "1" || req.body.autoAssign == 1) {
				requestNearbyDrivers(tripdata, req.body, req.body.userId);
			} else {
				var arrDvr = [];
				arrDvr.push({
					_id: req.body.driverId
				});
				sendRequestToDrivers(tripdata, req.body, arrDvr, req.body.userId)
			}
			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id });
		})
	});
};


/** Taxi Request Schedule from User / SEND if only Not Already Booked
 * Taxi Request Schedule from User
 * @input
 * @param
 * @return
 * @response
 */
export const requestScheduleTaxiFromMTD = (req, res) => {
	console.log(req.body)

	var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
	var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
	var reqtripFDT = GFunctions.getISODate(newDateFormat);

	var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
	var gmtFTime = new Date(newDateFormat + " " + "GMT+05:30").toGMTString();


	if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
		req.body.promoAmt = 0;
	}
	var promoAmt = req.body.promoAmt;
	var promoCode = req.body.promo.toUpperCase();

	var newDoc = new Trips(
		{
			triptype: "Schedule", //Ride,Schedule,Rental,Hail
			// tripno: crypto.randomBytes(5).toString('hex') , //change this logic or crypt Datetime
			date: GFunctions.sendTimeNow(),
			cpy: null,
			cpyid: null,
			dvr: null,
			dvrid: null,
			rid: req.body.userName,
			ridid: req.body.userId,
			fare: req.body.totalfare,
			taxi: req.body.serviceName,
			service: req.body.serviceid,
			csp: [{ //Cost split up
				base: req.body.basefare,
				dist: req.body.distance,
				distfare: req.body.distanceFare,
				time: req.body.time,
				timefare: req.body.timeFare,
				comison: "",

				promoamt: promoAmt, //AMount //if needed if valid chk and set amt
				promo: promoCode.toUpperCase(), //Validate if needed , add only valid code

				cost: req.body.totalfare,
				via: req.body.paymentMode,
			}],
			dsp: [{ //details split up
				start: "",
				end: "",
				from: req.body.pickupAddress,
				to: req.body.dropAddress,
				pLat: req.body.pickupLat,
				pLng: req.body.pickupLng,
				dLat: req.body.dropLat,
				dLng: req.body.dropLng
			}],
			estTime: req.body.time,
			status: "processing",
			tripDT: reqtripDT,
			utc: "GMT+05:30",
			tripFDT: reqtripFDT,
			gmtTime: gmtFTime
		}
	);
	Trips.findOne({ ridid: req.body.userId, status: "processing", tripFDT: reqtripFDT }, function (err, docs) {
		if (err) {
			console.log(err);
			return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
		}
		if (docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__("REQUEST_ALREADY"), 'error': err });
		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}

			if (req.body.autoAssign == "1" || req.body.autoAssign == 1) {
				requestNearbyDriversSch(tripdata, req.body, req.body.userId, reqtripDT);
			} else {
				var arrDvr = [];
				arrDvr.push({
					_id: req.body.driverId
				});
				sendRequestToDriversSch(tripdata, req.body, arrDvr, req.body.userId, reqtripDT);
			}

			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id });
		})
	});
};


//Admin Request Taxi in MTD


//CRON to send request to Drivers
/**
 * CRON to send request to Drivers
 * @input
 * @param
 * @return
 * @response
 */
export const sendScheduleTaxiRequestToDriver = () => {
	var timeGMT10MinutesBefore = GFunctions.getScheduleTaxiRequestTime(config.redtaxisettings.connectDailyTripBefore);
	var timeGMT5MinutesBefore = GFunctions.getScheduleTaxiRequestTime(Number(config.redtaxisettings.connectDailyTripBefore) - 5);
	var timeGMT5MinutesBefore1 = GFunctions.getScheduleTaxiRequestTime(Number(config.redtaxisettings.connectDailyTripBefore) - 10);
	Trips.find({
		bookingType: { $in: ['rideLater'] }, triptype: 'daily',
		// status: { $in: ['noresponse', 'processing']  },
		$or: [{ review: constantsValues.cancelTaxiByDriver }, { status: { $in: ['noresponse', 'processing'] } }],
		gmtTime: { $in: [timeGMT10MinutesBefore, timeGMT5MinutesBefore, timeGMT5MinutesBefore1] }
	}, function (err, docs) {
		if (docs.length) {
			docs.forEach(async function (doc) {

				var isRiderCurrentlyFreeToTakeNew = await isRiderCurrentlyFree(doc.ridid);
				if (isRiderCurrentlyFreeToTakeNew) {
					let userreq = {};
					let userId = doc.ridid, pickupLng = doc.dsp.startcoords[0], pickupLat = doc.dsp.startcoords[1], serviceType = doc.vehicle;

					if (requestTypeMethod == 'onebyone') {
						findNearbyDriversAndSendRequest(doc, userreq, userId, pickupLng, pickupLat, serviceType);//For One By One
					} else {
						//requestNearbyDrivers(tripdata, body, req.userId);
					}
				}

			})
		}
	});
}

export const retryNoResponseRequestApp = async (req, res) => {
	Trips.find({ _id: req.body.tripRequestId, status: { $in: ['noresponse', 'Cancelled'], dvrid: null } }, function (err, doc) {
		if (err) return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
		if (!doc) return res.status(409).json({ 'success': false, 'message': req.i18n.__("NO_TRIP_FOUND") });
		else {
			let userreq = {};
			let userId = doc.ridid, pickupLng = doc.startcoords[1], pickupLat = doc.startcoords[0], serviceType = doc.vehicle;

			if (requestTypeMethod == 'onebyone') {
				findNearbyDriversAndSendRequest(doc, userreq, userId, pickupLng, pickupLat, serviceType);//For One By One
			} else {
				//requestNearbyDrivers(tripdata, body, req.userId);
			}
			return res.status(200).json({ 'success': false, 'message': req.i18n.__("TRIP_REQUEST_RETRYING") });
		}
	});
}

/**
 * Driver Earnings = total,daily,weekly,monthly,yearly
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const driverEarningsReport = async (req, res) => {
	var date;
	var baseData = 'tripFDT'; //  tripFDT/createdAt
	date = moment().format('YYYY-MM-DD');

	if (req.body._page > 1) {
		date = moment().subtract(parseInt(req.body._page - 1) + 9, 'days').format('YYYY-MM-DD');
	}

	var currentDate = moment(date).add(1, 'days').format('YYYY-MM-DD');
	var pastDate1 = moment(date).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate2 = moment(pastDate1).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate3 = moment(pastDate2).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate4 = moment(pastDate3).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate5 = moment(pastDate4).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate6 = moment(pastDate5).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate7 = moment(pastDate6).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate8 = moment(pastDate7).subtract(1, 'days').format('YYYY-MM-DD');
	var pastDate9 = moment(pastDate8).subtract(1, 'days').format('YYYY-MM-DD');

	var driverId = new mongoose.Types.ObjectId(req.userId);
	var groupbyval = { _id: '$driver', amttopay: { $sum: "$amttopay" }, commision: { $sum: "$commision" }, nos: { $sum: 1 } };
	var day1 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(date), "$lte": new Date(currentDate) } } };
	var day2 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate1), "$lte": new Date(date) } } };
	var day3 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate2), "$lte": new Date(pastDate1) } } };
	var day4 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate3), "$lte": new Date(pastDate2) } } };
	var day5 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate4), "$lte": new Date(pastDate3) } } };
	var day6 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate5), "$lte": new Date(pastDate4) } } };
	var day7 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate6), "$lte": new Date(pastDate5) } } };
	var day8 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate7), "$lte": new Date(pastDate6) } } };
	var day9 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate8), "$lte": new Date(pastDate7) } } };
	var day10 = { "$match": { "driver": driverId, createdAt: { "$gte": new Date(pastDate9), "$lte": new Date(pastDate8) } } };

	var promisesToMake = [myEarningsDriver(day1, groupbyval, date), myEarningsDriver(day2, groupbyval, pastDate1), myEarningsDriver(day3, groupbyval, pastDate2), myEarningsDriver(day4, groupbyval, pastDate3), myEarningsDriver(day5, groupbyval, pastDate4), myEarningsDriver(day6, groupbyval, pastDate5), myEarningsDriver(day7, groupbyval, pastDate6), myEarningsDriver(day8, groupbyval, pastDate7), myEarningsDriver(day9, groupbyval, pastDate8), myEarningsDriver(day10, groupbyval, pastDate9)];
	var promises = Promise.all(promisesToMake);
	promises.then(function (results) {
		var filtered = results.filter(function (el) {
			return el != null;
		});
		return res.json(filtered);
	}).catch(function (error) {
		return res.status(409).json(error);
	});
}

/**
 *
 * @param {*} cityName
 * returns _id if cityName available in serviceavailablecities { city or in nearby array} else null
 */
export const getIsServiceAvailableInGivenCity = (data) => {
	var locationToFind = [];
	locationToFind[1] = parseFloat(data.pickupLat);
	locationToFind[0] = parseFloat(data.pickupLng);
	var tcf = [];
	tcf[0] = locationToFind;
	var ObjectToReturn;
	return new Promise((resolve, reject) => {
		// ServiceAvailableCities.index( { location : "2dsphere" } );
		ServiceAvailableCities.findOne(
			{ geometry: { $geoIntersects: { $geometry: { type: "Point", coordinates: locationToFind } } } }
			// { geometry: { $geoIntersects: { $geometry: { type: "Point", coordinates: locationToFind } } } }
			, {}, function (err, data) {
				if (err) {
					reject(ObjectToReturn = {
						success: false,
						data: err,
					})
				}
				else {
					resolve(ObjectToReturn = {
						success: true,
						data: data
					})
				}
			})
	})
}


export const getRentalPackage = async (req, res) => {
	try {
		var where = {};
		var cityData = await getIsServiceAvailableInGivenCity(req.body);
		// console.log(cityData.data.city)
		if (cityData.data != null)
			where = { "scIds.name": { "$in": [cityData.data.city, "Default"] } };
		else

			where = { "scIds.name": { "$in": ["Default"] } };
		// console.log(where)
		RentalPackage.find(where, {}, function (err, packages) {
			if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("SERVER_ERROR"), "error": err });
			if (packages) return res.status(200).json({ 'success': true, 'message': req.i18n.__("DETAILS_FETCHED_SUCCESSFULY"), "data": packages });
		});
	}
	catch (error) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("SERVER_ERROR"), "error": error });

	}
}

//Clear trip
export const clearNoEndedtrips = async (req, res) => {
	try {
		var uptoDate = moment().subtract(10, "days").utcOffset(config.utcOffset).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
		let TotTrips = await Trips.find({ "tripFDT": { "$lte": uptoDate }, "status": { $in: ["processing", "Progress"] } });
		return res.status(200).json({ 'success': false, 'message': req.i18n.__("SERVER_ERROR"), "error": TotTrips });//return just true
	} catch (error) {
		return res.status(500).json({ 'success': false, 'message': req.i18n.__("SERVER_ERROR"), "error": error });
	}

	/* try {
		var uptoDate = moment().subtract(10, "days").utcOffset(config.utcOffset).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
		let TotTripsLikeQuery = { "tripFDT": { "$lte": uptoDate }, "status": { $in: ["processing", "Progress"] } };
		Trips.updateMany(TotTripsLikeQuery, { $set: { status: "noresponse" } })
		return res.status(200).json({ 'success': false, 'message': 'Error on the server.', "error": TotTripsLikeQuery });
	} catch (error) {
		return res.status(500).json({ 'success': false, 'message': 'Error on the server.', "error": error });
	} */
}


//Package Flow

//Request
/**
 * [Taxi Request from User] = Checked
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const requestRentalTaxi = async (req, res) => {
	try {
		const body = req.body || {};

		if (!body.promo == "") {
			var promoAmtData = await Promo.findOne({ code: body.promo.toUpperCase() }, { amount: 1, code: 1 }).exec();
			if (promoAmtData) body.promoAmt = promoAmtData.amount;
		}

		var pickupLatlng = [];
		pickupLatlng[0] = body.pickupLng;
		pickupLatlng[1] = body.pickupLat;

		/* 		//GDM
				if (body.pickupLat) {
					const from = body.pickupLat + ',' + body.pickupLng;
					const to = body.dropLat + ',' + body.dropLng;
					var gdmResult = await GFunctions.getDistanceAndTimeFromGDM([from], [to]);
					if (config.distanceUnit == 'Miles') {
						var distanceInUnit = parseFloat(gdmResult.distanceValue * 0.000621371).toFixed(2);
					} else {
						var distanceInUnit = parseFloat(gdmResult.distanceValue / 1000).toFixed(2);
					}
					var timeInMinutes = parseFloat(gdmResult.timeValue / 60).toFixed(2);
					var distanceUnit = config.distanceSymbol ? config.distanceSymbol : ' KM';
					gdmResult.distanceLable = packagaDoc.distance + distanceUnit;
					gdmResult.startCords = [body.pickupLng, body.pickupLat];
					gdmResult.endcoords = [body.dropLng, body.dropLat];
				}
				//GDM   */

		let vehicleTypeDocs = await Vehicle.findById(body.vehicleTypeId, { "bkm": 1, "timeFare": 1, "tripTypeCode": 1, "distance": 1, "type": 1, "baseFare": 1, "file": 1, "description": 1, "asppc": 1, "comison": 1 });
		let packagaDoc = await RentalPackage.findById(body.packageId, { "name": 1, "distance": 1, "duration": 1, "price": 1 });

		var totalFare = getRentalEstimationFare(packagaDoc.distance, vehicleTypeDocs.bkm, vehicleTypeDocs.baseFare);

		var newDoc = new Trips(
			{
				packageId: body.packageId,
				// tripno: await TripHelpers.getTripNo(),
				requestFrom: body.requestFrom,
				requestId: body.adminId ? body.adminId : '',
				triptype: body.tripType,
				bookingType: body.bookingType,
				bookingFor: body.bookingFor,
				notes: body.notesToDriver ? body.notesToDriver : '',
				other: {
					ph: body.otherPh ? body.otherPh : '',
					phCode: body.otherPhCode ? body.otherPhCode : '',
					name: body.otherName ? body.otherName : '',
				},
				date: req.body.tripShownDate,
				cpy: null,
				cpyid: null,
				dvr: null,
				dvrid: null,
				rid: req.name,
				ridid: req.userId,
				hotelid: req.body.hotelId ? req.body.hotelId : null,
				fare: totalFare,
				vehicle: vehicleTypeDocs.type,
				service: vehicleTypeDocs._id,
				paymentMode: body.paymentMode,
				csp: { //Cost split up RFCNG
					base: vehicleTypeDocs.baseFare,
					dist: packagaDoc.distance,
					distfare: totalFare,
					time: packagaDoc.duration,
					timefare: vehicleTypeDocs.timeFare,
					comison: vehicleTypeDocs.comison,
					promoamt: body.promoAmt,
					promo: body.promo.toUpperCase(),
					cost: totalFare,
					conveyance: vehicleTypeDocs.conveyancePerKm,
					tax: 0, //TODO
					taxPercentage: vehicleTypeDocs.taxPercentage,
					via: body.paymentMode,
					driverCancelFee: vehicleTypeDocs.cancelationFeesDriver,
					riderCancelFee: vehicleTypeDocs.cancelationFeesRider,
					isNight: false,  //TODO
					isPeak: false, //TODO
					nightPer: 1, //TODO
					peakPer: 1, //TODO
					currency: config.currency, //TODO
					hotelcommision: 0, //TODO
				},
				dsp: {
					distanceKM: packagaDoc.distance,
					start: req.body.pickupAddress,
					end: "",
					startcoords: pickupLatlng ? pickupLatlng : null,
					endcoords: null,
				},
				estTime: packagaDoc.duration,
				status: "processing",
				tripOTP: [GFunctions.sendRandomizeCode('0', 4), GFunctions.sendRandomizeCode('0', 4)],
				scId: null,
				tripDT: body.tripDT,
				utc: body.utc,
				tripFDT: body.tripFDT,
				gmtTime: body.gmtTime,
				noofseats: body.noofseats,
			}
		);

		newDoc.save((err, tripdata) => {
			if (err) {
				return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
			}

			if (body.bookingType != "rideLater") {
				updateRiderFbStatus(req.userId, "Processing", tripdata._id, body.tripType); //processing = Req intermediate state
			}

			if (requestTypeMethod == 'onebyone') {
				findNearbyDriversAndSendRequest(tripdata, body, req.userId, body.pickupLat, body.pickupLng, body.serviceType);//For One By One
			} else {
				requestNearbyDrivers(tripdata, body, req.userId);
			}

			return res.status(200).json({ 'success': true, 'message': req.i18n.__("TAXI_REQUEST_SENDED"), "requestDetails": tripdata._id });
		})


	} catch (error) {
		console.log(error)
		return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': error.toString() });
	}
};//Package Flow


export const isRiderCurrentlyFree = async (riderID) => {
	try {
		let riderDoc = await Rider.findById(riderID).exec();
		if (riderDoc !== null) {
			if (riderDoc.curStatus != 'free') {
				let lastTrip = await Trips.findOne({ 'ridid': riderId, 'triptype': 'daily' }, { status: 1 }).sort({ 'tripFDT': -1 }).limit(1);
				if (lastTrip.status == "Finished" || lastTrip.status == "Cancelled") {
					let updateRiderCurStatus = await Rider.findOneAndUpdate({ '_id': riderID }, { 'curStatus': 'free' });
					return true
				}
				else return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	} catch (error) {
		return true;
	}
}

export const isRiderHasUpcomingTrips = async (riderID) => {
	try {
		var fromDate = new Date(GFunctions.getISODate());

		var tripData = await Trips.findOne({
			ridid: riderID, status: "processing", tripFDT: {
				'$gte': fromDate
			}
		});

		if (tripData !== null) {
			return true;
		} else {
			return false;
		}
	} catch (error) {
		return false;
	}
}

async function addHotelPayment (hotelPaymentDetails) {
	const newDoc = new HotelPayment(hotelPaymentDetails);
	newDoc.save((err, docs) => {
		if (err) {
			logger.error(`addHotelPayment ${err}`);
		} else {
			// console.log(`addDriverPayment ${details}`);
		}
	})
}

export const isRiderCurrentlyActive = async (riderID) => {
	try {
		let riderDoc = await Rider.findById(riderID).exec();
		if (riderDoc !== null) {
			if (riderDoc.softdel == 'active') {
				return true
			} else {
				return false;
			}
		} else {
			return true;
		}
	} catch (error) {
		return true;
	}
}

function findAndUpdateTripLocation (tripNo, lat, lng) {
	var updateTime = 0
	Trips.findOne({ 'tripno': tripNo, 'status': "Progress" }, { '_id': 1, 'triptype': 1 }, (err, docs) => {
		if (err) { console.log(err); }
		if (!docs) { console.log("Trip Not Found") }
		else {
			if (docs.triptype == 'daily') {
				updateTime = featuresSettings.locationUpdateAfter.daily;
				console.log("dailyupdateTime", updateTime)
			}
			else if (docs.triptype == 'rental') {
				updateTime = featuresSettings.locationUpdateAfter.rental;
				console.log("rentalupdateTime", updateTime)
			}
			else if (docs.triptype == 'outstation') {
				updateTime = featuresSettings.locationUpdateAfter.outstation;
				console.log("outstationupdateTime", updateTime)
			}
			updateTripLocation(docs._id, lat, lng, updateTime)
		}
	})
}

function updateTripLocation (tripId, lat, lng, updateTime) {
	var location = (Number(lat).toFixed(4)) + "," + (Number(lng).toFixed(4))
	TripLocation.findOne({ 'tripId': tripId }, {}, (err, doc) => {
		if (err) { console.log(err) }
		if (!doc) {
			var newDoc = new TripLocation({
				tripId: tripId,
				lastUpdated: GFunctions.getISODate(),
				loc: [location]
			})
			newDoc.save((err, doc) => {
				if (err) {
					console.log(err)
				}
				else {
					console.log("Updated")
				}
			});
		}
		else {
			var timeBtNowAndReq = GFunctions.getMinsBtDateTime(GFunctions.getISODate(), doc.lastUpdated);
			console.log('timeBtNowAndReq', timeBtNowAndReq)
			if (timeBtNowAndReq >= updateTime) {
				// for (var i = 0; i < 1339; i++) {
				TripLocation.findOneAndUpdate({ 'tripId': tripId }, { $push: { loc: location }, $set: { lastUpdated: GFunctions.getISODate() } }, (err, data) => {
					if (err) { console.log(err) }
					else { }
				});
				// }
			}
		}
	});
}
