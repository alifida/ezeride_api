const config = require('../config');
const curSmsGateway = config.smsGateway.smsGatewayName;
import * as GFunctions from './functions';
import Admin from '../models/admin.model';
import mongoose, { Query } from 'mongoose';


const request = require('request');

if (curSmsGateway == 'twilio') {
  var twilio = require('twilio');
  var accountSid = config.smsGateway.twilioaccountSid; // Your Account SID from www.twilio.com/console
  var authToken = config.smsGateway.twilioauthToken;   // Your Auth Token from www.twilio.com/console
  var twilioNo = config.smsGateway.twilioNo;   // From a valid Twilio number
  var client = new twilio(accountSid, authToken);
}

var nexmoConfig = "";
if (curSmsGateway == 'nexmo') {
  const Nexmo = require('nexmo');
  nexmoConfig = new Nexmo({
    apiKey: config.smsGateway.nexmoapiKey,
    apiSecret: config.smsGateway.nexmoapisecret,
  });
}

import Rider from '../models/rider.model';
import { Mongoose } from 'mongoose';
import { admin } from 'googleapis/build/src/apis/admin';

const sendSMSFor = {
  'verifyNumberRider': '<#> OTP to install ' + config.appName + ' app is {RANDOMSMS}',
  'verifyNumberDriver': '<#> OTP to install ' + config.appName + ' app is {RANDOMSMS}',
  'forgotPasswordDriver': 'Use this {OTPCODE} Time Password to reset your Password in  ' + config.appName,
  'forgotPasswordRider': 'Use this {OTPCODE} Time Password to reset your Password in  ' + config.appName,
  'emergencyMsg': '{NAME} ({PHONE}) is riding in ' + config.appName + ' has reached you because of some emergency, you can track his ride here {URLLINK}.',
  // 'emergencyMsg': '{NAME} ({PHONE}) is riding in ' + config.appName + ' has reached you because of some emergency, you can call his number.',
  'sendTripAcceptedSMSToRider': config.appName + ' Vehicle Number: {VEHICLENO},  Driver Name: {DRIVERNAME}, Driver Number: {DRIVERNO}, OTP to ride is {OTP}',
  'sendTripAcceptedSMSToRiderForApp': '{DRIVERNAME}+({DRIVERNO})+is+on+the+way+to+your+location+in+a+{VEHICLENNAME}+{VEHICLENO}.+Once+you+board+your+' + config.appName + '+ride,+please+share+OTP-{OTP}+with+driver+to+start+trip.' + config.appName,
  'tripEndPaymentToRider': config.appName + ' Please pay ' + config.currencySymbol + '{FEE} to driver.Thank you.',
  'sendPasswordToUser': 'Use this Password to Login to ' + config.appName + ' app. Your Password : {PASSWORD}. Thank you. Website : ' + config.landingurl,
  'rideLaterReceived': config.appName + ' - Your Request for TripNo : {TRIPNO} Has been sheduled successfully. Thank You. Website : ' + config.landingurl,
};

export const sendSmsMsg = (smsto, smsbody = '', phCode = config.phoneCode, sendSMSForKey = '', msgObj = {}) => {
  if (smsbody == '') smsbody = generateSMSBody(sendSMSForKey, msgObj);
  if (smsbody == false) {
    return false;
  }
  if (curSmsGateway == 'localAPI') {
    const formData = {
      message: smsbody,
      to: phCode + smsto
    };
    var smsRequestURL = GFunctions.convertLableDynamically(config.smsGateway.localAPIendpoint, formData);
    request.post(
      {
        url: smsRequestURL
      },
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log('body:', body);
        } else {
          console.log('error:', body);
        }
      }
    );
  }

  // var endpoint = "http://sms.nissisms.com/api/v4/?api_key=Ad27923d2030cb67e28f652c9ce85d473&method=sms&message=" + smsbody +"&to="+ smsto +"&sender=NISSII";

  // request.post(
  //   {
  //     url: endpoint
  //     // url: 'http://sms.nissisms.com/api/web2sms.php?username=vsrmhsch&password=Vsrmhs44&sender=VSRMHS',
  //     // form: formData
  //   },
  //   function (error, response, body) {
  //     if (!error && response.statusCode == 200) {
  //       console.log('body:', body);
  //     } else {
  //       console.log('error:', body);
  //     } 
  //   }
  // );


  /*   var endpoint = "http://websmsapp.in/api/mt/SendSMS?user=sjshettym007&password=12345&senderid=TELEOS&channel=Trans&DCS=0&flashsms=0&number=" + smsto + "&text=" + smsbody + "&route=2";
  
    request.get(
      {
        url: endpoint
      },
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log('body:', body);
        } else {
          console.log('error:', body);
        }
      }
    ); */

  if (curSmsGateway == 'twilio') {
    client.messages.create({
      body: smsbody,
      to: phCode + smsto,  // Text this number
      from: twilioNo // From a valid Twilio number    
    })
      .then((message) => console.log(message.body))//sid
      .catch((err) => console.log(err));
  }//Twilio


  if (curSmsGateway == 'nexmo') {
    var from = config.smsGateway.nexmoapiNumber;
    nexmoConfig.message.sendSms(
      from, smsto, smsbody, { type: 'unicode' },
      (err, responseData) => {
        if (err) {
          console.log(err);
        } else {
          console.dir(responseData);
        }
      }
    );
  }//Nexmo

  if (curSmsGateway == 'rahisi') { //Zanzibar
    var username = config.smsGateway.rahisiUsername;
    var password = config.smsGateway.rahisiPassword;
    var smstoNo = phCode + smsto;
    request.post(
      {
        url: `https://sms.rahisi.co.tz/api.php?do=sms&username=${username}&password=${password}&senderid=Oyaa&dest=${smstoNo}&msg=${smsbody}`
      },
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log('body:', body);
        } else {
          console.log('error:', body);
        }
      }
    );
  }//Rahisi

  if (curSmsGateway == "telnyx") {
    const headers = {
      'X-Profile-Secret': config.smsGateway.telnyxSecretKey,
    }

    const payload = {
      'from': config.smsGateway.telnyxNo,
      'to': phCode + smsto,
      'body': smsbody,
      'delivery_status_webhook_url': 'https://example.com/campaign/7214'
    }

    request.post({
      url: 'https://sms.telnyx.com/messages',
      headers: headers,
      json: payload
    }, function (err, resp, body) {
      console.log('error:', err);
      console.log('statusCode:', resp && resp.statusCode);
      console.log('body:', body);
    });
  }


}


/**
 * No Driver Found Alert To Rider
 * @param {*} requestFrom 
 * @param {*} ridid 
 */
export const noDriverFoundSMS = async (requestFrom, ridid, adminId = '') => {
  if (
    typeof requestFrom !== "undefined"
    && requestFrom === "admin"
  ) {
    let riderDoc = await Rider.findById(ridid).exec();
    if (riderDoc !== null) {
      let smsContent = "Sorry, We cannot able to Process your request now, please try after sometime.";
      sendSmsMsg(riderDoc.phone, smsContent);
    }

    if (adminId) {
      adminId = mongoose.Types.ObjectId(adminId);
      let adminDoc = await Admin.findById(adminId, { _id: 1, fcmId: 1 }).exec();
      console.log(adminDoc)
      GFunctions.sendAdminPushMsg(adminDoc.fcmId, 'No Driver Found!');
    }

  }
}

function generateSMSBody(sendSMSForKey, msgObj) {
  var smsbody = false;
  if (sendSMSForKey in sendSMSFor) {
    smsbody = GFunctions.convertLableDynamically(sendSMSFor[sendSMSForKey], msgObj);
  }
  return smsbody;
}