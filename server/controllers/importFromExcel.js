import mongoose from 'mongoose';

//import models
import Vehicle from '../models/vehicletype.model';
import Rider from '../models/rider.model';
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';
import Admin from '../models/admin.model';
import CompanyDetails from '../models/company.model';
import Vehicletype from '../models/vehicletype.model';
import DriverPayment from '../models/driverpayment.model';
import Wallet from '../models/wallet.model';
import Schedule from '../models/schedules.model';
import DriverBank from '../models/driverBank.model';
import DriverBankTransaction from '../models/driverBankTransaction.model';
import * as Mailer from './email.controller';
import * as smsGateway from './smsGateway';
import * as mailGateway from './mailGateway';
import * as Stripe from './stripe';
import * as TripHelpers from './tripHelper';
import * as fareCalculation from './fareCalculation';
import ServiceAvailableCities from '../models/serviceAvailableCities.model';
import FavAddress from '../models/favAddress.model';

import tripPaths from '../models/tripPaths.model';
const turf = require("@turf/turf")
const _ = require('lodash');

import * as appCtrl from '../controllers/app';
import * as GFunctions from './functions';
const nodemailer = require('nodemailer');

var firebase = require('firebase');
var config = require('../config');

import * as Braintree from './paymentGateway/braintree';
import * as paystack from './paymentGateway/paystack';

//Import helper
import * as DriverCtrl from './driver';
//Import helper

const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');

export const insertDriverData = (req, res) => {

  var filepath = '';
  if (req['file'] != null) {
    filepath = req['file'].path;
  }

  if (filepath == '') return res.status(409).json({ 'success': false, 'message': req.i18n.__("UPLOAD_VALID_FILE") });
  var inputFilePath = path.join(__dirname, '..', filepath);

  fs.createReadStream(inputFilePath)
    .pipe(csv())
    .on('data', function (data) {
      try {
        var driverData = {
          Code: data.id,
          Name: data.name,
          emailId: data.emailId,
          Mobile: data.mobile,
          Gender: data.gender,
          vehicletype: data.vehicleCategory,
          DrivingLicence: data.DrivingLicence,
          Color: data.Color,
          licence: data.vehicleNumber,
          Year: data.Year,
          model: data.Make,
          makename: data.Model,
          Password: config.resetPasswordTo,
          phoneCode: config.phoneCode,
          status: data.approved ? 'Accepted' : 'pending' //Accepted,pending
        };
        //perform the operation
        addDriverData(driverData)
      }
      catch (err) {
        //error handler
        console.log(err)
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
      }
    })
    .on('end', function () {
      //some final operation
      return res.status(200).json({ 'success': true, 'message': req.i18n.__("DATA_IMPORTED") });
    });

}

function addDriverData(data) {
  var id = mongoose.Types.ObjectId();
  var id2 = mongoose.Types.ObjectId();

  var newDoc = new Driver(
    {
      _id: id,
      code: data.Code,
      nic: '',
      fname: data.Name,
      lname: '',
      email: data.emailId,
      phcode: data.phoneCode,
      phone: data.Mobile,
      gender: data.Gender,
      cnty: "",
      cntyname: "",
      state: "",
      statename: "",
      city: "",
      cityname: "",
      cmpy: null,
      cur: "",
      actMail: "",
      actHolder: "",
      actNo: "",
      actBank: "",
      actLoc: "",
      actCode: "",
      softdel: "active",
      fcmId: "",

      "curStatus": "free",
      "curService": data.vehicletype,
      "serviceStatus": "active",
      "currentTaxi": id2,

      status: [{
        curstatus: 'active',
        docs: data.status
      }],
      licence: data.DrivingLicence,
      taxis: [{
        "color": data.Color,
        "driver": id,
        "cpy": "",
        "licence": data.licence,
        "year": data.Year,
        "model": data.model,
        "makename": data.makename,
        "_id": id2,
        "taxistatus": "active",
        "noofshare": 0,
        "share": false,
        "vehicletype": data.vehicletype,
        "registrationexpdate": "",
        "registration": "",
        "permitexpdate": "",
        "permit": "",
        "insuranceexpdate": "",
        "insurance": "",
        "type": [
          {}
        ],
        "handicap": "false"
      }]
    }
  );
  newDoc.setPassword(data.Password);
  newDoc.save((err, datas) => {
    if (err) { console.log(err); }
    DriverCtrl.addDriverDatatoFb(id);
    DriverCtrl.addDrivertaxisDataFB(id, id2, data);

    DriverCtrl.addDriverWallet(datas._id, datas.fname, '');
    DriverCtrl.addDriverBank(datas._id, datas.fname);

    DriverCtrl.updateDriverProofStatusInFB(id, data.status);

  })

};

export const insertFavAddress = async (req, res) => {
  var filepath = '';
  if (req['file'] != null) {
    filepath = req['file'].path;
  }

  if (filepath == '') return res.status(409).json({ 'success': false, 'message': req.i18n.__("UPLOAD_VALID_FILE") });
  var inputFilePath = path.join(__dirname, '..', filepath);

  fs.createReadStream(inputFilePath)
    .pipe(csv())
    .on('data', function (data) {
      try {
        var addressData = {
          name: data.name,
          lng: data.longitude,
          lat: data.latitude,
        };
        //perform the operation
        addFavAddressData(addressData)
      }
      catch (err) {
        //error handler
        console.log(err)
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
      }
    })
    .on('end', function () {
      //some final operation
      return res.status(200).json({ 'success': true, 'message': req.i18n.__("DATA_IMPORTED") });
    });
}

function addFavAddressData(data) {
  var newDoc = new FavAddress(
    {
      name: data.name,
      coords: [data.lng, data.lat]
    }
  );

  newDoc.save((err, datas) => {
    if (err) { console.log(err); }
  })
}