// ./express-server/controllers/todo.server.controller.js
import mongoose from 'mongoose';
import path from 'path';
import * as HelperFunc from './adminfunctions';
const mround = require('mongo-round');
const url = require('url');
const _ = require('lodash');
import * as GFunctions from './functions';
const moment = require("moment");

//import models
import DriverPayment from '../models/driverpayment.model';
import Trips from '../models/trips.model';
import Rider from '../models/rider.model';
import Driver from '../models/driver.model';
import Wallet from '../models/wallet.model';
import DriverPackage from '../models/driverPackage.model';
import { dataflow_v1b3 } from 'googleapis';

/**
 * Driver Pay Reports
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const driverPayReport = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };
  if (req.cityWise == 'exists') dateLikeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') dateLikeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  if (req.type == 'company') likeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  if (req.providerId) likeQuery['providerId'] = new mongoose.Types.ObjectId(req.providerId);
  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  let TotCnt = DriverPayment.aggregate([
    { "$match": dateLikeQuery },
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
      },
    },
    { "$match": likeQuery },
  ]);

  let Datas = DriverPayment.aggregate([
    { "$match": dateLikeQuery },
    {
      "$lookup": {
        "localField": "driver",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
        "commision": { $sum: "$commision" },
        "inhand": { $sum: "$inhand" },
        "digital": { $sum: "$digital" },
        "toSettle": { $sum: "$toSettle" },
        "code": { $addToSet: "$userinfo.code" },
        "scity": { $addToSet: "$userinfo.scity" },
        "scId": { $addToSet: "$userinfo.scId" },
        "cmpy": { $first: "$userinfo.cmpy" },
        "cntyname": { $first: "$cntyname" },
        "amttodriver": { $sum: "$amttodriver" },
        "providerId": { $addToSet: { $cond: [{ $ifNull: ['$userinfo.providerId', false] }, '$userinfo.providerId', ''] } }
      },
    },
    {
      $unwind: "$providerId"
    },
    {
      $project:
      {
        _id: 1,
        dvrfname: 1,
        count: 1,
        commision: mround('$commision', 2),
        inhand: mround('$inhand', 2),
        digital: mround('$digital', 2),
        toSettle: mround('$toSettle', 2),
        amttodriver: mround('$amttodriver', 2),
        code: 1,
        scity: 1,
        scId: 1,
        cmpy: 1,
        providerId: 1
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    skip,
    limit,
  ]);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0].length);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

export const driverPayReportDailyFormat = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };
  if (req.cityWise == 'exists') dateLikeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') dateLikeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  if (req.type == 'company') likeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }

  let TotCnt = DriverPayment.aggregate([
    { "$match": dateLikeQuery },
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
      },
    },
    { "$match": likeQuery },
  ]);

  let Datas = DriverPayment.aggregate([
    { "$match": dateLikeQuery },
    {
      "$lookup": {
        "localField": "driver",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
        "commision": { $sum: "$commision" },
        "inhand": { $sum: "$inhand" },
        "digital": { $sum: "$digital" },
        "toSettle": { $sum: "$toSettle" },
        "code": { $addToSet: "$userinfo.code" },
        "scity": { $addToSet: "$userinfo.scity" },
        "scId": { $addToSet: "$userinfo.scId" },
        "cmpy": { $first: "$userinfo.cmpy" },
        "cntyname": { $first: "$cntyname" },
        "amttodriver": { $sum: "$amttodriver" },
      },
    },
    {
      $project:
      {
        _id: 1,
        dvrfname: 1,
        count: 1,
        commision: mround('$commision', 2),
        inhand: mround('$inhand', 2),
        digital: mround('$digital', 2),
        toSettle: mround('$toSettle', 2),
        amttodriver: 1,
        code: 1,
        scity: 1,
        scId: 1,
        cmpy: 1

      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    skip,
    limit,
  ]);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0].length);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

/**
 * Driver Pay Reports Single
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const driverPayReportSingle = (req, res) => {
  DriverPayment.find({ driver: req.params.id, todvr: 'no' }, function (err, docs) {
    if (err) {
      res.json([]);
    } else {
      res.json(docs);
    }
  });
}


/**
 * All Ride Payment Report
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const paymentReport = async (req, res) => {

  if (req.query._sort == "" || typeof req.query._sort == "undefined") {
    req.query._sort = "createdAt"
  }

  if (req.query._order == "" || typeof req.query._order == "undefined") {
    req.query._order = "ASC"
  }
  var likeQuery = HelperFunc.likeQueryBuilder(req.query, DriverPayment);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = {};
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);
  sortQuery['tripFDT'] = 1;
  if (req.providerId) likeQuery['providerId'] = new mongoose.Types.ObjectId(req.providerId);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') likeQuery['cmpy'] = mongoose.Types.ObjectId(req.userId);
  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };
  if (likeQuery['createdAt']) {
    var today = likeQuery['createdAt']['$gte'];//"2019-01-23";//likeQuery['createdAt'];
    var todayplusone = likeQuery['createdAt']['$lte'];// moment(today).add(1, "month").format("YYYY-MM-DD");
  } else {
    var today = GFunctions.getISOTodayDate();
    var todayplusone = GFunctions.getISODateADayBuffer(1, "YYYY-MM-DD");
  }
  dateLikeQuery = {
    createdAt: { $gt: new Date(today), $lt: new Date(todayplusone) }
  };
  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }

  let countData = DriverPayment.find(likeQuery).count();
  // console.log(dateLikeQuery)
  let Datas = DriverPayment.aggregate([
    {
      "$match": dateLikeQuery
    },
    {
      "$lookup": {
        "localField": "driver",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo",

      }
    },
    {
      $unwind: {
        path: "$userinfo",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project:
      {
        "_id": 1,
        "tripno": 1,
        "driver": 1,
        "chId": 1,
        "todvr": 1,
        "ispaid": 1,
        "mtd": 1,
        "toSettle": 1,
        "amttodriver": 1,
        "inhand": 1,
        "outstanding": 1,
        "digital": 1,
        "stripedebt": 1,
        "walletdebt": 1,
        "promoamt": 1,
        "commision": 1,
        "cashpaid": 1,
        "amttopay": 1,
        "dvrfname": 1,
        "createdAt": 1,
        "__v": 1,

        code: "$userinfo.code",
        scity: "$userinfo.scity",
        scId: "$userinfo.scId",
        cmpy: "$userinfo.cmpy",
        providerId: { $cond: [{ $ifNull: ['$userinfo.providerId', false] }, '$userinfo.providerId', ''] }
      }
    },
    {
      "$match": likeQuery
    },
    {
      "$sort": sortQuery
    },
    skip,
    limit,
  ]);
  try {
    var promises = await Promise.all([countData, Datas]);
    res.header('x-total-count', promises[0]);
    // setHeader(name, value)
    res.setHeader('report-based', promises[0]);

    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }

}

export const paymentReportDaily = async (req, res) => {
  if (req.query.date) {
    var today = req.query.date;
    var todayplusone = moment(today).add(1, "days").format("YYYY-MM-DD");
  } else {
    var today = GFunctions.getISOTodayDate();
    var todayplusone = GFunctions.getISODateADayBuffer(1, "YYYY-MM-DD");
  }
  var dateLikeQuery = {
    createdAt: { $gt: new Date(today), $lt: new Date(todayplusone) }
  };

  let Datas = Trips.aggregate([
    {
      "$match": dateLikeQuery
    },

    {
      "$project": {
        createdAt: {
          $dateToString: { format: "%H", date: "$createdAt" }
        },
        tripno: 1,
        vehicle: 1,
        paymentMode: 1,
        status: 1,
        acsp: 1,
        csp: 1,
        paymentSts: 1,
        fare: 1,
        bookingFor: 1,
        "bookingTypeRideNow": { $cond: { if: { $eq: ["$bookingType", 'rideNow'] }, then: 1, else: 0 } },
        "bookingTypeRideLater": { $cond: { if: { $eq: ["$bookingType", 'rideLater'] }, then: 1, else: 0 } },
        "requestFromApp": { $cond: { if: { $eq: ["$requestFrom", 'app'] }, then: 1, else: 0 } },
        "requestFromAdmin": { $cond: { if: { $eq: ["$requestFrom", 'admin'] }, then: 1, else: 0 } },
        triptype: 1,
        adsp: 1,
      }
    },
    {
      $sort: {
        createdAt: -1
      }
    },

    {
      "$group": {
        _id: "$createdAt",
        count: { $sum: 1 },
        fare: { $sum: "$fare" },
        requestFromApp: { $sum: "$requestFromApp" },
        requestFromAdmin: { $sum: "$requestFromAdmin" },
        bookingTypeRideNow: { $sum: "$bookingTypeRideNow" },
        bookingTypeRideLater: { $sum: "$bookingTypeRideLater" },
        totalFare: { $sum: "$acsp.cost" },
        comison: { $sum: "$acsp.comison" },
        // vehicle : {
        //   $push : "$vehicle"
        // }
      }
    }
  ]);

  try {
    var promises = await Promise.all([Datas]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }

}

/**
 * markSettledDvrPayment
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const markSettledDvrPayment = (req, res) => {
  var dvrstosettle = req.body;
  dvrstosettle.forEach(function (element) {
    markSettledDvrPaymentFunc(element);
  });
  return res.json(req.body);
}

function markSettledDvrPaymentFunc(element) {
  //If paid out log Trx Id too
  var driverId = new mongoose.Types.ObjectId(element._id);
  var query = { driver: driverId };
  var update = { todvr: 'yes' };

  DriverPayment.update(query, update, { multi: true }, function (err) {
    if (err) {
      throw err;
    }
    else {
      console.log("updated!");
    }
  });

}


/**
 * [canceledTrips]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const canceledTrips = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);
  likeQuery['status'] = 'Cancelled';

  let TotCnt = Trips.find(likeQuery).count();

  let Datas = Trips.aggregate(
    [
      {
        "$lookup": {
          "localField": "dvrid",
          "from": "drivers",
          "foreignField": "_id",
          "as": "userinfo"
        }
      },
      {
        $unwind: {
          path: "$userinfo",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project:
        {
          "_id": 1,
          "date": 1,
          "tripno": 1,
          "cpy": 1,
          "cpyid": 1,
          "dvr": 1,
          "rid": 1,
          "ridid": 1,
          "taxi": 1,
          "service": 1,
          "estTime": 1,
          "__v": 1,
          "dvrid": 1,
          "tripFDT": 1,
          "utc": 1,
          "tripDT": 1,
          "driverfb": 1,
          "riderfb": 1,
          "needClear": 1,
          "curReq": 1,
          "reqDvr": 1,
          "review": 1,
          "status": 1,
          "adsp": 1,
          "acsp": 1,
          "dsp": 1,
          "csp": 1,
          "paymentSts": 1,
          "triptype": 1,
          "createdAt": 1,
          code: "$userinfo.code"
        }
      },

      { "$match": likeQuery },
      { "$sort": sortQuery },
      { "$skip": pageQuery.skip },
      { "$limit": pageQuery.take },
    ]);

  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
}



/**
 * [tripStatus]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const tripStatus = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') likeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});
  if (req.cityWise == 'exists') dateLikeQuery['scId'] = { "$in": req.scId };
  if (req.query['scity_like'] != undefined) dateLikeQuery['scity'] = req.query['scity_like'];
  if (req.type == 'company') dateLikeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);

  if (_.isEmpty(dateLikeQuery)) {
    var fromDate = new Date(GFunctions.getCommonMonthStartDate());
    var toDate = new Date(GFunctions.getISODate());
    dateLikeQuery =
      {
        tripFDT:
        {
          '$gte': fromDate,
          '$lte': toDate
        }
      }
  };

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  console.log(dateLikeQuery)
  // let TotCnt = Trips.find(likeQuery).count();

  let Datas = Trips.aggregate(
    [
      { "$match": dateLikeQuery },

      {
        $project:
        {
          tripId: "$_id",
          tripstatus: "$status",
          _id: 0,
          yearMonthDayUTC: { $dateToString: { format: "%d-%m-%Y", date: "$tripFDT" } },
        }
      },
      {
        "$group":
        {
          _id: { yearMonthDayUTC: "$yearMonthDayUTC", tripstatus: "$tripstatus" },
          "tripCount": { "$sum": 1 }
        }
      },

      {
        $project:
        {
          _id: 0,
          date: "$_id.yearMonthDayUTC",
          sortDate: { $toDate: "$_id.yearMonthDayUTC" },
          "noresponse": { $cond: { if: { $eq: ["$_id.tripstatus", 'noresponse'] }, then: "$tripCount", else: 0 } },
          "processing": { $cond: { if: { $eq: ["$_id.tripstatus", 'processing'] }, then: "$tripCount", else: 0 } },
          "canceled": { $cond: { if: { $eq: ["$_id.tripstatus", 'Cancelled'] }, then: "$tripCount", else: 0 } },
          "Finished": { $cond: { if: { $eq: ["$_id.tripstatus", 'Finished'] }, then: "$tripCount", else: 0 } },
        }
      },

      {
        "$group":
        {
          _id: "$date",
          "sortDate":{ $first: "$sortDate" },
          "noresponse": { "$sum": "$noresponse" },
          "processing": { "$sum": "$processing" },
          "canceled": { "$sum": "$canceled" },
          "Finished": { "$sum": "$Finished" },
        }
      },

      {
        "$sort": {
          sortDate: 1
        }
      },

      // sortQuery,
      // skip,
      // limit,
    ]);
  try {
    var promises = await Promise.all([Datas]);
    // res.header('x-total-count', promises[0]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
}

export const tripStatusDaily = async (req, res) => {
  if (req.query.date) {
    var today = req.query.date;
    var todayplusone = moment(today).add(1, "days").format("YYYY-MM-DD");
  } else {
    var today = GFunctions.getISOTodayDate();
    var todayplusone = GFunctions.getISODateADayBuffer(1, "YYYY-MM-DD");
  }
  var dateLikeQuery = {
    createdAt: { $gt: new Date(today), $lt: new Date(todayplusone) }
  };

  let Datas = Trips.aggregate([
    {
      "$match": dateLikeQuery
    },

    {
      "$project": {
        createdAt: {
          $dateToString: { format: "%H", date: "$createdAt" }
        },
        "finished": { $cond: { if: { $eq: ["$status", 'Finished'] }, then: 1, else: 0 } },
        "noresponse": { $cond: { if: { $eq: ["$status", 'noresponse'] }, then: 1, else: 0 } },
        "processing": { $cond: { if: { $eq: ["$status", 'processing'] }, then: 1, else: 0 } },
        "canceled": { $cond: { if: { $eq: ["$status", 'canceled'] }, then: 1, else: 0 } },
      }
    },
    {
      $sort: {
        createdAt: -1
      }
    },
    {
      "$group": {
        _id: "$createdAt",
        finished: { $sum: "$finished" },
        noresponse: { $sum: "$noresponse" },
        processing: { $sum: "$processing" },
        canceled: { $sum: "$canceled" },
      }
    }
  ]);

  try {
    var promises = await Promise.all([Datas]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

/**
 * [tripStatus]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const tripTypes = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') likeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});
  if (req.cityWise == 'exists') dateLikeQuery['scId'] = { "$in": req.scId };
  if (req.query['scity_like'] != undefined) dateLikeQuery['scity'] = req.query['scity_like'];
  if (req.type == 'company') dateLikeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);

  if (_.isEmpty(dateLikeQuery)) {
    var fromDate = new Date(GFunctions.getCommonMonthStartDate());
    var toDate = new Date(GFunctions.getISODate());
    dateLikeQuery = {
      tripFDT:
      {
        '$gte': fromDate,
        '$lte': toDate
      }
    }
  }
  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  let Datas = Trips.aggregate(
    [
      { "$match": dateLikeQuery },
      {
        $project:
        {
          tripId: "$_id",
          bookingType: "$bookingType",
          _id: 0,
          scity: "$scity",
          // cpy:"$cpy",
          yearMonthDayUTC: { $dateToString: { format: "%d-%m-%Y", date: "$tripFDT" } },
        }
      },
      {
        "$group":
        {
          _id: { yearMonthDayUTC: "$yearMonthDayUTC", bookingType: "$bookingType" },
          "scity": { $addToSet: "$scity" },
          // "cpy":{ $first:"$cpy"},
          "tripCount": { "$sum": 1 }
        }
      },

      {
        $project:
        {
          _id: 1,
          date: "$_id.yearMonthDayUTC",
          sortDate: { $toDate: "$_id.yearMonthDayUTC" },
          scity: "$scity",
          // cpy:"$cpy",
          "rideNow": { $cond: { if: { $eq: ["$_id.bookingType", 'rideNow'] }, then: "$tripCount", else: 0 } },
          "rideLater": { $cond: { if: { $eq: ["$_id.bookingType", 'rideLater'] }, then: "$tripCount", else: 0 } },
          "hailRide": { $cond: { if: { $eq: ["$_id.bookingType", 'hailRide'] }, then: "$tripCount", else: 0 } },
        }
      },

      {
        "$group":
        {
          _id: "$date",
          "sortDate":{ $first: "$sortDate" },
          scity: { $addToSet: "$scity" },
          // cpy:{ $first:"$cpy"},
          "rideNow": { "$sum": "$rideNow" },
          "rideLater": { "$sum": "$rideLater" },
          "hailRide": { "$sum": "$hailRide" },
        }
      },
      { $sort: { sortDate: 1 } }
      // skip,
      // limit,
    ]);

  try {
    var promises = await Promise.all([Datas]);
    // res.header('x-total-count', promises[0]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
}


export const tripTypesDaily = async (req, res) => {
  if (req.query.date) {
    var today = req.query.date;
    var todayplusone = moment(today).add(1, "days").format("YYYY-MM-DD");
  } else {
    var today = GFunctions.getISOTodayDate();
    var todayplusone = GFunctions.getISODateADayBuffer(1, "YYYY-MM-DD");
  }
  var dateLikeQuery = {
    createdAt: { $gt: new Date(today), $lt: new Date(todayplusone) }
  };

  let Datas = Trips.aggregate([
    {
      "$match": dateLikeQuery
    },
    {
      "$project": {
        createdAt: {
          $dateToString: { format: "%H", date: "$createdAt" }
        },
        "rideNow": { $cond: { if: { $eq: ["$bookingType", 'rideNow'] }, then: 1, else: 0 } },
        "rideLater": { $cond: { if: { $eq: ["$bookingType", 'rideLater'] }, then: 1, else: 0 } },
        "hailRide": { $cond: { if: { $eq: ["$bookingType", 'hailRide'] }, then: 1, else: 0 } },
      }
    },
    {
      $sort: {
        createdAt: -1
      }
    },
    {
      "$group": {
        _id: "$createdAt",
        rideNow: { $sum: "$rideNow" },
        rideLater: { $sum: "$rideLater" },
        hailRide: { $sum: "$hailRide" },
      }
    }
  ]);

  try {
    var promises = await Promise.all([Datas]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

/**
 * [tripBookingTypes]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const tripBookedBy = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') likeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});
  if (req.cityWise == 'exists') dateLikeQuery['scId'] = { "$in": req.scId };
  if (req.query['scity_like'] != undefined) dateLikeQuery['scity'] = req.query['scity_like'];
  if (req.type == 'company') dateLikeQuery['cpyid'] = mongoose.Types.ObjectId(req.userId);

  if (_.isEmpty(dateLikeQuery)) {
    var fromDate = new Date(GFunctions.getCommonMonthStartDate());
    var toDate = new Date(GFunctions.getISODate());
    dateLikeQuery = {
      tripFDT:
      {
        '$gte': fromDate,
        '$lte': toDate
      }
    }
  }

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }

  let Datas = Trips.aggregate(
    [
      { "$match": dateLikeQuery },

      {
        $project:
        {
          tripId: "$_id",
          requestFrom: "$requestFrom",
          _id: 0,
          scity: "$scity",
          // cpy:"$cpy",
          yearMonthDayUTC: { $dateToString: { format: "%d-%m-%Y", date: "$tripFDT" } },
        }
      },
      {
        "$group":
        {
          _id: { yearMonthDayUTC: "$yearMonthDayUTC", requestFrom: "$requestFrom" },
          "scity": { $addToSet: "$scity" },
          // "cpy":{ $first:"$cpy"},
          "tripCount": { "$sum": 1 }
        }
      },

      {
        $project:
        {
          _id: 1,
          date: "$_id.yearMonthDayUTC",
          sortDate: { $toDate: "$_id.yearMonthDayUTC" },
          scity: "$scity",
          // cpy:"$cpy",
          "app": { $cond: { if: { $eq: ["$_id.requestFrom", 'app'] }, then: "$tripCount", else: 0 } },
          "admin": { $cond: { if: { $eq: ["$_id.requestFrom", 'admin'] }, then: "$tripCount", else: 0 } },
          "hotel": { $cond: { if: { $eq: ["$_id.requestFrom", 'hotel'] }, then: "$tripCount", else: 0 } },
        }
      },

      {
        "$group":
        {
          _id: "$date",
          scity: { $addToSet: "$scity" },
          "sortDate":{ $first: "$sortDate" },
          // cpy:{ $first:"$cpy"},
          "app": { "$sum": "$app" },
          "admin": { "$sum": "$admin" },
          "hotel": { "$sum": "$hotel" },
        }
      },
      { $sort: { sortDate: 1 } }
      // skip,
      // limit,
    ]);

  try {
    var promises = await Promise.all([Datas]);
    // res.header('x-total-count', promises[0]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
}

export const tripBookedByDaily = async (req, res) => {
  if (req.query.date) {
    var today = req.query.date;
    var todayplusone = moment(today).add(1, "days").format("YYYY-MM-DD");
  } else {
    var today = GFunctions.getISOTodayDate();
    var todayplusone = GFunctions.getISODateADayBuffer(1, "YYYY-MM-DD");
  }
  var dateLikeQuery = {
    createdAt: { $gt: new Date(today), $lt: new Date(todayplusone) }
  };

  let Datas = Trips.aggregate([
    {
      "$match": dateLikeQuery
    },
    {
      "$project": {
        createdAt: {
          $dateToString: { format: "%H", date: "$createdAt" }
        },
        "app": { $cond: { if: { $eq: ["$requestFrom", 'app'] }, then: 1, else: 0 } },
        "admin": { $cond: { if: { $eq: ["$requestFrom", 'admin'] }, then: 1, else: 0 } },
        "hotel": { $cond: { if: { $eq: ["$requestFrom", 'hotel'] }, then: 1, else: 0 } },
      }
    },
    {
      $sort: {
        createdAt: -1
      }
    },
    {
      "$group": {
        _id: "$createdAt",
        app: { $sum: "$app" },
        admin: { $sum: "$admin" },
        hotel: { $sum: "$hotel" },
      }
    }
  ]);

  try {
    var promises = await Promise.all([Datas]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

/**
 * UserWallet 
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
/* export const userWallet = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  //if(req.type == 'company') likeQuery['cmpy'] = {"$in":req.userId};
  // if(req.query['scity_like']!=undefined)likeQuery['scity']= req.query['scity_like'] ; 

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }

  let TotCnt = Wallet.find(likeQuery).count();
  let Datas = Wallet.aggregate([
    {
      "$lookup": {
        "localField": "ridid",
        "from": "riders",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$project": {
        "bal": mround('$bal', 2),
        "trx": 1,
        "fname": "$userinfo.fname",
        "riderId": "$userinfo._id",
        "phone": "$userinfo.phone",
        "scId": "$userinfo.scId",
        "scity": "$userinfo.scity"
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    skip,
    limit
  ]);

  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }

} */
export const userWallet = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  //if(req.type == 'company') likeQuery['cmpy'] = {"$in":req.userId};
  // if(req.query['scity_like']!=undefined)likeQuery['scity']= req.query['scity_like'] ; 
  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  var sort = req.query._sort;

  if ((typeof sort).toString() == "undefined") {
    sortQuery['_id'] = 1;
  }

  let TotCnt = Wallet.find(likeQuery).count();
  let Datas = Wallet.aggregate([
    // { "$match": likeQuery }, 
    {
      "$lookup": {
        "localField": "ridid",
        "from": "riders",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$project": {
        "_id": 1,
        "bal": mround('$bal', 2),
        "trx": 1,
        "fname": "$userinfo.fname",
        "lname": "$userinfo.lname",
        "riderId": "$userinfo._id",
        "phone": "$userinfo.phone",
        "scId": "$userinfo.scId",
        "scity": "$userinfo.scity"
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    skip,
    limit
  ]);

  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(promises[1]);
  } catch (err) {
    return res.json([err]);
  }

}

/**
 * Referrer 
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const referrer = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  // likeQuery = HelperFunc.findQueryBuilder(req.query,likeQuery);   
  // likeQuery['status']  = 'canceled';  

  // let TotCnt = Rider.find(likeQuery).count() ;
  Wallet.aggregate([
    { "$match": { "trx.for": "reference" } }, //Make to "" as Referal - Referal Code i

    {
      "$lookup": {
        "localField": "ridid",
        "from": "riders",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$project": {
        "bal": 1,
        "trx": 1,
        "userinfo.fname": 1,
        "userinfo.phone": 1
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    { "$skip": pageQuery.skip },
    { "$limit": pageQuery.take },
  ], function (err, result) {
    if (err) {
      return res.json([]);
    }
    return res.json(result);
  });

}


/**
 * [tripTimeVariance]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const tripTimeVariance = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);
  likeQuery['status'] = 'Finished';

  let TotCnt = Trips.find(likeQuery).count();
  //let Datas = Trips.find(likeQuery).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery); //have to catch Err
  let Datas = Trips.aggregate(
    [
      {
        "$lookup": {
          "localField": "dvrid",
          "from": "drivers",
          "foreignField": "_id",
          "as": "userinfo"
        }
      },
      {
        $unwind: {
          path: "$userinfo",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project:
        {
          "_id": 1,
          "date": 1,
          "tripno": 1,
          "cpy": 1,
          "cpyid": 1,
          "dvr": 1,
          "rid": 1,
          "ridid": 1,
          "taxi": 1,
          "service": 1,
          "estTime": 1,
          "__v": 1,
          "dvrid": 1,
          "tripFDT": 1,
          "utc": 1,
          "tripDT": 1,
          "driverfb": 1,
          "riderfb": 1,
          "needClear": 1,
          "curReq": 1,
          "reqDvr": 1,
          "review": 1,
          "status": 1,
          "adsp": 1,
          "acsp": 1,
          "dsp": 1,
          "csp": 1,
          "paymentSts": 1,
          "triptype": 1,
          "createdAt": 1,
          code: "$userinfo.code"
        }
      },

      { "$match": likeQuery },
      { "$sort": sortQuery },
      { "$skip": pageQuery.skip },
      { "$limit": pageQuery.take },
    ]);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }

}


/**
 * [logReport]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const logReport = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);

  let TotCnt = Driver.find(likeQuery).count();
  //let Datas = Driver.find(likeQuery, { fname: 1, email: 1, phone: 1,last_out:1,last_in:1}).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery); //have to catch Err
  let Datas = Driver.aggregate([
    {
      $project: {
        _id: 0,
        'fname': 1,
        'email': 1,
        'last_in': 1,
        'last_out': 1,
        'totalMin': { $divide: [{ $subtract: ["$last_out", "$last_in"] }, 60 * 1000] },
        createdAt: 1
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    { "$skip": pageQuery.skip },
    { "$limit": pageQuery.take },
  ])
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}

/**
 *  All Driver Earnings
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const driverEarnings = async (req, res) => {
  //Match, sort, filter by No, limit, search, xtotal 
  /*     var likeQuery = HelperFunc.likeQueryBuilder(req.query); 
      var pageQuery = HelperFunc.paginationBuilder(req.query); 
      var totalCount = 10;  
      DriverPayment.aggregate([  
        {
          "$group"  : {
            "_id": '$driver',   
            "dvrfname": { $addToSet: "$dvrfname"  } ,   
            "count": {$sum: 1},  
          },  
        },   
        ], function (err, result) {
          if (err) { } else {
            totalCount = result.length; 
          }
        }); 
  
      DriverPayment.aggregate([  
          {
            "$group"  : {
              "_id": '$driver',   
              "dvrfname": { $addToSet: "$dvrfname"  } ,   
              "count": {$sum: 1}, 
              "commision": { $sum: "$commision" },  
              "inhand": { $sum: "$inhand" }, 
              "digital": { $sum: "$digital" }, 
              "toSettle": { $sum: "$toSettle" } 
            },  
          },  
  
          { $project : 
           {
             _id:1,
             dvrfname:1,
             count:1,
             commision : mround ('$commision', 2),
             inhand: mround ('$inhand', 2),  
             digital: mround ('$digital', 2),   
             toSettle: mround ('$toSettle', 2)  
               // count:{$cond:[{ $ne: ["$PlnSls", 0] },{$multiply:[{$divide: ['$ActSls', '$PlnSls']},100]},0]}
             }
  
           },
  
           {"$skip": pageQuery.skip},
           {"$limit": pageQuery.take},
           ], function (err, result) {
            if (err) {
              res.json(err); 
            } else {
              res.header('x-total-count',  totalCount );  
              res.json(result);
            }
          });   */
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  //console.log(likeQuery)


  let TotCnt = DriverPayment.aggregate([
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
      },
    },
  ]);
  // console.log(likeQuery)
  let Datas = DriverPayment.aggregate([
    {
      "$lookup": {
        "localField": "driver",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$group": {
        "_id": '$driver',
        "dvrfname": { $addToSet: "$dvrfname" },
        "count": { $sum: 1 },
        "commision": { $sum: "$commision" },
        "inhand": { $sum: "$inhand" },
        "digital": { $sum: "$digital" },
        "toSettle": { $sum: "$toSettle" },
        "code": { $addToSet: "$userinfo.code" },
        // "fname":{ $addToSet: "$userinfo.fname"} 
      },
    },
    //{ $match:likeQuery},
    {
      $project:
      {
        _id: 1,
        dvrfname: 1,
        count: 1,
        commision: mround('$commision', 2),
        inhand: mround('$inhand', 2),
        digital: mround('$digital', 2),
        toSettle: mround('$toSettle', 2),
        code: 1,
        // fname:1  
        // count:{$cond:[{ $ne: ["$PlnSls", 0] },{$multiply:[{$divide: ['$ActSls', '$PlnSls']},100]},0]}
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    { "$skip": pageQuery.skip },
    { "$limit": pageQuery.take },
  ]);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0].length);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}
/*   const Schema = mongoose.Schema;
  const ObjectId = Schema.Types.ObjectId;
export const test = (req,res) =>{
  Trips.find().limit(2).exec((err,data)=>{
   if(err) {}
   console.log(data[0].dvrid)


   })
  })
} */


export const packagePurchaseHistory = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});

  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };

  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }

  let TotCnt = DriverPackage.find(dateLikeQuery).count();
  let Datas = DriverPackage.aggregate([
    { "$match": dateLikeQuery },
    {
      "$lookup": {
        "localField": "driverId",
        "from": "drivers",
        "foreignField": "_id",
        "as": "userinfo"
      }
    },
    { "$unwind": "$userinfo" },
    {
      "$lookup": {
        "localField": "userinfo.providerId",
        "from": "admins",
        "foreignField": "_id",
        "as": "admininfo"
      }
    },
    {
      "$unwind": {
        path: "$admininfo",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project:
      {
        _id: 1,
        driverName: 1,
        code: "$userinfo.code",
        scity: "$userinfo.scity",
        scId: "$userinfo.scId",
        packageId: 1,
        packageName: 1,
        amount: 1,
        type: 1,
        credit: 1,
        purchaseDate: 1,
        "providerName": { $cond: [{ $ifNull: ['$admininfo', false] }, '$admininfo.fname', ''] }
      }
    },
    { "$match": likeQuery },
    { "$sort": sortQuery },
    skip,
    limit,
  ]);


  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[1].length);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}


export const deliveryReport = async (req, res) => {
  if (req.query._sort == "" || typeof req.query._sort == "undefined") {
    req.query._sort = "createdAt"
  }

  if (req.query._order == "" || typeof req.query._order == "undefined") {
    req.query._order = "ASC"
  }
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);
  likeQuery['vehicleFor'] = 'delivery';
  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };
  if (req.query.requestFrom == "without_limit") {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  let TotCnt = Trips.find(likeQuery).count();
  let Datas = Trips.aggregate(
    [
      {
        "$lookup": {
          "localField": "dvrid",
          "from": "drivers",
          "foreignField": "_id",
          "as": "userinfo"
        }
      },
      {
        $unwind: {
          path: "$userinfo",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project:
        {
          "_id": 1,
          "date": 1,
          "tripno": 1,
          "cpy": 1,
          "cpyid": 1,
          "dvr": 1,
          "rid": 1,
          "ridid": 1,
          "taxi": 1,
          "service": 1,
          "estTime": 1,
          "__v": 1,
          "dvrid": 1,
          "tripFDT": 1,
          "utc": 1,
          "tripDT": 1,
          "driverfb": 1,
          "riderfb": 1,
          "needClear": 1,
          "curReq": 1,
          "reqDvr": 1,
          "review": 1,
          "status": 1,
          "adsp": 1,
          "acsp": 1,
          "dsp": 1,
          "csp": 1,
          "paymentSts": 1,
          "triptype": 1,
          "createdAt": 1,
          code: "$userinfo.code",
          vehicleFor: 1,
          deliverydetails: 1,
          delivery: 1
        }
      },
      {
        "$match": likeQuery
      },
      {
        "$sort": sortQuery
      },
      skip,
      limit
    ]);
  console.log("Datas", Datas);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    console.log("promises", promises)
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
}

export const deliveryTrip = async (req, res) => {
  if (req.query._sort == "" || typeof req.query._sort == "undefined") {
    req.query._sort = "createdAt"
  }

  if (req.query._order == "" || typeof req.query._order == "undefined") {
    req.query._order = "DESC"
  }
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  likeQuery = HelperFunc.findQueryBuilder(req.query, likeQuery);
  likeQuery['vehicleFor'] = 'delivery';

  let TotCnt = Trips.find(likeQuery).count();

  let Datas = Trips.aggregate(
    [
      {
        "$lookup": {
          "localField": "dvrid",
          "from": "drivers",
          "foreignField": "_id",
          "as": "userinfo"
        }
      },
      {
        $unwind: {
          path: "$userinfo",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project:
        {
          "_id": 1,
          "date": 1,
          "tripno": 1,
          "cpy": 1,
          "cpyid": 1,
          "dvr": 1,
          "rid": 1,
          "ridid": 1,
          "taxi": 1,
          "service": 1,
          "estTime": 1,
          "__v": 1,
          "dvrid": 1,
          "tripFDT": 1,
          "utc": 1,
          "tripDT": 1,
          "driverfb": 1,
          "riderfb": 1,
          "needClear": 1,
          "curReq": 1,
          "reqDvr": 1,
          "review": 1,
          "status": 1,
          "adsp": 1,
          "acsp": 1,
          "dsp": 1,
          "csp": 1,
          "paymentSts": 1,
          "triptype": 1,
          "createdAt": 1,
          code: "$userinfo.code",
          vehicleFor: 1,
          deliverydetails: 1,
          delivery: 1,
          vehicle: 1
        }
      },

      { "$match": likeQuery },
      { "$sort": sortQuery },
      { "$skip": pageQuery.skip },
      { "$limit": pageQuery.take },
    ]);

  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0]);
    var resstr = promises[1];
    res.send(resstr);
  } catch (err) {
    return res.json([err]);
  }
} 

export const getDriverReports = async (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var dateLikeQuery = HelperFunc.findQueryBuilder(req.query, {});
  var skip = { "$skip": pageQuery.skip },
    limit = { "$limit": pageQuery.take };
  if (req.query.requestFrom == 'without_limit') {
    skip = { "$match": {} }
    limit = { "$match": {} }
  }
  if (req.providerId) likeQuery['providerId'] = new mongoose.Types.ObjectId(req.providerId);
  let TotCnt = Driver.find(likeQuery);
  let Datas = Driver.aggregate([
    // { "$match": likeQuery },
    {
      "$lookup": {
        "localField": "currentPackPurchaseId",
        "from": "driverpackages",
        "foreignField": "_id",
        "as": "packageinfo"
      }
    },
    {
      "$unwind": {
        path: "$packageinfo",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      "$lookup": {
        "localField": "providerId",
        "from": "admins",
        "foreignField": "_id",
        "as": "admininfo"
      }
    },
    {
      "$unwind": {
        path: "$admininfo",
        preserveNullAndEmptyArrays: true
      }
    },
    // {
    //   "$lookup": {
    //     "localField": "adminId",
    //     "from": "admins",
    //     "foreignField": "_id",
    //     "as": "admin"
    //   }
    // },
    // {
    //   "$unwind": {
    //     path: "$admin",
    //     preserveNullAndEmptyArrays: true
    //   }
    // },
    // {
    //   $project:
    //   {
    //     _id: 1,
    //     fname: 1,
    //     email: 1,
    //     code: 1,
    //     phone: 1,
    //     packageId: { $cond: [{ $ifNull: ["$packageinfo.packageId", false] }, "$packageinfo.packageId", ''] },
    //     amount: { $cond: [{ $ifNull: ["$packageinfo.amount", false] }, "$packageinfo.amount", ''] },
    //     type: { $cond: [{ $ifNull: ["$packageinfo.type", false] }, "$packageinfo.type", ''] },
    //     credit: { $cond: [{ $ifNull: ["$packageinfo.credit", false] }, "$packageinfo.credit", ''] },
    //     purchaseDate: { $cond: [{ $ifNull: ["$packageinfo.purchaseDate", false] }, "$packageinfo.purchaseDate", ''] },
    //     "providerName": { $cond: [{ $ifNull: ['$admininfo', false] }, '$admininfo.fname', ''] },
    //     "driverDocStatus": "$status.docs",
    //     "onlineStatus": "$online",
    //     "subscriptionPackageName": { $cond: [{ $ifNull: ["$subscriptionPackName", false] }, "$subscriptionPackName", ''] },
    //     "subscriptionEndDate": { $cond: [{ $ifNull: ["$subcriptionEndDate", false] }, "$subcriptionEndDate", ""] },
    //     "vehStatus": {
    //       "$map": {
    //         input: "$taxis",
    //         as: "val",
    //         in: { "taxistatus": "$$val.taxistatus" }
    //       }
    //     }
    //   }
    // },
    { "$match": likeQuery },
    { "$sort": { code: -1 } },
    skip,
    limit,
  ]);
  try {
    var promises = await Promise.all([TotCnt, Datas]);
    res.header('x-total-count', promises[0].length);
    var resstr = promises[1];
    var data = _.map(resstr, (d) => {
      if (d.taxis.length) {
        // var result = _.some(d.taxis, { 'taxistatus': "inactive" })
        var result = _.filter(d.taxis, ['taxistatus', "inactive"]);
        if (result) d.vehicleStatus = "inactive"
        else d.vehicleStatus = "active"
      }
      else {
        d.vehicleStatus = ""
      }
      return d
    })
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }
}