import mongoose from 'mongoose';

//import models
import Vehicle from '../models/vehicletype.model';
import Rider from '../models/rider.model';
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';
import Admin from '../models/admin.model';
import CompanyDetails from '../models/company.model';
import Vehicletype from '../models/vehicletype.model';
import DriverPayment from '../models/driverpayment.model';
import Wallet from '../models/wallet.model';
import Schedule from '../models/schedules.model';
import DriverBank from '../models/driverBank.model';
import DriverBankTransaction from '../models/driverBankTransaction.model';
import * as Mailer from './email.controller';
import * as smsGateway from './smsGateway';
import * as mailGateway from './mailGateway';
import * as Stripe from './stripe';
import * as TripHelpers from './tripHelper';
import * as fareCalculation from './fareCalculation';
import ServiceAvailableCities from '../models/serviceAvailableCities.model';

import RentalPackage from '../models/rentalPackage.model';
import tripPaths from '../models/tripPaths.model';
const turf = require("@turf/turf")
const _ = require('lodash');

import * as appCtrl from '../controllers/app';
import * as GFunctions from './functions';
const nodemailer = require('nodemailer');

import { getVehicleDataForLiveMeter } from './vehicletype';
import { insidePolygon } from 'geolocation-utils'
import * as CityLimitCalculationHelper from "../helpers/cityLimitCalculation.helper";


var firebase = require('firebase');
var config = require('../config');

import * as Braintree from './paymentGateway/braintree';
import * as paystack from './paymentGateway/paystack';

var PaystackTransfer = require('paystack-transfer')(config.paymentGateway.paystackSecretKey)
var allBanks = PaystackTransfer.all_banks;

export const testDF = (req, res) => {
  var available = req.body.available;
  // available = available.substring(1, available.length - 1);
  var parsedobj = JSON.parse(available);
  var arrayRes = [];
  Object.keys(parsedobj).forEach(function (key) {
    if (parsedobj[key]) {
      arrayRes.push(key);
    }
  });
  console.log(arrayRes);
  return res.json({ 'success': true, 'res': parsedobj });
}

export const puttripPaths = (req, res) => {

  /*   var result = {
      "time": Date.now(),
      "coordinates":  
        [78.10145,
          9.959397]
    }; */

  // var newDoc = new tripPaths(
  //   {
  //     // geometry: result,
  //     tripno: '1',
  //     // tripid : '1',
  //   }
  // );


  /*   tripPaths.update(
      { tripno: "1" },
      { $push: { result: friend } }, 
    ); */


  var objFriends = { lat: 9.88, lng: 78.123, time: Date.now() };
  tripPaths.findOneAndUpdate(
    { tripno: '1' },
    { $push: { geometry: objFriends } },
    function (error, success) {
      if (error) {
        res.send(error)
      } else {
        res.send(success)
      }
    });



}



export const addDefaultToDvr = async (req, res) => {

  let driver = await Driver.find({ $or: [{ cmpy: '' }, { cmpy: null }] }, { _id: 1, cmpy: 1 });
  try {
    console.log(driver.length)

    for (var i = 0; i < driver.length; i++) {
      addCMPY(driver[i]._id, req.body.cmpy)
    }
    //res.send(driver)
  }
  catch (err) {
    res.send(err)
  }
}

function addCMPY(id, value) {
  Driver.findByIdAndUpdate({ _id: id }, { cmpy: value }, function (err, data) {
    if (err) { console.log(id) }
    else { console.log("update") }
  })
}

export const getTrips = (req, res) => {

  Trips.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getvehicle = (req, res) => {

  Vehicle.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getRider = async (req, res) => {
  /*   Rider.find().exec((err, docs) => {
      if (err) {
        return res.json([]);
      }
      console.log(docs[0].email)
      res.send(docs);
    }); */

  // var riderData = await Rider.find({ lname: req.query.lname }, { hash: 0, salt: 0 });

  Rider.find({ lname: req.query.lname }, { hash: 0, salt: 0 }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    if (docs.length) {
      // docs.push({ profileurl: config.baseurl + docs[0].profile });
      // return res.json(docs);
      // let newObj = JSON.parse(JSON.stringify(docs[0])); 
      // console.log(newObj)

      var resObj = formatProfileRes(docs[0]);
      return res.json([resObj]);

    }
    else {
      return res.json([]);
    }
  })

}

function formatProfileRes(doc) {
  var newObj = {
    "phone": doc.phone,
    "email": doc.email,
    "lname": doc.lname,
    "fname": doc.fname,
    "status": doc.status,
    "referal": doc.referal,
    "balance": doc.balance,
    "gender": doc.gender,
    "address": doc.address,
    "rating": doc.rating,
    "EmgContact": doc.EmgContact,
    "profile": doc.profile,
    "phcode": doc.phcode,
  };
  return newObj;
}

function copy(mainObj) {
  let objCopy = {}; // objCopy will store a copy of the mainObj
  let key;

  for (key in mainObj) {
    objCopy[key] = mainObj[key]; // copies each property to the objCopy object
  }

  console.log(objCopy)
  return objCopy;
}

export const getDriver = (req, res) => {

  Driver.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getAdmin = (req, res) => {

  Admin.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getCompanyDetails = (req, res) => {

  CompanyDetails.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}
export const getVehicletype = (req, res) => {

  Vehicletype.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getdriverpayment = (req, res) => {

  DriverPayment.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getWallets = (req, res) => {

  Wallet.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}

export const getschedule = (req, res) => {

  Schedule.find().exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}



// export const findNChargeExistingUserWallet = (req,res) => { 
//     appCtrl.findNChargeExistingUserWallet('us',2,3);
// }

export const createSch = (req, res) => {
  var body = {}
  const newDoc = new Schedule(body);
  newDoc.save((err, docs) => {
    if (err) {
      return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") });
    }
    return res.json({ 'success': true, 'message': req.i18n.__('DATA_ADDED_SUCCESS'), docs });
  })
}

export const driverBankDetails = (req, res) => {

  DriverBank.find().exec((err, docs) => {
    if (err) {
      return res.json(err);
    }
    res.send(docs);
  });

}

export const getDriverBankTransaction = (req, res) => {

  DriverBankTransaction.find().exec((err, docs) => {
    if (err) {
      return res.json(err);
    }
    res.send(docs);
  });

}

var file = 'datasd.csv';

// var fs = require('fs')
// var csv = require('fast-csv')
// const parse = require('csv-parse')

export const parseCSV = (req, res) => {

  // let csvToJson = require('convert-csv-to-json');

  let fileInputName = './Drivers.csv';
  // let fileOutputName = 'myOutputFile.json';

  // csvToJson.generateJsonFileFromCsv(fileInputName, fileOutputName);

  const csvFilePath = fileInputName
  const csv = require('csvtojson')
  csv()
    .fromFile(fileInputName)
    .then((jsonObj) => {
      res.send(jsonObj);

      // console.log(jsonObj); 
      /**
       * [
       * 	{a:"1", b:"2", c:"3"},
       * 	{a:"4", b:"5". c:"6"}
       * ]
       */
    })

}


function addDriverData(data) {
  var id = mongoose.Types.ObjectId();
  var id2 = mongoose.Types.ObjectId();

  var newDoc = new Driver(
    {
      _id: id,
      code: data.DriverAccount,
      nic: data.NIC,
      fname: data.NAME,
      lname: '',
      email: '',
      phcode: '+94',
      phone: data.ContactNo,
      gender: 'Male',
      cnty: "",
      cntyname: "Sri Lanka",
      state: "",
      statename: "",
      city: "",
      cityname: "",
      cmpy: '',
      cur: "",
      actMail: "",
      actHolder: "",
      actNo: "",
      actBank: "",
      actLoc: "",
      actCode: "",
      softdel: "active",
      fcmId: "",

      "curStatus": "free",
      "curService": data.vehicletype,
      "serviceStatus": "active",
      "currentTaxi": id2,

      status: [{
        curstatus: 'active'
      }],
      licence: data.DrivingLiesencenumber,
      taxis: [{
        "color": data.COLOR,
        "driver": id,
        "cpy": "",
        "licence": data.V_NO,
        "year": "2017",
        "model": "INTEGRA",
        "makename": "ACURA",
        "_id": id2,
        "taxistatus": "active",
        "noofshare": 0,
        "share": false,
        "vehicletype": data.vehicletype,
        "registrationexpdate": "",
        "registration": "",
        "permitexpdate": "",
        "permit": "",
        "insuranceexpdate": "",
        "insurance": "",
        "type": [
          {
            "luxury": "false",
            "normal": "false",
            "basic": "false"
          }
        ],
        "handicap": "false"
      }]
    }
  );
  newDoc.setPassword(data.Password);
  newDoc.save((err, datas) => {
    if (err) { console.log(err); }
    addDriverDatatoFb(id);
    addDrivertaxisDataFB(id, id2, data);
  })

};


function addDriverDatatoFb(id) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");
  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    online_status: "0",
    proof_status: "pending",
    request: {
      drop_address: "0",
      etd: "0",
      picku_address: "0",
      request_id: "0",
      status: "0"
    },
    vehicle_id: "0"
  };

  id = id.toString();
  var usersRef = ref.child(id);

  usersRef.set(requestData, function (snapshot) {

  });
}

/*
Add and Update
*/
function addDrivertaxisDataFB(driverid, vehicleid, taxisdata) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("vehicle_list");

  var vehicletype = taxisdata.vehicletype;
  console.log(vehicletype);
  var key3 = (vehicletype).toString();
  var value3 = 0;
  var obj = {

  };
  obj[key3] = value3;


  var requestData = {
    category: obj,
    make: 'ACURA',
    model: 'INTEGRA',
    plate_num: taxisdata.V_NO,
    status: "0"
  };

  driverid = driverid.toString();
  vehicleid = vehicleid.toString();
  var usersRef = ref.child(driverid).child(vehicleid);

  usersRef.set(requestData, function (snapshot) {
  });
}

export const getDummyDvr = (req, res) => {

  var neededService = "Car"
  Driver.find({
    curService: { '$regex': neededService, $options: 'i' }
  }).exec((err, driverdata) => {
    if (err) {
      return res.json([]);
    }
    return res.json(driverdata);
  });


}

// //ONEBYONE TEST
// var distance = require('google-distance-matrix');
// export const getGEOMatchSort = (req, res) => {
//   Driver.find({ coords: { $ne: "" } }, { _id: 1, coords: 1 }).limit(4).exec((err, docs) => {
//     if (err) {
//       return res.json([]);
//     }
//     var convertedLatLon = convertCordsToGDMFormat(docs);
//     var origins = ['9.9254272, 78.1117957'];
//     var destinations = convertedLatLon;
//     distance.key(config.googleApi);
//     distance.units('metric');
//     distance.mode('driving');

//     distance.matrix(origins, destinations, function (err, distances) {
//       if (!err) { }
//       if (distances.status == 'OK') {
//         var resOutput = distances.rows[0].elements; 
//         var distanceArray = addDocIdAndGetOnlyDistanceArry(docs, resOutput); 
//         console.log(distanceArray)
//         var sortedDistanceArray = distanceArray.sort(dynamicSort("distVal")); //In Meters
//         res.send(sortedDistanceArray);
//       }
//     })
//   });
// }

// /**
//  * convertCordsToGDMFormat
//  * @param {*} docs 
//  */
// function convertCordsToGDMFormat(docs) {
//   var resultDoc = docs.map(function (items) {
//     var destinations = "";
//     var tmpDoc = items.coords;
//     destinations = tmpDoc[1] + "," + tmpDoc[0];
//     return destinations;
//   });
//   return resultDoc;
// }

// /**
//  * addDocIdAndGetOnlyDistanceArry
//  * @param {*} docs 
//  * @param {*} GDMop 
//  */
// function addDocIdAndGetOnlyDistanceArry(docs,GDMop) {
//   var totalArray = docs.length;
//   var GMDistAry  = [];
//   for (let i = 0; i < totalArray; i++) { 
//     let isDistOk = GDMop[i].status;
//     if(isDistOk == 'OK'){
//       let tempObj = {};
//       tempObj['id'] = docs[i]._id; 
//       //if dis val < or > in config use only
//       tempObj['distVal'] = GDMop[i].distance.value; 
//       GMDistAry.push(tempObj);  
//     } 
//   }
//   return GMDistAry;
// }

// /**
//  * dynamicSort
//  * @param {*} property 
//  */
// function dynamicSort(property) {
//   var sortOrder = 1;
//   if (property[0] === "-") {
//     sortOrder = -1;
//     property = property.substr(1);
//   }
//   return function (a, b) {
//     var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
//     return result * sortOrder;
//   }
// }


//ONEBYONE TEST
var distance = require('google-distance-matrix');

/**
 * [Taxi Request from User] = Checked
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
export const requestTaxiOBO2 = async (req, res) => {
  callTheOBOLoop('5b975ad8d6ea2726354e27ee');
}

export const requestTaxiOBO = async (req, res) => {
  if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
    req.body.promoAmt = 0;
  }
  var promoAmt = req.body.promoAmt;
  var promoCode = req.body.promo;

  var reqTripType = 'Ride';
  var newDoc = new Trips(
    {
      triptype: reqTripType,
      date: GFunctions.sendTimeNow(),
      cpy: "",
      cpyid: "",
      dvr: "",
      dvrid: null,
      rid: req.name,
      ridid: req.userId,
      fare: req.body.totalfare,
      taxi: req.body.serviceName,
      service: req.body.serviceid,
      csp: [{ //Cost split up RFCNG
        base: req.body.basefare,
        dist: req.body.distance,
        distfare: req.body.distanceFare,
        time: req.body.time,
        timefare: req.body.timeFare,
        comison: "",
        promoamt: promoAmt,
        promo: promoCode,
        cost: req.body.totalfare,
        via: req.body.paymentMode,
      }],
      dsp: [{ //details split up
        start: "",
        end: "",
        from: req.body.pickupAddress,
        to: req.body.dropAddress,
        pLat: req.body.pickupLat,
        pLng: req.body.pickupLng,
        dLat: req.body.dropLat,
        dLng: req.body.dropLng,
      }],
      estTime: req.body.time,
      status: "processing"
    }
  );

  req.userId = "5b57345aa55d730e908b8a6f";
  //checking is this user has processing trips of type RIDE
  Trips.findOne({ ridid: req.userId, status: "processing", triptype: "Ride" }, function (err, docs) {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    if (docs) return res.status(409).json({ 'success': false, 'message': req.i18n.__('REQUEST_ALREADY_INPROCESS'), 'error': err });
    newDoc.save((err, tripdata) => {
      if (err) {
        return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
      }
      // updateRiderFbStatus(req.userId, "Processing", tripdata._id); //processing = Req intermediate state
      findNearbyDrivers(tripdata, req.body, req.userId);
      return res.status(200).json({ 'success': true, 'message': req.i18n.__('TAXI_REQUEST_SEND'), "requestDetails": tripdata._id });
    })
  });
};

//Find Drivers
function findNearbyDrivers(tripdata, userreq, userid) {
  var requestRadius = config.requestRadius;
  var neededService = userreq.serviceName;
  Driver.find(

    // {
    //   coords: {
    //     $geoWithin: {
    //       $centerSphere: [[parseFloat(userreq.pickupLng), parseFloat(userreq.pickupLat)],
    //       requestRadius / 3963.2]
    //     },
    //   }, online: "1", curStatus: "free", curService: new RegExp(neededService, 'i')
    // }

    { coords: { $ne: "" } }, {}

  ).limit(10).exec((err, driverdata) => {
    // ).exec((err, driverdata) => { //50 Enough
    if (err) {
      // notifyRider(userid, "No Driver Found", tripdata._id); //Error on Server
    }
    if (driverdata.length <= 0) {
      // notifyRider(userid, "No Driver Found", tripdata._id);
      console.log('NDF')
    } else {
      filterNSendOBORequestToDrivers(tripdata, userreq, driverdata, userid);
    }
  });
}

function filterNSendOBORequestToDrivers(tripdata, userreq, driverdata, userid) {
  var convertedLatLon = convertCordsToGDMFormat(driverdata);
  var originsPoints = parseFloat(userreq.pickupLat) + "," + parseFloat(userreq.pickupLng);
  originsPoints = originsPoints.toString();
  var origins = [originsPoints];
  var destinations = convertedLatLon;
  distance.key(config.googleApi);
  distance.units('metric');
  distance.mode('driving');
  // console.log(driverdata);
  distance.matrix(origins, destinations, function (err, distances) {
    if (err) { console.log('GGMErr NDF', err) }
    else if (distances.status == 'OK') {
      var resOutput = distances.rows[0].elements;
      var distanceArray = addDocIdAndGetOnlyDistanceArry(driverdata, resOutput); //Merging In Driver and Geo
      var sortedDistanceArray = distanceArray.sort(dynamicSort("distVal")); //In Meters
      updateTaxiWithFoundDrivers(sortedDistanceArray, tripdata._id, userid);
      // sortedDistanceArray[{ id: 5b5c7fff0c0b8f2cbe3c995e, distVal: 602 },
      //   { id: 5b5c7fff0c0b8f2cbe3c994e, distVal: 791 },
      //   { id: 5b5c7fff0c0b8f2cbe3c995a, distVal: 2181 }]

      // res.send(sortedDistanceArray);
    }
    else {
      console.log('GGMErr NDF in Land')
    }
  })
}

/**
 * Log Found Drivers and No of Drivers in Trip Doc
 * @param {*} requestedDrivers 
 * @param {*} requestId 
 * @param {*} userid 
 */
function updateTaxiWithFoundDrivers(requestedDrivers, requestId, userid) {
  var curReqAry = [requestedDrivers.length, 0];
  var update = {
    reqDvr: requestedDrivers,
    curReq: curReqAry
  };
  Trips.findOneAndUpdate({ _id: requestId }, update, { new: true }, (err, doc) => {
    if (err) {
      console.log("updateTaxiStatus", err);
    }
    else {
      console.log('updateTaxiWithFoundDrivers', ' ok')
      callTheOBOLoop(requestId);//Let start OBO loop for this Trip
    }
  })
}

/**
 * callTheOBOLoop : Will process OBO Req if needed
 * @param {*} trip_id 
 */
function callTheOBOLoop(trip_id) {
  //Loop untill needClear : 'no' 
  Trips.findOne({ _id: trip_id, needClear: 'yes' }, { _id: 1, tripno: 1, reqDvr: 1, curReq: 1, dsp: 1, estTime: 1 }, function (err, docs) {
    if (err) { console.log("NTF") }
    if (docs) {

      //Find Current Driver 
      var curReqDriverIndex = docs.curReq[1];
      var maxIndex = docs.curReq[0];

      var allDriversAvail = docs.reqDvr;
      var obj = allDriversAvail.find(function (obj) { return obj.called === 0; });

      if (obj) {
        sendRequestToDriversIfFree(docs, obj.drvId);
        console.log("INOBO", obj.drvId);
      }

      // if (curReqDriverIndex<maxIndex){
      //   var   
      //   var obj = string1.find(function (obj) { return obj.called === 0; });

      //   var curReqDriver = docs.reqDvr[curReqDriverIndex];
      //   var curReqDriverId = curReqDriver.drvId;
      //   // var isDriverCalled = curReqDriver.called;
      //   // if(!isDriverCalled){
      //     // Update Index in Trip
      //     updateCurIndexToTrip(trip_id, curReqDriverIndex, maxIndex);

      //     //Request to Current Driver
      //     sendRequestToDriversIfFree(trip_id, curReqDriverId);
      //   // }else{
      //   //   callTheOBOLoop(trip_id);
      //   // } 

      // }else{
      //   // Update Index in Trip
      //   updateCurIndexToTrip(trip_id, maxIndex, maxIndex);
      //   //@TODO
      //   // notifyRider(userid, "No Driver Found", requestId); // Run out off Driver
      //   console.log("NDF ROD");
      // }

    }
  });
}

function updateCurIndexToTrip(trip_id, curReqDriverIndex, maxIndex) {
  var curReqAry = [maxIndex, curReqDriverIndex];
  Trips.findByIdAndUpdate(trip_id, {
    'curReq': curReqAry
  }, { 'new': true },
    function (err, doc) {
      if (err) { console.log('updateCurIndexToTrip', err); }
      console.log('updateCurIndexToTrip', ' OK');
    }
  );
}

function sendRequestToDriversIfFree(docs, curReqDriverId) {
  // Driver.findOne({ _id: curReqDriverId }, {},
  //   function (err, doc) {
  //     if (err) { 
  //       // Call the Next Driver
  //       // @TODO Assumed updateCurIndexToTrip done  
  //       updateAsThisDriverIsErrAndCallOBO(docs, curReqDriverId)
  //       callTheOBOLoop(docs._id);
  //     } else if (!doc){
  //       //Call the Next Driver 
  //       // @TODO Assumed updateCurIndexToTrip done 
  //       updateAsThisDriverIsErrAndCallOBO(docs, curReqDriverId) 
  //       callTheOBOLoop(docs._id); 
  //     }else{
  //       updateAsThisDriveriSCalled(docs, curReqDriverId);

  //     }
  //   }

  Driver.findById(curReqDriverId,
    function (err, doc) {
      if (err) { updateAsThisDriveriSCalled(docs, curReqDriverId, 1); }
      var curStatus = doc.curStatus;
      if (curStatus == "free") {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 1);
      } else if (curStatus == "requested") {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 0);
      } else {
        updateAsThisDriveriSCalled(docs, curReqDriverId, 1);
      }
    }
  );

}

function updateAsThisDriveriSCalled(tripDoc, driverid, callStatus = 1) {
  Trips.update(
    { _id: tripDoc._id, 'reqDvr.drvId': mongoose.Types.ObjectId(driverid) },
    {
      $set: {
        'reqDvr.$.called': callStatus
      }
    },
    { new: true },
    function (err, doc) {
      if (err) { console.log('err', err) }
      if (callStatus) {
        sendRequestToDrivers(tripDoc, driverid);
      } else {
        callTheOBOLoop(tripDoc._id);
      }
    }
  )



  // console.log('curReqDriverId', curReqDriverId)
  // Driver.findOne({ '_id': curReqDriverId, 'curStatus': 'free'  }, {},
  //   function (err, doc) {
  //     if (err) {
  //        console.log(err)
  //     } else if (!doc) {
  //       console.log(1) 
  //     } else {
  //       console.log(2)  
  //     }
  //   }
  // );


}


function updateAsThisDriverIsErrAndCallOBO(tripDoc, driverid) {
  Trips.update(
    { _id: tripDoc._id, 'reqDvr.drvId': mongoose.Types.ObjectId(driverid) },
    {
      $set: {
        'reqDvr.$.called': 1
      }
    },
    { new: true },
    function (err, doc) {
      if (err) { console.log('err', err) }
      console.log('doc', doc)
    }
  )
}

//Update fb
function sendRequestToDrivers(tripDoc, curReqDriverId) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");
  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    request: {
      drop_address: tripDoc.dsp[0].to,
      etd: tripDoc.estTime,
      picku_address: tripDoc.dsp[0].from,
      request_id: tripDoc._id,
      status: "1",
      datetime: "0",
      request_type: "Normal",
      review: "Taxi Request",
      request_no: 0
    }
  };

  var child = (curReqDriverId).toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData, function (error) {
    if (error) { } else {
      // Send FCM
      // findAndSendFCMToDriver(child, "New Request");
      updateDriverReqStatusINMongo(curReqDriverId, tripDoc._id);
    }
  });

}

function updateDriverReqStatusINMongo(driverid, tripId) {
  Driver.findByIdAndUpdate(driverid, {
    'curStatus': 'requested'
  }, { 'new': true },
    function (err, doc) {
      if (err) { }
      // findAndSendFCMToDriver(child, "New Request"); //Only
      clearDriverTaxiRequest(driverid, tripId);
    }
  );
}

function clearDriverTaxiRequest(driverid, tripId) {
  setTimeout(function () {
    needToResetDriver(driverid, tripId);
  }
    , 30000); //30000 = 30 sec
}

// const activeTimers = []; 
// export const convertSTO = (req, res) => { 
//   timerCheck('123467890');
//   return res.send('HI');
// }
// function timerCheck(tripId) {
//   var myVariables = {};
//   var variableName = tripId;
//   var newTimer = myVariables[variableName];
//   // myVariables[variableName] = 'mk'
//   // console.log(myVariables[variableName])
//   newTimer = setTimeout(function () {
//     console.log( Date.now() );
//   }
//     , 30000); //30000 = 30 sec
//   activeTimers.push(newTimer);
//   // clearTimeout( newTimer ); 
// }
// export const clearT = (req, res) => {
//   clearTimeoutChk('123467890'); 
//   // console.log(activeTimers)
//   return res.send('HI');
// }
// function clearTimeoutChk(tripId) {
//   var myVariables = {};
//   var variableName = tripId;
//   clearTimeout( myVariables[variableName] ); 
// }


function clearMyTripStatusFB(driverid, tripId) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");

  var requestData = {
    accept: {
      others: "0",
      trip_id: "0"
    },
    request: {
      drop_address: "0",
      etd: "0",
      picku_address: "0",
      request_id: "0",
      status: "0",
      datetime: "0",
      request_type: "0",
      review: "Time Out",
      request_no: 0
    }
  };

  var child = (driverid).toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData, function (error) {
    if (error) { } else { }
  });
}

function needToResetDriver(driverid, tripId) {
  Driver.findOne({ _id: driverid, 'curStatus': 'requested' }, {},
    function (err, doc) {
      if (err) { }
      if (!doc) { }
      if (doc) {
        clearMyTripStatusFB(driverid, tripId);
        changeMyTripStatusMongo(driverid, 'free');
        callTheOBOLoop(tripId);
      }
    }
  );
}

function changeMyTripStatusMongo(driverid, msg = "free") {
  Driver.findByIdAndUpdate(driverid, {
    'curStatus': msg
  }, { 'new': true },
    function (err, doc) {
      if (err) { }
    }
  );
}


//Helper OBOR
/**
 * convertCordsToGDMFormat
 * @param {*} docs 
 */
function convertCordsToGDMFormat(docs) {
  var resultDoc = docs.map(function (items) {
    var destinations = "";
    var tmpDoc = items.coords;
    destinations = tmpDoc[1] + "," + tmpDoc[0];
    return destinations;
  });
  return resultDoc;
}

/**
 * addDocIdAndGetOnlyDistanceArry
 * @param {*} docs 
 * @param {*} GDMop 
 */
function addDocIdAndGetOnlyDistanceArry(docs, GDMop) {
  var totalArray = docs.length;
  var GMDistAry = [];
  for (let i = 0; i < totalArray; i++) {
    let isDistOk = GDMop[i].status;
    if (isDistOk == 'OK') {
      let tempObj = {};
      tempObj['drvId'] = docs[i]._id;
      tempObj['called'] = 0;
      //if dis val < or > in config use only this @TODO
      //Also Limit only 10 Drivers @TODO
      tempObj['distVal'] = GDMop[i].distance.value;
      GMDistAry.push(tempObj);
    }
  }
  return GMDistAry;
}

/**
 * dynamicSort
 * @param {*} property 
 */
function dynamicSort(property) {
  var sortOrder = 1;
  if (property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    return result * sortOrder;
  }
}

//ONEBYONE TEST END


//Convert String To ID
export const convertSTO = (req, res) => {
  Trips.find({}).skip(0).exec((err, tripvalue) => {
    if (err) { }
    //var m=0;
    for (var i = 0; i < tripvalue.length; i++) {
      //console.log("hai");
      // console.log(tripvalue[i]._id);
      var driverId = tripvalue[i].dvrid;
      var tripID = tripvalue[i]._id;

      console.log(driverId);
      //m++

      removeFromDoc(tripID, driverId);


    }
    //onsole.log(m) 5b572f60a55d730e908b8a68  1663132149 5b572f60a55d730e908b8a68
  })
}


function removeFromDoc(tripID, driverId) {
  Trips.update({ _id: tripID }, { $unset: { dvrid: 1 } }).exec((err, data) => {
    if (err) { }
    addToFromDoc(tripID, driverId);
  });
}

function addToFromDoc(tripID, driverId) {
  Trips.update({ _id: tripID }, { $set: { "dvrid": driverId } }).exec((err, data) => {
    if (err) { }
    console.log("Success 1");
  })
}


import City from '../models/commonCity.model';

export const addCityData = (req, res) => {


  var data = [
    'Angampitiya',
    'Angoda',
    'Angulana',
    'Arangala',
    'Artigala',
    'Athurugiriya',
    'Attidiya',
    'Avissawella',
    'Bambalapitiya',
    'Battaramulla',
    'Bellanwila',
    'Bokundara',
    'Bope',
    'Boralesgamuwa',
    'Brahmanagama',
    'Cinnamon Gardens',
    'Dampe',
    'Dehiwala',
    'Dematagoda',
    'Egodawatta',
    'Embulgama',
    'Etulkotte',
    'Fort',
    'Galleface',
    'Ganegoda',
    'Gangodawila',
    'Godagama',
    'Gothaduwa',
    'Grandpass',
    'Habarakada',
    'Halpita',
    'Hanwella',
    'Havelock Town',
    'Hewainna',
    'Hokandara',
    'Homagama',
    'Horathuduwa',
    'Kadugoda',
    'Kalalgoda',
    'Kelanimulla',
    'Kesbewa',
    'Kirulapone',
    'Kohilawatta',
    'Kohuwala',
    'Kollupitiya',
    'Kolonnawa',
    'Kosgama',
    'Kotahena',
    'Kotikawatta',
    'Kottawa',
    'Kotte',
    'Kurugala',
    'Labugama',
    'Lenagala',
    'Lunawa',
    'Madinnagoda',
    'Madiwela',
    'Madola',
    'Maharagama',
    'Makuluduwa',
    'Malabe',
    'Malapalla',
    'Mampe',
    'Maradana',
    'Mattakkuliya',
    'Mattegoda',
    'Meegoda',
    'Mirihana',
    'Modara',
    'Narahenpita',
    'Nawala',
    'Nawinna',
    'Nedimala',
    'Nugegoda',
    'Orugodawatta',
    'Padukka',
    'Pamankada',
    'Pamunuwa',
    'Pannipitiya',
    'Peliyagoda',
    'Pepiliyana',
    'Petta',
    'Piliyandala',
    'Pinnawala',
    'Pitakotte',
    'Polgasowita',
    'Polhena',
    'Puwakpitiya',
    'Rajagiriya',
    'Ratmalana',
    'Rukmalgama',
    'Seethawaka',
    'Siddamulla',
    'Slave Island',
    'Suwarapola',
    'Thalahena',
    'Thalangama',
    'Thalapathpitiya',
    'Thalawathugoda',
    'Thimbirigasyaya',
    'TownHall',
    'Udahamulla',
    'Uggala',
    'Waga',
    'Welikada',
    'Weliwita',
    'Wellampitiya',
    'Wellawatte',
    'Werahera',
    'Wewila',
    'Yatawatura',
    'Embuldeniya',
    'Kalubowila',
    'Moratuwa',
    'Kaduwela',
    'Himbutana',
    'Mount Lavinia',
    'Kawdana',
    'Borella',
    'Pelawatta',
    'IDH',
    'Panagoda',
    'Madapatha',
    'Jayawardanapura',
    'Paranagama',
    'Biyagama',
    'Thalduwa',




  ];


  var arr = [];
  for (var i = 0; i < data.length; i++) {
    arr.push({ id: i + 1, name: data[i] })
  }

  var newDoc = new City({
    _id: 33,
    cities: arr
  })
  newDoc.save((err, data) => {
    if (err) { console.log(err) }
    else {
      res.send('ok');
    }
  })
}

export const testMail = (req, res) => {
  var tripno = req.body.tripno,
    mailTo = req.body.mailTo;
  Trips.findOne({ tripno: tripno }, function (err, doc) {
    if (err) { console.log('err1', err) }
    if (!doc) {
      console.log('NO Trip Found, Sending Sample')
      var doc = {
        tripno: 2276,
        acsp: { cost: 100 },
        date: '9-19-2018 5:42 pm',
        via: 'Cash'
      };

      Mailer.sendEmail('TripInvoice', mailTo, doc, res);
    }
    else {
      Mailer.sendEmail('TripInvoice', mailTo, doc, res);
    }
  })
}



/**
 * Share Location
 */
export const shareMyLocation = async (req, res) => {
  var TripId = req.params.id;

  Trips.aggregate([
    {
      $match: {
        status: {
          $nin: ['Cancelled', 'Cancelled', 'noresponse']
        },
        "_id": mongoose.Types.ObjectId(TripId),
      }
    },

    {
      $lookup: {
        localField: "ridid",
        from: "riders",
        foreignField: "_id",
        as: "riderCollections"
      }
    },
    { $unwind: "$riderCollections" },
    {
      $lookup: {
        localField: "dvrid",
        from: "drivers",
        foreignField: "_id",
        as: "driverCollections"
      }
    },
    { $unwind: "$driverCollections" },

    {
      $project: {
        "_id": 1,
        "ridid": 1,
        "tripno": 1,
        "adsp": 1,
        "dsp": 1,
        "status": 1,
        "driverCollections.profile": 1,
        "driverCollections._id": 1,
        "driverCollections.fname": 1,
        "driverCollections.email": 1,
        "driverCollections.phone": 1,
        "driverCollections.baseurl": 1,
        "driverCollections.curService": 1,
        "driverCollections.currentTaxi": 1,
        "riderCollections._id": 1,
        "riderCollections.profile": 1,
        "riderCollections.fname": 1,
        "riderCollections.email": 1,
        "riderCollections.phone": 1,
      }
    }

  ], function (err, result) {
    if (err) {
      return res.status(200).json({ 'success': false, 'err': err });
    }
    if (result.length == 0)
      return res.status(200).json({ 'success': false, 'result': result, 'message': req.i18n.__('NO_TRIP_FOUND') });
    else {
      var sendToData = {
        tripid: result[0]._id,
        tripDetails: {
          tripno: result[0].tripno,
          pickupAt: result[0].adsp.from,
          dropAt: result[0].dsp[0].to,
          startAt: result[0].adsp.start,
          endAt: result[0].dsp[0].end,
          status: result[0].status,
        },
        driver: {
          _id: result[0].driverCollections._id,
          name: result[0].driverCollections.fname,
          email: result[0].driverCollections.email,
          profile: config.baseurl + result[0].driverCollections.profile,
          phone: result[0].driverCollections.phone,
          curService: result[0].driverCollections.curService,
          currentTaxiId: result[0].driverCollections.currentTaxi,
        },
        rider: {
          _id: result[0].riderCollections._id,
          name: result[0].riderCollections.fname,
          email: result[0].riderCollections.email,
          profile: config.baseurl + result[0].riderCollections.profile,
          phone: result[0].riderCollections.phone,
        },
        supportEmail: config.mailFrom,
        supportNo: config.supportNo

      };
      return res.status(200).json({ 'success': true, 'result': sendToData });
    }

  });
}


export const validate = (req, res) => {
  req.checkBody("test1", "Enter a valid String.");
  req.checkBody("url", "Enter a valid URL address.").isURL();
  req.checkBody("mail", "Enter a valid email address.").isEmail();
  req.checkBody("num", "Enter a valid number.").isNumeric();
  req.checkBody("optional", "Optional");
  req.checkBody("must", "Must").notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).json({ 'success': false, 'err': errors });
  } else {
    return res.status(200).json({ 'success': true });
  }

}


export const sendMail = (req, res) => {
  let transporter = nodemailer.createTransport(config.smtpConfig);
  let mailOptions = {
    from: config.mailFrom, // sender address
    to: 'absnodes@gmail.com', // list of receivers
    subject: "Test Subject", // Subject line 
    html: "Test Body" // html body
  };
  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
  });
}


export const sendSms = async (req, res) => {
  smsGateway.sendSmsMsg(req.body.smsto, req.body.smsbody);
  return res.status(200).json({ 'success': true });
}

export const sendMailtest = async (req, res) => {
  req.body.data = JSON.parse(req.body.data);
  mailGateway.sendEmail(req.body.email, req.body.data, req.body.description);
  return res.status(200).json({ 'success': true });
}


export const testLoadsh = async (req, res) => {

  var all = {
    "success": true, "message": req.i18n.__("RESTAURANT_DETAIL"), "resultData": {
      "basic":
      {
        "id": "5bb352fb402fc0211cb404ab", "name": "kfc", "address": "test address", "rating": null, "logo": "public/file-1545889535599.jpg",
        "minTime": 10, "minAmt": 10, "menu": [{ "name": "Chicken", "count": 3 }, { "name": "Krusher", "count": 0 }]
      },
      "foodItem": [{
        "catName": "Chicken", "_id": "5bbc4f8a8bc4ea3ab5a27541",
        "items": [{
          "itemTag": "newlyadded", "recommended": true, "stock": true, "itemType": "nonveg", "itemOffer": 10, "itemprice": 150,
          "itemDesc": "undefined", "itemName": "Stripes", "_id": "5c24c667cb0ade6175c19e04", "count": 0, "toppings":
            [{ "name": "_abs_ngprof_post_to_timeline", "price": 500, "_id": "5c4026288a4195484525e1bf" },
            { "name": "_quick_overview", "price": 500, "_id": "5c4026288a4195484525e1be" }],
          "options": [{ "type": "Luxurious", "price": 500, "_id": "5c4026288a4195484525e1bd" },
          { "type": "Truck", "price": 1000, "_id": "5c4026288a4195484525e1bc" }], "itemImage": "public/file-1545913959243.png"
        }, { "itemTag": "bestseller", "recommended": true, "stock": true, "itemType": "veg", "itemOffer": 2, "itemprice": 23, "itemDesc": "undefined", "itemName": "virginmary", "_id": "5c25b5ce9bf0871b7b754e4a", "count": 0, "toppings": [], "options": [{ "price": 25, "type": "reg", "_id": "5c25b5ce9bf0871b7b754e4b" }], "itemImage": "public/file-1545975246010.jpeg" }, { "itemTag": "promoted", "recommended": true, "stock": true, "itemType": "veg", "itemOffer": 2, "itemprice": 12, "itemDesc": "undefined", "itemName": "virginsury", "_id": "5c25e189dcb7cd3890ebcced", "count": 0, "toppings": [{ "price": 3, "name": "34", "_id": "5c25e189dcb7cd3890ebccf0" }], "options": [{ "price": 0, "type": "reg", "_id": "5c25e189dcb7cd3890ebccef" }, { "price": 323, "type": "absD", "_id": "5c25e189dcb7cd3890ebccee" }], "itemImage": "public/menu-default.jpg" }]
      }, { "catName": "Krusher", "_id": "5bbc5904e1ef8e3c8faf69a3", "items": [] }], "recommendedItem": [{ "itemTag": "newlyadded", "recommended": true, "stock": true, "itemType": "nonveg", "itemOffer": 10, "itemprice": 150, "itemDesc": "undefined", "itemName": "Stripes", "_id": "5c24c667cb0ade6175c19e04", "count": 0, "toppings": [{ "name": "_abs_ngprof_post_to_timeline", "price": 500, "_id": "5c4026288a4195484525e1bf" }, { "name": "_quick_overview", "price": 500, "_id": "5c4026288a4195484525e1be" }], "options": [{ "type": "Luxurious", "price": 500, "_id": "5c4026288a4195484525e1bd" }, { "type": "Truck", "price": 1000, "_id": "5c4026288a4195484525e1bc" }], "itemImage": "public/file-1545913959243.png" }, { "itemTag": "bestseller", "recommended": true, "stock": true, "itemType": "veg", "itemOffer": 2, "itemprice": 23, "itemDesc": "undefined", "itemName": "virginmary", "_id": "5c25b5ce9bf0871b7b754e4a", "count": 0, "toppings": [], "options": [{ "price": 25, "type": "reg", "_id": "5c25b5ce9bf0871b7b754e4b" }], "itemImage": "public/file-1545975246010.jpeg" }, { "itemTag": "promoted", "recommended": true, "stock": true, "itemType": "veg", "itemOffer": 2, "itemprice": 12, "itemDesc": "undefined", "itemName": "virginsury", "_id": "5c25e189dcb7cd3890ebcced", "count": 0, "toppings": [{ "price": 3, "name": "34", "_id": "5c25e189dcb7cd3890ebccf0" }], "options": [{ "price": 0, "type": "reg", "_id": "5c25e189dcb7cd3890ebccef" }, { "price": 323, "type": "absD", "_id": "5c25e189dcb7cd3890ebccee" }], "itemImage": "public/menu-default.jpg" }], "availabile": true
    }
  };

  var foodItem = all.resultData['foodItem'];
  var foodItem1 = foodItem[0].items;

  var cartItem = [
    {
      itemOption: { optionName: '', optionPrice: 0 },
      customize: [],
      _id: '5c49891f51a8cb1f08fcd814',
      itemId: '5c24c667cb0ade6175c19e04',
      itemQty: 2,
      itemName: 'Stripes',
      itemPrice: 300
    },
    {
      itemOption: { optionName: '', optionPrice: 0 },
      customize: [],
      _id: '5c49891f51a8cb1f08fcd814',
      itemId: '5c25b5ce9bf0871b7b754e4a',
      itemQty: 3,
      itemName: 'Stripes',
      itemPrice: 300
    }
  ];

  _.forEach(cartItem, function (value, key) {
    var index = findNReplace(foodItem1, { _id: value.itemId });
    if (index >= 0) {
      foodItem1[index].count = value.itemQty;
    }
  });

  return res.status(200).json(foodItem1);
}


function findNReplace(foodItem1, entry1) {
  var index = _.findIndex(foodItem1, entry1);
  return index;
}


export const createStripeCustomer = async (req, res) => {
  Stripe.createCustomer(req, res);
}

export const createStripeConnectTest = async (req, res) => {
  Stripe.createStripeConnectTest(req.body.email, 'US')
    .then((resObj) => res.send(resObj))
    .catch((err) => res.send(err));
}

export const StripelistCustomersTest = async (req, res) => {
  Stripe.listCustomers(req, res);
}


export const testFirebase = async (req, res) => {

  /*     if (!firebase.apps.length) {
        firebase.initializeApp(config.firebasekey);
      }
      var db = firebase.database();
      var ref = db.ref("drivers_data");
      var requestData = {
        test: {
          test: "0",
          test: "0"
        },
        test: "0" 
      };
   
    var usersRef = ref.child('test');
  
      usersRef.update(requestData, function (snapshot) {
        return res.json({'success':true,'message':'Test Successfully', 'snap' :  snapshot});   
      });
      */

  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }

  var db = firebase.database();
  var ref = db.ref("new_col");

  var requestData = {
    name: "new1",
    age: "1",
    newob: {
      asd: 1
    }
  };

  // var id = datas._id.toString();
  var usersRef = ref.child("1");

  usersRef.set(requestData, function (snapshot) {
    console.log("addRiderDatatoFb", snapshot);
  });




}


export const parseGSTInvoce = async (req, res) => {
  TripHelpers.sendTripGSTReceipt(req.body.tripId, req.body.email);
}

export const transferAmountUsingNonceNRecharge = async (req, res) => {
  try {
    var nonceFromTheClient = req.body.payment_method_nonce;
    var rechargeAmount = req.body.rechargeAmount;
    var resObj = await Braintree.transferAmountUsingNonceNRecharge(nonceFromTheClient, rechargeAmount);
    return res.status(200).json(resObj);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_ADDING_AMT'), 'err': error });
  }
}

export const streamtrips = (req, res) => {
  res.header('Content-Type', 'text/event-stream');
  var interval_id = setInterval(function () {
    res.write("some data");
  }, 1000);
  setTimeout(function () {
    clearInterval(interval_id);
    res.end();
  }, 10000);
}

const moment = require("moment");

export const tripDTValue = (req, res) => {
  try {
    req.body.tripShownDate = GFunctions.sendTimeNow();
    req.body.tripDate = moment().utcOffset(config.utcOffset).format("DD-MM-YYYY");
    req.body.tripTime = moment().utcOffset(config.utcOffset).format("HH:mm a");

    var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
    var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
    var reqtripFDT = GFunctions.getDateTimeForSortings(newDateFormat);
    var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);

    // console.log('newDateFormat', reqtripDT)

    var gmtFTime = new Date(reqtripFDT).toGMTString();
    // console.log('gmtFTime', config.gmtZone)

    req.body.tripDT = reqtripDT;
    req.body.utc = req.body.utc ? req.body.utc : config.gmtZone;
    req.body.tripFDT = reqtripFDT;
    req.body.gmtTime = gmtFTime;
    req.body.tripShownDate = reqtripDT;

    console.log(req.body)
  } catch (error) {
    console.log(error)

  }

}


export const sendAdminPushMsg = async (req, res) => {
  GFunctions.sendAdminPushMsg("cwoLaxS4m9s:APA91bEd_qhHsrkqMbqu96MNC1psq-R6zLiWEBB7kQMKimsRsga2xHA7RI4YVO-YkGSRsk-u622L7YgsTfq_QSkhrsKxJxg5uADfCk6GAelvfc52GQgggLVk4b0-0dziByJvkETvStgx");
  return res.status(200).json({ 'success': true });
}


export const makeStripeSplitTransTest = async (req, res) => {
  var data = {
    driveramount: .8,
    totalamount: 1,
    custid: 'cus_EptRuQmft2Ht4z',
    driverConnectAcctId: 'acct_1EH8oEG7KSsn4uAl',
    stripeDesc: 'Trip - Credit',
  };
  Stripe.makeStripeSplitPayment(data)
    .then((resObj) => res.send(resObj))
    .catch((err) => res.send(err));
}


// Simmakkal, Madurai Main, Madurai, Tamil Nadu => Thirumangalam, Tamil Nadu
// 9.925910, 78.121529 => 9.824060, 77.990080

export const bboxCheck = (req, res) => {
  var line = turf.lineString([[9.925910, 78.121529], [9.824060, 77.990080]]);
  var bbox = turf.bbox(line);
  var bboxPolygon = turf.bboxPolygon(bbox);
  return res.json(bboxPolygon);
  // console.log(bboxPolygon)
}


export const getDiscount = (req, res) => {
  return res.json({ 'discount': 10 });
}

export const useragent = (req, res) => {
  return res.json(req.headers);
}


let request = require('async-request');

export const getGDM = async (req, res) => {
  const from = 9.9239637 + ',' + 78.1222102;
  const to = 9.9443944 + ',' + 78.1558679;

  var urlToCall = "https://maps.googleapis.com/maps/api/directions/json?origin=1017WT%20Oosteinde%2011%20Amsterdam&destination=Heineken%20Experience%20Amsterdam&key=AIzaSyAcojgYg79ssEaV_c1-7pRQpIKESob5Iz4";

  var response = await request(urlToCall);

  // var gdmResult = await GFunctions.getDistanceAndTimeFromGDMForEncode([from], [to]);
  // https://maps.googleapis.com/maps/api/directions/json?origin=1017WT%20Oosteinde%2011%20Amsterdam&destination=Heineken%20Experience%20Amsterdam&key=AIzaSyAcojgYg79ssEaV_c1-7pRQpIKESob5Iz4
  var resp = response.body.status;

  return res.send(resp);
}


export const getVehicleChargeApprox = async (req, res) => {
  var vehicleData = {
    perKMRate: 1.4,
    timeInMinutes: 1.2,
    BaseFare: 20,
    tax: 5,
    minFare: 1,
  };
  var getVehicleChargeApproxio = await fareCalculation.getVehicleChargeApprox(10, 5, vehicleData);
  console.log(getVehicleChargeApproxio)
  return res.json(getVehicleChargeApproxio);
}


export const checkReload = async (req, res) => {
  setTimeout(function () {
    return res.json({ 'success': true });
  }
    , 10000); //30000 = 30 sec
}


export const getVehicleDataForLiveMetertest = async (req, res) => {
  var getVehicleDataForLiveMetersadasd = await getVehicleDataForLiveMeter(req.body.type);
  res.json(getVehicleDataForLiveMetersadasd);
}

export const viewDriverWithFeatureBased = (req, res) => {
  var driverFind = {};
  driverFind["taxis.vehicletype"] = 'Go Moto';
  driverFind["taxis.feature"] = { "$all": ["WIFI", "AC"] };
  Driver.find(driverFind, function (err, data) {
    if (err) { return res.status(500).json(err) }
    return res.status(200).json(data)
  })
}

export const pointsInsideOnBoundary = async (req, res) => {
  var body = req.body

  let availableService = await ServiceAvailableCities.find({ "softDelete": false, "city": { "$ne": "Default" } }, { "cityBoundaryPolygon": 1 }).lean()//.distinct('cityBoundaryPolygon')
  //let point = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], cityBoundaryPolygon)
  let point = false
  _.forEach(availableService, (value) => {
    //console.log(value)
    point = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], value.cityBoundaryPolygon)
    console.log(point)
    if (point) {
      return
    }
  })
  return res.send(point)
}

export const getCityAddress = async (req, res) => {
  var body = req.query
  var cityData = await CityLimitCalculationHelper.findCityAndAddress(body.pickupLat, body.pickupLng);
  res.send(cityData)
}

//Default
export const createSubAccountFromBankForDriver = async (req, res) => {
  var originalData = {
    business_name: req.body.business_name,
    settlement_bank: req.body.settlement_bank,
    account_number: req.body.account_number,
    percentage_charge: req.body.percentage_charge,
    primary_contact_email: req.body.primary_contact_email,
    primary_contact_name: req.body.primary_contact_name,
  };

  // var originalData = {
  //   business_name: 'Sunshine Studios',
  //   settlement_bank: 'ASO Savings and Loans',
  //   account_number: '0193274682',
  //   percentage_charge: '18.2',
  //   primary_contact_email: 'mktest@gmail.com',
  //   primary_contact_name: 'mk',
  // };


  var getVehicleDataForLiveMetersadasd = await paystack.createSubAccountFromBankForDriver(originalData);
  res.json(getVehicleDataForLiveMetersadasd);

}

export const transferAmountNRechargeVendor = async (req, res) => {

  var originalData = {
    subaccount: req.body.accountId,
    email: req.body.cusEmail,
    amount: req.body.amount,
  };

  // var subaccountId = req.body.accountId;
  // var customerEmail = req.body.cusEmail;
  // var newamt = req.body.amount;


  var getSubaccountTransferData = await paystack.transferAmountNRechargeVendor(originalData);
  res.json(getSubaccountTransferData);

}


export const listBank = (req, res) => {
  res.json(allBanks);
}

export const createTransferRecipient = async (req, res) => {

  var originalData = {
    type: "nuban",
    name: "Zombie",
    description: "Zombier",
    account_number: "0221859505",
    bank_code: "058",
    currency: "NGN",
    metadata: {}
  };

  var recipientBank = _.filter(allBanks, { 'slug': req.body.recipientBank });// should be a bank object key value, so that can retrive bank code

  var recipientName = req.body.recipientName;
  var recipientDesc = req.body.recipientDesc;
  var recipientAccountNo = req.body.recipientAccountNo;
  var metaData = req.body.metaData; // default empty object {}

  //"Oluwaleke", "Me", "0221859505", allBanks.guaranty_trust_bank, {}

  //from npm call
  var getTransferRecipientData = await PaystackTransfer.createRecipient(recipientName, recipientDesc, recipientAccountNo, recipientBank[0], metaData);


  // var getTransferRecipientData = await PaystackTransfer.createRecipient("Oluwaleke", "Me", "0221859505", allBanks.guaranty_trust_bank, {});

  res.json(getTransferRecipientData);

  //from 3rd party call
  // var getTransferRecipientData = await paystack.createTransferRecipient(originalData);
  // res.json(getTransferRecipientData);
}

export const listTransferRecipient = async (req, res) => {
  var getTransferRecipients = await PaystackTransfer.listRecipients();
  res.json(getTransferRecipients);
}

export const payTransferRecipient = async (req, res) => {
  //source, reason, amount, recipient
  var payResponse = await PaystackTransfer.initiateSingle('balance', 'test transfer', '100', 'RCP_3h41iqkkkbvie3k');
  res.json(payResponse);
}

const invNum = require('invoice-number');

export const invoice = async (req, res) => {
  var test = invNum.next("DRV001");
  console.log(test)
}

//Rental

/**  
 * params : pickupLng, pickupLat
*/
export const rentalTest = async (req, res) => {
  let availableService = await ServiceAvailableCities.find({ "softDelete": false, "city": { "$ne": "Default" } }, { "cityBoundaryPolygon": 1 }).lean();
  let pickUpPoint = false;
  let servericeCityId = ""
  for (var value of availableService) {
    pickUpPoint = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], value.cityBoundaryPolygon)
    if (pickUpPoint) {
      //pickUpPoint = true;
      servericeCityId = value._id
      break;
    }
  }
  if (pickUpPoint == false) { return res.status(400).json({ 'message': req.i18n.__("SERVICE_CITY_NOT_AVAILABLE") }) }
  let rentalPackage = await RentalPackage.find({ "scIds.scId": servericeCityId.toString() })
  return res.status(200).json({ 'success': rentalPackage })
}

import { getRentalFareEstimationAtTripEnd } from '../modules/rental/rental.controller';

export const getRentalFinalAmt = async (req, res) => {
  var data = await getRentalFareEstimationAtTripEnd(req.body.vehicleTypeId, req.body.packageId, req.body.distanceKM, req.body.timeInMin);
  return res.status(200).json({ 'success': data })
}

//Rental

export const testUpload = async (req, res) => {
  if (req['file'] != null) {
    return res.status(200).json({ 'success': req['file'] })
  }
  return res.status(409).json({ 'success': false })
}

import ZoneCity from '../models/zoneCity.model';

export const testZone = async (req, res) => {
  try {
    var ZoneCityData = await ZoneCity.findOne({}).exec();
    var boundery = ZoneCityData.geometry.coordinates;
    let polygon = turf.polygon(boundery);

    // var polygon = turf.polygon([[[-5, 52], [-4, 56], [-2, 51], [-7, 54], [-5, 52]]]);

    let vertices = turf.explode(polygon);
    // let closestVertex = turf.nearest(dropPoint, vertices);
    let dropPoint = [parseFloat(req.body.droplat), parseFloat(req.body.droplng)];
    let closestVertex = turf.nearest(dropPoint, vertices);


    return res.status(409).json({ 'success': boundery, 'polygon': polygon, 'vertices': vertices, 'closestVertex': closestVertex })

  } catch (error) {
    return res.status(500).json({ 'message': error })
  }
}

export const testSCHAdd = async (req, res) => {
  var slotsArray = [{ "day": "Mon", "fromTime": 9878, "toTime": 54554 }, { "day": "Tue", "fromTime": 8789, "toTime": 6454 },
  { "day": "Mon", "fromTime": 9879, "toTime": 1010 }];
  var validSlots = [];
  var totalSlots = slotsArray.length;

  for (var i = 0; i < totalSlots; i++) {
    var currentSlot = slotsArray[i];
    var alreadyDayExists = checkIsSlotIsValid(_.filter(validSlots, { "day": currentSlot.day }), currentSlot);
    if (alreadyDayExists) return res.status(500).json({ 'message': currentSlot })
    validSlots.push(currentSlot);
  }

  return res.status(200).json({ 'message': "No Er" });

}

function checkIsSlotIsValid(validSlots, currentSlot) {
  var slotAlreadyExists = false;
  var currentFromTime = currentSlot.fromTime;
  if (validSlots.length) {
    slotAlreadyExists = _.filter(validSlots, i => Number(i.fromTime) <= currentFromTime && Number(i.toTime) >= currentFromTime);
    if (slotAlreadyExists.length) slotAlreadyExists = true;
  }
  return slotAlreadyExists;
}


export const testLanguage = async (req, res) => {
  return res.status(200).json({ 'message': req.i18n.__("FETCHED_SUCCESSFULY") });
}



export const testWorking = async (req, res) => {

  let Datas = Driver.aggregate([
    { "$limit": 10 },
    // { "$match": { "fname": "madhavan" } },
    {
      "$lookup": {
        "localField": "_id",
        "from": "driverpayments",
        "foreignField": "driver",
        "as": "userinfo"
      }
    },

    {
      $unwind: {
        path: "$userinfo",
        preserveNullAndEmptyArrays: true
      }
    },

    {
      $group:
      {
        _id: "$userinfo.driver",
        "amttopay": { $sum: "$userinfo.amttopay" },
        "amttopayavg": { $avg: "$userinfo.amttopay" },
        "total": { $sum: 1 },
        "code": { $first: "$code" },
        "fname": { $first: "$fname" },
        "lname": { $first: "$name" },
      }
    },

    {
      $project:
      {
        _id: 1,
        "amttopay": 1,
        "amttopayavg": { $ceil: "$amttopayavg" },
        "total": 1,
        "code": 1,
        "fname": 1,
        "lname": 1,
      }
    },


    /*  {
       $project : {
         "amttopay"  :1 ,
         "code": 1,
         "fname": 1,
         "lname": 1,
         "email": 1,
         "phone": 1,
       }
     }, */

  ]);

  try {
    var promises = await Promise.all([Datas]);
    var resstr = promises[0];
    res.send(resstr);
  } catch (err) {
    return res.json([]);
  }

}

export const updateDriverLocation = async (req, res) => {
  var driver = await Driver.find({}, { 'coords': 1 });
  var data = _.map(driver, (el) => {
    el = el.toObject();
    var update = {
      "driverLocation": {
        "coordinates": el.coords,
        "type": "Point"
      }
    }
    Driver.findOneAndUpdate({ '_id': el._id }, update, (err, doc) => {
      if (err) {
        console.log(err);
      }
      if (doc) {
        console.log(doc);
      }
    })
  })
  console.log("updated")
  res.json('Updated')
}