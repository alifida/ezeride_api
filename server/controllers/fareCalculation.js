// ./express-server/controllers/todo.server.controller.js
import mongoose from 'mongoose';

//import models
import Vehicletype from '../models/vehicletype.model';
import Driver from '../models/driver.model';
import Trips from '../models/trips.model';
import * as HelperFunc from './adminfunctions';
import logger from '../helpers/logger';
import labels from '../helpers/labels.helper';
import * as GFunctions from './functions';
import { parse } from 'fast-csv';
const config = require('../config');
const featuresSettings = require('../featuresSettings');
const moment = require('moment');
const _ = require('lodash');

export const getCityBasedVehicleCharge = async (serviceTypeId, pickupCity, distanceInKM, timeInMinutes, reqtime, waitingTime = 0, additionalFee, discountPercentage = 0, tripId = null) => {
  try {
    var vehicleData = await Vehicletype.findById(serviceTypeId).exec(); // @v2TODO pass pickupCity as null
    // @v2TODO pass pickupCity as null
    var vehicleDetails = {
      "type": vehicleData.type,
      "seats": vehicleData.asppc,
      "image": vehicleData.file,
      "available": vehicleData.available,
      "description": vehicleData.description,
      "features": vehicleData.features,
      "serviceId": (vehicleData._id).toString(),
    };

    //fareDetails Default
    var fareDetails = { 
      "perKMRate": vehicleData.bkm,
      "fareType": 'kmrate', 
      "distance": distanceInKM, //Actual Distance
      "KMFare": parseFloat(parseFloat(distanceInKM) * parseFloat(vehicleData.bkm)).toFixed(2), //Fare for Traveled KM
      "BaseFare": vehicleData.baseFare ? vehicleData.baseFare : 0 , //Base / Service fee
      "travelTime": timeInMinutes ? timeInMinutes : 0,
      "travelRate": vehicleData.timeFare ? vehicleData.timeFare : 0,
      "travelFare": 0,
      //Waiting Fare
      "timeRate": vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime ? vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime : 0, //Waiting Charge per min
      "waitingCharge": vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime ?  vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime : 0, //Waiting Charge per min
      "waitingTime": waitingTime, //Fare for Waiting Time
      "waitingFare": 0, //Fare for Waiting Time
      //cancelation Fees
      "cancelationFeesRider": vehicleData.cancelationFeesRider,
      "cancelationFeesDriver": vehicleData.cancelationFeesDriver,  
      //Pickup Charge
      "pickupCharge": 0, //Pickup Charge 
      "hotelcommision":0,
      "hotelcommisionAmt":0,

      //commision 
      "comison": vehicleData.comison ? vehicleData.comison : 0 , //commision percentage       
      "comisonAmt": 0, //commision to admin 
      "isTax": vehicleData.isTax,      
      "taxPercentage": vehicleData.taxPercentage ? vehicleData.taxPercentage : 0,
      "tax": 0, // tax amount 
      "minFare": vehicleData.mfare,//Minimum fare
      "flatFare": vehicleData.mfare,//flatFare   
      "oldCancellationAmt": 0, 
      "fareAmtBeforeSurge": vehicleData.mfare,
      "totalFareWithOutOldBal": vehicleData.mfare, 
      "totalFare": vehicleData.mfare,//Minimum fare is applied as Total  
      "BalanceFare": vehicleData.mfare,//Minimum fare is applied as Total  
      "DetuctedFare": 0,//Detucted via Promo/Digital,wallet etc 
      "totalFareBeforeDetucted": vehicleData.mfare, 
      "detuctedPercentage": 0, 
      "paymentMode": 'cash', 
      "currency": config.currency, 
      "additionalFee": additionalFee,
      "mandatorydiscountAmt": 0,
      "remarks": '',
    };

    if (featuresSettings.secondarycur){
      fareDetails.inSecondaryCur = GFunctions.sendSecondaryRate(fareDetails.totalFare);
    }
    /*var applyValues = {
      "applyCommission": featuresSettings.applyAdminCommission,
      "applyPeakCharge": featuresSettings.applyPeakCharge,
      "applyNightCharge": featuresSettings.applyNightCharge,
      "applyWaitingTime": featuresSettings.applyWaitingCharge,
      "applyTax": featuresSettings.applyTax,
      "applyPickupCharge": featuresSettings.applyPickupCharge,
    }; */

    var applyValues = featuresSettings.applyValues;

    var offers = {
      "offerPerUser": 0,
      "offerPerDay": 0,
      "discount": 0,
      "cmpyAllowance": false,
    };

    //Travel time fare //Final
    if (featuresSettings.applyTravelFare)  fareDetails['travelFare'] = parseFloat(Number(timeInMinutes) * Number(vehicleData.timeFare)).toFixed(2)

    //waiting time fare //Final
    fareDetails['waitingFare'] = getWaitingFare(vehicleData.chargeRatePerMinuteForExceededMinimumWaitingTime, vehicleData.allowMinimumWaitingTimeInMinutes, vehicleData.isWaitingTimeExceddedChargesApplicable, waitingTime );
    
    //Night Fare Percentage = value if applied or 0
    fareDetails['nightObj'] = getFareIfTimeFallsIn(vehicleData.nightHours[0], reqtime, 'night');

    //Peak Fare Percentage
    fareDetails['peakObj'] = getFareIfTimeFallsIn(vehicleData.peakHours[0], reqtime, 'peak');
    if (!fareDetails['peakObj']) fareDetails['peakObj'] = getFareIfTimeFallsIn(vehicleData.peakHours[1], reqtime, 'peak');
    
    //Pickup Charge //Final
    fareDetails['pickupCharge'] = getPickupCharge(vehicleData.conveyancePerKm, vehicleData.conveyanceType, vehicleData.conveyanceAvailable);
    if (featuresSettings.manualPickupChargeFromMTD && tripId){
      var tripData = await Trips.findById(tripId, { tripno:1, csp:1 }).exec();  
      if(tripData){
        var manualPickupChargeFromMTD = tripData.csp.conveyance;
        fareDetails['pickupCharge'] = Number(manualPickupChargeFromMTD) + Number(fareDetails['pickupCharge']);
      }
    }
    //Get Approx distance Obj
    fareDetails['distanceObj'] = getDistanceObj(vehicleData.distance, distanceInKM);

    //Distance wise modifications
    if (fareDetails['distanceObj'].length) {
      var distanceObj = fareDetails['distanceObj'][0];
      var distanceDetails = getPerKmRateForDist(fareDetails['distanceObj'][0]);
      fareDetails['fareType'] = distanceDetails.fareType;
      if (fareDetails['fareType'] == 'kmrate') {
        fareDetails['perKMRate'] = distanceDetails.fare;
      } else {
        fareDetails['flatFare'] = distanceDetails.fare ? distanceDetails.fare : fareDetails['flatFare'] ;
        fareDetails['minFare'] = distanceDetails.fare ? distanceDetails.fare : fareDetails['minFare']  ;
      }

      //ADDTIONAL FARE 
      var baseupperfare = 0;
      var firstupperkmlimit = 0;
      if (featuresSettings.applyAdditionalKMFareModel) {
        //ADDTIONAL FARE 
        if (fareDetails['distanceObj'].length) {
          // total = total KM – firstupperkmlimit * KM fare + baseupperfare
          var baseupperfareArray = getDistanceObj(vehicleData.distance, 0);
          if (baseupperfareArray) {
            baseupperfare = baseupperfareArray[0].distanceFarePerFlatRate;
            firstupperkmlimit = baseupperfareArray[0].distanceTo;
          }
        }
        //ADDTIONAL FARE
      }

      //Modified Apply 
      applyValues['applyCommission'] = distanceObj.applyCommission;
      applyValues['applyPeakCharge'] = distanceObj.applyPeakCharge;
      applyValues['applyNightCharge'] = distanceObj.applyNightCharge;
      applyValues['applyWaitingTime'] = distanceObj.applyWaitingTime;
      applyValues['applyTax'] = distanceObj.applyTax;
      applyValues['applyPickupCharge'] = distanceObj.applyPickupCharge;
      //Modified Offers
      offers['cmpyAllowance'] = distanceObj.cmpyAllowance;
      offers['offerPerUser'] = distanceObj.offerPerUser;
      offers['offerPerDay'] = distanceObj.offerPerDay;
      offers['discount'] = distanceObj.discount;
    }

    //calculate Total Fare (Daily)= Base Fare +  ( KM * KM Rate ) + ( Travel Fare * Travel Time ) + ( Waiting Fare * Waiting Time ) + Pickup Charge + Tax - Discount
    //KM Rate 
    fareDetails['KMFare'] = parseFloat(parseFloat(distanceInKM) * parseFloat(fareDetails['perKMRate'])).toFixed(2); //Final
    if (featuresSettings.applyAdditionalKMFareModel) {
      if (Number(distanceInKM) > firstupperkmlimit) {
        //ADDTIONAL FARE 
        var balKMFARE = (parseFloat(distanceInKM) - parseFloat(firstupperkmlimit)) * parseFloat(fareDetails['perKMRate']);
        fareDetails['KMFare'] = parseFloat(Number(balKMFARE) + Number(baseupperfare)).toFixed(2);
        //ADDTIONAL FARE
      }  
    }
   
    if (fareDetails['distanceObj'].length) {
      var distanceObj = fareDetails['distanceObj'][0];
      var distanceDetails = getPerKmRateForDist(fareDetails['distanceObj'][0]);
      fareDetails['fareType'] = distanceDetails.fareType;
      if (fareDetails['fareType'] == 'kmrate') {
      }
      else {
        fareDetails['KMFare'] = distanceDetails.fare;
      }
    }

    fareDetails['totalFare'] = fareDetails['BalanceFare'] = Number(fareDetails['BaseFare']) + Number(fareDetails['KMFare']) + Number(fareDetails['travelFare']) + Number(fareDetails['waitingFare']) + Number(fareDetails['pickupCharge']);

    if (featuresSettings.applyMandatoryDiscount) {
      fareDetails['DetuctedFare'] =  (Number(fareDetails['totalFare'] * (Number(discountPercentage) / 100))).toFixed(2);
      fareDetails['totalFare'] = Number(  Number(fareDetails['totalFare']) - Number(fareDetails['DetuctedFare']) );
    }
   
    fareDetails['fareAmtBeforeSurge'] = Number(fareDetails['totalFare']);
    var totalFare = getTotalFare(fareDetails, applyValues, offers, additionalFee);
    fareDetails['fareAmtBeforeSurge'] = totalFare.fareAmtBeforeSurge;
    fareDetails['fareAmt'] = totalFare.fareAmt;//without tax
    fareDetails['comisonAmt'] = totalFare.comisonAmt;
    // console.log( "totalFare.hotelcommisionAmt",totalFare.hotelcommisionAmt)
    // fareDetails['hotelcommisionAmt'] = totalFare.hotelcommisionAmt;
    fareDetails['tax'] = totalFare.tax;
    fareDetails['totalFareWithOutOldBal'] = Number(totalFare.totalFareWithOutOldBal);
    // fareDetails['fareAmtBeforeSurge'] = totalFare.fareAmtBeforeSurge;
    fareDetails['indiaGSTAmounts'] = totalFare['indiaGSTAmounts']; 

    //When rounding here, will not calcaulate commision to rounded number
   /*  if (featuresSettings.convertAllFareToGivenMultipler) {
      totalFare.totalFare = GFunctions.roundAmountToGivenMultiples(totalFare.totalFare);
    } */

    fareDetails['totalFare'] = fareDetails['BalanceFare'] = totalFare.totalFare;
    fareDetails['totalFareBeforeDetucted'] = fareDetails['totalFare'];
    if (featuresSettings.secondarycur) {
      fareDetails.inSecondaryCur = GFunctions.sendSecondaryRate(fareDetails.totalFare);
    }
    /* fareDetails = _.mapValues(fareDetails, function (v) { if(typeof v === 'number') {
      return parseFloat(v.toFixed(2));
    }else { return v; }  });//Round all to 2 Decimals */
     
    
    var resData = {
      'vehicleDetails': _.cloneDeep(vehicleDetails),
      'fareDetails': _.cloneDeep(fareDetails),
      'offers': _.cloneDeep(offers),
      'applyValues': _.cloneDeep(applyValues),
    }
    
    return resData;
  } catch (error) {
    logger.error(error); 
    return error;
  }
};

export const getVehicleChargeApprox = (distanceInKM, timeInMinutes, vehicleData) => {
  try {
    // var totalFare = KMFare + TimeFare + BaseFare + Tax; (Minimum fare)
    var totalFare;
    var KMFare = parseFloat(parseFloat(distanceInKM) * parseFloat(vehicleData['perKMRate'])).toFixed(2);
    var TimeFare = parseFloat(parseFloat(timeInMinutes) * parseFloat(vehicleData['timeInMinutes'])).toFixed(2);
    totalFare = Number(KMFare) + Number(TimeFare) + Number(vehicleData['BaseFare']);
    var tax = parseFloat(totalFare * parseFloat(vehicleData['tax']) / 100);
    totalFare = Number(totalFare) + Number(tax);
    if (Number(totalFare) < parseFloat(vehicleData['minFare'])) {
      totalFare = vehicleData['minFare'];
    }
    return (totalFare).toFixed(2);
  } catch (error) {
    logger.error(error);
    return 'NA';
  }
}

//calculate Total Fare (Daily)= Base Fare +  ( KM * KM Rate ) + ( Travel Fare * Travel Time ) + ( Waiting Fare * Waiting Time ) + Pickup Charge + Tax - Discount
function getTotalFare(fareDetails, applyValues, offers, additionalFee) {
  var resObj = {
    'fareAmt': 0, 'fareAmtBeforeSurge': 0, 'comisonAmt': 0, 'tax': 0, 'flatFare': 0, 'oldCancellationAmt': 0, 'totalFareWithOutOldBal': 0, 'totalFare': 0
  };
  //Flat Rate
  if (fareDetails['fareType'] == 'flatrate') {
    resObj['fareAmt'] = fareDetails['KMFare'];
  } else { //KM Fare
    resObj['fareAmt'] = fareDetails['KMFare'];
  }
   
  //Add Waiting Time
  if (applyValues['applyWaitingTime']) {
    resObj['fareAmt'] = parseFloat(resObj['fareAmt']) + parseFloat(fareDetails['waitingFare']);
  } 
  //Add Pickup Charge
  if (applyValues['applyPickupCharge']) {
    resObj['fareAmt'] = parseFloat(resObj['fareAmt']) + parseFloat(fareDetails['pickupCharge']);
  } 

  resObj['fareAmt'] = Number(fareDetails['BaseFare']) + Number(resObj['fareAmt']) + Number(fareDetails['travelFare']);
  resObj['fareAmtBeforeSurge'] = resObj['fareAmt'];

  //Add Peak & Night Multipler
  if (applyValues['applyPeakCharge']) {
    if (fareDetails['peakObj'].isApply) resObj['fareAmt'] = parseFloat(resObj['fareAmt']) * parseFloat(fareDetails['peakObj'].percentageIncrease);
  }
  if (applyValues['applyNightCharge']) {
    if (fareDetails['nightObj'].isApply) resObj['fareAmt'] = parseFloat(resObj['fareAmt']) * parseFloat(fareDetails['nightObj'].percentageIncrease);
  } 

  if (featuresSettings.addAdditionalFaresInTrip) {
    var additionalFares = addAdditionalFaresInTrip(additionalFee);
    resObj['fareAmt'] = Number(resObj['fareAmt']) + Number(additionalFares);
  }

  //Tax for (KM + Multipler + Waiting + Pickup )
  if (applyValues['applyTax']) {
    resObj['tax'] = parseFloat(fareDetails['taxPercentage']) * parseFloat(resObj['fareAmt']) / 100;
  }

  //Total Fare = Fare + tax
  resObj['totalFare'] = parseFloat(resObj['fareAmt']) + Number(resObj['tax']);
  //Check For Min Fare
  // if (fareDetails['fareType'] == 'kmrate'){
    if (parseFloat(resObj['totalFare']) < parseFloat(fareDetails['minFare']) ){
      resObj['totalFare'] = fareDetails['minFare'];
      resObj['fareAmtBeforeSurge'] = fareDetails['minFare'];
    }
  // }

  //When rounding here will calcaulate commision to rounded number
  if (featuresSettings.convertAllFareToGivenMultipler) {
    resObj['totalFare'] = GFunctions.roundAmountToGivenMultiples(resObj['totalFare']);
  }

  resObj['totalFareWithOutOldBal'] = resObj['totalFare'];

  //Calculate Commision for fare only no tax added
  resObj['comisonAmt'] = parseFloat(fareDetails['comison']) * parseFloat(resObj['totalFareWithOutOldBal']) / 100;
  if (featuresSettings.addBookingFeeToCommision){
    var amountwithoutBookingfee = Number(resObj['totalFareWithOutOldBal']) - Number(fareDetails['BaseFare']);
    resObj['comisonAmt'] = ((Number(fareDetails['comison']) * Number(amountwithoutBookingfee)) / 100) + Number(fareDetails['BaseFare']);
  }
 // resObj['hotelcommisionAmt'] = parseFloat(fareDetails['hotelcommision']) * parseFloat(resObj['totalFareWithOutOldBal']) / 100;

  // console.log("fareDetails['hotelcommision']",fareDetails['hotelcommision'])

  resObj = _.mapValues(resObj, function (v) {
    if (typeof v === 'number') {
      return parseFloat(v.toFixed(2));
  }else { return v; }  });//Round all to 2 Decimals 

  GFunctions.clearObj(resObj);  
  return resObj;
}

function getIndiaGSTAmounts(totalFare){
  var IndiaGSTAmounts = {};
  var gstFareBreakPercentage = featuresSettings.gst.gstFareBreakPercentage;
  var gstOnFareBreakPercentage1 = featuresSettings.gst.gstOnFareBreakPercentage1;
  var gstOnFareBreakPercentage2 = featuresSettings.gst.gstOnFareBreakPercentage2;
  IndiaGSTAmounts.totalfare1 = parseFloat(Number(totalFare) * Number(gstFareBreakPercentage) / 100).toFixed(2);
  IndiaGSTAmounts.totalfare2 = parseFloat(Number(totalFare) - Number(IndiaGSTAmounts.totalfare1)).toFixed(2);
  IndiaGSTAmounts.gst1On1 = parseFloat(Number(gstOnFareBreakPercentage1) * Number(IndiaGSTAmounts.totalfare1) / 100).toFixed(2);
  IndiaGSTAmounts.gst2On2 = parseFloat(Number(gstOnFareBreakPercentage2) * Number(IndiaGSTAmounts.totalfare2) / 100).toFixed(2);
  IndiaGSTAmounts.totalTax = parseFloat(Number(IndiaGSTAmounts.gst1On1) + Number(IndiaGSTAmounts.gst2On2)).toFixed(2);
  IndiaGSTAmounts.gstPerOn1 = gstOnFareBreakPercentage1;
  IndiaGSTAmounts.gstPerOn2 = gstOnFareBreakPercentage2;

  //Indian GST
  IndiaGSTAmounts.fee1 = IndiaGSTAmounts.totalfare1;
  IndiaGSTAmounts.cgstperentage1 = parseFloat(IndiaGSTAmounts.gstPerOn1 / 2).toFixed(2);
  IndiaGSTAmounts.cgst1 = parseFloat(IndiaGSTAmounts.gst1On1 / 2).toFixed(2);
  IndiaGSTAmounts.sgstperentage1 = parseFloat(IndiaGSTAmounts.gstPerOn1 / 2).toFixed(2);
  IndiaGSTAmounts.sgst1 = parseFloat(IndiaGSTAmounts.gst1On1 / 2).toFixed(2);
  IndiaGSTAmounts.subtotal1 = (Number(IndiaGSTAmounts.totalfare1) + Number(IndiaGSTAmounts.gst1On1)).toFixed(2);
  IndiaGSTAmounts.fee2 = IndiaGSTAmounts.totalfare2;
  IndiaGSTAmounts.cgstperentage2 = parseFloat(IndiaGSTAmounts.gstPerOn2 / 2).toFixed(2);
  IndiaGSTAmounts.cgst2 = parseFloat(IndiaGSTAmounts.gst2On2 / 2).toFixed(2);
  IndiaGSTAmounts.sgstperentage2 = parseFloat(IndiaGSTAmounts.gstPerOn2 / 2).toFixed(2);
  IndiaGSTAmounts.sgst2 = parseFloat(IndiaGSTAmounts.gst2On2 / 2).toFixed(2);
  IndiaGSTAmounts.subtotal2 = (Number(IndiaGSTAmounts.totalfare2) + Number(IndiaGSTAmounts.gst2On2)).toFixed(2);
  IndiaGSTAmounts.finalamt = totalFare;

  return IndiaGSTAmounts;
}
/**
 * Get waiting fare if its ecxceeds minTime
 * @param {*} rate 
 * @param {*} minTime 
 * @param {*} isApplicable 
 * @param {*} waitingTime (Mins)
 */
function getWaitingFare(rate, minTime, isApplicable, waitingTime) {
  var waitingFare = 0;
  if(isApplicable){
    if(Number(waitingTime) > Number(minTime) ){
      if (waitingTime <= 1) waitingTime = 0;
      waitingFare = Number(rate) * Number(waitingTime);
    }
  }
  return waitingFare.toFixed(2);
}

/**
 * getFareIfTimeFallsIn = if HOurs within given time it gives percentage to increase
 * @param {*} hours
 * @param {*} now 
 *          "isApply": false,
            "percentageIncrease": 1.5,
            "alertLable": "Notes : Peak Fare x{PERCENTAGE} ({TIME})"
 */
function getFareIfTimeFallsIn(hours, now, forType='peak' ) {
  var resObj = { 'isApply': false, 'percentageIncrease': 0, 'alertLable' : '' };
  var format = 'HH:mm:ss';
  if (!now || now == '') {
    now = GFunctions.sendTimeNow(format); 
  }else{
    var time = now.split(' ');
    if (time[1] == 'AM' || time[1] == 'PM') {
      var time = moment(now, 'hh:mm A').format("HH:mm");
      now = time
      console.log('AM/PM', now)
    }
    else {
      now = now
      console.log('24format', now)
    }
  }

  var to = moment(hours.to, format),
    from = moment(hours.from, format),
    tempnow = moment(now, format),
    now = moment(tempnow, format);

  if(from > to){ //22PM to 8AM 
    if (now.isBetween(to, from)) { resObj.isApply = false; } else { resObj.isApply = true;  }
  }else{
    if (now.isBetween(from, to)) { resObj.isApply = true; } else { resObj.isApply = false; }
  } 
     
  if (forType == 'peak') {
    resObj.percentageIncrease = Number(getPercentageMultipiler(hours.percentPeakFare));
    resObj.alertLable = GFunctions.convertLableDynamically(labels.isPeakExistAlertLabel, { 'PERCENTAGE': resObj.percentageIncrease, 'TIME': hours.from + ' - ' + hours.to } );
  } else {
    resObj.percentageIncrease = Number(getPercentageMultipiler(hours.percentNightFare));
    resObj.alertLable = GFunctions.convertLableDynamically(labels.isNightExistAlertLabel, { 'PERCENTAGE': resObj.percentageIncrease, 'TIME': hours.from + ' - ' + hours.to });
  } 

  return  GFunctions.roundAllValuesToTwoDigits(resObj); 
}

function getPercentageMultipiler(percentage) {
  return parseFloat(1 + (parseFloat(percentage) / 100)).toFixed(2);
}

function getPickupCharge(conveyancePerKm, conveyanceType, conveyanceAvailable) { 
  if (conveyanceAvailable) {
    if (conveyanceType == 'flatrate' ) {
      return conveyancePerKm;
    } else {
      return 0;
    }
  }else {
    return 0;
  }
}

function getDistanceObj(distanceArray, distanceInKM) { 
  var filteredFareOffers;
  if (distanceArray){
    var filteredFareOffers = _.filter(distanceArray, i => Number(i.distanceFrom) <= distanceInKM && Number(i.distanceTo) >= distanceInKM);
  }
  return filteredFareOffers;
}

function getPerKmRateForDist(distance) {
  var distObj = {};
  distObj.fareType = distance.distanceFareType; 
  if (distance.distanceFareType == 'flatrate'){
    distObj.fare = distance.distanceFarePerFlatRate; 
  }else{
    distObj.fare = distance.distanceFarePerKM;  
  }
  return distObj;
}

function addAdditionalFaresInTrip(arrayValue) {
  var additionalFares = 0;
  if (arrayValue){
  arrayValue.forEach((element, index, array) => {
    additionalFares = additionalFares + Number(element.amount);
  });
}
  return additionalFares;
}