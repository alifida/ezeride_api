import path from 'path';
import Trips from '../models/trips.model';
import Driver from '../models/driver.model';

const firebase = require('firebase');

const config = require('../config');
const notificationContent = require('../notificationContent');
const moment = require("moment");
const GoogleMapsAPI = require("googlemaps");
const fs = require('fs');
const momentTimezone = require("moment-timezone");
const crypto = require('crypto');
const FCM = require('fcm-push');
const serverKey = config.fcmServer;
const randomize = require('randomatic');
const _ = require('lodash');
import logger from '../helpers/logger';
import { concave } from '@turf/turf';
const featuresSettings = require('../featuresSettings');

const GoogleDistanceMatrix = require('google-distance-matrix');
GoogleDistanceMatrix.key(config.googleApi);

const nodeGeocoder = require("node-geocoder");
const options = {
  provider: 'google',
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: config.googleApi, // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};
const geocoder = nodeGeocoder(options);

/**
 * send random code
 * @param  {[type]} type [description]
 * @param  {Number} no   [description]
 * @return {[type]}      [description]
 */
export const sendRandomizeCode = (type, no = 4) => {
  if (config.appMode == 'dev') {
    return 1111;
  } else {
    return randomize(type, no, { exclude: '0' });
  }
}


/**
 * send Formated Time
 * @param  {[type]} time [description] 
 * @return {[type]} 72000     [description]
 */
export const getFormatTime = (time = '00:00:00') => {
  if (time == '') return "";
  var arr = time.split(":");
  // var storedTime = hours * 3600 + minutes * 60 + seconds;

  var storedTime = Number(arr[0] * 3600) + Number(arr[1] * 60) + Number(arr[2]);
  return storedTime;
}


/**
 * send Formated Time from 7200000
 * @param  {[type]} time [description] 
 * @return {[type]}   '00:00:00'   [description]
 */
export const fromStoredTime = (storedTime = '000000') => {
  var hours = storedTime / 3600; // needs to be an integer division
  var leaves = storedTime - hours * 3600
  var minutes = leaves / 60
  var seconds = leaves - 60 * minutes
  return hours + ':' + minutes + ':' + seconds
}



function convertTotalMinTohma(num = '000000') {
  var hours = Math.floor(num / 60);
  var median = 'AM';
  if (hours > 12) { hours = hours - 12; median = 'PM'; }
  var minutes = num % 60;
  return hours + ":" + minutes + " " + median;
}

/**
 * Send FCM to To id
 * @input title,msg,to
 * @param 
 * @return null
 * @response null
 */
export const sendFCMMsg = (touser, msg = "Message from " + config.appName, subject, title = config.appName/*,contentType key*/) => {
  var oldMsg = msg;

  if (subject != ' ') {
    var msg = notificationContent[notificationContent.defaultLanguage][subject];
    console.log("msg", msg);
  }
  else {
    var msg = msg;
    console.log("msg", msg);
  }
  if (!msg) msg = oldMsg;

  /*notificationContent[driverLangCode].${content type dynamically} If it is changed to dynamically the default lang is en */
  if (touser) { //If Only Device Token exists
    touser = touser.toString();
    var fcm = new FCM(serverKey);

    var fcmmessage = {
      to: touser,
      priority: 'high',
      // collapse_key: 'your_collapse_key', 
      data: {
        title: title,
        message: msg,
        sound: 'default',
      },
      notification: {
        title: title,
        body: msg,
        sound: 'default',
      }
    };
    // console.log(fcmmessage);

    //callback style
    fcm.send(fcmmessage, function (err, response) {
      if (err) {
        logger.error("sendFCMMsgErr", err);
      } else {
        //logger.info("sendFCMMsg",response);
      }
    });

  }
}

export const sendAdminPushMsg = (touser, msg = "Message from " + config.appName, title = config.appName) => {
  if (touser) { //If Only Device Token exists
    touser = touser.toString();
    var fcm = new FCM(config.adminfcmServer);

    var fcmmessage = {
      to: touser,
      priority: 'high',
      // collapse_key: 'your_collapse_key', 
      data: {
        title: title,
        message: msg,
        sound: 'default',
      },
      notification: {
        title: title,
        body: msg,
        sound: 'default',
      }
    };

    // console.log(fcmmessage);
    //callback style
    fcm.send(fcmmessage, function (err, response) {
      if (err) {
        logger.error("sendFCMMsgErr", err);
      } else {
        logger.info("sendFCMMsg");
      }
    });

  }
}

/**
 * Send JS Formated Now Time
 * @input  
 * @param 
 * @return null
 * @response null
 */
export const sendJSTimeNow = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  return Date.now();
}

/**
 * Send Formated Now Time
 * @input  
 * @param 
 * @return null
 * @response null
 */
export const sendTimeNow = (timeformat = "M-D-YYYY h:mm a") => {
  var now = moment().utcOffset(config.utcOffset);
  return now.format(timeformat);
}

export const sendPast7Day = (timeformat = "M-D-YYYY h:mm a") => {
  var myDate = moment().subtract(7, 'days');
  myDate = new Date(myDate);
  return myDate;
}

export const sendPast31Day = (timeformat = "M-D-YYYY h:mm a") => {
  var myDate = moment().subtract(31, 'days');
  myDate = new Date(myDate);
  return myDate;
}

export const sendPast365Day = (timeformat = "M-D-YYYY h:mm a") => {
  var myDate = moment().subtract(365, 'days');
  myDate = new Date(myDate);
  return myDate;
}

export const pastDay = (timeformat = "M-D-YYYY h:mm a") => {
  var myDate = moment().subtract(1, 'days');
  myDate = new Date(myDate);
  return myDate;
}

export const getISODate = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  // will return the current time in India.
  var t = moment().utcOffset(config.utcOffset).format(timeformat);
  return t;
}

export const getISODateADayBuffer = (addDays = 1, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var myDate = moment().add(addDays, "days").utcOffset(config.utcOffset).format(timeformat);
  return myDate;
}

export const getISOTodayDate = (dateTime, timeformat = "YYYY-MM-DDT00:00:00.000[Z]") => {
  var t = moment().utcOffset(config.utcOffset).format(timeformat);
  return t;
}

export const getDateTimeForSortings = (dateTime, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var result = moment(dateTime, "MM/DD/YYYY HH:mm a").utcOffset(config.utcOffset).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
  // var t = moment(dateTime).utcOffset(config.utcOffset).format(timeformat);
  return result;
}

export const getDateTimeinThisFormat = (dateTime, inTimeformat = "MM/DD/YYYY HH:mm a", outTimeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]", ) => {
  var result = moment(dateTime, inTimeformat).format(outTimeformat);
  // var t = moment(dateTime).utcOffset(config.utcOffset).format(timeformat);
  return result;
}

export const getDateTimeForUserLable = (dateTime, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  if (!dateTime) return null;
  var result = moment(dateTime, timeformat).format("YYYY-MM-DD");
  return result;
}

export const getRespCountryDateTime = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  // will return the current time in Resp Offset.
  var t = moment().utcOffset(config.utcOffset).format(timeformat);
  return t;
}

export const getHoursBtDateTime = (endDate, startDate, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var start_date = moment(startDate);
  var end_date = moment(endDate);
  var duration = moment.duration(end_date.diff(start_date));
  var hours = duration.asHours();
  hours = hours.toFixed(2);
  // var minutes = parseInt(duration.asMinutes()) % 60;
  return hours;
}

export const getMinsBtDateTime = (endDate, startDate, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var start_date = moment(startDate);
  var end_date = moment(endDate);
  var duration = moment.duration(end_date.diff(start_date));
  // var hours = duration.asHours();
  // hours = hours.toFixed(2);
  var minutes = parseInt(duration.asMinutes());
  return minutes;
}

export const getUpcomingSchListBuffer = (addMinutes = 15, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var myDate = moment().add(addMinutes, "minutes").utcOffset(config.utcOffset).format(timeformat);
  return myDate;
}

export const getUpcomingSchListMinusBuffer = (Minutes = 15, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var myDate = moment().subtract(Minutes, "minutes").utcOffset(config.utcOffset).format(timeformat);
  return myDate;
}

export const getUpcomingSchListMinusBufferServer = (Minutes = 15, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var myDate = moment().subtract(Minutes, "minutes").format(timeformat);
  return myDate;
}

export const getUpcomingSchListHourBufferServer = (Hours = 1, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var myDate = moment().subtract(Hours, "hours").utcOffset(config.utcOffset).format(timeformat);
  return myDate;
}

export const getSubcriptionValidityDate = (startdate, addDays, timeformat = "YYYY-MM-DDT00:00:00.000[Z]") => {
  addDays = Number(addDays) + 1;
  var myDate = moment(startdate).add(addDays, "days").utcOffset(config.utcOffset).format(timeformat);
  // var new_date = moment(startdate, "YYYY-MM-DDTHH:mm:ss.SSS[Z]");  
  // new_date.add(addDays, 'days').utcOffset(config.utcOffset).format(timeformat);
  return myDate;
}


export const getISOTodayDateForTripPrefix = (timeformat = "YYMMDD") => {
  var t = moment().utcOffset(config.utcOffset).format(timeformat);
  return t;
}

/**
 * Send Formated Time from EpochTime 
 * @input   date = '19-05-2018' , time='08:00 AM'
 * @param 
 * @return  "05/19/2018 05:06 PM"
 * @response  
 */
export const sendFormatedTime = (date, time = '08:00 AM') => {
  if (date) {
    var fromDate = date.split('-');
    var newDateFormat = fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2] + ' ' + time;
    return newDateFormat;
  }
}

/**
 * Send Formated Time from EpochTime 
 * @input   date = '19-05-2018 08:00 AM'
 * @param 
 * @return  "05/19/2018 05:06 PM"
 * @response  
 */
export const sendFormatedDTime = (date) => {
  if (date) {
    var fromDate = date.split('-');
    var newDateFormat = fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2];
    return newDateFormat;
  }
}

// /**
//  * Send GMT Formated Time 
//  * @input   date = '19-05-2018 08:00 AM'
//  * @param 
//  * @return  "05/19/2018 05:06 PM"
//  * @response  
//  */
//  export const sendGMTFormatedDTime = (date,utc) => {  
//   var gmtFTime = new Date( date + " " + utc).toGMTString();
//   return gmtFTime;
// }  

/**
 * Send ISO Date 
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const sendISODateTime = (unix, timeformat = "M-D-YYYY h:mm a") => {
  var day = moment.unix(unix); //seconds
  return day.format(timeformat);
}

/**
 * Get Current Year
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const getCurrentYear = () => {
  var year = moment().format('YYYY');
  return year;
}

/**
 * Send ISO Date format : Notification
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const getSCHNotificationISODT = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  var epoch = moment().unix();

  var june = moment();
  var epoch = june.tz('Asia/Kolkata').format(timeformat);

  var myDate = moment().utc().format(timeformat);
  // var myDate  = moment("10/15/2014 9:00").utc().format(timeformat);
  return epoch;

}

/**
 * Send GMT Current Date format : Notification
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const getSCHNotificationGMTDT = (timeformat = "YYYY-MM-DDTHH:mm:00.000[Z]") => {
  // var epoch = moment().unix();

  // var june  = moment(); 
  // var epoch = june.tz('Asia/Kolkata').format(timeformat); 

  // var myDate  = moment().utc().format(timeformat);
  // var then = moment(now).subtract(20, "minutes").toDate()
  //Notify b4 5min
  var myDate = moment().add(5, "minutes").utc().format(timeformat);
  var myDate = new Date(myDate).toGMTString();

  // var myDate  = moment("10/15/2014 9:00").utc().format(timeformat);
  return myDate;

}

//Mon, 21 May 2018 06:21:00 GMT
export const getCommonDTFormat = (dt, timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  // var nnn = moment(dt , 'D, d M YYYY HH:mm:00 GMT').format(timeformat);
  var gmtCurrent = moment(dt).format(timeformat);
  return gmtCurrent;
}

export const getCommonMonthStartDate = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  const startOfMonth = moment().startOf('month').format(timeformat);
  // const endOfMonth = moment().endOf('month').format(timeformat);
  return startOfMonth;
}

export const getCommonWeekStartDate = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  const startOfWeek = moment().startOf('week').format(timeformat);
  return startOfWeek;
}

export const getCommonWeekEndDate = (timeformat = "YYYY-MM-DDTHH:mm:ss.SSS[Z]") => {
  const startOfWeek = moment().endOf('week').format(timeformat);
  return startOfWeek;
}

const publicConfig = {
  key: config.googleApi,
  stagger_time: 1000, // for elevationPath
  encode_polylines: true
};
const gmAPI = new GoogleMapsAPI(publicConfig);

// pending save to Mongo
export const saveGMap = async (docs = '') => {
  const from = docs.adsp.pLat + ',' + docs.adsp.pLng;
  const to = docs.adsp.dLat + ',' + docs.adsp.dLng;

  var params = {
    size: '600x300',
    maptype: 'roadmap',
    markers: [
      {
        location: from,
        label: 'A',
        color: 'green',
        shadow: true
      },
      {
        location: to,
        label: 'B',
        color: 'red',
        shadow: true
      }
    ],
    style: [
      {
        feature: 'road',
        element: 'all',
        rules: {
          hue: '0x00ff00'
        }
      }
    ]
  };
  var filepath = './public/gmap/' + docs.tripno + '.png';

  var Mapurl = gmAPI.staticMap(params); // return static map URL
  gmAPI.staticMap(params, function (err, binaryImage) {
    if (err) {
      return res.status(500).json();
    } else {
      fs.writeFile(filepath, binaryImage, 'binary', function (err) {
        if (err) return res.status(200).json({ 'success': false, 'TripDetail': docs, 'Mapurl': "" });
        docs.adsp.map = config.baseurl + 'public/gmap/' + docs.tripno + '.png';
        //send map to save = pending 
        return res.status(200).json({ 'success': true, 'TripDetail': docs, 'Mapurl': Mapurl });
      })
    }
  });

}


export const saveStaticMapForTrip = async (tripData = '', path = []) => {
  const from = tripData.adsp.pLat + ',' + tripData.adsp.pLng;
  const to = tripData.adsp.dLat + ',' + tripData.adsp.dLng;
  var params = {
    size: '600x300',
    maptype: 'roadmap',
    markers: [
      {
        location: from,
        label: 'A',
        color: 'green',
        shadow: true
      },
      {
        location: to,
        label: 'B',
        color: 'red',
        shadow: true
      }
    ],
    style: [
      {
        feature: 'road',
        element: 'all',
        rules: {
          hue: '0x00ff00'
        }
      }
    ],
    path: [
      {
        color: '0x0000ff',
        weight: '5',
        points: path
      }
    ]
  };
  var filepath = './public/gmap/' + tripData.tripno + '.png';
  gmAPI.staticMap(params, function (err, binaryImage) {
    if (err) {
      logger.error(err);
    } else {
      fs.writeFile(filepath, binaryImage, 'binary', function (err) {
        if (err) logger.error(err);
        var staticMapURL = config.baseurl + 'public/gmap/' + tripData.tripno + '.png';
        saveMapPathInTrip(tripData._id, staticMapURL);
      })
    }
  });
}

function saveMapPathInTrip(tripId, map) {
  Trips.findOneAndUpdate({ _id: tripId }, { 'adsp.map': map }, { new: false }, (err, doc) => {
    if (err) {
      logger.error(err);
    }
    logger.info('Map Updated');
  })
}

//String Helpers

/**
 * Send Formated Number from any
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const sendFormatedNumber = (value, round = 2) => {
  if (isNaN(value)) {
    return 0;
  } else {
    value = Number(value);
    return value.toFixed(round);
  }
}

//String Helpers

//Send City From lat,lon

/**
 * Get City from lat lon
 * @input  
 * @param 
 * @return  
 * @response  
 */
export const getCityFromLatLon = (lat, lon) => {
  var city = '';
  // Using callback
  return new Promise(function (resolve, reject) {
    geocoder.reverse({ lat: 45.767, lon: 4.833 }, function (err, res) {
      if (err) {
        resolve('');
      }
      else {
        city = res[0].city;
        resolve(city);
      }
    });
  })
}


/**
 * Send Some unique Code 
 * @param  {Number} char [No of char code needed]
 * @return {[type]}      [description]
 */
export const sendUniqueCode = (char = 5) => {
  return crypto.randomBytes(char).toString('hex'); //change this logic or crypt Datatime RFCNG
}


export const getDriverFBStatusAndUpdate = (driverId) => {
  var id = driverId;
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("drivers_data");
  id = id.toString();
  var DriverRef = ref.child(id);
  DriverRef.once('value').then(function (snap) {
    var data = snap.val();
    var requestStatus = data.request.status;
    // console.log(requestStatus);
    if (requestStatus == 0) {
      //He has in no trip
      freeTheDriver(driverId);
    } else {
      // console.log('Status is one "1"');
    }
  });
}

export const freeTheDriver = (driverId) => {
  Driver.findByIdAndUpdate(driverId, {
    'curStatus': 'free'
  }, { 'new': true },
    function (err, doc) {
      if (err) { }
    }
  );
}

// RFCNG


export const getScheduleTaxiRequestTime = (addMinutes = 5, timeformat = "YYYY-MM-DDTHH:mm:00.000[Z]") => {
  var myDate = moment().add(addMinutes, "minutes").utc().format(timeformat);
  var myDate = new Date(myDate).toGMTString();
  return myDate;

}

export const minusSomeMinToCurrentTime = (Minutes = 5, timeformat = "YYYY-MM-DDTHH:mm:00.000[Z]") => {
  var myDate = moment().subtract(Minutes, "minutes").utcOffset(config.utcOffset).format(timeformat);
  // var myDate = new Date(myDate).toGMTString();
  return myDate;
}

/**
 * Return distinct array
 * @param {*} items array
 * @param {*} prop  toFilter By key
 */
export const getDistinctInArray = (items, prop) => {
  var unique = [];
  var distinctItems = [];

  _.each(items, function (item) {
    if (unique[item[prop]] === undefined) {
      distinctItems.push(item);
    }

    unique[item[prop]] = 0;
  });

  return distinctItems;
}

/**
 * clear Obj 
 * @param {*} ObjRef 
 * @param {*} timer 
 */
export const clearObj = (ObjRef, timer = 5000) => {
  setTimeout(() => {
    Object.keys(ObjRef).forEach(function (key) { delete ObjRef[key]; });
  }, timer);
}


/**
 * get Distance And Time From GDM for Single Orign and distination
 *  @param origins =  [ '9.924068,78.123846' ] , destinations = [ '9.914485,78.123122' ]
 *  @return Success Response : {"error":false,"distanceValue":1857,"distanceLable":"1.9 km","timeValue":465,"timeLable":"8 mins","from":"152 A/5, North Veli Street, Near Bharat Petrolium, North Veli Street, Madurai, Tamil Nadu 625001, India","to":"Panthadi Street, Mahal Area, Madurai Main, Madurai, Tamil Nadu 625001, India"}
 */
export const getDistanceAndTimeFromGDM = async (origins, destinations) => {

  GoogleDistanceMatrix.units('metric');
  GoogleDistanceMatrix.mode('driving');

  let data = {
    'error': false,
    'msg': '',
    'distanceValue': 0,//Meters
    'distanceLable': '',
    'timeValue': 0,//Minutes
    'timeLable': '',
    'from': '',
    'to': '',
  }

  return new Promise(function (resolve, reject) {

    GoogleDistanceMatrix.matrix(origins, destinations, function (err, distances) {
      try {
        if (err) {
          data["error"] = true;
          data["msg"] = err.toString();
          reject(data);
        }
        if (!distances) {
          data["error"] = true;
          data["msg"] = "Error Getting Estimation, Please check your distination Address.";
          reject(data);
        }
        if (
          typeof distances !== "undefined"
          && distances !== null
          && typeof distances.status !== "undefined"
          && distances.status == 'OK'
          && typeof distances.rows !== "undefined"
          && typeof distances.rows[0] !== "undefined"
          && typeof distances.rows[0].elements !== "undefined"
          && typeof distances.rows[0].elements[0] !== "undefined"
          && typeof distances.rows[0].elements[0].distance !== "undefined"
        ) {
          data.from = distances.origin_addresses[0];
          data.to = distances.destination_addresses[0];
          data.distanceLable = distances.rows[0].elements[0].distance.text;
          data.distanceValue = distances.rows[0].elements[0].distance.value;
          data.timeLable = distances.rows[0].elements[0].duration.text;
          data.timeValue = distances.rows[0].elements[0].duration.value;
          resolve(data);

        } else { // If Api gives error response
          var errMsg = distances.error_message;
          if (!errMsg) {
            errMsg = distances.rows[0].elements[0].status;
            if (errMsg == "ZERO_RESULTS") errMsg = "Error Getting Estimation, Please check your distination Address.";
          }
          data["error"] = true;
          data["msg"] = errMsg;
          reject(data);
        }
      } catch (error) {
        data["error"] = true;
        data["msg"] = error.toString();
        reject(data);
      }
    });

  })
}

//Notify Rider
export const notifyRider = async (userid, msg, requestId, tripstatus = "noresponse") => {
  var update = {
    status: tripstatus,
    review: msg
  };

  Trips.findOneAndUpdate({ _id: requestId }, update, { new: false }, (err, doc) => {
    if (err) { } else { updateRiderFbStatus(userid, msg, requestId); }
  });
}

export const updateRiderFbStatus = (userid, msg, requestId = '0') => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("riders_data");
  var requestData = {
    tripstatus: msg,
    requestId: requestId
  };
  var child = userid.toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData);
}

/**
 * Return "Notes : Peak Fare x0.5 (20.22 - 8411.54)"
 * @param {*} label 'Notes : Peak Fare x{PERCENTAGE} ({TIME})'
 * @param {*} obj  {'PERCENTAGE' : .5, 'TIME' : '20.22 - 8411.54' }
 */
export const convertLableDynamically = (label, obj) => {
  var newLable = label;
  for (var key in obj) {
    let strToReplace = '{' + key + '}';
    newLable = newLable.replace(strToReplace, obj[key]);
  }
  return newLable;
}

export const convertObjToMongooseSchema = (label, obj) => {
  var resObj = _.mapValues(string1, function (v) {
    if (typeof v === 'number') {
      return 'Number';
    } else if (typeof v === 'string') {
      return typeof v;
    } else if (typeof v === 'boolean') {
      return 'Boolean';
    }
  });

  return resObj;
}

/**
 * roundAllValuesToTwoDigits
 * @param {*} params  OBJ
 */
export const roundAllValuesToTwoDigits = (resObj) => {
  var resultObj = _.mapValues(resObj, function (v) {
    if (typeof v === 'number') {
      return parseFloat(v.toFixed(2));
    } else { return v; }
  });//Round all to 2 Decimals   
  return resultObj;
}

/** 
 * standard date format
*/
export const setFormatDate = (date) => {
  if (date) {
    var result = moment(date, ["DD-MM-YYYY", "YYYY-MM-DD"]).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
    return result;
  } else {
    return null
  }
}


export const sendSecondaryRate = (amount = 0) => {
  var conversionRate = featuresSettings.conversionRate;
  var convertRate = parseFloat(Number(amount) * Number(conversionRate)).toFixed(featuresSettings.roundOff);
  return convertRate;
}

export const hideEmailDataForDemo = (string) => {
  if (!string) return "";
  var domain = string.split(".");
  var firstTwo = string.substr(0, 2);
  if (!domain[1]) domain[1] = "com";
  var res = firstTwo + "****@**" + domain[1];
  return res;
}

export const hidePhoneDataForDemo = (string) => {
  if (!string) return "";
  var last = string.slice(-3);
  return "*******" + last;
}

export const getSession = (key) => {
  let session = require('continuation-local-storage').getNamespace('session');
  if (session != undefined) {
    return session.get(key);
  }
  return '';
}


export const getDistanceAndTimeFromGDMForEncode = async (origins, destinations) => {

  GoogleDistanceMatrix.units('metric');
  GoogleDistanceMatrix.mode('driving');

  let data = {
    'error': false,
    'msg': '',
    'distanceValue': 0,//Meters
    'distanceLable': '',
    'timeValue': 0,//Minutes
    'timeLable': '',
    'from': '',
    'to': '',
  }

  return new Promise(function (resolve, reject) {

    GoogleDistanceMatrix.matrix(origins, destinations, function (err, distances) {
      try {
        if (err) {
          data["error"] = true;
          data["msg"] = err.toString();
          reject(data);
        }
        if (!distances) {
          data["error"] = true;
          data["msg"] = err.toString();
          reject(data);
        }
        if (
          typeof distances !== "undefined"
          && distances !== null
          && typeof distances.status !== "undefined"
          && distances.status == 'OK'
          && typeof distances.rows !== "undefined"
          && typeof distances.rows[0] !== "undefined"
          && typeof distances.rows[0].elements !== "undefined"
          && typeof distances.rows[0].elements[0] !== "undefined"
          && typeof distances.rows[0].elements[0].distance !== "undefined"
        ) {

          resolve(distances);

        } else { // If Api gives error response
          data["error"] = true;
          data["msg"] = distances.error_message;
          reject(data);
        }
      } catch (error) {
        data["error"] = true;
        data["msg"] = err.toString();
        reject(data);
      }
    });

  })
}

export const convertToNumber = (value) => {
  if (value == 'true') {
    return true;
  }
  else if (value == 'false') {
    return false;
  }
  else if (value > 0) {
    value = parseInt(value);
    return value;
  } else {
    return value;
  }
}

export const restartServer = () => {
  setTimeout(function () {
    process.exit(1);
  }
    , 10000); //30000 = 30 sec
}

export const getFirebaseSupportedChars = (value) => {
  if (value) {
    value = value.replace('/', '_').replace('.', '_').replace('#', '_').replace('$', '_').replace(']', '_').replace('[', '_');
    return value;
  } else {
    return "_";
  }
}

export const isStarExistsInString = (value) => {
  if (value) {
    return value.includes("*");
  } else {
    return false;
  }
}

export const convertAllNumbersToString = (obj) => {
  obj = _.mapValues(obj, function (v) {
    if (typeof v === 'number') {
      return v.toFixed(2);
    } else { return v; }
  });//Round all to 2 Decimals 
  return obj;
}

export const roundAmountToNearByFive = (value) => {
  if (typeof value === 'number') {
    return Math.ceil(value / 5) * 5;
  } else { return value; }
}

export const roundAmountToGivenMultiples = (value, multipler = featuresSettings.multipler) => {
  var baseUnit = value / multipler;
  var decimalNo = (baseUnit + "").split(".");
  if (Number(decimalNo[1]) <= 5) {
    return Math.floor(baseUnit) * multipler;
  } else {
    return Math.ceil(baseUnit) * multipler;
  }
}