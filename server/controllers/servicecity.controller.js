import mongoose from 'mongoose';
import path from 'path';
import * as HelperFunc from './adminfunctions';
const url = require('url');
var util = require('util');
var request = require('async-request');
const randomize = require('randomatic');
const moment = require('moment');



import ServiceAvailableCities from '../models/serviceAvailableCities.model';
import ZoneCity from '../models/zoneCity.model';
import { insidePolygon } from 'geolocation-utils';

/**
 *getAvailbleservice cities
 * @param {*} req 
 * @param {*} res 
 */
export const getAvailableServiceCity = (req, res) => {

    ServiceAvailableCities.aggregate([
        { "$match": { "softDelete": false, "status": true } },
        {
            "$project": {
                "_id": 1,
                "city": 1,
                "cityId": 1,
                "stateId": 1,
                "countryId": 1,
                "softDelete": 1,
                "status": 1,
                "nearby": 1,
                "label": "$city",
                "value": "$_id",
                "currency": 1,
            }
        }
    ]).exec((err, docs) => {
        if (err) { return res.status(500).json({ 'status': false, 'message': req.i18n.__('SEVER_ERROR'), 'error': err.toString() }) }
        //return res.status(200).json(docs);
        res.send(docs)
    });
};

export const getCityBoundaryPolygonForANewCity = async (req, res) => {
    try {
        if (req.query.city == "" || req.query.city == undefined) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("CITY_EMPTY")}) }
        //if(req.query.city != "Madurai"){ return res.status(409).json({'success':false,'message':req.i18n.__("DATA_NOT_FOUND")})}
        let response;
        response = await request("https://nominatim.openstreetmap.org/search.php?q=" + req.query.city + "&polygon_geojson=1&format=json", {
            method: 'GET',
            headers: {
                useragent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
                referer: 'https://github.com/xbgmsharp/node-nominatim2',
                polygon_geojson: 1,
                format: 'json'
            },
        });

        if (typeof response === "string") {
            response = JSON.parse(response);
        }

        if (response.statusCode === 200) {
            let body = response.body;
            body = JSON.parse(body);
            let cityBoundaryPolygon = [];
            if (body.length !== 0) {
                // console.log( body[0])
                cityBoundaryPolygon = body[0]["geojson"]["coordinates"];
            }
            return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETECHED_SUCCESS"), 'data': cityBoundaryPolygon })
        }
    }
    catch (error) {
        return res.status(409).json({ 'success': false, 'message': error.toString() });
    }
}

export const getCityBoundaryPolygon = (req, res) => {
    ServiceAvailableCities.find({ "$or": [{ "city": req.params.city }, { "cityCode": req.params.city }, { "cityId": req.params.city }] }, {}).exec((err, docs) => {
        if (err) { return res.status(500).json({ 'status': false, 'message': req.i18n.__('SEVER_ERROR'), 'error': err.toString() }) }
        console.log(docs.length)
        if (docs.length == 0) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("DATA_NOT_FOUND")}) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETECHED_SUCCESS"), 'data': docs });
    });
}

export const addServiceAvailableCity = (req, res) => {

    ServiceAvailableCities.find({ "city": req.body.city, softDelete: false }, {}, { "sort": { "createdAt": -1 } }, function (err, docs) {
        if (err) {
            return res.status(500).json({ 'success': false, 'message': req.i18n.__('SEVER_ERROR'), 'error': err.toString() });
        }
        if (docs.length > 0) {
            if (docs[0].softDelete == false && docs[0].status == true) return res.status(409).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS_ACTIVE')});
            else if (docs[0].softDelete == false && docs[0].status == false) return res.status(409).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS_INACTIVE') });
            //else if (docs.status == true && req.body.status == true) return res.status(401).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS') });
        }
        var LocArray = [];
        if (req.body.cityBoundaryPolygon.length != 0) LocArray = [req.body.cityBoundaryPolygon];
       
        var newDoc = new ServiceAvailableCities({
            city: req.body.city,
            cityId: req.body.cityId,
            cityBoundaryPolygon: req.body.cityBoundaryPolygon,
            stateId: req.body.stateId,
            countryId: req.body.countryId,
            currency: req.body.currency,

            geometry: {
                type: "Polygon",
                coordinates: LocArray
            }
        });
        newDoc.save((err, datas) => {
            if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
            return res.json({ 'success': true, 'message': req.i18n.__('DATA_ADDED_SUCCESS'), 'data': datas });
        });
    });
}

export const updateServiceAvailableCity = async (req, res) => {

    try {
        // console.log(req.body)
        let serviceAvailableDocs = await ServiceAvailableCities.findOne({ city: "Default" });


        if (serviceAvailableDocs._id == req.body._id && serviceAvailableDocs.city == req.body.city) {
            return res.status(409).json({ 'success': false, 'message': req.i18n.__("UPDADATE_DEFAULT_CITY")})
        }
        ServiceAvailableCities.find({ "city": req.body.city, "cityId": req.body.cityId, softDelete: false }, {}, { "sort": { "createdAt": -1 } }, function (err, docs) {
            if (err) {

                return res.status(500).json({ 'success': false, 'message': req.i18n.__('SEVER_ERROR'), 'error': err.toString() });
            }
            if (docs.length > 0 && docs[0]._id != req.body._id) {
                if (req.body.status == 'true' || req.body.status == true) { var bodyStatus = true }
                else if (req.body.status == 'false' || req.body.status == false) { var bodyStatus = false }

                if (docs[0].status == true && bodyStatus == true || docs[0].status == true && bodyStatus == false) return res.status(409).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS_ACTIVE')});
                else if (docs[0].softDelete == false && docs[0].status == false) return res.status(409).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS_INACTIVE') });
                else if (docs[0].softDelete == false && docs[0].status == true && bodyStatus == true) return res.status(401).json({ 'status': false, 'message': req.i18n.__('CITY_EXISTS') });
            }
            var LocArray = [];
            if (req.body.cityBoundaryPolygon.length != 0) LocArray = [req.body.cityBoundaryPolygon];
          
            let updateDoc = {
                "city": req.body.city,
                "cityId": req.body.cityId,
                "cityBoundaryPolygon": req.body.cityBoundaryPolygon,
                "status": req.body.status,
                "stateId": req.body.stateId,
                "countryId": req.body.countryId,
                "currency": req.body.currency,
                "geometry": {
                    "type": "Polygon",
                    "coordinates": LocArray
                }
            };
            let whereClause = {
                "_id": req.body._id
            };
            ServiceAvailableCities.updateOne(whereClause, updateDoc, (err, doc) => {
                if (err) { return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
                return res.json({ 'success': true, 'message': req.i18n.__('SERVICE_UPDATE_SUCCESS'), 'data': doc });
            })
        });
    }
    catch (err) {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR.."), 'error': err.toString() })
    }
}

export const deleteServiceAvailableCity = async (req, res) => {
    let newDoc = {
        "softDelete": true,
        "status": true,
    }
    let whereClause = {
        "_id": req.params._id
    };
    try {
        let serviceCitiesDocs = await ServiceAvailableCities.findOne({ city: "Default" });
        if (serviceCitiesDocs.cityId == req.params.cityId) {
            return res.status(409).json({ 'success': false, 'message': req.i18n.__("DELETE_DEFAULT_CITY") })
        }
        ServiceAvailableCities.updateOne(whereClause, newDoc, function (err, docs) {
            if (err) { return res.status(500).json({ 'status': false, 'message': req.i18n.__('SEVER_ERROR'), 'errer': err.toString() }) }
            return res.json({ 'success': true, 'message': req.i18n.__('DELETED_SUCCESS') })
        })
    }
    catch (err) {
        return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR.."), 'error': err.toString() })
    }
};

export const serviceAvailableCityNearby = async (req, res) => {
    var updateData = {
        city: req.body.city,
        cityId: req.body.cityId
    }
    ServiceAvailableCities.findByIdAndUpdate(req.params.id, {
        $push: { nearby: updateData }
    }, { 'new': true },
        function (err, doc) {
            if (err) {
                return res.status(500).json({ 'success': false, 'message': err.message, 'err': err });
            }
            return res.json({ 'success': true, 'message':('NEAR_CITY_ADDED'), 'data': doc });
        }
    );
}

export const delServiceAvailableCityNearby = async (req, res) => {
    ServiceAvailableCities.findOne({ _id: req.params.id }).exec((err, docs) => {
        if (err) return res.json({ 'success': false, 'message':('SOME_ERROR'), 'error': err });
        if (!docs) return res.json({ 'success': false, 'message':('SERVICE_NOT_AVAILABLE'), 'dvr': req.body.driver });
        docs.nearby.remove(req.params.nearbyId);
        docs.save(function (err, op) {
            if (err) return res.json({ 'success': false, 'message':( 'SOME_ERROR'), 'error': err });
            return res.json({ 'success': true, 'message':('DELETED_SUCCESS'), 'data': op });
        });
    })
}

export const updateCityBoundaryPolygon = (req, res) => {
    // console.log(req.body); console.log(req.params)
    ServiceAvailableCities.findOneAndUpdate({ "city": req.params.city }, { "cityBoundaryPolygon": req.body.cityBoundaryPolygon }, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message':("SOME_ERROR"), 'error': err }) }
        if (!doc) return res.status(409).json({ 'success': false, 'message':('SERVICE_NOT_AVAILABLE')});
        return res.status(200).json({ 'success': true, 'message':("SERVICE_BOUNDARY_UPDATE_SUCCESS")})
    })
}

export const getOuterPolygon = (req, res) => {
    var response = {};
    ServiceAvailableCities.findOne({ "$or": [{ "city": req.params.city }, { "cityCode": req.params.city }, { "cityId": req.params.city }] }, {}).exec((err, docs) => {
        if (err) { return res.status(500).json({ 'status': false, 'message':('SEVER_ERROR'), 'error': err.toString() }) }
        if (!docs) { return res.status(409).json({ 'success': false, 'message':("DATA_NOT_FOUND") }) }
        response['outerPolygon'] = docs.outerPolygon
        if (docs.outerPolygon.length == 0) { response['outerPolygon'] = docs.cityBoundaryPolygon }
        return res.status(200).json({ 'success': true, 'message':("FETECHED_SUCCESS"), 'data': [response] });
    });
}


export const updateOuterPolygon = (req, res) => {
    var LocArray = [];
    if (req.body.outerPolygon.length != 0) LocArray = [req.body.outerPolygon];
    ServiceAvailableCities.findOneAndUpdate({ "city": req.params.city }, {
        "outerPolygon": req.body.outerPolygon, "geometry.coordinates": LocArray
    }, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message':("SOME_ERROR"), 'error': err }) }
        if (!doc) return res.status(409).json({ 'success': false, 'message':('SERVICE_NOT_AVAILABLE') });
        return res.status(200).json({ 'success': true, 'message':("SERVICE_BOUNDARY_UPDATE_SUCCESS") })
    })
}

export const getServiceCity = (req,res) => {
    ServiceAvailableCities.find({},{ outerPolygon:0, cityBoundaryPolygon:0 }, function(err,docs){
        if(err){ return res.status(500).json({'success':false,'message':("SOME_ERROR"),'error':err})}
        if(!docs){ return res.status(409).json({'success':false,'message':("DATA_NOT_FOUND")})}
        return res.status(200).json({'success':true,'message':("FETECHED_SUCCESS"),'data':docs});
    })
}

export const addZoneCity = (req, res) => {
    var LocArray = [];
    LocArray = [req.body.latlngArray];
    var newDoc = new ZoneCity({
        scId: req.body.servicecityId,
        name: req.body.name,
        surgeType: req.body.surgeType,
        kmSurge: req.body.kmSurge,
        timeSurge: req.body.timeSurge,
        softDelete: false,
        geometry: {
            type: "Polygon",
            coordinates: LocArray
        }
    });
    newDoc.save((err, datas) => {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
        return res.json({ 'success': true, 'message': req.i18n.__('DATA_ADDED_SUCCESS'), 'data': datas });
    });
}

export const updateZoneCity = (req, res) => {
    var LocArray = [];
    LocArray = [req.body.latlngArray];
    var updateData = {
        geometry: {
            type: "Polygon",
            coordinates: LocArray
        }
    };
    ZoneCity.findOneAndUpdate({ _id: req.body.zoneId }, updateData , function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': ("SOME_ERROR"), 'error': err }) }
        if (!doc) return res.status(409).json({ 'success': false, 'message': ('SERVICE_NOT_AVAILABLE') });
        return res.status(200).json({ 'success': true, 'message': ("SERVICE_BOUNDARY_UPDATE_SUCCESS") })
    })
}

export const updateZoneCityBasic = (req, res) => {
    var updateData = {
        name: req.body.name,
        surgeType: req.body.surgeType,
        kmSurge: req.body.kmSurge,
        timeSurge: req.body.timeSurge,
    };
    ZoneCity.findOneAndUpdate({ _id: req.body.zoneId }, updateData, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': ("SOME_ERROR"), 'error': err }) }
        if (!doc) return res.status(409).json({ 'success': false, 'message': ('SERVICE_NOT_AVAILABLE') });
        return res.status(200).json({ 'success': true, 'message': ("SERVICE_BOUNDARY_UPDATE_SUCCESS") })
    })
}

export const deleteZoneCityBasic = (req, res) => {
    ZoneCity.findByIdAndRemove(req.params.serviceId, (err, docs) => {
        if (err) {
            return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") });
        }
        return res.json({ 'success': true, 'message': req.i18n.__("DELETED_SUCCESSFULLY"), docs });
    })
}

export const getZoneCity = async (req, res) => {
    ZoneCity.find({ scId: req.params.serviceId },{}, function (err, docs) {
        if (err) { return res.status(500).json({ 'success': false, 'message': ("SOME_ERROR"), 'error': err }) }
        if (!docs) { return res.status(409).json({ 'success': false, 'message': ("DATA_NOT_FOUND") }) }
        return res.status(200).json({ 'success': true, 'message': ("FETECHED_SUCCESS"), 'data': docs });
    })
}

export const getServiceCityWithZones = async (req, res) => {
    var paramsData = {
        lat: req.body.lat,
        lng: req.body.lng,
	};
    var serviceId = await getServiceCityIdFromLatLog(paramsData);
    if(serviceId != null){
        var serviceId = serviceId[0];
        var ServiceAvailableCitiesData = await ServiceAvailableCities.findById(serviceId, { cityBoundaryPolygon: 1 }).exec();
        var ZoneCityData = await ZoneCity.find({ scId: ServiceAvailableCitiesData._id }, { geometry: 1, name:1}).exec();
        return res.status(409).json({
            'success': true, 'message': req.i18n.__("FETECHED_SUCCESS"), 'ServiceAvailableCitiesData': ServiceAvailableCitiesData,
            'ZoneCityData': ZoneCityData });
    }else{
        return res.status(409).json({ 'success': true, 'message': req.i18n.__("DATA_NOT_FOUND") });
    }
}

export const getServiceCityIdFromLatLog = async (params) => {
    try {
        let where = {}, serviceCityId = [];
        let availableService = await ServiceAvailableCities.find({ "softDelete": false }, { "cityBoundaryPolygon": 1, "city": 1 }).lean();
        let dropPoint = false;
        for (var value of availableService) {
            if (value.city == "Default") {
                // serviceCityId.push(value._id)
            } else {
                dropPoint = insidePolygon([parseFloat(params.lng), parseFloat(params.lat)], value.cityBoundaryPolygon)
                if (dropPoint) {
                    serviceCityId.push(value._id)
                    break;
                }
            }
        }
        console.log('dropPoint', dropPoint)
        if (dropPoint == true) { return serviceCityId; }
        else { return null; }
    }
    catch (error) {
        console.log(error)
        return null;
    }
}
