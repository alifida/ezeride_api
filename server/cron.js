import { sendSchAndTaxiReqStatus, sendScheduleTaxiRequestToDriver, clearNoEndedtrips } from './controllers/app';
import { cronInactiveDrivers, resetDriversPerDayEarnings, resetDriversSubscription, notifySubscriptionEndDate } from './controllers/driver';
const featuresSettings = require('./featuresSettings');
const cron = require('node-cron');
const CronJob = require('cron').CronJob;

console.log('CRON - running every  1 minute');
cron.schedule('*/1 * * * *', function () {
  // console.log('running every  1 minute');
  sendSchAndTaxiReqStatus(); //Used to send notification to Driver about arival time
  sendScheduleTaxiRequestToDriver(); //Used to send request if no Driver accepted.
  cronInactiveDrivers(); //Inactivate Drivers lessthan this update time
});

// if (featuresSettings.calculateDriverEarningAtEveryDay) {
//   new CronJob('0 0 0 * * *', function () {
//     //will run every day at 12:00 AM
//     resetDriversPerDayEarnings();
//   })
// }

cron.schedule('*/59 * * * *', function () {
  if (featuresSettings.payPackageTypes.includes("subscription")) {
    resetDriversSubscription();
  }
});

var cronJob1 = new CronJob({
  cronTime: '0 0 0 * * *',
  onTick: function () {
    //Your code that is to be executed on every midnight
    if (featuresSettings.calculateDriverEarningAtEveryDay) {
      resetDriversPerDayEarnings();
    }
    //check the drivers subscription 
    if (featuresSettings.payPackageTypes.includes("subscription")) {
      resetDriversSubscription();
      notifySubscriptionEndDate();
    }

    // clearNoEndedtrips();
  },
  start: true,
  runOnInit: false
})