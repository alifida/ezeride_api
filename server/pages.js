import mongoose from 'mongoose';
import path from 'path';
import Menus from './../models/pagesmenu.model';
import * as HelperFunc from './adminfunctions';
import serviceAvailableCities from '../models/serviceAvailableCities.model';

var fs = require('fs')

export const sendAboutus = (req, res) => {
    fs.readFile(__dirname + '/html/about.html', 'utf8', function read(err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), doc })
    })
}

export const updateAboutus = (req, res) => {
    fs.writeFile(__dirname + '/html/about.html', req.body.data, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("UPDATED_SUCCESSFULLY") })
    })
}

export const sendPrivacypolicy = (req, res) => {
    fs.readFile(__dirname + '/html/privacypolicy.html', 'utf8', function read(err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), doc })
    })
}

export const updatePrivacypolicy = (req, res) => {
    fs.writeFile(__dirname + '/html/privacypolicy.html', req.body.data, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("UPDATED_SUCCESSFULLY") })
    })
}

export const sendTnc = (req, res) => {
    fs.readFile(__dirname + '/html/tnc.html', 'utf8', function read(err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), doc })
    })
}

export const updateTnc = (req, res) => {
    fs.writeFile(__dirname + '/html/tnc.html', req.body.data, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("UPDATED_SUCCESSFULLY") })
    })
}

export const sendRiderTnc = (req, res) => {
    fs.readFile(__dirname + '/html/ridertnc.html', 'utf8', function read(err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("FETCHED_SUCCESSFULY"), doc })
    })
}

export const updateRiderTnc = (req, res) => {
    fs.writeFile(__dirname + '/html/ridertnc.html', req.body.data, function (err, doc) {
        if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
        return res.status(200).json({ 'success': true, 'message': req.i18n.__("UPDATED_SUCCESSFULLY") })
    })
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
export const addMenuSettings = (req, res) => {
    if (req.body.group && (req.body.menuslist.length != 0)) {

        Menus.findOne({ type: req.body.group }, function (err, user) {

            if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });
            if (user) return res.status(200).json({ 'success': false, 'message': req.i18n.__("ADMIN_TYPE_PRESENT") });
            else {
                var newDoc = new Menus({
                    type: req.body.group,
                    menus: req.body.menuslist,

                });
                newDoc.save((err, data) => {
                    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") }) }
                    return res.status(200).json({ 'success': true, 'message': req.i18n.__("ADDED_SUCCESSFULLY"), 'datas': data })
                })
            }


        });
    }
    else {
        if (!req.body.group) return res.status(200).json({ 'success': false, 'message': req.i18n.__("BAD_REQUEST_TYPE") });
        if (req.body.menuslist.length == 0) return res.status(200).json({ 'success': false, 'message': req.i18n.__("BAD_REQUEST_MENUS") });
    }
}
/**
* 
* @param {*} req 
* @param {*} res 
*/
export const getMenuSettings = (req, res) => {

    Menus.findOne({ type: req.body.group }, function (err, user) {

        if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });

        if (user) return res.status(200).json({ 'success': false, 'message': req.i18n.__("ADMIN_TYPE_PRESENT"), "data": user });
        if (!user) return res.status(200).json({ 'success': false, 'message': req.i18n.__("NOT_PRESENT") });

    })
}
export const getMenuList = async (req, res) => {
    var likeQuery = HelperFunc.likeQueryBuilder(req.query);
    var pageQuery = HelperFunc.paginationBuilder(req.query);
    var sortQuery = HelperFunc.sortQueryBuilder(req.query);
    let TotCnt = Menus.find(likeQuery).count();
    let Datas = Menus.find(likeQuery).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery);
    try {
        var promises = await Promise.all([TotCnt, Datas]);
        res.header('x-total-count', promises[0]);
        var resstr = promises[1];
        res.send(resstr);
    } catch (err) {
        return res.json([]);
    }
}
/**
* 
* @param {*} req 
* @param {*} res 
*/
export const editMeunsSettings = (req, res) => {

    Menus.findOneAndUpdate({ _id: req.body.id }, { menus: req.body.menuslist }, function (err, user) {

        if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });
        if (user) return res.status(200).json({ 'success': true, 'message': req.i18n.__("UPDATED_SUCCESSFULLY"), "data": user });

    })
}

export const getServiceCities = async (req, res) => {
    serviceAvailableCities.find({ city: { $ne: "Default" } }, { city: 1, nearby: 1 }, function (err, data) {
        if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });
        if (data) return res.status(200).json({ 'success': true, "data": data });
    })
}
