// ./express-server/app.js

import express from 'express';
import path from 'path';
import logger from 'morgan';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';

// import bb from 'express-busboy'; 
var bodyParser = require('body-parser');
var multer = require('multer');
var validator = require('express-validator');
const trimmer = require('express-trimmer');
const mustacheExpress = require('mustache-express');
var winston = require('winston'),
  expressWinston = require('express-winston');
const i18n2 = require('i18n-2');
var debugMode = true;
const app = express();

// import routes
import uberRoutes from './routes/uber.server.route';
import uberAppRoutes from './routes/uberapp.server.route';
import uberTestRoutes from './routes/ubertest.server.route';
import uberHotelRoutes from './routes/uberhotel.server.route';
import uberComapnyRoutes from './routes/ubercompany.server.route';
import uberTwilioRoutes from './helpers/twilio/index';

import deliveryRoutes from './modules/delivery/delivery.route';
import incentiveRoutes from './modules/incentive/incentive.route';
import cancelationRoutes from './modules/cancelation/cancelation.route';
import rentalRoutes from './modules/rental/rental.route';

import cron from './cron';


const config = require('./config');
// define our app using express

app.engine('html', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');
// allow-cors
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  res.header('access-control-expose-headers', 'x-total-count');

  // allow preflight  
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use(bodyParser.json({ limit: '50mb' })); // for parsing application/json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); // for parsing application/x-www-form-urlencoded
app.use(trimmer);
app.use(validator());

if (debugMode) {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  // express-winston logger makes sense BEFORE the router
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: true,
        colorize: true
      })
    ]
  }));
}

// configure app
app.use(logger('dev'));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/webadmin', express.static(path.join(__dirname, '../webadmin')));
app.use('/landing', express.static(path.join(__dirname, '../landing')));
app.use('/locales', express.static(path.join(__dirname, 'locales')));

// set the port
const port = process.env.PORT || config.port;

// connect to database
mongoose.Promise = global.Promise;

app.use((req, res, next) => {
  var url = req.protocol + '://' + req.headers.host + req.originalUrl;
  // connectWithRetry();
  next();
});

// app.use((req, res, next) => { //Or check in VT only if admin
//   if(req.method == "DELETE"){
//     return res.status(500).json({ 'success': false, 'message': 'Deletion not allowed on demo' });
//   }else{
//   next();
//   }
// });





//Connect to Db 
const options = {
  

  useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: 30, // Retry up to 30 times
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  // server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
};

const connectWithRetry = () => {
  // console.log('MongoDB connection with retry')
  mongoose.connect("mongodb://localhost/" + config.database, options).then(() => {
  }).catch(err => {
    console.log('MongoDB connection unsuccessful, retry after 5 seconds.', err)
    setTimeout(connectWithRetry, 3000)
  })
};

connectWithRetry();

// add Source Map Support
SourceMapSupport.install();

app.post('/profile', (req, res, next) => {
  // console.dir(req.headers['content-type']);
  console.log(req.body);
  return res.end('Api working');
});

i18n2.expressBind(app, {
  locales: ['en', 'es'],
  cookieName: 'locale',
  extension: ".json"
});

app.use(function (req, res, next) {
  if (req.headers['accept-language'] && req.headers['accept-language'].length < 3) {
    let language = (req.headers['accept-language']) ? req.headers['accept-language'] : config.appDefaultLanguageCode;
    req.i18n.setLocale(language);
  } else {
    req.i18n.setLocale(config.appDefaultLanguageCode);
  }

  // req.i18n.setLocale('en');

  next();
})

app.use('/adminapi/cancelation', cancelationRoutes);
app.use('/adminapi/incentive', incentiveRoutes);
app.use('/adminapi', uberRoutes);
app.use('/api/twilio', uberTwilioRoutes);
app.use('/api/delivery', deliveryRoutes);
app.use('/api/rental', rentalRoutes);
app.use('/api', uberAppRoutes);
app.use('/test', uberTestRoutes);
app.use('/hotel', uberHotelRoutes);
app.use('/company', uberComapnyRoutes);


app.get('/', (req, res) => {
  return res.end('Api working');
})


// catch 404
app.use((req, res, next) => {
  res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});

//  catch errors
app.use((err, req, res, next) => {
  const error = {
    message: err.message || 'Internal Server Error.',
  };
  if (err.errors) {
    error.errors = {};
    const { errors } = err;
    if (Array.isArray(errors)) {
      error.errors = errors.reduce((obj, error) => {
        const nObj = obj;
        nObj[error.field] = error.messages[0].replace(/"/g, '');
        return nObj;
      }, {})
    } else {
      Object.keys(errors).forEach(key => {
        error.errors[key] = errors[key].message;
      });
    }
  }
  if (!err.status) err.status = 500;
  if (err.status == 400 && err.message == 'validation error') {
    var errorString = '';
    err.errors.forEach(key => {
      if (errorString == '') {
        errorString = (key.messages[0].replace(/"/g, ''))//.replace(/"/g, '');
      }

      else
        errorString += ',' + (key.messages[0]).replace(/"/g, '');
    })
    console.log(errorString)
    err.message = errorString;
  }

  res.status(err.status).json({
    success: false,
    message: (err.message).toString(),
    error: error.errors
  });
});


//-----------------------------------

const https = require('https');
const fs = require('fs');
var key = fs.readFileSync('ezeride.com.au.key');
var cert = fs.readFileSync('34ccd5a1ff85ce65.crt');
var bundle_cert = fs.readFileSync('gd_bundle-g2-g1.crt');
 
 const httpsoptions = {
  key: key,
  cert: cert,
  ca: bundle_cert
 }
 
app.get('/', (req, res) => {
   res.send('Now using https..');
});
var server = https.createServer(httpsoptions , app);

server.listen(port, () => {
  console.log(`App Server Listening at ------------------------ ${port}`);
});

  

// start the server
/*app.listen(port, () => {
  console.log(`App Server Listening at ${port}`);
});*/


 

