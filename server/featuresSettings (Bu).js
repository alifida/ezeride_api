// featuresSettings.js
// App features Common Settings
const featuresSettings =  {
  'isCityWise': true, //Vehicle and Fare etc act citywise  0/1
  'isServiceAvailable': true, //if not modify in getServiceBasicfare
  'isMultipleCompaniesAvailable': true, //if Admin manages multiple companies
  'isCommisionToAdmin': false, //is at end of trip Commision need to take
  'isDoubleChargeNeeded': false, //is isDoubleChargeNeeded for crossing City limit
  'fareCalculationType': 'normal', //{indiaGst,normal}
  'gst' :{
    'gstFareBreakPercentage': 80, //For indiaGst
    'gstOnFareBreakPercentage1': 5, //For First Fare Total
    'gstOnFareBreakPercentage2': 18, //For Second Fare Total
  },
  'applyTravelFare': true, //total time fare
  'applyValues' :{
    'applyNightCharge': true,
    'applyPeakCharge': true,
    'applyWaitingTime': true, //waiting time charge
    'applyTax': true,
    'applyCommission': true, //Admin Commision
    'applyPickupCharge': true,  
  }, 
  'isETANeeded': false,
  'isRiderCancellationAmtApplicable': true,
  'isDriverCancellationAmtApplicable': false, 
  'calculateDriverEarningAtEveryDay': false, //Used to show Driver total Payment in app / day
  'applyBlockOldCancellationAmt': true, 
  'isDriverCreditModuleEnabled': false, //Helps to show alert if credits are low, reduce commision from credits, etc
  'addBookingFeeToCommision': true, //addBookingFeeTo Admin Commision
  'driverPayouts' : {
    'adminCommision': 'driverWallet',  //From Driver Wallet (driverWallet,manualPay)
    'payoutType': 'driverPrepaidWallet',//Driver Have to Recharge to Take Trip. (driverPrepaidWallet,driverPostpaid)
    'deductAmountFromDriverWallet': 'commision', //this amount will be decuted from Wallet (totalFare,commision,none) 
    'driverCreditAmountAlertLimit': 100, //Driver will recive alert to pay to admin after reaching this limit.
    'driverCreditAmountOfflineLimit': 0, //Driver Have to pay to admin, after reching this amount in his wallet for Prepaid / Post paid 
    'driverPayoutAmountLimitMax': 100, //Driver can request Pay0ut after this amount
    'driverStripeConnect': true, //Only for Direct payout in Stripe
    'driverStripeConnectCountry': 'US', //this country bank account
    'driverStripeSplitPayout': true, //Direct payout
  },
  'riderWallet': true, //If true applicable only for Digital Payment
  'riderRechargeWalletInClientSide': true, //If true applicable only for Digital Payment where recharge done in App client side
  'riderCard' : true, //Rider Can Add Card, (supported in some payment gateways)
  'riderTripPaidInClientSide': false, //If true applicable only for Digital Payment where recharge done in App client side
  //Driver app Defaults
  'defaultPhoneCode': '+91',
  'defaultlang': 'EN',
  'defaultcur': 'USD',
  'defaultCountryId': '101',//India
  'defaultStateId': '35',//TN
  //Currency Settings
  'primarycur': 'USD',
  'secondarycur': false, //Currency or false
  'conversionRate': 1,
  'roundOff': 2,
  //Offers
  'isRiderReferalCodeAvailable': false, 
  'referalSettings' : {
    'isRiderReferalCodeAvailable': false, 
    'riderReferalAmount': 10, 
    'isDriverReferalCodeAvailable': false, 
    'driverReferalAmount': 10, 
  },
  'isPromoCodeAvailable': true, 
  'isOffersForRideAvailable': false, 
  'passwordVerificationMethod': 'email', //admin
  'passwordVerificationMethodForUser': 'email', //email or sms
  'registerOTPVerificationMethod': 'email', //email or sms or both
  'tripsAvailable': ['Daily', 'Package', 'Outstation'],
  'hailTaxi': true, //is hail taxi available
  'socialLogin': true, //is socialLogin available
  'callMasking': true, //is callMasking available
  'payPackageTypes': ['commision', 'subscription'],
};

module.exports = featuresSettings;