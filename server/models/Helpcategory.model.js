import mongoose from 'mongoose';

var HelpcategorySch = mongoose.Schema({  
  	eStatus         : String,
  	iDisplayOrder   : String,
  	vTitle_EN       : String,
});

var Helpcategory = mongoose.model('helpcategorys', HelpcategorySch);
module.exports = Helpcategory;