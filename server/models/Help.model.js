import mongoose from 'mongoose';

var HelpSch = mongoose.Schema({  
  	ihelpcategoryId         : String,
    ihelpcategorytitle		: String, 
  	iDisplayOrder       	: String,
  	vTitle_EN               : String,
  	English           		: String
});

var Help = mongoose.model('helps', HelpSch);
module.exports = Help;