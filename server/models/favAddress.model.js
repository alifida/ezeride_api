import mongoose from 'mongoose';

var FavAddressSchema = mongoose.Schema({
    name: { type: String, default: "" },
    coords: { type: [Number], index: '2dsphere', default: [0, 0] },
    createdAt: { type: Date, default: Date.now },
});

var FavAddress = mongoose.model('favouriteaddress', FavAddressSchema);
module.exports = FavAddress;
