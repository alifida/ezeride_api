import mongoose from 'mongoose';

var PushSchema=mongoose.Schema({

    createdAt : { type: Date, default: Date.now },
    forWhom   : { type: String , default: '' }, 
    message   : { type: String , default: '' },
    forType   : { type: Number, default: null} , // 1 -> push notification and 2 -> sms notification

});

var pushNotification=mongoose.model('pushnotificbackup',PushSchema);
module.exports=pushNotification;