import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

var TripLocationSchema = mongoose.Schema({
    createdAt: { type: Date, default: Date.now },
    tripId: { type: ObjectId, ref: 'trips', default: null },
    lastUpdated: { type: Date, default: null },
    // loc: { type: [[Number]], default: [] }, //array of arrays of Numbers
    loc: [String], //array of Strings
});

var TripLocation = mongoose.model('triplocation', TripLocationSchema);
module.exports = TripLocation; //trip route