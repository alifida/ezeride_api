import mongoose from 'mongoose';
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
import * as GFunctions from '../controllers/functions';

var DriverTaxiSchema = new mongoose.Schema({
  ownername: { type: String, default: "" },  // only one for a individual vehicle (ALPHANUMERIC), no image needed
  makeid: String,
  makename: String, 
  model: String,
  year: String,
  licence: String, 
  cpy: String, 
  driver: String,
  color: String,
  handicap: { type: String, default: "false" },
  type: [
   /*  {
    basic: { type: String, default: "false" },
    normal: { type: String, default: "false" },
    luxury: { type: String, default: "false" }
    } */
  ],
  insurance: { type: String , default: "" }, 
  insuranceexpdate: { type: Date , default: null },  
  insurancenumber: { type: String, default: "" },
  permit: { type: String , default: "" } , 
  permitexpdate :  { type: Date , default: null } , 
  registration: { type: String, default: "" }, // Image, RC No.
  registrationBack : { type: String, default: "" },
  registrationexpdate: { type: Date, default: null }, // Image, RC No.
  registrationnumber: { type: String, default: "" }, // Image, RC No.
  chaisis: { type: String, default: "" }, // (chassis number is the last six digits of your car’s Vehicle Identification Numbers VIN)
  vin_number: { type: String, default: "" }, // only one for a individual vehicle (ALPHANUMERIC), no image needed
  others1: { type: String , default: "" } ,
  vehicletype :  { type: String , default: "vehicletype" } , 
  share: { type: Boolean, default: false }, //is share available
  noofshare: { type: Number, default: 0 }, 
  taxistatus: { type: String, default: "inactive" } ,
  image: { type: String, default: "" } ,
  imageBack: { type: String, default: "" } ,
  // features: [{
  //   lable: { type: String, default: "" },
  //   status: { type: Boolean, default: false}
  // }], 
  // features: { type: Object },

  isDaily: { type: Boolean, default: true },
  isRental: { type: Boolean, default: false },
  isOutstation: { type: Boolean, default: false },

}
  ,
  {
    toObject: {
      transform: function (doc, ret) {
        ret.insuranceexpdate = GFunctions.getDateTimeForUserLable(ret.insuranceexpdate);
        ret.permitexpdate = GFunctions.getDateTimeForUserLable(ret.permitexpdate);
        ret.registrationexpdate = GFunctions.getDateTimeForUserLable(ret.registrationexpdate);
      }
    },
    toJSON: {
      transform: function (doc, ret) {
        ret.insuranceexpdate = GFunctions.getDateTimeForUserLable(ret.insuranceexpdate);
        ret.permitexpdate = GFunctions.getDateTimeForUserLable(ret.permitexpdate);
        ret.registrationexpdate = GFunctions.getDateTimeForUserLable(ret.registrationexpdate);
      }
    }
  }

  );


var DriverSchema = mongoose.Schema({
  createdAt:{ type: Date,  default: Date.now  },
  code: { type: String, unique: true },
  nic: { type: String, default: "" },
  fname: String,
  lname: String, 
  email: { type: String, trim: true }, 
  phcode: { type: String, default: config.phoneCode }, 
  phone: { type: String, unique: true, required: true }, 
  gender: String, 
  hash: String,
  salt: String,
  DOB: { type: Date, default : null }, 
  cnty: String,
  cntyname: String,
  state: String,
  statename: String,
  city: String,
  cityname: String,
  address: String,
  cmpy: { type: ObjectId, ref: 'companydetails', default : null },
  isIndividual: { type: Boolean, default: true },
  // isDaily: { type: Boolean, default: true },
  // isRental: { type: Boolean, default: false },
  // isOutstation: { type: Boolean, default: false },
  isHail: { type: Boolean, default: false },
  lang: String,
  cur: { type: String, default: config.paymentGateway.paymentGatewayCurrency },  
  actMail: String,
  actHolder: String,
  actNo: String,
  actBank: String,
  actLoc: String,
  actCode: String,
  profile:  {type: String,  default : "public/file-default.png"   }, 
  baseurl: { type: String, default: config.baseurl }, 
  status: [{
    curstatus:     { type: String, default : "active" }, 
    models: { type: String, default : "no" } , 
    docs :  { type: String, default : "pending" },
    canoperate :  { type: String, default : "no" },
  }],
  // status: {
  //   curstatus  : { type: String, default : "active" }, 
  //   models     : { type: String, default : "no" } , 
  //   docs       : { type: String, default : "pending" },//Accepted or pending
  //   canoperate : { type: String, default : "no" },
  // },
  taxis : [DriverTaxiSchema],
  
  nationIdback : { type: String, default: "" },
  licenceBackImg : { type: String, default: ''},
  licence: { type: String, default: "" }, 
  licenceNo:   { type: String , default: ""  }, 
  licenceexp:{ type: Date , default: null }, 
  insurance: { type: String , default: "" }, 
  insuranceexp:{ type: Date , default: null },  
  passing :  { type: String , default: ""  },
  passingexp:{ type: Date , default: null },    
  revenue: { type: String, default: "" },
  revenueexp: { type: Date, default: null },    
  currentTaxi :  { type: String , default : ""  },
  serviceStatus :  { type: String , default : "inactive"  },
  curService :  { type: String , default : ""  },
  curStatus: { type: String, default: "free" },// OBO => If Request Recived (requested), 
  curVehicleNo: { type: String, default: "" }, 
  others1: { type: String, default: "" }, 
  vin_number: { type: String, default: "" }, //vehicleidentificationno
  share: { type: Boolean, default: false }, //is share available 
  noofshare: { type: Number, default: 0 }, 
  sharebooked: { type: Number, default: 0 }, //occupied share 
  online: { type: Boolean, default: 0 }, 
  coords: { type: [Number], index: '2dsphere', default: [0, 0] },
  driverLocation: {
    type: { type: String, required: true, enum: 'Point', default: 'Point' },
    coordinates: { type: [Number], required: true, default: [0, 0]  }
  },
  rating: {  
    rating: { type: String, default: "0" },
    nos: { type: Number, default: 0 },  //Rated trips
    tottrip: { type: Number, default: 0 }, //Total trips
    star: { type: Number, default: 0 }, //Five Star trips
    cmts: { type: String, default: "" }, 
  },  

  wallet: { type: Number, default: 0 },
  canceledCount: { type: Number, default: 0 },//No of times cancelled Trip for Current Date
  lastCanceledDate: { type: String, default: '' },//Last Time Canceled Date = DD-MM-YYYY
  
  todayAmt : {
    lastdate: { type: String, default: null },
    trips: { type: Number, default: 0 },
    amt: { type: Number, default: 0 },
  },

  curTrip: { type: String, default: "" }, 
  isConnected: { type: Boolean , default: false },//Stripe connect account added TODO direct pay enabled
  fcmId: { type: String, default: "" } , 
  last_in: { type: Date, default: null },
  last_out: { type: Date, default: null },
  lastUpdate: { type: Date, default: null }, //last updated time of Location
  lastCron: { type: Date, default: null }, //offline by admin, at this time
  scId: { type: ObjectId, ref: 'serviceavailablecities', default: null },
  scity: { type: String, default: null },
  softdel :  { type: String , default : "active"  } ,
  verificationCode : { type: String , default : ""},
  loginType: { type: String, default: "normal" },
  loginId: { type: String, default: "" },
  callmask: { type: String, default: "" },
  blockuptoDate: { type: String, default: '' }, //Blocked upto Canceled Date = DD-MM-YYYY
  subcriptionEndDate: { type: Date, default: null }, 
  isSubcriptionActive: { type: Boolean, default: false },
  subscriptionPackId : { type: ObjectId, ref: 'payPackage', default: null },
  subscriptionPackName : { type : String , default : ""},  
  subscriptionPackPurchaseId : { type: ObjectId, ref: 'driverPackage', default: null },
  referenceCode : { type: String, default: '' },  
  referal: { type: String, default: "" }, // Driver own refferal code
  referredCode : { type: String, default: ""}, //If joined with someone referal mean, that referred person code
  tripCount : { type: Number, default: 0}, //Maintain tripcount for refferal (when reach zero amount credit)
  referrealRecharge : { type : Boolean, default: true}, // recharge status if settled(true) or not (false)
  referalInviteApproval : { type : Boolean, default : false },
  isDriverAllowedOtherStates: { type: Boolean, default: false },
  providerId: { type: ObjectId, default: null }
}
  ,
  {
    toObject: {
      transform: function (doc, ret) {
        ret.licenceexp = GFunctions.getDateTimeForUserLable(ret.licenceexp);
        ret.insuranceexp = GFunctions.getDateTimeForUserLable(ret.insuranceexp);
        ret.passingexp = GFunctions.getDateTimeForUserLable(ret.passingexp);
      }
    },
    toJSON: {
      transform: function (doc, ret) {
        ret.licenceexp = GFunctions.getDateTimeForUserLable(ret.licenceexp);
        ret.insuranceexp = GFunctions.getDateTimeForUserLable(ret.insuranceexp);
        ret.passingexp = GFunctions.getDateTimeForUserLable(ret.passingexp);
      }
    }
  }

//V2 DEMO
  /* ,
  {
    toObject: {
      transform: function (doc, ret) {
      	ret.licenceexp = GFunctions.getDateTimeForUserLable(ret.licenceexp);
        ret.insuranceexp = GFunctions.getDateTimeForUserLable(ret.insuranceexp);
        ret.passingexp = GFunctions.getDateTimeForUserLable(ret.passingexp);
        ret.phone = GFunctions.hidePhoneDataForDemo(ret.phone);
        ret.email = GFunctions.hideEmailDataForDemo(ret.email);
      }
    },
    toJSON: {
      transform: function (doc, ret) {
      	ret.licenceexp = GFunctions.getDateTimeForUserLable(ret.licenceexp);
        ret.insuranceexp = GFunctions.getDateTimeForUserLable(ret.insuranceexp);
        ret.passingexp = GFunctions.getDateTimeForUserLable(ret.passingexp);
        ret.email = GFunctions.hideEmailDataForDemo(ret.email);
        ret.phone = GFunctions.hidePhoneDataForDemo(ret.phone);
      }
    }
  }
  } */
  //V2 DEMO
);

 //V2 DEMO 
/* DriverSchema.post('aggregate', outputFormat);
function outputFormat(doc) {
	doc.map((el => {
		el.phone = GFunctions.hidePhoneDataForDemo(el.phone);
		el.email = GFunctions.hideEmailDataForDemo(el.email);
	}))
} */
//V2 DEMO 

// DriverSchema.index({ location: "2dsphere" });

DriverSchema.methods.setPassword = function(password="abservetech"){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000,64,'sha512').toString('hex');
}; 

DriverSchema.methods.validPassword = function(password="abservetech",salt,hashval) {
  var hash = crypto.pbkdf2Sync(password, salt , 1000, 64,'sha512').toString('hex');
  return hashval === hash;
};

DriverSchema.methods.generateJwt = function(_id,email,name,type="driver") {
 var expiry = new Date();
 expiry.setDate(expiry.getDate() + 7); // 7 days
 return jwt.sign({ 
   id: _id, 
   email: email, 
   name: name,
   type: type, 
 }, config.secret ); 
};

DriverSchema.methods.getPassword = function(password="abservetech"){
  const obj = {};
  obj.salt = crypto.randomBytes(16).toString('hex');
  obj.hash = crypto.pbkdf2Sync(password, obj.salt, 1000,64,'sha512').toString('hex'); 
  return obj;
}; 
 
DriverSchema.index({ driverLocation: '2dsphere' });
// db.drivers.createIndex({driverLocation:"2dsphere"});


var Driver = mongoose.model('drivers', DriverSchema);
module.exports = Driver;  


