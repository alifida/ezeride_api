// ./express-server/controllers/todo.server.controller.js
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

//import models
import Rider from '../models/rider.model';
import Wallet from '../models/wallet.model';
import * as GFunctions from './functions';
import * as HelperFunc from './adminfunctions';
// import { sendSmsMsg } from './smsmate';  
import * as smsGateway from './smsGateway';
import { sendEmail } from './mailGateway';
// import { sendSmsMsg } from './smsGateway';  
import RiderPerDay from '../models/riderperDay.model';
import { isBuffer } from 'util';
import CancelReasons from '../models/cancellationReason.model';
import Trips from '../models/trips.model';

const featuresSettings = require('../featuresSettings');
const notificationContent = require('../notificationContent');
var config = require('../config');
var firebase = require('firebase');
var async = require('async');
var crypto = require('crypto');
const Mustache = require('mustache');
const fs = require('fs');
const moment = require('moment');

/**
 * Reset Password  RiderResetPasswordFromAdmin
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const riderResetPasswordFromAdmin = (req, res) => {
  Rider.findById(req.body.riderid, function (err, docs) {
    if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
    else if (!docs) {
      return res.status(409).json({ 'success': false, 'message': req.i18n.__('RIDER_NOT_FOUND') });
    }
    else {
      var password = config.resetPasswordTo;
      if (req.params.type == 'change') {
        password = req.body.password;
      }
      docs.setPassword(password);
      docs.save(function (err, op) {
        if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
        else {
          smsGateway.sendSmsMsg(docs.phone, 'Your Password Has Been Reseted to ' + password + '. Thank You.' + password, docs.phcode);
          return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_RESETED' + password) })
        }
      })
    }
  });
}

export const checkPhoneAvail = (req, res) => {
  Rider.find({ phone: req.body.phone }, { '_id': 1, 'phone': 1, 'fname': 1, 'lname': 1, 'email': 1 }, { "pwd": 0 }).limit(10).exec((err, docs) => {
    if (err) {
      return res.status(401).json({ 'success': false, 'message': req.i18n.__('USER_NOT_FOUND') });
    }
    if (!docs) return res.status(401).json({ 'success': false, 'message': req.i18n.__('USER_NOT_FOUND') });
    return res.status(200).json({ 'success': true, 'users': docs });
  });
};

export const verifyNumber = (req, res) => {
  var findOrCondition = [{ "$and": [{ 'phone': req.body.phone }, { "phcode": req.body.phcode }] }];
  if (req.body.email) findOrCondition.push({ 'email': (req.body.email).toLowerCase() })

  Rider.findOne({ $or: findOrCondition }, function (err, user) {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
    if (user) return res.status(409).json({ 'success': false, 'message': req.i18n.__('PHONE_EMAIL_ALREADY_EXISTS') });
    var randomSMS = GFunctions.sendRandomizeCode('0', 4);
    // sendSmsMsg(req.body.phone, '', req.body.phcode, 'verifyNumberDriver', { 'RANDOMSMS': randomSMS });     
    if (featuresSettings.registerOTPVerificationMethod == 'email' || featuresSettings.registerOTPVerificationMethod == 'both') {
      sendEmail(req.body.email, {}, '<#> OTP to install ' + config.appName + ' app is ' + randomSMS)
    }
    if (featuresSettings.registerOTPVerificationMethod == 'sms' || featuresSettings.registerOTPVerificationMethod == 'both') {
      smsGateway.sendSmsMsg(req.body.phone, '', req.body.phcode, 'verifyNumberRider', { 'RANDOMSMS': randomSMS });
    }
    return res.json({ 'success': true, 'message': req.i18n.__('OTP_SEND_TO_YOUR') + ' Phone No.', 'code': randomSMS.toString() });
  })
};

export const riderUpdateVerifiedData = (req, res) => {
  var updateData = {
    phone: req.body.phone,
    phcode: req.body.phcode,
  };
  var findOrCondition = [{ "$and": [{ 'phone': req.body.phone }, { "phcode": req.body.phcode }] }];
  if (req.body.email) {
    updateData = {
      email: req.body.email
    }
    findOrCondition.push({ 'email': (req.body.email).toLowerCase() })
  }

  Rider.findOne({ $or: findOrCondition }, function (err, user) {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
    if (user) return res.status(409).json({ 'success': false, 'message': req.i18n.__('PHONE_EMAIL_ALREADY_EXISTS') });

    Rider.findOneAndUpdate({ _id: req.userId }, updateData, { new: true }, (err, doc) => {
      if (err) {
        return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
      }
      return res.json({ 'success': true, 'message': req.i18n.__('PROFILE_UPDATED_SUCCESSFULLY') });
    })
  })
};

export const addData = (req, res) => {
  var errMsg = 'PHONE_ALREADY_EXISTS';
  var findOrCondition = [{ "$and": [{ 'phone': req.body.phone }, { "phcode": req.body.phcode }] }];
  if (req.body.email) {
    findOrCondition.push({ 'email': req.body.email })
    errMsg = 'PHONE_EMAIL_ALREADY_EXISTS';
  }

  Rider.findOne({ $or: findOrCondition }, function (err, user) {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
    if (user) return res.status(401).json({ 'success': false, 'message': errMsg });

    var newDoc = new Rider();
    newDoc.fname = req.body.fname;
    newDoc.lname = req.body.lname;
    newDoc.email = req.body.email;
    newDoc.phone = req.body.phone;
    newDoc.gender = req.body.gender;
    newDoc.cnty = req.body.cnty;
    newDoc.cntyname = req.body.cntyname;
    newDoc.state = req.body.state;
    newDoc.statename = req.body.statename;
    newDoc.city = req.body.city;
    newDoc.cityname = req.body.cityname;
    newDoc.lang = req.body.lang;
    newDoc.cur = req.body.cur;
    newDoc.phcode = req.body.phcode;
    newDoc.fcmId = req.body.fcmId;

    newDoc.scity = req.body.scity ? req.body.scity : null;
    newDoc.scId = req.body.scId ? req.body.scId : null;

    newDoc.loginType = req.body.loginType;
    newDoc.loginId = req.body.loginId;

    newDoc.nic = req.body.nic;

    if (req.body.loginType == 'facebook' || req.body.loginType == 'google') {
      newDoc.loginType = req.body.loginType;
      newDoc.loginId = req.body.loginId;
    } else {
      newDoc.setPassword(req.body.password);
    }

    newDoc.setReferal();
    newDoc.save((err, datas) => {
      if (err) {
        return res.json({ 'success': false, 'message': err.message, 'err': err });
      }
      var token = newDoc.generateJwt(datas._id, datas.email, datas.fname, "rider");
      var data = { name: datas.fname };
      if (datas.email) sendEmail(datas.email, data, 'Welcome')
      addRiderDatatoFb(datas);
      processSignupBonus(datas._id);
      processReferalCode(req.body.referal, datas._id);
      GFunctions.sendFCMMsg(datas.fcmId, 'Signup Successfully', 'signup');
      return res.status(200).json({ 'success': true, 'message': req.i18n.__('REGISTERED_SUCCESSFULLY'), "datas": [{ "name": datas.fname, "email": datas.email }], token: token });
    })

  })

};



export const addRiderDataFromMTD = (req, res, next) => {
  if (req.body.newuser != 1) {
    req.name = req.body.fname;
    req.userId = req.body.userId;
    next();
  } else {

    var errMsg = 'Phone No. already Exists.';
    var findOrCondition = [{ "$and": [{ 'phone': req.body.phone }] }];
    if (req.body.email) {
      findOrCondition.push({ 'email': req.body.email })
      errMsg = 'Phone No. Or Email already Exists.';
    }

    Rider.findOne({ $or: findOrCondition }, function (err, user) {
      if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
      if (user) return res.status(401).json({ 'success': false, 'message': errMsg });

      var newDoc = new Rider();
      newDoc.fname = req.body.fname;
      newDoc.lname = req.body.lname;
      newDoc.email = req.body.email;
      newDoc.phone = req.body.phone;
      newDoc.setPassword(config.resetPasswordTo);
      newDoc.phcode = config.phoneCode;

      newDoc.save((err, datas) => {
        if (err) {
          return res.json({ 'success': false, 'message': err.message, 'err': err });
        }
        req.body.userId = datas._id;
        addRiderDatatoFb(datas);
        req.name = req.body.fname;
        req.userId = datas._id;
        next();
      })
    })
  }
};

export const addRiderDataFromHail = (req, res, next) => {
  var errMsg = 'Phone No. already Exists.';
  var findOrCondition = [{ "$and": [{ 'phone': req.body.phone }] }];
  if (req.body.email) {
    findOrCondition.push({ 'email': req.body.email })
    errMsg = 'Phone No. Or Email already Exists.';
  }

  Rider.findOne({ $or: findOrCondition }, function (err, user) {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
    if (user) {
      req.body.riderName = user.fname;
      req.body.riderId = user._id;
      next();
    } else {
      var newDoc = new Rider();
      newDoc.fname = req.body.fname;
      newDoc.lname = req.body.lname;
      newDoc.email = req.body.email;
      newDoc.phone = req.body.phone;
      newDoc.setPassword(config.resetPasswordTo);
      newDoc.phcode = config.phoneCode;


      newDoc.save((err, datas) => {
        if (err) {
          return res.json({ 'success': false, 'message': err.message, 'err': err });
        }
        addRiderDatatoFb(datas);
        var data = { name: datas.fname };
        if (req.body.email) sendEmail(req.body.email, data, 'Welcome')
        smsGateway.sendSmsMsg(req.body.phone, '', req.body.phcode, 'sendPasswordToUser', { 'PASSWORD': config.resetPasswordTo });
        req.body.riderId = datas._id;
        req.body.riderName = req.body.fname;
        next();
      })

    }
  })
};

function addRiderDatatoFb(datas) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("riders_data");
  var requestData = {
    current_tripid: "0",
    email_id: datas.email ? datas.email : "",
    name: datas.fname,
    tripstatus: "",
    tripdriver: "",
    requestId: ""
  };
  var id = datas._id.toString();
  var usersRef = ref.child(id);

  usersRef.set(requestData, function (snapshot) {
    // return res.json({'success':true,'message':'Password Updated Successfully', 'snap' :  snapshot});  
    console.log("addRiderDatatoFb", requestData);
  });
}

/**
 * Process Referal : Check code, get amount, update wallet
 * @input  
 * @param 
 * @return 
 * @response  
 */
function processReferalCode(referalCode, userId) {
  if (featuresSettings.referalSettings.isRiderReferalCodeAvailable) {
    if (!referalCode || referalCode == "") return false;
    Rider.findOne({ referal: referalCode }, function (err, docs) {
      if (err) { console.log('processReferalCode'); }
      else if (docs) {
        var referalAmt = featuresSettings.referalSettings.riderReferalAmount;
        var refererAmt = featuresSettings.referalSettings.riderRefererAmount;
        if (refererAmt) { //who gaves code
          addAmtToReferalWallet(docs._id, referalCode, refererAmt, 'Referer Credits', 'Credit');
        }
        if (referalAmt) { //who uses code
          // createWalletAndaddAmtToUser(userId, referalCode, config.referalAmt);
          addAmtToReferalWallet(userId, referalCode, referalAmt, 'Referal Credits', 'Credit');
        }
      }
    });
  }
}

function processSignupBonus(userId) {
  if (featuresSettings.riderSignupBonus) {
    var refererAmt = featuresSettings.riderSignupBonusAmount;
    var referalCode = GFunctions.sendRandomizeCode('Aa0', 6);
    addAmtToReferalWallet(userId, referalCode, refererAmt, 'Signup Bonus');
  }
}



/**
 * Add Amount To Given User Wallet 
 * @input  
 * @param 
 * @return 
 * @response  
 */
function addAmtToReferalWallet(userId, trxid = '', amount = 0, desc = 'reference', type = 'Credit') {
  var tranxData = {
    trxid: trxid,
    amt: parseFloat(amount),
    date: GFunctions.sendTimeNow(),
    type: type,
    for: desc,
  }

  // 1.chk Wallet
  Wallet.findOne({ ridid: userId }, function (err, doc1) {
    if (err) { }
    else if (!doc1) {

      //Add New Wallet Details
      var newDoc = new Wallet(
        {
          ridid: userId,
          bal: amount,
          trx: tranxData
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          console.log("addCardDetailsToWallet2", userId);
        } else {
          if (docs) {
            console.log("addCardDetailsToWallet2", userId);
          }
        }
      })
      //Add New Wallet Details

    } else if (doc1) {

      Wallet.findOneAndUpdate({ ridid: userId }, {
        $push: { trx: tranxData }
      }, { 'new': true },
        function (err, doc) {
          if (err) {
            console.log('addAmtToReferalWallet', err);
          } else {
            if (doc) {
              updateWalletBal(doc._id, amount, type);
            }
            // console.log('addAmtToReferalWallet', doc);  
          }
        }
      );
    }
  })
  // 1.chk Wallet
}

export const addWalletSettlementData = (req, res) => {
  if (typeof req.body.riderId === "undefined" || req.body.riderId === "") {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") })
  }
  var addeddate = GFunctions.getDateTimeinThisFormat(req.body.paymentDate, "YYYY-MM-DD");
  Wallet.findOne({ ridid: req.body.riderId }, function (err, doc) {
    if (err) return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    else if (!doc) {
      var id1 = new ObjectId;

      var newdoc = new Wallet
        ({
          ridid: req.body.riderId,
          bal: 0,
          trx: {
            _id: id1,
            trxid: req.body.trxId,
            for: req.body.description,
            amt: req.body.amt,
            type: req.body.type,
            date: addeddate,
            bal: 0
          },
        });

      newdoc.save((err, docs) => {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
        }
        var tranx = newdoc.trx.id(id1);
        let Tranxamt = tranx.amt;
        let Tranxbal = tranx.bal;
        let types = tranx.type;
        var total;
        var total1;
        if (types == 'debit') {
          total = parseFloat(parseFloat(docs.bal) - parseFloat(Tranxamt)).toFixed(2);
        }
        if (types == 'credit') {
          total = parseFloat(parseFloat(docs.bal) + parseFloat(Tranxamt)).toFixed(2);
        }
        docs.bal = total;
        tranx.bal = total;

        docs.save(function (err, docs) {
          if (err) {
            return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR_UPDATING'), 'error': err });
          }
          else {
            return res.json({ 'success': true, 'message': req.i18n.__('RIDER_AMOUNT_SUCCESS'), 'docs': docs });
          }
        })
      })
    }
    else if (doc) {
      var id1 = new ObjectId;

      var trxdetails =
      {
        _id: id1,
        trxid: req.body.trxId,
        for: req.body.description,
        amt: req.body.amt,
        type: req.body.type,
        date: addeddate,
        bal: 0
      }
      Wallet.findOneAndUpdate({ ridid: req.body.riderId }, { $push: { trx: trxdetails } }, { 'new': true }, function (err, docs) {
        if (err) {
          return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR_UPDATING'), 'error': err });
        }
        var tranx = docs.trx.id(id1);
        let Tranxamt = tranx.amt;
        let Tranxbal = tranx.bal;
        let types = tranx.type;
        var total;
        var total1;
        if (types == 'debit') {
          total = parseFloat(parseFloat(docs.bal) - parseFloat(Tranxamt)).toFixed(2);
        }
        if (types == 'credit') {
          total = parseFloat(parseFloat(docs.bal) + parseFloat(Tranxamt)).toFixed(2);
        }
        docs.bal = total;
        tranx.bal = total;

        docs.save(function (err, docs) {
          if (err) {
            return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR_UPDATING'), 'error': err });
          }
          else {
            return res.json({ 'success': true, 'message': req.i18n.__('DRIVER_ACCOUNT_ADDED_SUCCESSFULLY'), 'docs': docs });
          }
        })
      })
    }
  });
}

/**
 * Update Wallet Bal.
 * @input  
 * @param 
 * @return 
 * @response  
 */
function updateWalletBal(walletId, amount, type = 'Credit') {
  Wallet.findById(walletId, function (err, docs) {
    if (err) { } else if (!docs) { }
    else {
      let oldbal = docs.bal;
      var newbal = oldbal;
      if (type == 'Credit') {
        newbal = parseFloat(oldbal) + parseFloat(amount);
      }
      if (type == 'Debit') {
        newbal = parseFloat(oldbal) - parseFloat(amount);
      }
      docs.bal = newbal;
      docs.save(function (err, op) {
        if (err) {
          console.log('updateWalletBal', err);
        }
        else {
          console.log('updateWalletBal', docs);
        }

      })
    }
  });
}


/**
 * Update rider Details 
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const updateData = (req, res) => {
  var isStarExistsInPhone = GFunctions.isStarExistsInString(req.body.phone);
  var isStarExistsInEmail = GFunctions.isStarExistsInString(req.body.email);

  var newDoc =
  {
    fname: req.body.fname,
    lname: req.body.lname,
    gender: req.body.gender,
    lang: req.body.lang,
    cur: req.body.cur,
    scId: req.body.scId,
    nic: req.body.nic,

    scity: req.body.scity,
    cnty: req.body.cnty,
    cntyname: req.body.cntyname,
    state: req.body.state,
    statename: req.body.statename,
    city: req.body.city,
    cityname: req.body.cityname,
    status: req.body.status,
  }

  if (!isStarExistsInPhone) newDoc.phone = req.body.phone;
  if (!isStarExistsInEmail) newDoc.email = req.body.email;

  Rider.findOneAndUpdate({ _id: req.body._id }, newDoc, { new: true }, (err, todo) => {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    return res.json({ 'success': true, 'message': req.i18n.__('DETAILS_UPDATED'), todo });
  });
};

/**
 * Delete rider Details 
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const deleteData = (req, res) => {
  /*   Rider.findByIdAndRemove(req.params.id, (err,docs) => {
      if(err){
        return res.json({'success':false,'message':req.i18n.__('SOME_ERROR')});
      } 
      return res.json({'success':true,'message':'Rider Details Deleted successfully'}); 
    }) */

  var update = {
    "softdel": 'inactive'
  }
  Rider.findOneAndUpdate({ _id: req.params.id }, update, { new: true }, (err, doc) => {
    if (err) { return res.status(401).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'err': err }); }
    else {
      updateRiderActiveStatusInFB(req.params.id, 'inactive');
      return res.json({ 'success': true, 'message': req.i18n.__('RIDER_INACTIVE_SUCCESS') });
    }
  })

}

export const riderActivate = (req, res) => {
  var update = {
    "softdel": 'active'
  }
  Rider.findOneAndUpdate({ _id: req.params.id }, update, { new: true }, (err, doc) => {
    if (err) { return res.status(401).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'err': err }); }
    else {
      updateRiderActiveStatusInFB(req.params.id, 'active');
      return res.json({ 'success': true, 'message': req.i18n.__('RIDER_ACTIVE_SUCCESS') });
    }
  })
}


function updateRiderActiveStatusInFB(riderId, status) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("riders_data");
  var requestData = {
    active_status: status
  };
  var child = riderId.toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData);

  usersRef.update(requestData, function (error) {
    if (error) { logger.error(error); } else {
      console.log(child, "New Request");
    }
  });

}

/**
 * Get Referal Amt As set by admin
 * @input  
 * @param 
 * @return Amount in USD
 * @response  
 */
function validateAndGetAmt(referalCode) {
  return 10;
}

/**
 * create Empty Wallet And add Amt To User
 * @input  
 * @param 
 * @return  
 * @response  
 */
function createWalletAndaddAmtToUser(userId, referalCode, referalAmt) {
  Rider.findById(userId, function (err, docs) {
    if (err) { } else if (!docs) { }
    else {

      docs.card.id = referalCode;
      docs.card.currency = 'usd';
      docs.card.last4 = '';

      docs.save(function (err, op) {
        if (err) { console.log("createWalletAndaddAmtToUser"); }
        else {
          console.log("addCardToMongo", userId);
          addCardDetailsToWallet(userId, referalCode, referalAmt);
        }
      })

    }
  });
}

/**
 * Add CardDetails To Wallet
 * @input  
 * @param 
 * @return  
 * @response  
 */
function addCardDetailsToWallet(userId, referalCode, referalAmt) {
  // 1.chk Wallet
  Wallet.findOne({ ridid: userId }, function (err, doc) {
    if (err) { }
    else if (!doc) {

      //Add New Wallet Details
      var newDoc = new Wallet(
        {
          ridid: userId,
          bal: 0,
          stripe: {
            id: referalCode,
            currency: 'usd',
            last4: ''
          }
        }
      );
      newDoc.save((err, docs) => {
        if (err) {
          console.log("addCardDetailsToWallet2", userId);
        } else {
          addAmtToReferalWallet(userId, referalCode, referalAmt);
          console.log("addCardDetailsToWallet3", userId);
        }
      })
      //Add New Wallet Details


    } else if (doc) {

      doc.stripe.id = userId;
      doc.stripe.currency = 'usd';
      doc.stripe.last4 = '';
      doc.save(function (err, op) {
        if (err) {
          console.log("addCardDetailsToWallet4", userId);
        }
        else {
          addAmtToReferalWallet(userId, referalCode, referalAmt);
          console.log("addCardDetailsToWallet5", userId);
        }
      })

    }
  })
  // 1.chk Wallet
}



export const getData = (req, res) => {
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.query.scity_like != undefined) likeQuery['scity'] = { "$in": req.query.scity_like };

  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var totalCount = 10;
  // (/true/i)
  if (req.query.status_like != undefined) {
    likeQuery["status"] = likeQuery["status"].test("true");
  }



  Rider.find(likeQuery).count().exec((err, cnt) => {
    if (err) { }
    totalCount = cnt;
  });

  Rider.find(likeQuery, { "hash": 0, "salt": 0 }).skip(pageQuery.skip).limit(pageQuery.take).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.header('x-total-count', totalCount);
    res.send(docs);
  });

}

export const login = (req, res) => {
  var riderWhere = {};
  var userName = req.body.username ? req.body.username : req.body.email;
  var checkPassword = true;
  if (req.body.loginType == 'facebook' || req.body.loginType == 'google') {
    riderWhere = { 'loginType': req.body.loginType, 'loginId': req.body.loginId };
    checkPassword = false;
  } else {
    riderWhere = { $or: [{ "$and": [{ 'phone': userName }] }, { 'email': (userName).toLowerCase() }] };
  }

  Rider.findOne(riderWhere, function (err, user) {
    var newDoc = Rider();
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('ERROR_SERVER') });
    if (!user) return res.status(404).json({ 'success': false, 'message': req.i18n.__('USER_NOT_FOUND') });
    if (user.softdel == 'inactive') return res.status(401).json({ 'success': false, 'message': req.i18n.__("ACCOUNT_WAS_INACTIVATED_BY_ADMIN") });
    if (checkPassword) {
      var passwordIsValid = newDoc.validPassword(req.body.password, user.salt, user.hash);
      if (!passwordIsValid) return res.status(401).json({ 'success': false, 'message': req.i18n.__('INVALID_PASSWORD') });
    }
    var token = newDoc.generateJwt(user._id, user.email, user.fname, "rider");
    addFCMId(req.body.fcmId, user._id);
    res.status(200).json({ 'success': true, 'message': req.i18n.__('LOGIN_SUCCESS'), token: token });
  })
}

function addFCMId(fcmId, userid) {
  var update = {
    fcmId: fcmId
  }
  Rider.findOneAndUpdate({ _id: userid }, update, { new: true }, (err, doc) => {
    if (err) { console.log("addFCMId", err); }
    else { console.log("addFCMId", fcmId); }
  })
}

export const getAppData = (req, res) => {
  Rider.find({ _id: req.userId }, { hash: 0, salt: 0 }).exec(async (err, docs) => {
    if (err) {
      return res.json([]);
    }
    if (docs.length) {
      /*      docs.push({  profileurl: config.baseurl+docs[0].profile  });  
            return res.json(docs);*/

      var resObj = formatProfileRes(docs[0]);
      var resArray = [resObj];
      resArray.push({ profileurl: config.baseurl + docs[0].profile });
      let language = config.appDefaultLanguageCode;
      if (req.headers['accept-language'] && req.headers['accept-language'].length < 3) {
        language = (req.headers['accept-language']) ? req.headers['accept-language'] : config.appDefaultLanguageCode;
      }
      var cancelReason = await CancelReasons.findOne({ "language": language }, { 'riderCancelReason': 1 });
      if (cancelReason){
        resArray.push({ "riderCancellationReasons": cancelReason.riderCancelReason });
      }else{
        resArray.push({ "riderCancellationReasons":  [] });
      }  
      return res.json(resArray);

    }
    else {
      return res.json([]);
    }
  })
}

function formatProfileRes(doc) {
  var newObj = {
    "phone": doc.phone,
    "email": doc.email,
    "lname": doc.lname,
    "fname": doc.fname,
    "status": doc.status,
    "referal": doc.referal,
    "balance": doc.balance,
    "gender": doc.gender,
    "address": doc.address,
    "rating": doc.rating,
    "EmgContact": doc.EmgContact,
    "profile": doc.profile,
    "phcode": doc.phcode,
    "__v": doc.__v,
    "callmask": doc.callmask,
    "loginId": doc.loginId,
    "loginType": doc.loginType,
    "verificationCode": doc.verificationCode,
    "cityname": doc.cityname,
    "city": doc.city,
    "statename": doc.statename,
    "state": doc.state,
    "cntyname": doc.cntyname,
    "cnty": doc.cnty,
    "scity": doc.scity,
    "lastCanceledDate": doc.lastCanceledDate,
    "canceledCount": doc.canceledCount,
    "lang": doc.lang,
    "fcmId": doc.fcmId,
    "card": doc.card,
    "cur": doc.cur,
    "nic": doc.nic,
  };
  return newObj;
}

export const updateAppData = (req, res) => {

  var filename = "";
  var filepath = "";
  if (req['file'] != null) {
  
/*  req.body.profile = req['file'].path;
    filename = req['file'].filename;
    filepath = req['file'].path;
*/
var tmppath = (req['file'].path).replace(/\\/g, '/') ;
    req.body.profile = tmppath;
    filename = req['file'].filename;
    filepath = tmppath;


  } else { }


/*************************************/

//fs.writeFile(__dirname + '/../output.txt', filepath , function (err, doc) {});


/**********************************************************/


  var updateData = {
    fname: req.body.fname,
    phcode: req.body.phcode,
    lname: req.body.lname,
    email: req.body.email,
    nic: req.body.nic,
    cnty: req.body.cnty,
    state: req.body.state,
    city: req.body.city,
    lang: req.body.lang,
    cur: req.body.cur,
  };


  if (filepath) {
    updateData.profile = filepath;
  }

  Rider.findOneAndUpdate({ _id: req.userId }, updateData, { new: true }, (err, doc) => {
    if (filepath == "") { filepath = doc.profile; }
    if (err) {
      return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    }
    return res.json({ 'success': true, 'message': req.i18n.__('PROFILE_UPDATED_SUCCESSFULLY'), "request": req.body, 'fileurl': config.baseurl + filepath });
  })

}


export const riderImage = (req, res) => {
  var filename = "";
  var filepath = "";
  if (req['file'] != null) {
    req.body.profile = req['file'].path;
    filename = req['file'].filename;
    filepath = req['file'].path;
  } else { }
  var updateData = {};
  if (filepath) {
    updateData.profile = filepath;
  }

  Rider.findOneAndUpdate({ _id: req.userId }, updateData, { new: true }, (err, doc) => {
    if (filepath == "") { filepath = doc.profile; }
    if (err) {
      return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    }
    return res.json({ 'success': true, 'message': req.i18n.__('PROFILE_UPDATED_SUCCESSFULLY'), "request": req.body, 'fileurl': config.baseurl + filepath });
  })

}

export const updatePassword = (req, res) => {
  if (req.body.newpassword != req.body.confirmpassword) {
    return res.json({ 'success': false, 'message': req.i18n.__('NEW_CONFIRM_PASSWORD_DIFFERENT') });
  }

  Rider.findById(req.userId, function (err, docs) {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    else {
      if (!docs) return res.status(404).json({ 'success': false, 'message': req.i18n.__('USER_NOT_FOUND') });
      var newDoc = Rider();
      var passwordIsValid = newDoc.validPassword(req.body.oldpassword, docs.salt, docs.hash);
      if (!passwordIsValid) return res.json({ 'success': false, 'message': req.i18n.__('INVALID_PASSWORD') });
      var obj = newDoc.getPassword(req.body.newpassword);
      var update = {
        salt: obj.salt,
        hash: obj.hash
      }
      Rider.findOneAndUpdate({ _id: req.userId }, update, { new: false }, (err, doc) => {
        if (err) { return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
        return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_UPDATED') });
      })
    }
  });

}

//Emergency Contact 
export const addEmgContact = (req, res) => {
  var updateData = {
    name: req.body.name,
    number: req.body.number
  }
  Rider.findByIdAndUpdate(req.userId, {
    $push: { EmgContact: updateData }
  }, { 'new': true },

    function (err, doc) {
      if (err) {
        return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
      }
      return res.json({ 'success': true, 'message': req.i18n.__('CONTACT_SUCCESS'), 'contacts': doc.EmgContact });
    }
  );
}

export const getEmgContact = (req, res) => {
  Rider.find({ _id: req.userId }, { hash: 0, salt: 0 }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    if (docs.length) {
      return res.json(docs[0].EmgContact);
    }
    else {
      return res.json([]);
    }
  })
}

export const delEmgContact = (req, res) => {
  Rider.findOne({ _id: req.userId }).exec((err, docs) => {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    if (!docs) return res.json({ 'success': false, 'message': req.i18n.__('USER_NOT_FOUND'), 'error': err });
    docs.EmgContact.remove(req.body.emgContactId);
    docs.save(function (err, op) {
      if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
      return res.json({ 'success': true, 'message': req.i18n.__('DELETED_SUCCESSFULLY'), 'contacts': docs.EmgContact });
    });
  })
}

export const putEmgContact = (req, res) => {
  Rider.findById(req.userId, function (err, docs) {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    var EmgContact = docs.EmgContact.id(req.body.emgContactId);

    taxi.name = req.body.name;
    taxi.number = req.body.number;

    docs.save(function (err, op) {
      if (err) return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
      return res.json({ 'success': true, 'message': req.i18n.__('UPDATED_SUCCESSFULLY'), 'drivertaxis': docs.EmgContact });
    });

  });
}

//Emergency Contact 

export const getRidersAddress = async (req, res) => {
  try {
    let addressCheck = await Rider.findOne({ _id: req.userId }, { address: 1 });
    return res.json({ 'success': true, 'message': req.i18n.__('FAV_ADDRESS'), 'Address': addressCheck.address })
  } catch (err) {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err })
  }
}

/**
 * Add Driver Address , for : home / work
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const addRidersAddress = async (req, res) => {
  try {
    var lable = req.body.lable ? req.body.lable : req.body.for;

    let addressCheck = await Rider.findOne({ _id: req.userId, 'address.lable': lable }, { 'address.$': 1 });
    if (addressCheck == null) {
      Rider.findByIdAndUpdate(req.userId,
        {
          "$push": {
            address: {
              lable: lable,
              address: req.body.address,
              Coords: [parseFloat(req.body.lng), parseFloat(req.body.lat)]
            }
          }
        }
        , { new: true }, function (err, docs) {
          if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
          else { return res.json({ 'success': true, 'message': req.i18n.__('ADDRESS_ADDED_SUCCESS'), 'Address': docs.address }) }
        });
    }

    else {
      Rider.findById(req.userId, function (err, update) {
        if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }) }
        var Address = update.address.id(addressCheck.address[0]._id);
        Address.lable = lable;
        Address.address = req.body.address;
        Address.Coords = [parseFloat(req.body.lng), parseFloat(req.body.lat)];

        update.save((err, docs) => {
          if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }) }
          return res.json({ 'success': true, 'message': req.i18n.__('ADDRESS_UPDATE_SUCCESS'), 'Address': docs.address })
        })
      })
    }
  }
  catch (err) {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err })
  }
}

/**
 * Delete Driver Address , for : home / work
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const deleteRiderAddress = (req, res) => {
  Rider.update({ _id: req.userId }, { '$pull': { address: { _id: req.params.addressId } } }, function (err, docs) {
    if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
    else { return res.json({ 'success': true, 'message': req.i18n.__('ADDRESS_DELETE_SUCCESS') }) }
  });
}


/**
 * Reset Password  
 * @input  
 * @param 
 * @return 
 * @response  
 */
export const riderResetPassword = (req, res) => {

  Rider.findOne({ phone: req.body.phone }, function (err, docs) {
    if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
    else if (!docs) {
      return res.status(409).json({ 'success': false, 'message': req.i18n.__('PHONE_NO_IS_INVALID') });
    }
    else {
      docs.setPassword(req.body.password);
      docs.save(function (err, op) {
        if (err) { return res.status(409).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
        else { return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_RESETED') }) }
      })

    }
  });

}


function sendOTPToMail(mailid, otp) {
  const nodemailer = require('nodemailer');

  let smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // upgrade later with STARTTLS
    auth: {
      user: 'abservetech.smtp@gmail.com',
      pass: 'smtp@345'
    }
  };
  // 'host' => 'ssl://smtp.gmail.com', 'port' => 465, 'username' => 'abservetech.smtp@gmail.com', 'password' => 'smtp@345', 'transport' => 'Smtp'
  let transporter = nodemailer.createTransport(smtpConfig);

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"Admin " <abservetech.smtp@gmail.com>', // sender address
    to: mailid, // list of receivers
    subject: 'Password Reset', // Subject line
    text: 'Please use this OTP to Reset Password', // plain text body
    html: otp // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info)); 
  });
  // });

}

export const riderById = (req, res) => {

  Rider.find({ _id: req.params.id }, { "pwd": 0 }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });

}


export const updateRiderOldBalanceDetails = async (riderId, amtToReduce) => {
  try {
    let RiderWallet = await Rider.findById(riderId, { balance: 1 });
    RiderWallet.balance = Number(RiderWallet.balance) - Number(amtToReduce);
    updateBalanceAmountinFB(riderId, RiderWallet.balance);
    RiderWallet.save()
  }
  catch (err) {
    console.log(err)
  }
}


/*
checkBalanceLimit
*/
export const updateBalanceAmountinFB = (riderId, balance) => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }

  var db = firebase.database();
  var ref = db.ref("riders_data");
  var requestData = {
    oldBalance: balance
  };
  var child = riderId.toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData);

  usersRef.update(requestData, function (error) {
    if (error) { console.log(error); } else {
      console.log("updateDriverCreditsInFB");
    }
  });

}

export const addCancelationAmtToRider = async (riderId, tripId) => {
  try {
    let tripData = await Trips.findOne({ tripno: tripId }, { 'csp.riderCancelFee': 1 });
    var amtToDebit = tripData.csp.riderCancelFee;
    updateRiderWalletCredits(riderId, amtToDebit);
  }
  catch (err) {
    console.log(err)
  }
}

export const updateRiderWalletCredits = async (riderId, amtToDebit) => {
  try {
    let RiderWallet = await Rider.findById(riderId, { balance: 1, canceledCount: 1, lastCanceledDate: 1 });
    checkCancellationLimit(riderId, RiderWallet.canceledCount, RiderWallet.lastCanceledDate, RiderWallet.balance);
    RiderWallet.balance = Number(RiderWallet.balance) + Number(amtToDebit);
    updateBalanceAmountinFB(riderId, RiderWallet.balance);
    RiderWallet.save()
  }
  catch (err) {
    console.log(err)
  }
}

/*
Check Cancelation limit and Inc Count for Today
*/
export const checkCancellationLimit = async (riderId, canceledCount, lastCanceledDate, balance) => {
  try {
    // format the current date
    var now = moment();
    var today = now.format("DD-MM-YYYY");
    var lcd = lastCanceledDate;
    var newCanceledCount = 1;
    if (lcd == today) {
      newCanceledCount = Number(canceledCount) + 1;
    }
    console.log('riderId', riderId)
    Rider.findByIdAndUpdate(riderId,
      {
        'canceledCount': newCanceledCount,
        'lastCanceledDate': today,
      }
      , { new: true }, function (err, docs) {
        if (err) { console.log(err) }
        checkCancelLimitAndUpdateFB(riderId, canceledCount, lastCanceledDate);
      });
  }
  catch (err) {
    console.log(err)
  }
}

export const riderForgotPassword = (req, res) => {
  var userName = req.body.email;
  var message;
  console.log({ "$or": [{ 'email': userName }, { 'phone': userName }] });
  Rider.findOne({ "$or": [{ 'email': userName }, { 'phone': userName }] }, function (err, doc) {
    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'error': err }) }
    if (!doc) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("NOT_FOUND") }) }

    doc.verificationCode = GFunctions.sendRandomizeCode('0', 6)

    doc.save((err, userDoc) => {
      if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'error': err }) }
      if (featuresSettings.passwordVerificationMethodForUser == 'email') {
        var data = { name: userDoc.fname, email: userDoc.email, otp: userDoc.verificationCode, url: config.baseurl + 'api/riderChangePassword/' + userDoc.verificationCode + '/' + userDoc._id };
        sendEmail(userDoc.email, data, 'Reset password')
        message = req.i18n.__("PASSWORD_VERIFICATION_CODE_TO_MAIL")
      }
      else if (featuresSettings.passwordVerificationMethodForUser == 'sms') {
        smsGateway.sendSmsMsg(userDoc.phone, '', userDoc.phcode, 'forgotPasswordRider', { 'OTPCODE': userDoc.verificationCode });
        message = req.i18n.__("PASSWORD_VERIFICATION_CODE_TO_MBL")
      }
      return res.status(200).json({ 'success': true, 'message': message, 'OTP': userDoc.verificationCode })
    })
  })
}

export const changePasswordTemplate = (req, res) => {
  fs.readFile(__dirname + '/html/changePasswordForApp.html', 'utf8', (err, template) => {
    var params = {
      loginRedirectUrl: config.landingurl,
      url: config.baseurl + 'api/riderChangePassword/',
      id: req.params.code + '/' + req.params.id,
    }
    var html = Mustache.render(template, params);
    return res.send(html)
  });
}

export const changePassword = (req, res) => {
  if (req.body.newPwd != req.body.conPwd) {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__('NEW_CONFIRM_PASSWORD_DIFFERENT') });
  }

  Rider.findOne({ "_id": req.params.id, "verificationCode": req.params.code }, function (err, docs) {
    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'error': err }) }
    if (!docs) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("RESET_LINK_EXPIRED") }) }
    var newDoc = Rider();
    var obj = newDoc.getPassword(req.body.newPwd);
    var update = {
      salt: obj.salt,
      hash: obj.hash,
      verificationCode: ''
    }

    Rider.findOneAndUpdate({ "_id": docs._id }, update, { new: false }, (err, doc) => {
      if (err) { return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
      return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_UPDATED') });
    })
  })
}

export const riderResetPasswordWithOTP = (req, res) => {
  if (req.body.newPwd != req.body.conPwd) {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__('NEW_CONFIRM_PASSWORD_DIFFERENT') });
  }
  var userName = req.body.email;
  Rider.findOne({ "$or": [{ 'email': userName }, { 'phone': userName }] }, function (err, doc) {
    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'error': err }) }
    if (!doc) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("NOT_FOUND") }) }
    if (doc.verificationCode != req.body.otp) { return res.status(409).json({ 'success': false, 'message': "USER_NOT_FOUND" }) }
    var newDoc = Rider();
    var obj = newDoc.getPassword(req.body.newPwd);
    var update = {
      salt: obj.salt,
      hash: obj.hash,
      verificationCode: ''
    }
    Rider.findOneAndUpdate({ "_id": doc._id }, update, { new: false }, (err, doc) => {
      if (err) { return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
      return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_UPDATED') });
    })
  })
}

/**  
 * changePasswordByApp
 * params : userName(email or phone),newPwd, conPwd,otp
*/
export const changePasswordByApp = (req, res) => {
  if (req.body.password != req.body.confirmpassword) {
    return res.status(409).json({ 'success': false, 'message': req.i18n.__('NEW_CONFIRM_PASSWORD_DIFFERENT') });
  }
  var userName = req.body.email;
  Rider.findOne({ '$or': [{ 'email': userName }, { 'phone': userName }] }, function (err, doc) {
    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER"), 'error': err }) }
    if (!doc) { return res.status(409).json({ 'success': false, 'message': req.i18n.__("USER_NOT_FOUND") }) }
    if (doc.verificationCode === "" || doc.verificationCode !== req.body.otp) {
      return res.status(409).json({ 'success': false, 'message': req.i18n.__("INVALID_OTP!") })
    }

    var newDoc = Rider();
    var obj = newDoc.getPassword(req.body.password);
    var update = {
      salt: obj.salt,
      hash: obj.hash,
      verificationCode: ''
    }

    Rider.findOneAndUpdate({ "_id": doc._id }, update, { new: false }, (err, docs) => {
      if (err) { return res.json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err }); }
      return res.json({ 'success': true, 'message': req.i18n.__('PASSWORD_UPDATED') });
    })
  })
}

/** 
 * sent message to emergency contact
 * params : tripId
 */
export const emergencyMsg = (req, res) => {
  Rider.findById(req.userId, async function (err, userDoc) {
    if (err) { return res.status(500).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err }) }
    if (!userDoc) return res.status(200).json({ 'success': false, 'message': req.i18n.__("USER_NOT_FOUND") })
    var tripDetails = await Trips.findOne({ 'tripno': userDoc.curTripno }, { '_id': 1 });
    var URL = config.baseurl + 'public/shareTrip/locater.html?tripId=' + tripDetails._id
    userDoc.EmgContact.forEach(el => {
      smsGateway.sendSmsMsg(el.number, '', userDoc.phcode, 'emergencyMsg', { 'NAME': userDoc.fname, 'PHONE': userDoc.phone, 'URLLINK': URL })
    });
    smsGateway.sendSmsMsg(config.supportNo, 'Emergency Message From Rider : ' + userDoc.fname + ' Phone No. : ' + userDoc.phone + " Trip Id : " + userDoc.curTripno, config.phoneCode);
    return res.status(200).json({ 'success': false, 'message': req.i18n.__("MESSAGE_SENDED_SUCCESSFULLY") })
  })
}

export const ridersForNotificationTest = (req, res) => {
  Rider.find({
    fcmId: { $exists: true }
  }, { "_id": 1, 'fname': 1, 'phone': 1, 'fname': 1, 'fcmId': 1 }).exec((err, docs) => {
    if (err) {
      return res.json([]);
    }
    res.send(docs);
  });
}


export const getRiderRating = async (req, res) => {
  if (
    typeof req.query._sort === "undefined"
    || req.query._sort === ""
  ) {
    req.query._sort = "rating.rating";
  }

  if (
    typeof req.query._order === "undefined"
    || req.query._order === ""
  ) {
    req.query._order = "ASC";
  }
  var sortQuery = HelperFunc.sortQueryBuilder(req.query);
  var likeQuery = HelperFunc.likeQueryBuilder(req.query);
  var pageQuery = HelperFunc.paginationBuilder(req.query);
  var btQuery = HelperFunc.btQueryBuilder(req.query, {});

  if (req.cityWise == 'exists') likeQuery['scId'] = { "$in": req.scId };
  if (req.type == 'company') likeQuery['cpyid'] = { "$in": req.userId };

  if (btQuery["Rating"]) {
    likeQuery['rating.rating'] = btQuery["Rating"];
    delete btQuery["Rating"];
  }

  let count = Rider.find(likeQuery).count().exec();
  let riderlist = Rider.find(likeQuery, { fname: 1, phone: 1, _id: 1, rating: 1, code: 1, profile: 1 }).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery).exec();

  try {
    var promises = await Promise.all([count, riderlist]);
    res.header('x-total-count', promises[0]);
    return res.status(200).json(promises[1]);
  } catch (err) {
    return res.status(200).json({});
  }
}

export const riderPushToken = (req, res) => {
  var newDoc =
  {
    fcmId: req.body.fcmId,
  }
  Rider.findOneAndUpdate({ _id: req.userId }, newDoc, { new: true }, (err, adminDoc) => {
    if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__('SOME_ERROR'), 'error': err });
    return res.status(200).json({ 'success': true, 'message': req.i18n.__('TOKEN_UPDATED') });
  });
}

// RiderPerDay
export const updateRiderPerDayCancels = async (riderId, cancelledAmount, adminCommision) => {
  try {
    var todayDate = GFunctions.getISOTodayDate();
    var findQuery = { riderId: riderId, date: todayDate };
    var todayDataExists = await RiderPerDay.findOne(findQuery).exec();
    if (todayDataExists) {
      var updateDate = {
        nooftripsCancelled: Number(todayDataExists.nooftripsCancelled) + Number(1),
        cancelledAmount: Number(todayDataExists.cancelledAmount) + cancelledAmount,
      }
      await RiderPerDay.findOneAndUpdate({ riderId: riderId, date: todayDate }, updateDate).exec();
      return true;
    } else {
      var newDoc = new RiderPerDay(
        {
          riderId: riderId,
          cancelledAmount: cancelledAmount,
          nooftripsCancelled: 1,
          date: todayDate,
        }
      );
      await newDoc.save();
      return true;
    }
  } catch (error) {
    console.log('updateRiderPerDayCancels', error);
    return false;
  }
}

export const updateAddCancelationChargeToRider = async (riderId, amtToDebit, cancelLimitForDays, noOfDriverCancelAllowed, chargeDriverCancelationAmountFrom) => {
  try {
    var todayDate = GFunctions.getISOTodayDate();
    var findQuery = { riderId: riderId, date: todayDate };
    var todayDataExists = await RiderPerDay.findOne(findQuery).exec();
    if (todayDataExists) {
      // var cancelledAmount = Number(todayDataExists.cancelledAmount);
      if (chargeDriverCancelationAmountFrom == 'wallet') updateRiderWalletCancelationCredits(driverId, amtToDebit);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log('updateAddCancelationChargeToRider', error);
    return false;
  }
}

export const updateRiderWalletCancelationCredits = async (riderId, amtToDebit) => {
  try {
    let RiderWallet = await Rider.findById(riderId, { balance: 1, canceledCount: 1, lastCanceledDate: 1 });
    // (riderId, RiderWallet.canceledCount, RiderWallet.lastCanceledDate, RiderWallet.balance);
    RiderWallet.balance = Number(RiderWallet.balance) - Number(amtToDebit);
    updateBalanceAmountinFB(riderId, RiderWallet.balance);
    RiderWallet.save();
    addAmtToReferalWallet(riderId, '', amtToDebit, 'Cancellation Charge', 'Debit');
  }
  catch (err) {
    console.log(err)
  }
}


export const updateRiderWalletCreditsInRiderFirebase = async (riderId, amount) => { //mkmk
  try {
    let RiderWallet = await Rider.findById(riderId, { balance: 1, canceledCount: 1, lastCanceledDate: 1 });
    // RiderWallet.balance = Number(RiderWallet.balance) - Number(amtToDebit);
    updateBalanceAmountinFB(riderId, amount);
    RiderWallet.save();
  }
  catch (err) {
    console.log(err)
  }
}

export const updateBlockTheRiderIfCancelExceeds = async (riderId, ifcancelExceedsBlockUserFor, noOfDriverCancelAllowed) => {
  try {
    var todayDate = GFunctions.getISOTodayDate();
    var findQuery = { riderId: riderId, date: todayDate };
    var todayDataExists = await RiderPerDay.findOne(findQuery).exec();
    if (todayDataExists) {
      var nooftripsCancelled = Number(todayDataExists.nooftripsCancelled);
      if (Number(nooftripsCancelled) >= Number(noOfDriverCancelAllowed)) {
        var myDate = moment(todayDate).add(ifcancelExceedsBlockUserFor, "days").utcOffset(config.utcOffset).format("DD-MM-YYYY");
        Rider.findByIdAndUpdate(RiderPerDay,
          {
            'blockuptoDate': myDate,
          }
          , { new: true }, function (err, docs) {
            if (err) { }
            blockRiderInFB(riderId, myDate);
          });
        return true;
      }
    } else {
      return false;
    }
  } catch (error) {
    console.log('updateBlockTheRiderIfCancelExceeds', error);
    return false;
  }
}

export const blockRiderInFB = (riderId, blockuptoDate, cancelExceeds = 1) => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebasekey);
  }
  var db = firebase.database();
  var ref = db.ref("riders_data");
  var requestData = {
    cancelExceeds: cancelExceeds,
    blockuptoDate: blockuptoDate,
  };
  var child = riderId.toString();
  var usersRef = ref.child(child);
  usersRef.update(requestData);

  usersRef.update(requestData, function (error) {
    if (error) { console.log(error); } else {
      console.log("blockRiderInFB");
    }
  });
}

export const resetRidersBlocking = async () => {
  try {
    var todayDate = GFunctions.getISOTodayDate();
    todayDate = moment(todayDate).subtract(1, 'days').format('DD-MM-YYYY');
    var findQuery = {
      blockuptoDate: todayDate
    };
    var todayDataExists = await Rider.find(findQuery).exec();
    if (todayDataExists) {
      todayDataExists.forEach(function (u) {
        blockRiderInFB(u._id, '', 0);
      })
    }
    Rider.update(findQuery, { blockuptoDate: '' }, { multi: true });
  } catch (error) {
    console.log('resetRidersBlocking', error);
    return false;
  }
}


// RiderPerDay



/**
 * Set Driver Current location
 * @param {*} req 
 * @param {*} res 
 */
export const RiderLocation = (req, res) => {
  var driverLat = parseFloat(req.body.lat);
  var driverLng = parseFloat(req.body.lon);
  if (driverLat && driverLng) {
    var update = {
      coords: [driverLng, driverLat],
      lastUpdate: GFunctions.getRespCountryDateTime()
    }
    Rider.findOneAndUpdate({
      _id: req.userId
    }, update, {
        new: false
      }, (err, doc) => {
        if (err) {
          return res.status(500).json({
            'success': false,
            'message': req.i18n.__("SOME_ERROR"),
            'error': err
          });
        }
        return res.json({
          'success': true,
          'message': req.i18n.__("LOCATION_CHANGED_SUCCESSFULLY")
        });
      })
  } else {
    return res.status(409).json({
      'success': false,
      'message': req.i18n.__("LOCATION_NOT_VALID")
    });
  }

}

export const updateRiderPhone = (req, res) => {
  Rider.find({ _id: { $ne: req.body.riderId }, phone: req.body.phone }, (err, doc) => {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
    if (doc.length) return res.json({ 'success': true, 'message': req.i18n.__("PHONE.NO_ALREADY_EXISTS") });
    else {
      var newDoc =
      {
        phone: req.body.phone,
      }

      Rider.findOneAndUpdate({ _id: req.body.riderId }, newDoc, { new: true }, (err, todo) => {
        if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
        return res.json({ 'success': true, 'message': req.i18n.__("DETAILS_UPDATED"), todo });
      });
    }
  })
};

export const updateRiderEmail = (req, res) => {
  Rider.find({ _id: { $ne: req.body.riderId }, email: req.body.email }, (err, doc) => {
    if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
    if (doc.length) return res.json({ 'success': true, 'message': req.i18n.__("EMAIL_ALREADY_EXISTS") });
    else {
      var newDoc =
      {
        email: req.body.email,
      }

      Rider.findOneAndUpdate({ _id: req.body.riderId }, newDoc, { new: true }, (err, todo) => {
        if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
        return res.json({ 'success': true, 'message': req.i18n.__("DETAILS_UPDATED"), todo });
      });
    }
  })
};