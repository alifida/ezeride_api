const isValidCoordinates = require('is-valid-coordinates');
const config = require('../config');
const featuresSettings = require('../featuresSettings');
const moment = require("moment");

import * as GFunctions from '../controllers/functions';
import CompanyDetails from '../models/company.model';
import Driver from '../models/driver.model';
import ServiceAvailableCities from '../models/serviceAvailableCities.model';
import EstimationModel from '../models/estimation.model';
import Vehicletype from '../models/vehicletype.model';
import { isRiderCurrentlyFree, isRiderHasUpcomingTrips, isRiderCurrentlyActive } from '../controllers/app';

const invNum = require('invoice-number');

/**
 * Get Estimation fare for Particlar Vehicle for given pick and drop place
 * @param {*} req 
 * @param {*} res 
 */
export const estimationFare = async (req, res, next) => {

    if (
        typeof req.body.pickupLat === "undefined"
        || req.body.pickupLat === ""
        || typeof req.body.pickupLng === "undefined"
        || req.body.pickupLng === ""
        || !isValidCoordinates(req.body.pickupLng = parseFloat(req.body.pickupLng), req.body.pickupLat = parseFloat(req.body.pickupLat))
    ) {
        return res.status(409).json({
            'success': false,
            'message': 'Pickup location should be valid.'
        });
    } else if (
        typeof req.body.dropLat === "undefined"
        || req.body.dropLat === ""
        || typeof req.body.dropLng === "undefined"
        || req.body.dropLng === ""
        || !isValidCoordinates(req.body.dropLng = parseFloat(req.body.dropLng), req.body.dropLat = parseFloat(req.body.dropLat))
    ) {
        return res.status(409).json({
            'success': false,
            'message': 'Drop location should be valid.'
        });
    } else {
        next();
    }
}

export const requestTaxi = async (req, res, next) => {
    if (featuresSettings.checkInactiveCon) {
        var isRiderCurrentlyActiveToTakeNew = await isRiderCurrentlyActive(req.userId);
        if (!isRiderCurrentlyActiveToTakeNew) {
            return res.status(409).json({
                'success': false,
                'message': 'Your account was Inactivate.Please Contact Admin.'
            });
        }
    }
    if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
        req.body.promoAmt = 0;
    }

    if (req.body.promo == "" || req.body.promo == undefined) {
        req.body.promo = "";
    }

    if (typeof req.body.requestFrom === "undefined") {
        req.body.requestFrom = "app";
    }

    if (typeof req.body.tripType === "undefined") {
        req.body.tripType = "daily";
    }

    if (typeof req.body.bookingType === "undefined" || req.body.bookingType === "ride-now") {
        req.body.bookingType = "rideNow";
    }

    if (req.body.bookingType == "ride-Later" || req.body.bookingType == "Schedule") {
        req.body.bookingType = "rideLater";
    }

    if (req.body.bookingType == "rideNow") {
        var isRiderCurrentlyFreeToTakeNew = await isRiderCurrentlyFree(req.userId);
        if (!isRiderCurrentlyFreeToTakeNew) {
            return res.status(409).json({
                'success': false,
                'message': 'You already have one Trip in Progress, Please finshed that Trip and try again.'
            });
        }
    }

    if (req.body.bookingType == "rideLater") {
        var isRiderHasUpcomingTripsLater = await isRiderHasUpcomingTrips(req.userId);
        if (isRiderHasUpcomingTripsLater) {
            return res.status(409).json({
                'success': false,
                'message': 'You already have one Upcoming Trip in Progress, Please finshed that Trip and try again.'
            });
        }
    }

    req.body.tripShownDate = GFunctions.sendTimeNow("D-M-YYYY h:mm a");
    if (req.body.bookingType == "rideLater") {
        var utcLength = getStringLength(req.body.utc);
        if (utcLength < 4) {
            req.body.utc = config.gmtZone;
        }
        var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
        var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
        var reqtripFDT = GFunctions.getDateTimeinThisFormat(newDateFormat, "MM/DD/YYYY HH:mm a");
        // var reqtripFDT = GFunctions.getDateTimeForSortings(newDateFormat);

        var timeBtNowAndReq = GFunctions.getMinsBtDateTime(reqtripFDT, GFunctions.getISODate());
        if (Number(timeBtNowAndReq) < 15) {
            return res.status(409).json({
                'success': false,
                'message': 'Ride Later Should be greater than 15 Mins.'
            });
        }

        req.body.processNow = false;
        if (Number(timeBtNowAndReq) < 30) {
            req.body.processNow = true;
        }

        var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
        req.body.utc = req.body.utc ? req.body.utc : config.gmtZone;
        var gmtFTime = new Date(newDateFormat + " " + req.body.utc).toGMTString();
        req.body.tripDT = reqtripDT;
        req.body.tripFDT = reqtripFDT;
        req.body.gmtTime = gmtFTime;
        req.body.tripShownDate = GFunctions.getDateTimeinThisFormat(reqtripFDT, "YYYY-MM-DDTHH:mm:ss.SSS[Z]", "D-M-YYYY h:mm a");
    } else {
        req.body.tripDate = moment().utcOffset(config.utcOffset).format("DD-MM-YYYY");
        req.body.tripTime = moment().utcOffset(config.utcOffset).format("HH:mm a");
        var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
        var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
        // console.log(newDateFormat);
        var reqtripFDT = GFunctions.getDateTimeinThisFormat(newDateFormat);
        // console.log('reqtripFDT', reqtripFDT);
        var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
        var gmtFTime = new Date(reqtripFDT).toGMTString();

        req.body.tripDT = reqtripDT;
        req.body.utc = req.body.utc ? req.body.utc : config.gmtZone;
        // req.body.tripFDT = reqtripFDT;
        req.body.tripFDT = GFunctions.getISODate();
        req.body.gmtTime = gmtFTime;
        // req.body.tripShownDate = reqtripDT;
    }

    if (!req.body.driverAssignmentType) {
        req.body.driverAssignmentType = 'auto-assign';
    }

    if (req.body.tripType === "daily") {
        if (req.body.estimationId) {
            var estimationDetails = await EstimationModel.findById(req.body.estimationId);
            if (estimationDetails) {
                req.body.vehicleDetailsAndFare = estimationDetails.vehicleDetailsAndFare;
                req.body.distanceDetails = estimationDetails.distanceDetails;
            } else {
                return res.status(409).json({
                    'success': false,
                    'message': 'Error Sending Request.Please Try again.'
                });
            }
        } else {
            if (req.body.isWithoutEstimation) {
                req.body.vehicleDetailsAndFare = {
                    fareDetails: {
                        totalFare: 0,
                        BaseFare: 0,
                        KMFare: 0,
                        waitingFare: 0,
                        comisonAmt: 0,
                        totalFare: 0,
                        tax: 0,
                        taxPercentage: 0,
                        cancelationFeesDriver: 0,
                        cancelationFeesRider: 0,
                        nightObj: {
                            isApply: false,
                            percentageIncrease: 1,
                        },
                        peakObj: {
                            isApply: false,
                            percentageIncrease: 1,
                        },
                        currency: config.currency,
                        hotelcommisionAmt: 0,
                    },
                    vehicleDetails: {
                        type: req.body.serviceType,
                        serviceId: req.body.serviceTypeId ? req.body.serviceTypeId : '',
                    },
                    distance: '',
                };
                req.body.distanceDetails = {
                    distanceValue: '',
                    timeValue: '',
                    from: req.body.pickupAddress,
                    to: '',
                    endcoords: [0, 0],
                    startCords: [parseFloat(req.body.pickupLng), parseFloat(req.body.pickupLat)],
                    estTime: '',
                };
            } else {
                req.body.vehicleDetailsAndFare = JSON.parse(req.body.vehicleDetailsAndFare);
                req.body.distanceDetails = JSON.parse(req.body.distanceDetails);
            }
        }
        // req.body.pickuplat = body.distanceDetails['startCords'];
        // req.body.pickuplng = body.distanceDetails['startCords'];
        // req.body.droplat = body.distanceDetails['startCords'];
        // req.body.droplng = body.distanceDetails['startCords'];
    } else if (req.body.tripType === "rental") {

    }

    if (typeof req.body.paymentMode === "undefined") {
        req.body.paymentMode = featuresSettings.defaultPaymentMethod;
    }

    req.body.paymentMode = (req.body.paymentMode).toLowerCase();

    if (typeof req.body.noofseats === "undefined") {
        req.body.noofseats = 1;
    }

    next();
}

export const requestTaxiRetry = async (req, res, next) => {
    if (featuresSettings.checkInactiveCon) {
        var isRiderCurrentlyActiveToTakeNew = await isRiderCurrentlyActive(req.userId);
        if (!isRiderCurrentlyActiveToTakeNew) {
            return res.status(409).json({
                'success': false,
                'message': 'Your account was Inactivate.Please Contact Admin.'
            });
        }
    }
    var dataChanged = false;
    if (req.body.estimationId) dataChanged = true;

    if (dataChanged) {
        if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
            req.body.promoAmt = 0;
        }

        if (req.body.promo == "" || req.body.promo == undefined) {
            req.body.promo = "";
        }

        if (typeof req.body.tripType === "undefined") {
            req.body.tripType = "daily";
        }

        if (typeof req.body.bookingType === "undefined" || req.body.bookingType === "ride-now") {
            req.body.bookingType = "rideNow";
        }

        if (req.body.bookingType == "ride-Later" || req.body.bookingType == "Schedule") {
            req.body.bookingType = "rideLater";
        }

        req.body.tripShownDate = GFunctions.sendTimeNow();
        if (req.body.bookingType == "rideLater") {
            var utcLength = getStringLength(req.body.utc);
            if (utcLength < 4) {
                req.body.utc = config.gmtZone;
            }
            var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
            var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
            var reqtripFDT = GFunctions.getDateTimeForSortings(newDateFormat);
            var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
            var gmtFTime = new Date(newDateFormat + " " + req.body.utc).toGMTString();
            req.body.tripDT = reqtripDT;
            req.body.utc = req.body.utc ? req.body.utc : config.gmtZone;
            req.body.tripFDT = reqtripFDT;
            req.body.gmtTime = gmtFTime;
            req.body.tripShownDate = reqtripDT;
        } else {
            req.body.tripDate = moment().utcOffset(config.utcOffset).format("DD-MM-YYYY");
            req.body.tripTime = moment().utcOffset(config.utcOffset).format("HH:mm a");
            var reqtripDT = req.body.tripDate + ' ' + req.body.tripTime;
            var newDateFormat = GFunctions.sendFormatedTime(req.body.tripDate, req.body.tripTime);
            // console.log(newDateFormat);
            var reqtripFDT = GFunctions.getDateTimeinThisFormat(newDateFormat);
            // console.log('reqtripFDT', reqtripFDT);
            var newDateFormat = GFunctions.sendFormatedDTime(reqtripDT);
            var gmtFTime = new Date(reqtripFDT).toGMTString();

            req.body.tripDT = reqtripDT;
            req.body.utc = req.body.utc ? req.body.utc : config.gmtZone;
            // req.body.tripFDT = reqtripFDT;
            req.body.tripFDT = GFunctions.getISODate();
            req.body.gmtTime = gmtFTime;
            req.body.tripShownDate = reqtripDT;
        }

        if (!req.body.driverAssignmentType) {
            req.body.driverAssignmentType = 'auto-assign';
        }

        if (req.body.tripType === "daily") {
            if (req.body.estimationId) {
                var estimationDetails = await EstimationModel.findById(req.body.estimationId);
                if (estimationDetails) {
                    req.body.vehicleDetailsAndFare = estimationDetails.vehicleDetailsAndFare;
                    req.body.distanceDetails = estimationDetails.distanceDetails;
                } else {
                    return res.status(409).json({
                        'success': false,
                        'message': 'Error Sending Request.Please Try again.'
                    });
                }
            } else {
                req.body.vehicleDetailsAndFare = JSON.parse(req.body.vehicleDetailsAndFare);
                req.body.distanceDetails = JSON.parse(req.body.distanceDetails);
            }
            // req.body.pickuplat = body.distanceDetails['startCords'];
            // req.body.pickuplng = body.distanceDetails['startCords'];
            // req.body.droplat = body.distanceDetails['startCords'];
            // req.body.droplng = body.distanceDetails['startCords'];
        }

        if (typeof req.body.paymentMode === "undefined") {
            req.body.paymentMode = featuresSettings.defaultPaymentMethod;
        }

        req.body.paymentMode = (req.body.paymentMode).toLowerCase();

        if (typeof req.body.noofseats === "undefined") {
            req.body.noofseats = 1;
        }

    }//New data

    next();
}

function getStringLength(inputtxt) {
    var str = new String(inputtxt);
    return str.length
}

export const tripCurrentStatus = async (req, res, next) => {
    if (typeof req.body.waitingTime !== "number") {
        req.body.waitingTime = 0;
    }
    next();
}

export const verifyNumberDriver = async (req, res, next) => {
    if (
        typeof req.body.phcode === "undefined"
        || req.body.phcode === ""
    ) {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }
    next();
}

export const driverAddData = async (req, res, next) => {
    if (
        typeof req.body.requestFrom !== "undefined"
        && req.body.requestFrom !== ""
    ) {
        req.body.requestFrom = 'app';
    }

    if (
        typeof req.body.phcode === "undefined"
        || req.body.phcode === ""
    ) {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }

    if (typeof req.body.lang === "undefined") {
        req.body.lang = featuresSettings.defaultlang;
    }

    if (typeof req.body.cur === "undefined") {
        req.body.cur = featuresSettings.defaultcur;
    }

    if (req.body.cmpy == '' || typeof req.body.cmpy === 'undefined') {
        let companyDoc = await CompanyDetails.findOne({ name: 'Default' });
        if (companyDoc) {
            req.body.cmpy = companyDoc._id;
        } else {
            req.body.isIndividual = false;
        }
    }

    if (!featuresSettings.socialLogin) {
        if (
            typeof req.body.password === "undefined"
            || req.body.password === ""
        ) {
            return res.status(409).json({ 'success': false, 'message': 'Password should be valid.' });
        }
    }

    if (!req.body.loginType) {
        req.body.loginType == 'normal';
    }

    if (req.body.loginType != 'normal' && typeof req.body.loginType != "undefined" && req.body.loginType != "") {
        if (
            typeof req.body.loginId === "undefined"
            || req.body.loginId === "" || typeof req.body.loginType === "undefined" || req.body.loginType === ""
        ) {
            return res.status(409).json({ 'success': false, 'message': 'Error Registering with Social Login' });
        }
    }


    if (typeof req.body.gender === "undefined") {
        req.body.gender = "Male"
    }

    if (typeof req.body.scId === "undefined") {
        if (featuresSettings.isCityWise) {
            //assign default SCID, and name
            let ServiceAvailableCitiesDoc = await ServiceAvailableCities.findOne({ city: 'Default' });
            req.body.scity = ServiceAvailableCitiesDoc.city;
            req.body.scId = ServiceAvailableCitiesDoc._id;
        } else {
            req.body.scity = null;
            req.body.scId = null;
        }
    }

    let TotCnt = await Driver.findOne({}, {}, { sort: { 'createdAt': -1 } }).exec();
    if (TotCnt != null) {
        req.body.code = invNum.next(TotCnt.code);
    } else {
        req.body.code = 'DRV001';
    }

    //check age
    if (featuresSettings.dobMandatory) {
        let currentDate = moment().utcOffset(config.utcOffset).format("YYYY-MM-DD");
        let age = moment(currentDate).diff(req.body.DOB, 'years')
        if (age < 18) {
            return res.status(409).json({ "success": false, 'message': "The date doesn't look right.Be sure to use your actual Date Of Birth." })
        }
    }

    next();
}

export const verifyNumberRider = async (req, res, next) => {
    if (
        typeof req.body.phcode === "undefined"
        || req.body.phcode === ""
    ) {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }
    next();
}

export const riderLogin = async (req, res, next) => {
    if (typeof req.body.phcode === "undefined" || req.body.phcode === "") {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }
    if (featuresSettings.socialLogin) {
        if (typeof req.body.loginType === "undefined" || req.body.loginType == "") {
            req.body.loginType = 'normal';
            req.body.loginId = '';
        }
    }
    next();
}


export const driverLogin = async (req, res, next) => {
    if (typeof req.body.phcode === "undefined" || req.body.phcode === "") {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }
    if (featuresSettings.socialLogin) {
        if (typeof req.body.loginType === "undefined" || req.body.loginType == "") {
            req.body.loginType = 'normal';
            req.body.loginId = '';
        }
    }
    next();
}

export const riderAddData = async (req, res, next) => {
    if (
        typeof req.body.requestFrom !== "undefined"
        && req.body.requestFrom !== ""
    ) {
        req.body.requestFrom = 'app';
    }

    if (
        typeof req.body.phcode === "undefined"
        || req.body.phcode === ""
    ) {
        req.body.phcode = featuresSettings.defaultPhoneCode;
    }

    if (typeof req.body.lang === "undefined") {
        req.body.lang = featuresSettings.defaultlang;
    }

    if (typeof req.body.cur === "undefined") {
        req.body.cur = featuresSettings.defaultcur;
    }

    if (!featuresSettings.socialLogin) {
        if (
            typeof req.body.password === "undefined"
            || req.body.password === ""
        ) {
            return res.status(409).json({ 'success': false, 'message': 'Password should be valid.' });
        }
    }

    if (typeof req.body.loginId === "undefined") {
        req.body.loginType == 'normal';
    }

    if (req.body.loginType != 'normal' && typeof req.body.loginType != "undefined" && req.body.loginType != "") {
        if (
            typeof req.body.loginId === "undefined"
            || req.body.loginId === "" || typeof req.body.loginType === "undefined" || req.body.loginType === ""
        ) {
            return res.status(409).json({ 'success': false, 'message': 'Error Registering with Social Login' });
        }
    }

    if (typeof req.body.gender === "undefined") {
        req.body.gender = "Male"
    }

    if (typeof req.body.scId === "undefined") {
        if (featuresSettings.isCityWise) {
            //assign default SCID, and name
            let ServiceAvailableCitiesDoc = await ServiceAvailableCities.findOne({ city: 'Default' });
            req.body.scId = ServiceAvailableCitiesDoc._id;
            req.body.scity = ServiceAvailableCitiesDoc.city;
        } else {
            req.body.scity = null;
            req.body.scId = null;
        }
    }

    next();
}

export const validatePromo = async (req, res, next) => {
    if (featuresSettings.isPromoCodeAvailable) {
        if (typeof req.body.promoCode === "undefined" || req.body.promoCode === "") {
            return res.status(409).json({ 'success': false, 'message': 'Promo Code should be valid.' });
        }
    } else {
        return res.status(409).json({ 'success': false, 'message': 'Promo Code is not applicable.' });
    }
    next();
}

export const estimationFareForHailTaxi = async (req, res, next) => {

    if (
        typeof req.body.pickupLat === "undefined"
        || req.body.pickupLat === ""
        || typeof req.body.pickupLng === "undefined"
        || req.body.pickupLng === ""
        || !isValidCoordinates(req.body.pickupLng = parseFloat(req.body.pickupLng), req.body.pickupLat = parseFloat(req.body.pickupLat))
    ) {
        return res.status(409).json({
            'success': false,
            'message': 'Pickup location should be valid.'
        });
    } else if (
        typeof req.body.dropLat === "undefined"
        || req.body.dropLat === ""
        || typeof req.body.dropLng === "undefined"
        || req.body.dropLng === ""
        || !isValidCoordinates(req.body.dropLng = parseFloat(req.body.dropLng), req.body.dropLat = parseFloat(req.body.dropLat))
    ) {
        return res.status(409).json({
            'success': false,
            'message': 'Drop location should be valid.'
        });
    }

    let DriverData = await Driver.findById(req.userId, { currentTaxi: 1, curService: 1 }).exec();
    if (!DriverData) {
        return res.status(409).json({
            'success': false,
            'message': 'Error Finding Estimation Fare!Please Select Vehicle Type.'
        });
    }

    var vehicleData = await Vehicletype.findOne({ type: DriverData.curService }).exec(); // @v2TODO pass pickupCity as null

    req.body.serviceTypeId = vehicleData._id;
    req.body.serviceType = DriverData.curService;
    req.body.pickupCity = '';

    next();
}


export const requestHailTaxi = async (req, res, next) => {
    if (req.body.promoAmt == "" || req.body.promoAmt == undefined) {
        req.body.promoAmt = 0;
    }

    if (req.body.promo == "" || req.body.promo == undefined) {
        req.body.promo = "";
    }

    if (typeof req.body.requestFrom === "undefined") {
        req.body.requestFrom = "app";
    }

    if (typeof req.body.tripType === "undefined") {
        req.body.tripType = "daily";
    }

    if (typeof req.body.bookingType === "undefined") {
        req.body.bookingType = "hailRide";
    }

    req.body.tripShownDate = GFunctions.sendTimeNow();

    if (!req.body.driverAssignmentType) {
        req.body.driverAssignmentType = 'auto-assign';
    }

    if (req.body.tripType === "daily") {
        if (req.body.estimationId) {
            var estimationDetails = await EstimationModel.findById(req.body.estimationId);
            if (estimationDetails) {
                req.body.vehicleDetailsAndFare = estimationDetails.vehicleDetailsAndFare;
                req.body.distanceDetails = estimationDetails.distanceDetails;
            } else {
                return res.status(409).json({
                    'success': false,
                    'message': 'Error Sending Request.Please Try again.'
                });
            }
        } else {
            req.body.vehicleDetailsAndFare = JSON.parse(req.body.vehicleDetailsAndFare);
            req.body.distanceDetails = JSON.parse(req.body.distanceDetails);
        }
    }

    req.body.paymentMode = "cash";

    req.body.paymentMode = (req.body.paymentMode).toLowerCase();

    next();
}

function getStringLength(inputtxt) {
    var str = new String(inputtxt);
    return str.length
}