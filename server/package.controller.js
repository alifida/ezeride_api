import mongoose from 'mongoose';
import * as HelperFunc from './adminfunctions';

//import models
import Package from '../models/package.model';
import RentalPackage from '../models/rentalPackage.model';
//add package
//param --> req.body.pkname,amt,minkm,minhr,maxkm,maxhr
const _ = require('lodash');

export const addPackage = (req, res) => {
    var newDoc = new Package({
        pkname: req.body.pkname,
        amt: req.body.amt,
        minkm: req.body.minkm,
        minhr: req.body.minhr,
        pkm: req.body.pkm,
        phr: req.body.phr
    });
    newDoc.save((err, data) => {
        if (err) { console.log(err); }
        return console.log('Package Added');
    })
}

//view package

export const viewPackage = async (req, res) => {
    var likeQuery = HelperFunc.likeQueryBuilder(req.query);
    var pageQuery = HelperFunc.paginationBuilder(req.query);
    var sortQuery = HelperFunc.sortQueryBuilder(req.query);

    let TotCnt = Package.find(likeQuery).count();
    let Datas = Package.find(likeQuery).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery);

    try {
        var promises = await Promise.all([TotCnt, Datas]);
        res.header('x-total-count', promises[0]);
        var resstr = promises[1];
        res.send(resstr);
    } catch (err) {
        return res.json([]);
    }

}
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
export const addRentalPackage = (req, res) => {
    RentalPackage.findOne({ "name": req.body.name }, {}, function (err, user) {
        if (err) return res.status(500).json({ 'success': false, 'message': req.i18n.__("ERROR_SERVER") });
        if (user) return res.status(401).json({ 'success': false, 'message': req.i18n.__("PACKAGE_NAME_ALREADY_EXISTS") });
        else {
            var newDoc = {
                name: req.body.name,
                price: req.body.price,
                duration: req.body.duration,
                distance: req.body.distance,
            };

            var scIds = req.body.scIds;
            scIds = JSON.parse(scIds);
            newDoc = new RentalPackage(newDoc);

            _.forEach(scIds, function (element, i) {
                newDoc.scIds.push(element);
            });

            newDoc.save((err, data) => {
                if (err) { return res.status(500).json({ "success": false, "message": req.i18n.__("SOME_ERROR"), "error": err }) }
                else { return res.status(200).json({ "success": true, "message": req.i18n.__("PACKAGE_ADDED_SUCCESSFULLY"), "data": data }) }
            })
        }
    });
}
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
export const viewRentalPackage = async (req, res) => {
    var likeQuery = HelperFunc.likeQueryBuilder(req.query);
    var pageQuery = HelperFunc.paginationBuilder(req.query);
    var sortQuery = HelperFunc.sortQueryBuilder(req.query);

    let TotCnt = RentalPackage.find(likeQuery).count();
    let Datas = RentalPackage.find(likeQuery).skip(pageQuery.skip).limit(pageQuery.take).sort(sortQuery);

    try {
        var promises = await Promise.all([TotCnt, Datas]);
        res.header('x-total-count', promises[0]);
        var resstr = promises[1];
        res.send(resstr);
    } catch (err) {
        return res.json([]);
    }

}

/**
 * Delete Admin Profile  
 * @input  
 * @param 
 * @return 
 * @response  
 */

export const deleteRentalPackage = (req, res) => {
    RentalPackage.findByIdAndRemove(req.params.id, (err, docs) => {
        if (err) {
            return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR") });
        }
        return res.json({ 'success': true, 'message': req.i18n.__("DATA_DELETED_SUCCESSFULLY") });
    })
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
export const updateRentalPackage = (req, res) => {
    RentalPackage.findOne({ _id: req.body._id }).exec((err, doc) => {
        if (err) return res.json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
        if (!doc) return res.json({ 'success': false, 'message': req.i18n.__("PACKAGE_NOT_FOUND") });
        doc.name = req.body.name,
            doc.price = req.body.price,
            doc.duration = req.body.duration,
            doc.distance = req.body.distance

        // var  rental=req.body.rental;
        let oldScIds = JSON.parse(req.body.oldScIds);
        var scIds = req.body.scIds;
        scIds = JSON.parse(scIds);

        var result = _.xorBy(oldScIds, scIds, 'scId');
        // if(rental.isRental=='true')doc.rental=rental;

        var oldValues = []; var newValues = [];
        _.forEach(result, function (value, key) {
            let isExist = _.has(value, '_id')
            if (isExist) {
                oldValues.push(value);
            } else {
                newValues.push(value);
            }
        })

        _.forEach(oldValues, function (element, i) {
            doc.scIds.pull({ '_id': element._id });
        });

        _.forEach(newValues, function (element, i) {
            doc.scIds.push(element);
        });

        doc.save(function (err, op) {
            if (err) return res.status(409).json({ 'success': false, 'message': req.i18n.__("SOME_ERROR"), 'error': err });
            return res.json({ 'success': true, 'message': req.i18n.__("DETAILS_UPDATED"), op });
        });
    })
}