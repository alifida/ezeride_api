// ./express-server/routes/uber.server.route.js
import express from 'express'; 

const trimRequest = require('trim-request');
const verifyToken = require('../../routes/VerifyToken');
import * as appValidate from '../../validations/appValidate'; 
 
// get an instance of express router
const router = express.Router();
import * as rentalCtrl from './rental.controller';

//Frontend
router.route('/packageList').post(rentalCtrl.rentalPackageList);
router.route('/fareEstimation').post(rentalCtrl.rentalFareCalculationAllVehicleType);
router.route('/fareEstimatioSingle').post(rentalCtrl.rentalFareEstimateSingleVehilce);

export default router;
