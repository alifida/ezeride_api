//import model
import RentalPackage from '../../models/rentalPackage.model';
import ServiceAvailableCities from '../../models/serviceAvailableCities.model';
import Vehicle from '../../models/vehicletype.model';
import Promo from '../../models/promo.model';
import * as GFunctions from '../../controllers/functions';

//import package
import { insidePolygon } from 'geolocation-utils';
import _ from 'lodash';

//import config file
import featuresSettings from '../../featuresSettings';
import config from '../../config';

export const rentalPackageList = async (req, res) => {
	try {
		let where = {}, body = req.body, serviceCityId = [];
		if (featuresSettings.isCityWise) {
			let availableService = await ServiceAvailableCities.find({ "softDelete": false }, { "cityBoundaryPolygon": 1, "city": 1 }).lean();
			let pickUpPoint = false;
			for (var value of availableService) {
				if (value.city == "Default") {
					serviceCityId.push(value._id)
				}
				else {
					if (value.cityBoundaryPolygon.length != 0) {
						pickUpPoint = insidePolygon([parseFloat(body.pickupLng), parseFloat(body.pickupLat)], value.cityBoundaryPolygon)
						if (pickUpPoint) {
							serviceCityId.push(value._id)
							break;
						}
					}
				}
			}
			if (pickUpPoint == false) { return res.status(400).json({ 'message': "Service Not Available in This City." }) }
			where = { "scIds.scId": { "$in": serviceCityId } };
		}

		let rentalPackage = await RentalPackage.find(where, { "name": 1, "distance": 1, "duration": 1, "price": 1 })
		return res.status(200).json({ 'success': true, 'message': "Details fetched Successfully", "packageDetail": rentalPackage, "serviceDetail": serviceCityId })
	}
	catch (error) {
		return res.status(500).json({ 'success': false, 'message': 'Error on the server.', "error": error });
	}
}

export const rentalFareCalculationAllVehicleType = async (req, res) => {
	try {
		var
			body = req.body,
			serviceId = body.serviceId.split(',');

		let vehicleTypeDocs = await Vehicle.find({ "tripTypeCode": body.tripTypeCode, "scIds.scId": { "$in": serviceId } }, { "bkm": 1, "timeFare": 1, "tripTypeCode": 1, "type": 1, "baseFare": 1, "file": 1, "description": 1, "asppc": 1 });
		let packagaDoc = await RentalPackage.findById(body.packageId, { "name": 1, "distance": 1, "duration": 1, "price": 1 });

		var response = [];
		_.forEach(vehicleTypeDocs, (val) => {
			response.push({
				"_id": val._id,
				"tripTypeCode": val.tripTypeCode,
				"type": val.type,
				"bkm": val.bkm,
				"timeFare": val.timeFare,
				"tripTypeCode": val.tripTypeCode,
				"type": val.type,
				"baseFare": val.baseFare,
				"packageDistance": packagaDoc.distance,
				"packageDuration": packagaDoc.duration,
				"file": config.baseurl + val.file,
				"description": val.description,
				"seat": val.asppc,
				"packageId": packagaDoc._id,
				"fare": getRentalEstimationFare(packagaDoc.distance, val.bkm, val.baseFare)
			})
		})
		return res.status(200).json({ "success": true, "message": "Fetched successfully", "data": response })
	}
	catch (err) {
		return res.status(500).json({ 'success': false, 'message': 'Error on the server.', "error": err });
	}
}

/**  
 * body : vehicleTypeId, packageId, 
*/
export const rentalFareEstimateSingleVehilce = async (req, res) => {
	try {
		var body = req.body;
		let vehicleTypeDocs = await Vehicle.findById(body.vehicleTypeId, { "bkm": 1, "timeFare": 1, "tripTypeCode": 1, "distance": 1, "type": 1, "baseFare": 1, "file": 1, "description": 1, "asppc": 1 });
		let packagaDoc = await RentalPackage.findById(body.packageId, { "name": 1, "distance": 1, "duration": 1, "price": 1 });

		var response = {
			"_id": vehicleTypeDocs._id,
			"tripTypeCode": vehicleTypeDocs.tripTypeCode,
			"type": vehicleTypeDocs.type,
			"bkm": vehicleTypeDocs.bkm,
			"timeFare": vehicleTypeDocs.timeFare,
			"tripTypeCode": vehicleTypeDocs.tripTypeCode,
			"type": vehicleTypeDocs.type,
			"baseFare": vehicleTypeDocs.baseFare,
			"packageName": packagaDoc.name,
			"packageDistance": packagaDoc.distance,
			"packageDuration": packagaDoc.duration,
			"file": config.baseurl + vehicleTypeDocs.file,
			"description": vehicleTypeDocs.description,
			"seat": vehicleTypeDocs.asppc,
			"packageId": packagaDoc._id,
			"fare": getRentalEstimationFare(packagaDoc.distance, vehicleTypeDocs.bkm, vehicleTypeDocs.baseFare)
		}
		return res.status(200).json({ "success": true, "message": "Fetched successfully", "data": response })
	}
	catch (err) {
		return res.status(500).json({ 'success': false, 'message': 'Error on the server.', "error": err });
	}
}

/* export const rentalFareEstimateSingleVehilce = async (req,res) =>{
	try{
		var 
		body 				   = req.body,
		additionalDuration     = 0,
		additionalDurationFare = 0,
		additionalDistance 	   = 0;

		let vehicleTypeDocs = await Vehicle.findById(body.vehicleTypeId,{"bkm":1,"timeFare":1,"tripTypeCode":1,"distance":1,"type":1,"baseFare":1});
		let packageDoc = await RentalPackage.findById(body.packageId,{"name":1,"distance":1,"duration":1,"price":1});

		body.distance = packageDoc.distance; //mk

		if ( (parseFloat(body.distance)) && (parseFloat(packageDoc.distance) > parseFloat(body.distance)) ){
			additionalDistance = parseFloat(body.distance) - parseFloat(packageDoc.distance);
		}

		let packageDuration = parseFloat(packageDoc.duration) * 60

		if(packageDuration < parseFloat(body.duration)) {
			additionalDuration     = parseInt((parseFloat(body.duration) - packageDuration) / 60);
			additionalDurationFare = additionalDuration * getDistanceObj(vehicleTypeDocs.distance, body.distance)[0].additionalFarePerHrs
		}

		var response = {
			"_id"         			 : body.vehicleTypeId,
			"type"        			 : vehicleTypeDocs.type,
			"packageName"		     : packageDoc.name,
			"additionalDurationFare" : additionalDurationFare,
			"totalFare"   			 : getRentalEstimationFare(body.distance, vehicleTypeDocs.bkm,vehicleTypeDocs.baseFare, additionalDistance,vehicleTypeDocs.bkm,additionalDurationFare )
		}
		
		return res.status(200).json({"success":true, "message":"Fetched successfully", "data":response})
	}
	catch(err){
		return res.status(500).json({ 'success': false, 'message': 'Error on the server.', "error": err });
	}
} */

export const getRentalEstimationFare = (distance, farePerKm, baseFare, additionalDistance = 0, additionaFarePerKm = 0, additionalDurationFare = 0) => {
	console.log(distance, farePerKm, baseFare, additionalDistance, additionaFarePerKm, additionalDurationFare);
	let fareCalculation;
	fareCalculation = (parseFloat(distance) * parseFloat(farePerKm)) + parseFloat(baseFare) + (parseFloat(additionalDistance) * additionaFarePerKm)
	return fareCalculation + parseFloat(additionalDurationFare)
}

function getDistanceObj(distanceArray, distanceInKM) {
	var filteredFareOffers;
	if (distanceArray) {
		var filteredFareOffers = _.filter(distanceArray, i => Number(i.distanceFrom) <= Number(distanceInKM) && Number(i.distanceTo) >= Number(distanceInKM));
	}
	return filteredFareOffers;
}

export const getRentalFareEstimationAtTripEnd = async (vehicleTypeId, packageId, distanceKM, timeInMin) => {
	try {
		let vehicleTypeDocs = await Vehicle.findById(vehicleTypeId, { "bkm": 1, "timeFare": 1, "tripTypeCode": 1, "distance": 1, "type": 1, "baseFare": 1, "file": 1, "description": 1, "asppc": 1 });

		var additionalDuration = 0,
			additionalDurationFare = 0,
			additionalDistance = 0;

		let availableService = await ServiceAvailableCities.find({ "softDelete": false }, { "cityBoundaryPolygon": 1, "city": 1 }).lean();
		var serviceCityId = [];
		for (var value of availableService) {
			if (value.city == "Default") {
				serviceCityId.push(value._id)
			}
		}

		if (featuresSettings.isCityWise) {
			let packageDataScids = await RentalPackage.findById(packageId, { "name": 1, "distance": 1, "duration": 1, "price": 1, "scIds": 1 });
			var packageScids = packageDataScids.scIds;
			packageScids.forEach(element => {
				serviceCityId.push(element.scId)
			});
		}

		let packageDoc = await RentalPackage.findOne({ distance: { $lte: distanceKM }, "scIds.scId": { "$in": serviceCityId } }, { "name": 1, "distance": 1, "duration": 1, "price": 1 }).sort({ distance: -1 });


		if (Number(packageDoc.distance) < Number(distanceKM)) {
			additionalDistance = parseFloat(distanceKM) - parseFloat(packageDoc.distance);
		}

		let packageDuration = parseFloat(packageDoc.duration) * 60;

		if (packageDuration < parseFloat(timeInMin)) {
			additionalDuration = (parseFloat(timeInMin) - packageDuration) / 60;
			additionalDuration = parseInt(additionalDuration);
			var additionalTimeFare = vehicleTypeDocs.timeFare;
			var additionalTimeFareKmWise = getDistanceObj(vehicleTypeDocs.distance, distanceKM)[0].additionalFarePerHrs;
			if (additionalTimeFareKmWise) additionalTimeFare = additionalTimeFareKmWise;
			additionalDurationFare = additionalDuration * additionalTimeFare;
			console.log('additionalTimeFareKmWise', additionalTimeFare, additionalDuration)
		}

		var response = {
			"_id": vehicleTypeDocs._id,
			"tripTypeCode": vehicleTypeDocs.tripTypeCode,
			"type": vehicleTypeDocs.type,
			"bkm": vehicleTypeDocs.bkm,
			"timeFare": vehicleTypeDocs.timeFare,
			"tripTypeCode": vehicleTypeDocs.tripTypeCode,
			"type": vehicleTypeDocs.type,
			"baseFare": vehicleTypeDocs.baseFare,
			"packageName": packageDoc.name,
			"packageDistance": packageDoc.distance,
			"packageDuration": packageDoc.duration,
			"file": config.baseurl + vehicleTypeDocs.file,
			"description": vehicleTypeDocs.description,
			"seat": vehicleTypeDocs.asppc,
			"packageId": packageDoc._id,
			"fare": getRentalEstimationFare(packageDoc.distance, vehicleTypeDocs.bkm, vehicleTypeDocs.baseFare, additionalDistance, vehicleTypeDocs.bkm, additionalDurationFare)
		}
		return response;
	}
	catch (err) {
		console.log(err)
		return 0;
	}
}