const cancelConfigObj = {
  "cancelExists": true,
  "ifcanceledAddCharge": true,
  "ifcanceledBlockUser": true,
  "cancelLimitForDays": 1,
  "noOfDriverCancelAllowed": 2,
  "noOfRiderCancelAllowed": 5,
  "ifcancelExceedsBlockUserFor": 1,
  "chargeDriverCancelationAmountFrom": "wallet"
};module.exports = cancelConfigObj