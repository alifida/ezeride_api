// constantText.js
module.exports = {
	'welcome': 'Your Account is Activated.', 
	'isNightExistAlertLabel': 'Notes : Night Fare x{PERCENTAGE} ({TIME})', //Eg. Notes :Night Fare x1.5 (10PM - 6AM)
	'isPeakExistAlertLabel': 'Notes : Peak Fare x{PERCENTAGE} ({TIME})', //Eg. Notes :Peak Fare x1.5 (10AM - 12PM)
	'riderCancelLimitExceeds': 'You have Cancelled two trips. /n You will be able to Book rides tomorrow.', 
	'driverCancelLimitExceeds' : 'You have Cancelled three trips. /n You will be able to take rides tomorrow.', 
	'exceededminimumBalanceDriverAlert': 'Please maintain minimum Rs.500/- to continue your rides.', 
	'minimumBalanceDriverAlert': 'You have to maintain minimum Rs.500/- to continue your rides.', 
	'tripEndSmsMsg': 'Thanks For Your Trip With Nissi', 
	// 'OutStationAlert': "Please select out station since your drop location is too far from pickup location and it's city limit.", 
	'OutStationAlert': "Drop point is out of City limit, service will be available after 2 Weeks.", 
	//Api responses
	'cancelTaxiByRider': "Taxi Cancelled successfully.", 
	'cancelTripByRider': "Trip Cancelled successfully", 
	'cancelTripByDriver': "Trip Cancelled successfully", 
};